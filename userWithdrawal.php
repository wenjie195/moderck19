<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Bank.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$allBank = getBank($conn, " WHERE status = 'Available' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/" />
<link rel="canonical" href="https://agentpnchc.com/" /> -->
<meta property="og:title" content="WITHDRAWAL | MODERCK" />
<title>WITHDRAWAL | MODERCK</title>


<?php include 'css.php'; ?>
</head>

<body class="body">

<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">SALES COMMISSION WITHDRAWAL</h1><?php include 'header.php'; ?>
</div>
    
<div id="main-start">
	<div class="width100 inner-bg inner-padding">

    <!-- <h1 class="h1 red-text text-center login-h1"><br><b><?php echo $userData->getSalesCommission();?></b></h1> -->
    <h1 class="h1 red-text text-center login-h1"><br><b><?php echo $userData->getSalesCommissionMonth();?></b></h1>

        <div class="login-div margin-auto">
            <form action="utilities/userWithdrawalFunction.php" method="POST">

                <div class="fake-input-div">
                    <input type="text" class="input-css clean icon-input dark-tur-text2" placeholder="AMOUNT" id="amount" name="amount" required>
                </div>

                <div class="clear"></div>

                <div class="fake-input-div">
                    <!-- <input type="text" class="input-css clean icon-input dark-tur-text2" placeholder="BANK NAME" id="bank_name" name="bank_name" required> -->

                    <select class="input-css clean icon-input dark-tur-text2" name="bank_name" id="bank_name" required>
                        <option value="">SELECT BANK NAME</option>
                        <?php
                        for ($cnt=0; $cnt <count($allBank) ; $cnt++)
                        {
                        ?>
                            <option value="<?php echo $allBank[$cnt]->getName(); ?>"> 
                                <?php echo $allBank[$cnt]->getName(); ?>
                            </option>
                        <?php
                        }
                        ?>
                    </select>  

                </div>

                <div class="clear"></div>

                <div class="fake-input-div">
                    <input type="text" class="input-css clean icon-input dark-tur-text2" placeholder="BANK ACCOUNT HOLDER" id="bank_account_holder" name="bank_account_holder" required>
                </div>

                <div class="fake-input-div">
                    <input type="text" class="input-css clean icon-input dark-tur-text2" placeholder="BANK ACCOUNT NUMBER" id="bank_account_number" name="bank_account_number" required>
                </div>

                <button class="clean white-button ow-red-bg white-text" name="submit">SUBMIT</button>
        
            </form>
        </div>

    </div>
</div>

<?php include 'js.php'; ?>

</body>
</html>