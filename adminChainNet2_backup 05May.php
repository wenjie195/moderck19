<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $uid = $_SESSION['uid'];
// $uid = "a3943a935f6379757c6db39b8474967e";
// $uid = "c7b20dbc8b39d50703c15594073ce731";
$uid = "2e0fc313e14cffeba3cfad9cd9859980";

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$userRHDetails = getReferralHistory($conn, " WHERE referral_id =? ", array("referral_id"), array($uid), "s");
$userLevel = $userRHDetails[0];

// $getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);
// $sessionUid = "a3943a935f6379757c6db39b8474967e";
// $sessionUid = "c7b20dbc8b39d50703c15594073ce731";
$sessionUid = "2e0fc313e14cffeba3cfad9cd9859980";
$getWho = getWholeDownlineTree($conn, $sessionUid,false);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Chain Net | MODERCK" />
<title>Chain Net | MODERCK</title>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Chain Net</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    <div class="width100 same-padding min-height100 padding-top overflow overflow-x">

    <?php $level = $userLevel->getCurrentLevel();?>

        <div class="width100 overflow-x">
            <table class="width100 gold-table ow-text-left-table">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>LINK</th>
                        <th>REFERRAL</th>
                        <th>USERNAME</th>
                        <th>RANK</th>
                        <th>PV</th>
                        <th>GPV</th>
                        <th>DATE OF LAST PURCHASE</th>
                        <th>STATUS</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    
        <?php
        $conn = connDB();
        if($getWho)
        {
          for($cnt = 0;$cnt < count($getWho) ;$cnt++)
          {
            $totalDownline = 0;
            $downline = 0;
            $directDownline = 0;
            $downlineCurrentLvl = 0;
            $totalDirectDownline = 0;
            $downline = $getWho[$cnt]->getCurrentLevel();
            $directDownline = $downline + 1;

            $downlineUid = $getWho[$cnt]->getReferralId();
            $getWhoII = getWholeDownlineTree($conn,$downlineUid,false);
            if ($getWhoII)
            {
              for ($i=0; $i <count($getWhoII) ; $i++)
              {
                $allDownlineUid = $getWhoII[$i]->getReferralId();
                $downlineCurrentLvl = $getWhoII[$i]->getCurrentLevel();
                if ($directDownline == $downlineCurrentLvl)
                {
                  $totalDirectDownline++;
                }
              }
            }
            ?>
              <tr>
                <!-- <td></td> -->
                <td><?php echo ($cnt+1)?></td>

                <td>
                  <?php
                    $downlineLvl = $getWho[$cnt]->getCurrentLevel();
                    echo $lvl = $downlineLvl - $level;
                  ?>
                </td>

                <td>
                  <?php
                    $uplineUid = $getWho[$cnt]->getReferrerId();
                    $uplineDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uplineUid), "s");
                    echo $uplineUsername = $uplineDetails[0]->getUsername();
                  ?>
                </td>

                <td>
                  <?php
                      $userUid = $getWho[$cnt]->getReferralId();
                      $thisUserDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($userUid), "s");
                      echo $username = $thisUserDetails[0]->getUsername();
                      // $userSalesComms = $thisUserDetails[0]->getSalesCommission();
                  ?>
                </td>

                <td><?php echo $thisUserDetails[0]->getRank();?></td>
                
                <td>
                  <?php
                  $conn = connDB();
                  {
                    $downlineUid = $getWho[$cnt]->getReferralId();  
                    $myOrder = getOrders($conn, "WHERE uid = ? AND payment_status = 'APPROVED' ",array("uid"), array($downlineUid), "s");
                    $personalPV = 0; // initital
                    if($myOrder)
                    {
                      for ($b=0; $b <count($myOrder) ; $b++)
                      {
                        $personalPV += $myOrder[$b]->getProductValue();
                        // $personalPV = $myOrder[$b]->getProductValue();
                      }
                    }
                  }
                  ?>
                  <?php echo $personalPV;?>
                </td>

                <td>
                  <?php
                  $conn = connDB();
                  {
                    // $uid = $_POST['downline_uid'];
                    $downlineUid = $getWho[$cnt]->getReferralId();

                    $groupSales = 0; // initital
                    $groupSalesFormat = number_format(0,4); // initital
                    $directDownline = 0; // initital
                    $personalSales = 0; // initital

                    $referralDetails = getReferralHistory($conn, "WHERE referral_id = ?",array("referral_id"),array($downlineUid), "s");
                    $referralCurrentLevel = $referralDetails[0]->getCurrentLevel();
                    $directDownlineLevel = $referralCurrentLevel + 1;
                    $referrerDetails = $referrerDetails = getWholeDownlineTree($conn, $downlineUid, false);
                    if ($referrerDetails)
                    {
                      for ($i=0; $i <count($referrerDetails) ; $i++)
                      {
                        $currentLevel = $referrerDetails[$i]->getCurrentLevel();
                        if ($currentLevel == $directDownlineLevel)
                        {
                          $directDownline++;
                        }
                        $referralId = $referrerDetails[$i]->getReferralId();
                        $downlineDetails = getOrders($conn, "WHERE uid = ? AND payment_status = 'Approved' ",array("uid"),array($referralId), "s");                        
                        if ($downlineDetails)
                        {
                          for ($b=0; $b <count($downlineDetails) ; $b++)
                          {
                            $personalSales += $downlineDetails[$b]->getProductValue();
                          }
                        }
                      }
                      $groupSales += $personalSales;
                      $groupSalesFormat = number_format($groupSales,4);
                    }
                  }
                  ?>
                  <!-- <?php //echo $groupSales;?> -->
                  <?php echo $totalPV = $groupSales + $personalPV ;?>
                </td>

                <td>
                  <?php
                  $conn = connDB();
                  {
                    $lastOrder = getOrders($conn, "WHERE uid = ? AND payment_status = 'APPROVED' ORDER BY date_created DESC LIMIT 1 ",array("uid"), array($userUid), "s");
                    if($lastOrder)
                    {
                      // echo $lastOrder[0]->getDateCreated();
                      echo $latestOrders = date('d.m.Y',strtotime($lastOrder[0]->getDateCreated()));
                    }
                    else
                    {
                      echo "-";  
                    }
                  }
                  ?>
                </td>

                <td></td>
                <td></td>
              </tr>
            <?php
          }
          ?>
        <?php
        }
        $conn->close();
        ?>

                </tbody>
            </table>
        </div>


    </div>
    </div>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>