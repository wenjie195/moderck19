<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
// require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/PreOrderList.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$productOneUid = '35117b1cb67c78efa860c4882c2e5745';
$productOneName = 'Verju Foryn Deer Placenta (30s)';
// $status = 'Pending';

function addProductOne($conn,$userUid,$productOneUid,$productOneName,$item1Amount)
{
     if(insertDynamicData($conn,"preorder_list",array("user_uid","product_uid","product_name","quantity"),
          array($userUid,$productOneUid,$productOneName,$item1Amount),"ssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

// // first order / rebates (upline) begin 
function firstOrderProductValueUpline($conn,$bonusUid,$receiverUid,$receiverName,$pvAmount,$pvName)
{
     if(insertDynamicData($conn,"bonus",array("uid","receiver_uid","receiver","amount","bonus_type"),
     array($bonusUid,$receiverUid,$receiverName,$pvAmount,$pvName),"sssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function firstOrderRedemptionPointUpline($conn,$bonusUid,$receiverUid,$receiverName,$rpAmount,$rpName)
{
     if(insertDynamicData($conn,"bonus",array("uid","receiver_uid","receiver","amount","bonus_type"),
     array($bonusUid,$receiverUid,$receiverName,$rpAmount,$rpName),"sssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
// // first order / rebates (upline) end

// // first order / rebates (purchaser) begin 
function productValueRebates($conn,$bonusUid,$receiverUid,$receiverName,$pvRebateAmount,$pvRebateName)
{
     if(insertDynamicData($conn,"bonus",array("uid","receiver_uid","receiver","amount","bonus_type"),
     array($bonusUid,$receiverUid,$receiverName,$pvRebateAmount,$pvRebateName),"sssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function redemptionPointRebates($conn,$bonusUid,$receiverUid,$receiverName,$rpRebateAmount,$rpRebateName)
{
     if(insertDynamicData($conn,"bonus",array("uid","receiver_uid","receiver","amount","bonus_type"),
     array($bonusUid,$receiverUid,$receiverName,$rpRebateAmount,$rpRebateName),"sssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
// // first order / rebates (purchaser) end

$allUser = getUser($conn);
if($allUser)
{
    for($cntA = 0;$cntA < count($allUser) ;$cntA++)
    {
        echo $allUser[$cntA]->getUsername();
        $userUid = $allUser[$cntA]->getUid();
        echo "<br>";

        $userSalesCommission = $allUser[$cntA]->getSalesCommission();
        $userRedemptionPoint = $allUser[$cntA]->getRedemptionPoint();

        $item1Amount = 0;
        $orderListDetails = getOrderList($conn, " WHERE user_uid = ? AND product_uid = '35117b1cb67c78efa860c4882c2e5745' ",array("user_uid"),array($userUid),"s");
        // $orderListDetails = getOrderList($conn, " WHERE user_uid = ? AND product_uid = '547297ffdb9f49d6e5ff1db5208f2530' ",array("user_uid"),array($userUid),"s");
        // $orderListDetails = getOrderList($conn, " WHERE user_uid = ? AND product_uid = '712e761968f09667885752f04d48c77f' ",array("user_uid"),array($userUid),"s");
        if($orderListDetails)
        {
            // $item1 = 0;
            for($cnt = 0;$cnt < count($orderListDetails) ;$cnt++)
            {
                // echo "Ordering : ";
                // $orderListDetails[$cnt]->getUserUid();
                $item1Amount += $orderListDetails[$cnt]->getQuantity();
                // $orderListDetails[$cnt]->getProductName();
                // echo "<br>";
            }

            // echo "Ordering : ";
            // echo $item1;
            // echo "<br>";
            // echo "<br>";

        }
        echo "Ordering : ";
        echo $item1Amount;
        echo "<br>";
        echo "<br>";

        // filter out non buyer
        if($item1Amount > 0)
        {

            $preOrderListRow = getPreOrderList($conn, " WHERE user_uid = ? AND product_uid = '35117b1cb67c78efa860c4882c2e5745' ",array("user_uid"),array($userUid),"s");
            if($preOrderListRow)
            {
                $previosuQuantity = $preOrderListRow[0]->getQuantity();
                $renewQuantity = $item1Amount + $previosuQuantity;
                // echo $renewQuantity = $item1Amount + $previosuQuantity;

                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($renewQuantity)
                {
                  array_push($tableName,"quantity");
                  array_push($tableValue,$renewQuantity);
                  $stringType .=  "d";
                }
                array_push($tableValue,$userUid);
                $stringType .=  "s";
                $updateLevelTwoBonus = updateDynamicData($conn,"preorder_list"," WHERE user_uid = ? ",$tableName,$tableValue,$stringType);
                if($updateLevelTwoBonus)
                {
                    echo "Done";

                    $userRH = getReferralHistory($conn, " WHERE referral_id = ? ",array("referral_id"),array($userUid),"s");
                    echo $directUplineUid = $userRH[0]->getReferrerId();
                    echo "<br>";  
                    $directUplineDetails = getUser($conn, " WHERE uid =? ",array("uid"),array($directUplineUid),"s");
                    echo $directUplineSalesCommission = $directUplineDetails[0]->getSalesCommission();
                    echo "<br>";  
                    echo $directUplineRedemptionPoint = $directUplineDetails[0]->getRedemptionPoint();

                    $productValue = '125';
                    $redemptionPoint = '125';

                    if($item1Amount == 1)
                    {

                        echo $firstOrderPV = 0.20 * ($productValue);
                        echo $firstOrderRP = 0.02 * ($redemptionPoint);
                        echo $directUplineFirstOrderSalesCommission = $firstOrderPV + $directUplineSalesCommission;
                        echo $directUplineFirstOrderRedemptionPoint = $firstOrderRP + $directUplineRedemptionPoint;

                    }
                    elseif($item1Amount > 1)
                    {
                        $remainAmount = $item1Amount - 1;

                        echo $rebatesPV = (0.20 * ($totalProductValue))*($remainAmount);
                        echo $rebatesRP = (0.02 * ($totalRedemptionPoint))*($remainAmount);
                        echo $purchaserRebatesSalesCommission = $rebatesPV + $userSalesCommission;
                        echo $purchaserRebatesRedemptionPoint = $rebatesRP + $userRedemptionPoint;

                        echo $firstOrderPV = 0.20 * ($productValue);
                        echo $firstOrderRP = 0.02 * ($redemptionPoint);
                        echo $directUplineFirstOrderSalesCommission = $firstOrderPV + $directUplineSalesCommission;
                        echo $directUplineFirstOrderRedemptionPoint = $firstOrderRP + $directUplineRedemptionPoint;
                    }

                }
                else
                {
                  echo "Fail";
                }

            }
            else
            {
                if(addProductOne($conn,$userUid,$productOneUid,$productOneName,$item1Amount))
                {}
                else
                {}
            }

            // if(addProductOne($conn,$userUid,$productOneUid,$productOneName,$item1Amount))
            // {}
            // else
            // {}

        }

        // if(addProductOne($conn,$uid,$productOneUid,$productOneName,$status))
        // {}
        // else
        // {}
    }
}

// echo "<br>";
// echo "<br>";
// echo "<br>";

// $userUidTest = 'b5dd5d41441804b08323a38685c348a5';

// // $orderListDetails = getOrderList($conn);
// $orderListDetails = getOrderList($conn, " WHERE user_uid = ? AND product_uid = '35117b1cb67c78efa860c4882c2e5745' AND status = 'Sold' ",array("user_uid"),array($userUidTest),"s");
// if($orderListDetails)
// {
//     $item1 = 0;
//     for($cnt = 0;$cnt < count($orderListDetails) ;$cnt++)
//     {
//         // $item1 = 0;
//         // echo $orderListDetails[$cnt]->getUserUid();
//         // echo "      |       ";
//         // echo $orderListDetails[$cnt]->getQuantity();
//         // echo "      |       ";
//         // echo $orderListDetails[$cnt]->getProductName();
//         // echo "<br>";
//         $orderListDetails[$cnt]->getUserUid();
//         $item1 += $orderListDetails[$cnt]->getQuantity();
//         $orderListDetails[$cnt]->getProductName();
//     }
// }
// echo $item1;
?>