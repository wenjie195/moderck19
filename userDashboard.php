<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$getWho = getWholeDownlineTree($conn, $uid,false);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!-- PDF Page 52 -->
<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/userdashboard.php" />
<link rel="canonical" href="https://agentpnchc.com/userdashboard.php" /> -->
<meta property="og:title" content="User Dashboard | MODERCK" />
<title>User Dashboard | MODERCK</title>

<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Dashboard</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">

    <div class="width100 inner-bg inner-padding">
    <!-- <div class="clear"></div> -->

        <div class="same-padding width100 min-sp-height overflow padding-top30">
           <div class="first-flex">
                <!-- <a href="profile.php"> -->
                <a href="#.php"  class="ow-flex-three2">
                    <div class="three-div-inner">
                            
                            <p class="three-div-p gold-text">MYSC<br><?php echo $userData->getSalesCommission();?></p>
                    </div>
                </a>
                <a href="#.php"  class="ow-flex-three2">
                    <div class="three-div-inner">
                            
                            <p class="three-div-p gold-text">MYRP<br><?php echo $userData->getRedemptionPoint();?></p>
                    </div>
                </a>
                <a href="#.php"  class="ow-flex-three2">
                    <div class="three-div-inner">
                            
                            <p class="three-div-p gold-text">KPTS</p>
                    </div>
                </a> 
                <a href="#.php"  class="ow-flex-three2">
                    <div class="three-div-inner">
                        <!-- <p class="three-div-p gold-text">Last Purchase<br>12-09-2021</p> -->
                        <p class="three-div-p gold-text">Last Purchase<br>
                        <?php
                        $conn = connDB();
                        {
                            $lastOrder = getOrders($conn, "WHERE uid = ? AND payment_status = 'APPROVED' ORDER BY date_created DESC LIMIT 1 ",array("uid"), array($uid), "s");
                            if($lastOrder)
                            {
                                // echo $lastOrder[0]->getDateCreated();
                                echo $latestOrders = date('d.m.Y',strtotime($lastOrder[0]->getDateCreated()));
                            }
                            else
                            {
                                echo "-";  
                            }
                        }
                        ?>
                        </p>
                    </div>
                </a>
                <a href="#"  class="ow-flex-three2">
                    <?php
                    $conn = connDB();
                    {
                        $myOrder = getOrders($conn, "WHERE uid = ? AND payment_status = 'APPROVED' ",array("uid"), array($uid), "s");
                        // $myOrder = getOrders($conn, "WHERE uid = ? ",array("uid"), array($uid), "s");
                        $personalPV = 0; // initital
                        if($myOrder)
                        {
                            for ($b=0; $b <count($myOrder) ; $b++)
                            {
                                $personalPV += $myOrder[$b]->getSubtotal();
                            }
                        }
                    }
                    ?>

                    <div class="three-div-inner">
                        <!-- <p class="three-div-p gold-text">A</p> -->
                        <p class="three-div-p gold-text">Total Purchase (MYR)<br><?php echo $personalPV;?></p>
                    </div>
                </a>

                <a href="#"  class="ow-flex-three2">

                  <?php
                  $conn = connDB();
                  {
                    $downlineUid = $uid;

                    $groupSales = 0; // initital
                    $groupSalesFormat = number_format(0,4); // initital
                    $directDownline = 0; // initital
                    $personalSales = 0; // initital

                    $referralDetails = getReferralHistory($conn, "WHERE referral_id = ?",array("referral_id"),array($downlineUid), "s");
                    $referralCurrentLevel = $referralDetails[0]->getCurrentLevel();
                    $directDownlineLevel = $referralCurrentLevel + 1;
                    $referrerDetails = $referrerDetails = getWholeDownlineTree($conn, $downlineUid, false);
                    if ($referrerDetails)
                    {
                      for ($i=0; $i <count($referrerDetails) ; $i++)
                      {
                        $currentLevel = $referrerDetails[$i]->getCurrentLevel();
                        if ($currentLevel == $directDownlineLevel)
                        {
                          $directDownline++;
                        }
                        $referralId = $referrerDetails[$i]->getReferralId();
                        // $downlineDetails = getOrders($conn, "WHERE uid = ? AND payment_status = 'Approved' ",array("uid"),array($referralId), "s");      
                        $downlineDetails = getOrders($conn, "WHERE uid = ? ",array("uid"),array($referralId), "s");                  
                        if ($downlineDetails)
                        {
                          for ($b=0; $b <count($downlineDetails) ; $b++)
                          {
                            $personalSales += $downlineDetails[$b]->getSubtotal();
                          }
                        }
                      }
                      $groupSales += $personalSales;
                      $groupSalesFormat = number_format($groupSales,4);
                    }
                  }
                  ?>

                    <?php $totalPV = $groupSales + $personalPV ;?>

                    <div class="three-div-inner">
                        <!-- <p class="three-div-p gold-text">B</p> -->
                        <p class="three-div-p gold-text">Group Total Purchase (MYR)<br><?php echo $totalPV;?></p>
                    </div>
                </a>                      
            </div>
            <div class="clear"></div>

            <!-- <h1 class="top-title brown-text padding-top30">Purchase History</h1>
            <div class="width100 scroll-div">
                <table class="gold-table">
                    <thead>
                    <tr>
                        <th>S/N</th>
                        <th>DATE</th>
                        <th>REF NO</th>
                        <th>USERNAME</th>
                        <th>PRODUCT NAME</th>
                        <th>PRODUCT CODE</th>
                        <th>QUANTITY</th>
                        <th>UNIT PRICE(RM)</th>
                        <th>TOTAL AMOUNT (RM)</th>
                        <th>ACTION</th>
                    </tr>
                    </thead>
                    <tr>
                        <td>1</td>
                        <td>12.09.2021</td>
                        <td>INV 21090001</td>
                        <td>Emily100</td>
                        <td>VFSP30</td>
                        <td>10VFSP130</td>
                        <td>3</td>
                        <td>100.00</td>
                        <td>300.00</td>
                        <td><form><button class="clean dark-tur-link view-link"><u>View</u></button></form></td>
                    </tr>
                </table> -->
        </div>
        
        
        <!-- <h1 class="top-title brown-text padding-top30">Order Tracking</h1> -->

        <h1 class="top-title brown-text padding-top30">Referral Link</h1>



    <?php
        $actual_link = $path = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $fullPath = dirname($path); 
    ?>
    <!-- <input type="hidden" id="linkCopy" value="<?php //echo "https://".$fullPath."/register.php?referrerUID=".$uid ;?>"> -->
    <input type="hidden" id="linkCopy" value="<?php echo "https://".$fullPath."/eCommerceSitePreRgs.php?referrerUID=".$uid ;?>">

    <h1 class="top-title brown-text">
        <!-- <b>Referral Link:</b><br>  -->
        <!-- <a id="invest-now-referral-link" href="<?php //echo 'https://'.$fullPath.'/register.php?referrerUID='.$uid;?>" class="ref-link" target="_blank"> -->
        <a id="invest-now-referral-link" href="<?php echo 'https://'.$fullPath.'/eCommerceSitePreRgs.php?referrerUID='.$uid;?>" class="ref-link" target="_blank">
        <?php echo "https://".$fullPath."/eCommerceSitePreRgs.php?referrerUID=".$uid ;?>
        </a>
    </h1>

    <div class="clear"></div>

    <div class="text-center width100 overflow">
        <button class="clean white-text left-button same-dual-button" id="copy-referral-link" style="margin-right:0 !important;">Copy</button>
        <!-- <a href="https://www.the-qrcode-generator.com/" target="_blank" class="clean white-text right-button same-dual-button">
            Generate QR Code
        </a> -->
        <div class="clear"></div>
<img src="https://chart.googleapis.com/chart?chs=150x150&amp;cht=qr&amp;chl=<?php echo 'https://'.$fullPath.'/register.php?referrerUID='.$uid;?>" class="qr-code"  style="margin-top:20px !important;" />
        <!--<div class="chat-section">
            <div id="divQRCode"></div>
        </div>-->

    </div>

    <div class="clear"></div>

    </div>
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>

<script>
$("#copy-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          $(this).text("Copied");
          $(this).css("background-color","#f19100");
          // putNoticeJavascript("Copied!! ","");
      });
      $("#invest-now-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          putNoticeJavascript("Copied!! ","");
      });
</script>

<!-- <script type="text/javascript">
    $(document).ready(function()
    {
        $("#divQRCode").load("userQRCode.php");
    setInterval(function()
    {
        $("#divQRCode").load("userQRCode.php");
    }, 900000);
    });
</script> -->

</body>
</html>