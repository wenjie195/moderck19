<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Address.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allProduct = getProduct($conn, " WHERE status = 'Available' ");
$products = getProduct($conn, "WHERE status = 'Available' ");

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    addToCart();
    // createOrder($conn,$uid);
    header('Location: ./shoppingCartPreRgs.php');
}

$productListHtml = "";

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,1,true);
}else{
    if(isset($_POST['product-list-quantity-input'])){
        $productListHtml = createProductList($products,1,$_POST['product-list-quantity-input']);
    }else{
        $productListHtml = createProductList($products);
    }
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="All Product | MODERCK" />
<title>All Product | MODERCK</title>
<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">All Product</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    
    <div class="same-padding width100 min-sp-height overflow padding-top30 scroll-div padding-bottom30">

        <div class="width100 scroll-div">

        <?php
        // Program to display URL of current page.
        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
        $link = "https";
        else
        $link = "http";

        // Here append the common URL characters.
        $link .= "://";

        // Append the host(domain name, ip) to the URL.
        $link .= $_SERVER['HTTP_HOST'];

        // Append the requested resource location to the URL
        $link .= $_SERVER['REQUEST_URI'];

        // Print the link
        // echo $link;
        ?>

        <?php
        $str1 = $link;
        if(isset($_GET['referrerUID']))
        {
          $referrerUidLink = $_GET['referrerUID'];
        }
        else
        {
          $referrerUidLink = "";
        }
        // echo $referrerUidLink;
        // echo "<br>";
        ?>

        <?php
            $tempUid = md5(uniqid());
            // echo $tempUid = md5(uniqid());
        ?>

        <h1 class="top-title brown-text"><?php  $tempUid ;?></h1>
        <h1 class="top-title brown-text"><?php  $referrerUidLink ;?></h1>

        <form method="POST">
            <div id="main-start" class="main-start2">
                <div class="width100 inner-bg inner-padding">
                    <div class="same-padding width100 min-sp-height overflow padding-top30 scroll-div button-padding-bottom">
                        <div class="width103 product-big-div">
                            <?php echo $productListHtml; ?> 

                            <div class="clear"></div>

                            <!-- <div class="width100 text-center stay-bottom-add">  -->
                            <div class="stay-bottom-height"></div>
                        </div>    
                    </div>
                </div>
            </div>

            <div class="width100 fixed-bottom-div">
                <button class="clean white-button ow-red-bg white-text cart-button">Add To Cart</button>
            </div> 
        </form>

		</div>
    </div>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
    $(".button-minus").on("click", function(e)
    {
        e.preventDefault();
        var $this = $(this);
        var $input = $this.closest("div").find("input");
        var value = parseInt($input.val());
        if (value > 1)
        {
            value = value - 1;
        } 
        else 
        {
            value = 0;
        }
        $input.val(value);
    });

    $(".button-plus").on("click", function(e)
    {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest("div").find("input");
    var value = parseInt($input.val());
    if (value < 100)
    {
        value = value + 1;
    }
    else
    {
        value = 100;
    }
    $input.val(value);
    });
</script>

</body>
</html>