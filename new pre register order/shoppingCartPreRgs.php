<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
// require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/CartPreRgs.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $uid = $_SESSION['uid'];
$uplineUid = $_SESSION['upline_uid'];
$uid = $_SESSION['temp_uid'];

$conn = connDB();

// $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userData = $userDetails[0];
// $products = getPreOrderList($conn, "WHERE user_uid = ? AND status = 'Pending' ",array("user_uid"),array($uid),"s");

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    addToCart();
    createOrder($conn,$uid);
    // header('Location: ./shoppingCartCheckout.php');
    header('Location: ./shoppingCartCheckoutPreRgs.php');
}

$productListHtml = "";

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Shopping Cart | MODERCK" />
<title>Shopping Cart  | MODERCK</title>
<?php include 'css.php'; ?>
</head>
<body class="body">

<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Shopping Cart</h1><?php include 'header.php'; ?>
</div>
    
<div id="main-start">
	<div class="width100 inner-bg inner-padding">

    <h1 class="top-title brown-text"><?php echo $uid ;?></h1>
    <h1 class="top-title brown-text"><?php echo $uplineUid ;?></h1>

        <div class="table-padding width100 same-padding details-min-height padding-top2 overflow overflow-x">

            <!-- <div class="right-delete-div">
                <a href="shoppingCartEmpty.php">
                    <button class="clean delete-button">Delete All <img src="img/delete2.png" class="delete-png"></button>
                </a>
            </div>     -->

            <div class="clear"></div>

            <div class="width100 overflow-x cart-big-div2">
            	<div class="cart-big-div">
            	<div class="per-product-div per-product-div2">
                                <div class="left-cart-img-div">
									<p class="fake-table-header">Product</p>
                                </div>

                            
                            <div class="left-product-details">
                                <p class="fake-table-header">Details</p>
							</div>
							<div class="middle-product-details">
                                <p class="fake-table-header">Qty</p>
							</div>
							<div class="last-product-details">
                                <p class="fake-table-header">Price (RM)</p>
							</div>							
                        
                 </div> 
                <form method="POST">
                    <?php
                        $conn = connDB();
                        if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'])
                        {
                            $productListHtml = getShoppingCart($conn,2);
                            echo $productListHtml;
                        }
                        else
                        {
                            echo " <h3> YOUR CART IS EMPTY </h3>";
                        }


                        // if(array_key_exists('xclearCart', $_POST))
                        // {
                        //     xclearCart();
                        // }
                        // else
                        // {
                        // // code...
                        //     unset($productListHtml);
                        // }

                        $conn->close();
                    ?>
                </form>

            </div>
			</div>
        </div>

    </div>
</div>    

<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>