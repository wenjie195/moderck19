<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

// require_once dirname(__FILE__) . '/classes/Address.php';
require_once dirname(__FILE__) . '/classes/Checkout.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
// require_once dirname(__FILE__) . '/classes/OrderList.php';
// require_once dirname(__FILE__) . '/classes/PreOrderList.php';
// require_once dirname(__FILE__) . '/classes/States.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $uid = $_SESSION['uid'];
// $orderUid = $_SESSION['order_uid'];

$uid = $_SESSION['temp_uid'];
$uplineUid = $_SESSION['upline_uid'];
$orderUid = $_SESSION['order_uid'];

$conn = connDB();

// $addressDetails = getAddress($conn," WHERE user_uid = ? AND default_ship = 'Yes' ", array("user_uid") ,array($uid),"s");

$id = getOrders($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1 ",array("uid"),array($uid),"s");
$orderUid = $id[0]->getId();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{    
    $id = rewrite($_POST["order_id"]);
    $name = rewrite($_POST["recipient_name"]);
    $contactNo = rewrite($_POST["mobile_no"]);
    $houseRoad = rewrite($_POST["house_road"]);
    $postcode = rewrite($_POST["postcode"]);
    $city = rewrite($_POST["city"]);
    $state = rewrite($_POST["state"]);
    $country = rewrite($_POST["country"]);
}

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,2);
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Checkout | MODERCK" />
<title>Checkout | MODERCK</title>
<?php include 'css.php'; ?>
</head>
<body class="body">

<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Checkout</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">

        <div class="table-padding width100 same-padding details-min-height padding-top2 overflow overflow-x">
            <div class="width100 overflow-x">

                <h1 class="top-title brown-text">Shipping Information</h1>

                <div class="clear"></div>   

                <!-- <form method="POST" action="utilities/createOrderFunction.php"> -->
                <!-- <form method="POST"  action="shipping.php"> -->
                <form method="POST"  action="shoppingCartPaymentDetails.php">
                    <div class="dual-input">
                        <p class="top-p">Recipient Name</p>
                        <input type="text" class="line-input clean" placeholder="Recipient Name" id="recipient_name" name="recipient_name" required>
                    </div>
                        
                    <div class="dual-input second-dual-input">  
                        <p class="top-p">Mobile Number</p>
                        <input type="text" class="line-input clean" placeholder="Mobile Number" id="mobile_no" name="mobile_no" required>
                    </div> 

                    <div class="clear"></div>   

                    <div class="dual-input">
                        <p class="top-p">House No & Street</p>
                        <input type="text" class="line-input clean" placeholder="House/Road No"  id="house_road" name="house_road" required>
                    </div>  

                    <div class="dual-input second-dual-input">  
                        <p class="top-p">Postcode</p>
                        <input type="text" class="line-input clean" placeholder="Postcode" id="postcode" name="postcode" required>
                    </div>  

                    <div class="clear"></div>      

                    <div class="dual-input">
                        <p class="top-p">City</p>
                        <input type="text" class="line-input clean" placeholder="City"  id="city" name="city" required>                
                    </div>  

                    <div class="dual-input second-dual-input">  
                        <p class="top-p">State</p>
                        <input type="text" class="line-input clean" placeholder="State"  id="state" name="state" required>                    
                    </div>  

                    <div class="clear"></div>      

                    <div class="dual-input">
                        <p class="top-p">Country</p>     
                        <input type="text" class="line-input clean" placeholder="Country" id="country" name="country" required>
                    </div>    

                    <div class="clear"></div>      

                    <div class="dual-input">
                        <input class="line-input clean" type="hidden" id="uid" name="uid" value="<?php echo $orderUid;?>">
                    </div> 

                    <div class="clear"></div>      
			</div>
            <div class="width100 overflow-x cart-big-div2">
            	<div class="cart-big-div">
            	<div class="per-product-div per-product-div2">
                                <div class="left-cart-img-div">
									<p class="fake-table-header">Product</p>
                                </div>

                            
                            <div class="left-product-details">
                                <p class="fake-table-header">Details</p>
							</div>
							<div class="middle-product-details">
                                <p class="fake-table-header">Qty</p>
							</div>
							<div class="last-product-details">
                                <p class="fake-table-header">Price (RM)</p>
							</div>							
                        
                 </div>       
                        <?php echo $productListHtml; ?>
                    </div>
                    
                </form>

            </div>
        </div>

        <?php
        if($products)
        {
        $totalOrderAmount = 0;
        for ($cnt=0; $cnt <count($products) ; $cnt++)
        {
            $totalOrderAmount += $products[$cnt]->getTotalPrice();
            // echo "<br>";
            // echo '123';
        }
        }
        else
        {
            $totalOrderAmount = 0 ;
        }
        ?>
    
    </div>
</div>
<?php include 'js.php'; ?>

</body>
</html>