-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 28, 2022 at 03:01 PM
-- Server version: 5.7.37-cll-lve
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qlianmen_moderck`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `recipient` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `house_road` text,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `default_ship` varchar(255) DEFAULT NULL,
  `default_bill` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `date_input` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` text,
  `status` varchar(255) DEFAULT 'Available',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `uid`, `name`, `status`, `date_created`, `date_updated`) VALUES
(1, '06c127b75607fbc55e0f7fa76876e361', 'AFFIN BANK BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(2, '6dee76c60d4f09f75ab81ab62d5a2dd7', 'AL-RAJHI BANKING & INVESTMENT CORP (M) BHD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(3, '68076f863de75a51e8911c2d86310c3f', 'ALLIANCE BANK MALAYSIA BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(4, 'bb1d75a1fb9c328276a137f3816e8cac', 'AMBANK BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(5, '93b8a668af6a6f9d03d1de1b12170a14', 'BANGKOK BANK BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(6, '459272517a932ca7f6e882bd5173900a', 'BANK ISLAM MALAYSIA BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(7, 'e043742b2732d61a3852a886bcbaea2d', 'BANK KERJASAMA RAKYAT MALAYSIA BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(8, '9ef2d47377fd0de570fbd8d952c5aec4', 'BANK MUALAMAT MALAYSIA BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(9, 'f32700ca6edceabaecfb7616e5bda308', 'BANK OF AMERICA (MALAYSIA) BHD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(10, 'a591623238110380240c3cda58939ef8', 'BANK OF CHINA (MALAYSIA) BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(11, 'd3a1c33879531c02b8be59806d3ce2c4', 'BANK OF TOKYO-MITSUBISHI UFJ (M) BHD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(12, 'e8e813e8ee0d38bb7c94a92c05ceb571', 'BANK PERTANIAN MALAYSIA BHD (AGROBANK)', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(13, '77bc3d6f900e57b485f7879fb656867b', 'BANK SIMPANAN NASIONAL BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(14, '46daed02a5d01779186dc13e9e3a7e40', 'BNP PARIBAS MALAYSIA BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(15, '5e2158eaf252c5dbacf9e5109e45af06', 'CIMB BANK BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(16, '401f400073e3aca1438bdd85abbca9da', 'CITIBANK', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(17, 'da085efe5d9a4386b5dff75fc736c4ed', 'DEUTSCHE BANK (MALAYSIA) BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(18, '13ae748e4a65553cd36b0b31f086fa14', 'HONG LEONG BANK BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(19, 'c05207b8c2838dde0e0139839c4b6ec9', 'HSBC BANK MALAYSIA BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(20, '83a92b35a6f72c6473bc536b68a65b17', 'MAYBANK', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(21, '454a652140749ac66e8d8f4b7312ad9f', 'MBSB BANK BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(22, '05cc473a1658a50bc92b87ef02ca2c91', 'PUBLIC BANK BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(23, 'afc819fa433bc05e242c8c21e8c3d65f', 'RHB BANK BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45');

-- --------------------------------------------------------

--
-- Table structure for table `billing_address`
--

CREATE TABLE `billing_address` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `order_uid` varchar(255) DEFAULT NULL,
  `recipient` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `house_road` text,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `notice` text,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bonus`
--

CREATE TABLE `bonus` (
  `id` int(11) NOT NULL,
  `order_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `receiver_uid` varchar(255) DEFAULT NULL,
  `receiver` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `bonus_type` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bonus_sales_or_rebate`
--

CREATE TABLE `bonus_sales_or_rebate` (
  `id` int(11) NOT NULL,
  `order_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `receiver_uid` varchar(255) DEFAULT NULL,
  `receiver` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `bonus_type` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bonus_star`
--

CREATE TABLE `bonus_star` (
  `id` int(11) NOT NULL,
  `order_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `receiver_uid` varchar(255) DEFAULT NULL,
  `receiver` varchar(255) DEFAULT NULL,
  `upline_uid` varchar(255) DEFAULT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `personal_pv` varchar(255) DEFAULT NULL,
  `group_pv` varchar(255) DEFAULT NULL,
  `month_year` varchar(255) DEFAULT NULL,
  `bonus_type` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bonus_star_claim`
--

CREATE TABLE `bonus_star_claim` (
  `id` int(11) NOT NULL,
  `order_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `receiver_uid` varchar(255) DEFAULT NULL,
  `receiver` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `bonus_type` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bonus_star_store`
--

CREATE TABLE `bonus_star_store` (
  `id` int(11) NOT NULL,
  `order_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `receiver_uid` varchar(255) DEFAULT NULL,
  `receiver` varchar(255) DEFAULT NULL,
  `upline_uid` varchar(255) DEFAULT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `personal_pv` varchar(255) DEFAULT NULL,
  `group_pv` varchar(255) DEFAULT NULL,
  `month_year` varchar(255) DEFAULT NULL,
  `bonus_type` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id` bigint(20) NOT NULL,
  `level` varchar(255) DEFAULT NULL,
  `sales_commission` varchar(255) DEFAULT NULL,
  `redemption_point` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id`, `level`, `sales_commission`, `redemption_point`, `date_created`, `date_updated`) VALUES
(1, '1', '10', '2', '2022-01-26 09:16:36', '2022-01-26 09:16:36'),
(2, '2', '8', '2', '2022-01-26 09:16:36', '2022-01-26 09:16:36'),
(3, '3', '6', '2', '2022-01-26 09:17:13', '2022-01-26 09:17:13'),
(4, '4', '0', '6', '2022-01-26 09:17:13', '2022-01-26 09:17:13'),
(5, '5', '0', '6', '2022-01-26 09:17:28', '2022-01-26 09:17:28'),
(6, '6', '0', '6', '2022-01-26 09:17:28', '2022-01-26 09:17:28'),
(7, '7', '0', '6', '2022-01-26 09:17:41', '2022-01-26 09:17:41');

-- --------------------------------------------------------

--
-- Table structure for table `livestream`
--

CREATE TABLE `livestream` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `channel` text,
  `date` varchar(255) DEFAULT NULL,
  `time_start` varchar(255) DEFAULT NULL,
  `time_end` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ordering_list`
--

CREATE TABLE `ordering_list` (
  `id` bigint(20) NOT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `product_uid` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `main_product_uid` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `original_price` varchar(255) DEFAULT NULL,
  `final_price` varchar(255) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `totalPrice` varchar(255) DEFAULT NULL,
  `product_value` varchar(255) DEFAULT NULL,
  `redemption_point` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `address_one` varchar(2500) DEFAULT NULL,
  `address_two` varchar(2500) DEFAULT NULL,
  `address_three` varchar(2500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `subtotal` decimal(50,0) DEFAULT NULL,
  `total` decimal(50,0) DEFAULT NULL COMMENT 'include postage fees',
  `product_value` varchar(255) DEFAULT NULL,
  `redemption_point` varchar(255) DEFAULT NULL,
  `bonus_status` varchar(255) DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_amount` int(255) DEFAULT NULL,
  `payment_reference` varchar(255) DEFAULT NULL,
  `payment_status` varchar(200) DEFAULT NULL COMMENT 'pending, accepted/completed, rejected, NULL = nothing',
  `shipping_status` varchar(200) DEFAULT NULL COMMENT 'pending, shipped, refunded',
  `shipping_method` varchar(200) DEFAULT NULL,
  `shipping_date` date DEFAULT NULL,
  `tracking_number` varchar(200) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `receipt` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_list`
--

CREATE TABLE `order_list` (
  `id` bigint(20) NOT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `product_uid` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `main_product_uid` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `original_price` varchar(255) DEFAULT NULL,
  `final_price` varchar(255) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `totalPrice` varchar(255) DEFAULT NULL,
  `product_value` varchar(255) DEFAULT NULL,
  `redemption_point` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `preorder_list`
--

CREATE TABLE `preorder_list` (
  `id` bigint(20) NOT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `product_uid` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `main_product_uid` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `original_price` varchar(255) DEFAULT NULL,
  `final_price` varchar(255) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `totalPrice` varchar(255) DEFAULT NULL,
  `product_value` varchar(255) DEFAULT NULL,
  `redemption_point` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `product_value` varchar(255) DEFAULT NULL,
  `redemption_point` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `description` text,
  `description_two` text,
  `keyword_one` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `uid`, `name`, `code`, `price`, `product_value`, `redemption_point`, `status`, `description`, `description_two`, `keyword_one`, `image_one`, `image_two`, `date_created`, `date_updated`) VALUES
(1, '35117b1cb67c78efa860c4882c2e5745', 'Verju Foryn Deer Placenta (30’s)', '10VFDP3001-01-01', '250', '125', '125', 'Available', 'Description for Verju Foryn Deer Placenta (30’s)', NULL, NULL, '30s.PNG', NULL, '2022-01-24 06:23:56', '2022-02-17 09:09:57'),
(2, '547297ffdb9f49d6e5ff1db5208f2530', 'Verju Foryn Deer Placenta (60’s)', '10VFDP3002-01-01', '450', '225', '225', 'Available', 'Description for Verju Foryn Deer Placenta (60’s)', NULL, NULL, '60s.PNG', NULL, '2022-01-24 06:47:09', '2022-02-17 09:10:09'),
(3, '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150’s)', '10VFDP3005-01-01', '1000', '500', '500', 'Available', 'Description for Verju Foryn Deer Placenta (150’s)', NULL, NULL, '150s.PNG', NULL, '2022-01-24 08:19:32', '2022-02-17 09:10:13');

-- --------------------------------------------------------

--
-- Table structure for table `referral_history`
--

CREATE TABLE `referral_history` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `current_level` int(100) NOT NULL,
  `current_status` varchar(255) DEFAULT 'Member',
  `order_status` varchar(255) DEFAULT NULL,
  `top_referrer_id` varchar(100) NOT NULL COMMENT 'the topmost person''s uid',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `referral_history`
--

INSERT INTO `referral_history` (`id`, `referrer_id`, `referral_id`, `referral_name`, `current_level`, `current_status`, `order_status`, `top_referrer_id`, `date_created`, `date_updated`) VALUES
(1, 'a7bd724138f51e77ac86dc66b64bfdcb', '8b869e82f40590a72f02e0ea26e10d93', 'reserved07', 0, 'Member', NULL, 'a7bd724138f51e77ac86dc66b64bfdcb', '2022-04-21 04:18:03', '2022-04-21 04:19:48'),
(2, '8b869e82f40590a72f02e0ea26e10d93', 'a3943a935f6379757c6db39b8474967e', 'reserved06', 0, 'Member', NULL, 'a7bd724138f51e77ac86dc66b64bfdcb', '2022-04-21 04:22:27', '2022-04-21 04:24:51'),
(3, 'a3943a935f6379757c6db39b8474967e', '7eab09ac6d24aaf8d69959d9a59a3716', 'reserved05', 0, 'Member', NULL, 'a7bd724138f51e77ac86dc66b64bfdcb', '2022-04-21 04:22:49', '2022-04-21 04:24:55'),
(4, '7eab09ac6d24aaf8d69959d9a59a3716', '885aefee344dc2ab8d539582d2506a90', 'reserved04', 0, 'Member', NULL, 'a7bd724138f51e77ac86dc66b64bfdcb', '2022-04-21 04:23:09', '2022-04-21 04:24:58'),
(5, '885aefee344dc2ab8d539582d2506a90', 'af77d5fd9cfef2e70b662959c736ced3', 'reserved03', 0, 'Member', NULL, 'a7bd724138f51e77ac86dc66b64bfdcb', '2022-04-21 04:23:31', '2022-04-21 04:25:00'),
(6, 'af77d5fd9cfef2e70b662959c736ced3', 'e2bf3d321d2ceacb98710bb8cd058ad7', 'reserved02', 0, 'Member', NULL, 'a7bd724138f51e77ac86dc66b64bfdcb', '2022-04-21 04:23:54', '2022-04-21 04:25:03'),
(7, 'e2bf3d321d2ceacb98710bb8cd058ad7', '02f3c4f7da85bcebb928109e8836b9be', 'reserved01', 0, 'Member', NULL, 'a7bd724138f51e77ac86dc66b64bfdcb', '2022-04-21 04:24:12', '2022-04-21 04:25:05'),
(8, '02f3c4f7da85bcebb928109e8836b9be', '2e0fc313e14cffeba3cfad9cd9859980', 'company', 1, 'Member', NULL, 'a7bd724138f51e77ac86dc66b64bfdcb', '2022-04-21 04:24:38', '2022-04-21 04:25:08');

-- --------------------------------------------------------

--
-- Table structure for table `sales_commission`
--

CREATE TABLE `sales_commission` (
  `id` int(11) NOT NULL,
  `order_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `receiver_uid` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `rank` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sales_commission_backup`
--

CREATE TABLE `sales_commission_backup` (
  `id` int(11) NOT NULL,
  `order_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `receiver_uid` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `rank` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `transfer_record`
--

CREATE TABLE `transfer_record` (
  `id` int(11) NOT NULL,
  `transaction_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `transfer_to` varchar(255) DEFAULT NULL,
  `transfer_type` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `receiver_uid` varchar(255) DEFAULT NULL,
  `receiver` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'PENDING',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `upgrading`
--

CREATE TABLE `upgrading` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `remark` text,
  `edit_by` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `icno` varchar(255) DEFAULT NULL,
  `password` char(64) DEFAULT NULL,
  `salt` char(64) DEFAULT NULL,
  `birth_date` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `zipcode` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_acc_number` varchar(255) DEFAULT NULL,
  `bank_acc_name` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT '1' COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT '1' COMMENT '0 = admin, 1 = normal user',
  `rank` varchar(255) DEFAULT 'Member',
  `achieve_rank` varchar(255) DEFAULT NULL,
  `first_order` varchar(255) DEFAULT '0',
  `order_status` varchar(255) DEFAULT NULL,
  `sales_commission` decimal(20,2) NOT NULL,
  `redemption_point` decimal(20,2) NOT NULL,
  `sales_commission_month` decimal(20,2) NOT NULL,
  `redemption_point_month` decimal(20,2) NOT NULL,
  `wallet` decimal(20,2) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `fullname`, `firstname`, `lastname`, `icno`, `password`, `salt`, `birth_date`, `country`, `phone_no`, `address`, `zipcode`, `state`, `bank_name`, `bank_acc_number`, `bank_acc_name`, `login_type`, `user_type`, `rank`, `achieve_rank`, `first_order`, `order_status`, `sales_commission`, `redemption_point`, `sales_commission_month`, `redemption_point_month`, `wallet`, `date_created`, `date_updated`) VALUES
(1, 'a7bd724138f51e77ac86dc66b64bfdcb', 'admin', 'admin@gmail.com', NULL, 'admin', 'admin', NULL, '23eb111bfed87d266953601d3c287f55c324b0788452919232a1222ae036a823', '0dfee12f6549614366e94a959e057af1f5ce4d06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2020-03-11 01:40:01', '2022-04-21 02:18:20'),
(2, '8b869e82f40590a72f02e0ea26e10d93', 'reserved07', 'reserved07@gmail.com', NULL, NULL, NULL, NULL, '37e9246dc3e5d523266a97942e0a49c4ed8297437bd4f37d391e0f4368847999', '9b7732f52ff8b2b09107760001daf6432b59ee95', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2022-04-21 04:18:03', '2022-04-21 04:25:39'),
(3, 'a3943a935f6379757c6db39b8474967e', 'reserved06', 'reserved06@gmail.com', NULL, NULL, NULL, NULL, '8b4be097c670bd14bf471e1e85c074de5ad5d52a625c56733bbe6345525cffa0', 'bc172e4b7f9fe7c52be9c7341fb9b92a428bff47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2022-04-21 04:22:27', '2022-04-21 04:25:42'),
(4, '7eab09ac6d24aaf8d69959d9a59a3716', 'reserved05', 'reserved05@gmail.com', NULL, NULL, NULL, NULL, '14bb949f7deac8cf78306a0d15310b1324ddb5080bbbe46a89ff751fb83b1bc2', 'b56ab1beaf2192d69b455e69575280d9315f7c93', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2022-04-21 04:22:49', '2022-04-21 04:25:45'),
(5, '885aefee344dc2ab8d539582d2506a90', 'reserved04', 'reserved04@gmail.com', NULL, NULL, NULL, NULL, '223495329414a3516a9160f7ec8d7c3065027957038539cc231b54aa8687a982', '595012aaf327e87032a8f1163f9fd31d3982aa31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2022-04-21 04:23:09', '2022-04-21 04:25:47'),
(6, 'af77d5fd9cfef2e70b662959c736ced3', 'reserved03', 'reserved03@gmail.com', NULL, NULL, NULL, NULL, '156287f72e926a699c449c2e9d80085596432ef552ebdea1d96742e2b045b199', '17678535fad1257ae3789e8c16f57cbe0ae9a04e', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2022-04-21 04:23:31', '2022-04-21 04:25:49'),
(7, 'e2bf3d321d2ceacb98710bb8cd058ad7', 'reserved02', 'reserved02@gmail.com', NULL, NULL, NULL, NULL, '4fe233118430bb79a05917b717c337a4a7f3f5bf2a3172d0a828706d78302b0f', 'e38f871eadbc26393e8c8c07bc4a2944a1fbc40f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2022-04-21 04:23:54', '2022-04-21 04:25:51'),
(8, '02f3c4f7da85bcebb928109e8836b9be', 'reserved01', 'reserved01@gmail.com', NULL, NULL, NULL, NULL, '9eb13b06e80f498eaa1ddd7f1cf925ab475d1e6bcc94b3888e5b7cb20c4dd634', 'e4f52bec3c604d3047db30c7f76511c023e42625', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2022-04-21 04:24:12', '2022-04-21 04:25:54'),
(9, '2e0fc313e14cffeba3cfad9cd9859980', 'company', 'company@gmail.com', NULL, NULL, NULL, NULL, 'c3b1671b848ca19b4c78467ed645b8cb6f5bca75ccd534f92b1b400bbbcd05cf', '35bb85e390064b5722729cf51bd6a6e936cf6dc9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2022-04-21 04:24:38', '2022-04-21 04:24:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_address`
--
ALTER TABLE `billing_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bonus`
--
ALTER TABLE `bonus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bonus_sales_or_rebate`
--
ALTER TABLE `bonus_sales_or_rebate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bonus_star`
--
ALTER TABLE `bonus_star`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bonus_star_claim`
--
ALTER TABLE `bonus_star_claim`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bonus_star_store`
--
ALTER TABLE `bonus_star_store`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `livestream`
--
ALTER TABLE `livestream`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ordering_list`
--
ALTER TABLE `ordering_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_list`
--
ALTER TABLE `order_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `preorder_list`
--
ALTER TABLE `preorder_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_commission`
--
ALTER TABLE `sales_commission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_commission_backup`
--
ALTER TABLE `sales_commission_backup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transfer_record`
--
ALTER TABLE `transfer_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upgrading`
--
ALTER TABLE `upgrading`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `billing_address`
--
ALTER TABLE `billing_address`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bonus`
--
ALTER TABLE `bonus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bonus_sales_or_rebate`
--
ALTER TABLE `bonus_sales_or_rebate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bonus_star`
--
ALTER TABLE `bonus_star`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bonus_star_claim`
--
ALTER TABLE `bonus_star_claim`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bonus_star_store`
--
ALTER TABLE `bonus_star_store`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `livestream`
--
ALTER TABLE `livestream`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ordering_list`
--
ALTER TABLE `ordering_list`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_list`
--
ALTER TABLE `order_list`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `preorder_list`
--
ALTER TABLE `preorder_list`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `referral_history`
--
ALTER TABLE `referral_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sales_commission`
--
ALTER TABLE `sales_commission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_commission_backup`
--
ALTER TABLE `sales_commission_backup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transfer_record`
--
ALTER TABLE `transfer_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `upgrading`
--
ALTER TABLE `upgrading`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
