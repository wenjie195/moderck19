-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2022 at 09:38 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_moderck`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `recipient` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `house_road` text DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `default_ship` varchar(255) DEFAULT NULL,
  `default_bill` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `uid`, `user_uid`, `recipient`, `mobile`, `house_road`, `city`, `state`, `postcode`, `country`, `default_ship`, `default_bill`, `status`, `date_created`, `date_updated`) VALUES
(1, '9ab6ab51cb8aef44c0fec9f662d08bbe', 'b5dd5d41441804b08323a38685c348a5', 'User One', '0124455665', '101, Jln User One', 'BPL', 'PG', '10110', 'MAS', 'Yes', 'Yes', 'Available', '2022-04-18 06:26:36', '2022-04-18 06:26:36'),
(2, '4f091c18b85e18805ef298073dfbb709', '3208f7e0641debe426e374a74ae3a607', 'Mike 1.5', '0121515115', '15 Jln Mike', 'Tmn Mike15', 'KDH', '11515', 'MAS', 'Yes', 'Yes', 'Available', '2022-04-18 08:16:08', '2022-04-18 08:16:08');

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `date_input` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` text DEFAULT NULL,
  `status` varchar(255) DEFAULT 'Available',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `uid`, `name`, `status`, `date_created`, `date_updated`) VALUES
(1, '06c127b75607fbc55e0f7fa76876e361', 'AFFIN BANK BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(2, '6dee76c60d4f09f75ab81ab62d5a2dd7', 'AL-RAJHI BANKING & INVESTMENT CORP (M) BHD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(3, '68076f863de75a51e8911c2d86310c3f', 'ALLIANCE BANK MALAYSIA BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(4, 'bb1d75a1fb9c328276a137f3816e8cac', 'AMBANK BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(5, '93b8a668af6a6f9d03d1de1b12170a14', 'BANGKOK BANK BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(6, '459272517a932ca7f6e882bd5173900a', 'BANK ISLAM MALAYSIA BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(7, 'e043742b2732d61a3852a886bcbaea2d', 'BANK KERJASAMA RAKYAT MALAYSIA BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(8, '9ef2d47377fd0de570fbd8d952c5aec4', 'BANK MUALAMAT MALAYSIA BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(9, 'f32700ca6edceabaecfb7616e5bda308', 'BANK OF AMERICA (MALAYSIA) BHD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(10, 'a591623238110380240c3cda58939ef8', 'BANK OF CHINA (MALAYSIA) BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(11, 'd3a1c33879531c02b8be59806d3ce2c4', 'BANK OF TOKYO-MITSUBISHI UFJ (M) BHD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(12, 'e8e813e8ee0d38bb7c94a92c05ceb571', 'BANK PERTANIAN MALAYSIA BHD (AGROBANK)', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(13, '77bc3d6f900e57b485f7879fb656867b', 'BANK SIMPANAN NASIONAL BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(14, '46daed02a5d01779186dc13e9e3a7e40', 'BNP PARIBAS MALAYSIA BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(15, '5e2158eaf252c5dbacf9e5109e45af06', 'CIMB BANK BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(16, '401f400073e3aca1438bdd85abbca9da', 'CITIBANK', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(17, 'da085efe5d9a4386b5dff75fc736c4ed', 'DEUTSCHE BANK (MALAYSIA) BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(18, '13ae748e4a65553cd36b0b31f086fa14', 'HONG LEONG BANK BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(19, 'c05207b8c2838dde0e0139839c4b6ec9', 'HSBC BANK MALAYSIA BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(20, '83a92b35a6f72c6473bc536b68a65b17', 'MAYBANK', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(21, '454a652140749ac66e8d8f4b7312ad9f', 'MBSB BANK BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(22, '05cc473a1658a50bc92b87ef02ca2c91', 'PUBLIC BANK BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45'),
(23, 'afc819fa433bc05e242c8c21e8c3d65f', 'RHB BANK BERHAD', 'Available', '2021-12-03 08:43:45', '2021-12-03 08:43:45');

-- --------------------------------------------------------

--
-- Table structure for table `billing_address`
--

CREATE TABLE `billing_address` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `order_uid` varchar(255) DEFAULT NULL,
  `recipient` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `house_road` text DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `notice` text DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `billing_address`
--

INSERT INTO `billing_address` (`id`, `uid`, `order_uid`, `recipient`, `mobile`, `house_road`, `city`, `state`, `postcode`, `country`, `notice`, `status`, `date_created`, `date_updated`) VALUES
(1, '3208f7e0641debe426e374a74ae3a607', '6123f764a746f91920fd9810b8d3d689', 'Mike 1.5 BI', '012151511555', '15 Jln Mike BI', 'Tmn Mike15 BI', 'KDH BI', '11555', 'MAS BI', 'Notice FOR Billing Info', NULL, '2022-04-18 08:17:14', '2022-04-18 08:17:14'),
(2, 'b5dd5d41441804b08323a38685c348a5', 'f7d8c90ea22b01b15b57d269aa68594b', 'User One BI', '0124455888', '101, Jln User One BI', 'BPL BI', 'ASD', '24242', 'MAS BI', 'hahahaa', NULL, '2022-04-18 08:19:27', '2022-04-18 08:19:27');

-- --------------------------------------------------------

--
-- Table structure for table `bonus`
--

CREATE TABLE `bonus` (
  `id` int(11) NOT NULL,
  `order_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `receiver_uid` varchar(255) DEFAULT NULL,
  `receiver` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `bonus_type` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bonus`
--

INSERT INTO `bonus` (`id`, `order_uid`, `uid`, `username`, `receiver_uid`, `receiver`, `amount`, `bonus_type`, `status`, `date_created`, `date_updated`) VALUES
(1, 'b5dd5d41441804b08323a38685c348a51643192776', 'b5dd5d41441804b08323a38685c348a5', 'user', '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '300', '10% Sales Commission (Lvl 1)', NULL, '2022-04-19 07:03:56', '2022-04-19 07:03:56'),
(2, 'b5dd5d41441804b08323a38685c348a51643192776', 'b5dd5d41441804b08323a38685c348a5', 'user', '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '60', '2% Redemption Point (Lvl 1)', NULL, '2022-04-19 07:03:56', '2022-04-19 07:03:56'),
(3, 'b5dd5d41441804b08323a38685c348a51643192776', 'b5dd5d41441804b08323a38685c348a5', 'user', 'd357f663ae35b716359741bb217e7157', 'ZKChua', '240', '8% Sales Commission (Lvl 2)', NULL, '2022-04-19 07:03:56', '2022-04-19 07:03:56'),
(4, 'b5dd5d41441804b08323a38685c348a51643192776', 'b5dd5d41441804b08323a38685c348a5', 'user', 'd357f663ae35b716359741bb217e7157', 'ZKChua', '60', '2% Redemption Point (Lvl 2)', NULL, '2022-04-19 07:03:56', '2022-04-19 07:03:56'),
(5, 'b5dd5d41441804b08323a38685c348a51643192776', 'b5dd5d41441804b08323a38685c348a5', 'user', 'b41fca733e4f10f28827335c631b0515', 'MLNg', '180', '6% Sales Commission (Lvl 3)', NULL, '2022-04-19 07:03:56', '2022-04-19 07:03:56'),
(6, 'b5dd5d41441804b08323a38685c348a51643192776', 'b5dd5d41441804b08323a38685c348a5', 'user', 'b41fca733e4f10f28827335c631b0515', 'MLNg', '60', '2% Redemption Point (Lvl 3)', NULL, '2022-04-19 07:03:56', '2022-04-19 07:03:56'),
(7, 'b5dd5d41441804b08323a38685c348a51643192776', 'b5dd5d41441804b08323a38685c348a5', 'user', '1d3533310d46d70c3802b8f1efc391dc', 'LH Chua', '180', '6% Redemption Point (Lvl 4)', NULL, '2022-04-19 07:03:56', '2022-04-19 07:03:56'),
(8, 'b5dd5d41441804b08323a38685c348a51643192776', 'b5dd5d41441804b08323a38685c348a5', 'user', 'ba8592381362dadc1fc9b60257bf19bb', 'ireneyeoh', '180', '6% Redemption Point (Lvl 5)', NULL, '2022-04-19 07:03:56', '2022-04-19 07:03:56'),
(9, 'b5dd5d41441804b08323a38685c348a51643192776', 'b5dd5d41441804b08323a38685c348a5', 'user', 'a38875a5fd6dbd97f903822d0b3f64ff', 'PS Ng', '180', '6% Redemption Point (Lvl 6)', NULL, '2022-04-19 07:03:56', '2022-04-19 07:03:56'),
(10, 'b5dd5d41441804b08323a38685c348a51643192776', 'b5dd5d41441804b08323a38685c348a5', 'user', 'd0f951ec5bdbc848c52f846fb973d6da', 'YeohShaun', '180', '6% Redemption Point (Lvl 7)', NULL, '2022-04-19 07:03:56', '2022-04-19 07:03:56'),
(11, 'a68fbe04f13c6d9e906bcb0e422cd0a11644478288', 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', 'b5dd5d41441804b08323a38685c348a5', 'user', '50', '10% Sales Commission (Lvl 1)', NULL, '2022-04-19 07:03:59', '2022-04-19 07:03:59'),
(12, 'a68fbe04f13c6d9e906bcb0e422cd0a11644478288', 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', 'b5dd5d41441804b08323a38685c348a5', 'user', '10', '2% Redemption Point (Lvl 1)', NULL, '2022-04-19 07:03:59', '2022-04-19 07:03:59'),
(13, 'a68fbe04f13c6d9e906bcb0e422cd0a11644478288', 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '40', '8% Sales Commission (Lvl 2)', NULL, '2022-04-19 07:03:59', '2022-04-19 07:03:59'),
(14, 'a68fbe04f13c6d9e906bcb0e422cd0a11644478288', 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '10', '2% Redemption Point (Lvl 2)', NULL, '2022-04-19 07:03:59', '2022-04-19 07:03:59'),
(15, 'a68fbe04f13c6d9e906bcb0e422cd0a11644478288', 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', 'd357f663ae35b716359741bb217e7157', 'ZKChua', '30', '6% Sales Commission (Lvl 3)', NULL, '2022-04-19 07:03:59', '2022-04-19 07:03:59'),
(16, 'a68fbe04f13c6d9e906bcb0e422cd0a11644478288', 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', 'd357f663ae35b716359741bb217e7157', 'ZKChua', '10', '2% Redemption Point (Lvl 3)', NULL, '2022-04-19 07:03:59', '2022-04-19 07:03:59'),
(17, 'a68fbe04f13c6d9e906bcb0e422cd0a11644478288', 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', 'b41fca733e4f10f28827335c631b0515', 'MLNg', '30', '6% Redemption Point (Lvl 4)', NULL, '2022-04-19 07:03:59', '2022-04-19 07:03:59'),
(18, 'a68fbe04f13c6d9e906bcb0e422cd0a11644478288', 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', '1d3533310d46d70c3802b8f1efc391dc', 'LH Chua', '30', '6% Redemption Point (Lvl 5)', NULL, '2022-04-19 07:03:59', '2022-04-19 07:03:59'),
(19, 'a68fbe04f13c6d9e906bcb0e422cd0a11644478288', 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', 'ba8592381362dadc1fc9b60257bf19bb', 'ireneyeoh', '30', '6% Redemption Point (Lvl 6)', NULL, '2022-04-19 07:03:59', '2022-04-19 07:03:59'),
(20, 'a68fbe04f13c6d9e906bcb0e422cd0a11644478288', 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', 'a38875a5fd6dbd97f903822d0b3f64ff', 'PS Ng', '30', '6% Redemption Point (Lvl 7)', NULL, '2022-04-19 07:03:59', '2022-04-19 07:03:59'),
(21, '6f844ad00ada79c1547f5f839d92c5eb', 'd0a0661528e10beed710f467137e1b97', 'Bklim0805', 'b9dd1080282915d3256dbda4b4b93960', 'weitsong', '240', '10% Sales Commission (Lvl 1)', NULL, '2022-04-19 07:04:01', '2022-04-19 07:04:01'),
(22, '6f844ad00ada79c1547f5f839d92c5eb', 'd0a0661528e10beed710f467137e1b97', 'Bklim0805', 'b9dd1080282915d3256dbda4b4b93960', 'weitsong', '48', '2% Redemption Point (Lvl 1)', NULL, '2022-04-19 07:04:01', '2022-04-19 07:04:01'),
(23, '6f844ad00ada79c1547f5f839d92c5eb', 'd0a0661528e10beed710f467137e1b97', 'Bklim0805', '267cd1469afa7e914a78672d7b3549b8', 'KentChiam', '192', '8% Sales Commission (Lvl 2)', NULL, '2022-04-19 07:04:01', '2022-04-19 07:04:01'),
(24, '6f844ad00ada79c1547f5f839d92c5eb', 'd0a0661528e10beed710f467137e1b97', 'Bklim0805', '267cd1469afa7e914a78672d7b3549b8', 'KentChiam', '48', '2% Redemption Point (Lvl 2)', NULL, '2022-04-19 07:04:01', '2022-04-19 07:04:01'),
(25, '6f844ad00ada79c1547f5f839d92c5eb', 'd0a0661528e10beed710f467137e1b97', 'Bklim0805', '70691b70ee884b1439e8a73ce33c0eef', 'company', '144', '6% Sales Commission (Lvl 3)', NULL, '2022-04-19 07:04:01', '2022-04-19 07:04:01'),
(26, '6f844ad00ada79c1547f5f839d92c5eb', 'd0a0661528e10beed710f467137e1b97', 'Bklim0805', '70691b70ee884b1439e8a73ce33c0eef', 'company', '48', '2% Redemption Point (Lvl 3)', NULL, '2022-04-19 07:04:01', '2022-04-19 07:04:01'),
(27, '6f844ad00ada79c1547f5f839d92c5eb', 'd0a0661528e10beed710f467137e1b97', 'Bklim0805', 'c7b20dbc8b39d50703c15594073ce731', 'Reserve06', '144', '6% Redemption Point (Lvl 4)', NULL, '2022-04-19 07:04:01', '2022-04-19 07:04:01'),
(28, '6f844ad00ada79c1547f5f839d92c5eb', 'd0a0661528e10beed710f467137e1b97', 'Bklim0805', '6f687ff16c5606ddae9cd8616435a401', 'Reserve05', '144', '6% Redemption Point (Lvl 5)', NULL, '2022-04-19 07:04:01', '2022-04-19 07:04:01'),
(29, '6f844ad00ada79c1547f5f839d92c5eb', 'd0a0661528e10beed710f467137e1b97', 'Bklim0805', 'f79e2c7602cf0102e57dfdfcfcac0f00', 'Reserve04', '144', '6% Redemption Point (Lvl 6)', NULL, '2022-04-19 07:04:01', '2022-04-19 07:04:01'),
(30, '6f844ad00ada79c1547f5f839d92c5eb', 'd0a0661528e10beed710f467137e1b97', 'Bklim0805', '07b610a86ed7dc1a0cf9f484057066b4', 'Reserve03', '144', '6% Redemption Point (Lvl 7)', NULL, '2022-04-19 07:04:01', '2022-04-19 07:04:01'),
(31, 'c3263bccfb131fa6a68b8f98f7320b311649825531', 'c3263bccfb131fa6a68b8f98f7320b31', 'user two', 'b5dd5d41441804b08323a38685c348a5', 'user', '47.5', '10% Sales Commission (Lvl 1)', NULL, '2022-04-19 07:04:03', '2022-04-19 07:04:03'),
(32, 'c3263bccfb131fa6a68b8f98f7320b311649825531', 'c3263bccfb131fa6a68b8f98f7320b31', 'user two', 'b5dd5d41441804b08323a38685c348a5', 'user', '9.5', '2% Redemption Point (Lvl 1)', NULL, '2022-04-19 07:04:03', '2022-04-19 07:04:03'),
(33, 'c3263bccfb131fa6a68b8f98f7320b311649825531', 'c3263bccfb131fa6a68b8f98f7320b31', 'user two', '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '38', '8% Sales Commission (Lvl 2)', NULL, '2022-04-19 07:04:03', '2022-04-19 07:04:03'),
(34, 'c3263bccfb131fa6a68b8f98f7320b311649825531', 'c3263bccfb131fa6a68b8f98f7320b31', 'user two', '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '9.5', '2% Redemption Point (Lvl 2)', NULL, '2022-04-19 07:04:03', '2022-04-19 07:04:03'),
(35, 'c3263bccfb131fa6a68b8f98f7320b311649825531', 'c3263bccfb131fa6a68b8f98f7320b31', 'user two', 'd357f663ae35b716359741bb217e7157', 'ZKChua', '28.5', '6% Sales Commission (Lvl 3)', NULL, '2022-04-19 07:04:03', '2022-04-19 07:04:03'),
(36, 'c3263bccfb131fa6a68b8f98f7320b311649825531', 'c3263bccfb131fa6a68b8f98f7320b31', 'user two', 'd357f663ae35b716359741bb217e7157', 'ZKChua', '9.5', '2% Redemption Point (Lvl 3)', NULL, '2022-04-19 07:04:03', '2022-04-19 07:04:03'),
(37, 'c3263bccfb131fa6a68b8f98f7320b311649825531', 'c3263bccfb131fa6a68b8f98f7320b31', 'user two', 'b41fca733e4f10f28827335c631b0515', 'MLNg', '28.5', '6% Redemption Point (Lvl 4)', NULL, '2022-04-19 07:04:03', '2022-04-19 07:04:03'),
(38, 'c3263bccfb131fa6a68b8f98f7320b311649825531', 'c3263bccfb131fa6a68b8f98f7320b31', 'user two', '1d3533310d46d70c3802b8f1efc391dc', 'LH Chua', '28.5', '6% Redemption Point (Lvl 5)', NULL, '2022-04-19 07:04:03', '2022-04-19 07:04:03'),
(39, 'c3263bccfb131fa6a68b8f98f7320b311649825531', 'c3263bccfb131fa6a68b8f98f7320b31', 'user two', 'ba8592381362dadc1fc9b60257bf19bb', 'ireneyeoh', '28.5', '6% Redemption Point (Lvl 6)', NULL, '2022-04-19 07:04:03', '2022-04-19 07:04:03'),
(40, 'c3263bccfb131fa6a68b8f98f7320b311649825531', 'c3263bccfb131fa6a68b8f98f7320b31', 'user two', 'a38875a5fd6dbd97f903822d0b3f64ff', 'PS Ng', '28.5', '6% Redemption Point (Lvl 7)', NULL, '2022-04-19 07:04:03', '2022-04-19 07:04:03'),
(41, '6123f764a746f91920fd9810b8d3d689', '3208f7e0641debe426e374a74ae3a607', 'mike1.5', 'b5dd5d41441804b08323a38685c348a5', 'user', '87.5', '10% Sales Commission (Lvl 1)', NULL, '2022-04-19 07:04:06', '2022-04-19 07:04:06'),
(42, '6123f764a746f91920fd9810b8d3d689', '3208f7e0641debe426e374a74ae3a607', 'mike1.5', 'b5dd5d41441804b08323a38685c348a5', 'user', '17.5', '2% Redemption Point (Lvl 1)', NULL, '2022-04-19 07:04:06', '2022-04-19 07:04:06'),
(43, '6123f764a746f91920fd9810b8d3d689', '3208f7e0641debe426e374a74ae3a607', 'mike1.5', '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '70', '8% Sales Commission (Lvl 2)', NULL, '2022-04-19 07:04:06', '2022-04-19 07:04:06'),
(44, '6123f764a746f91920fd9810b8d3d689', '3208f7e0641debe426e374a74ae3a607', 'mike1.5', '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '17.5', '2% Redemption Point (Lvl 2)', NULL, '2022-04-19 07:04:06', '2022-04-19 07:04:06'),
(45, '6123f764a746f91920fd9810b8d3d689', '3208f7e0641debe426e374a74ae3a607', 'mike1.5', 'd357f663ae35b716359741bb217e7157', 'ZKChua', '52.5', '6% Sales Commission (Lvl 3)', NULL, '2022-04-19 07:04:06', '2022-04-19 07:04:06'),
(46, '6123f764a746f91920fd9810b8d3d689', '3208f7e0641debe426e374a74ae3a607', 'mike1.5', 'd357f663ae35b716359741bb217e7157', 'ZKChua', '17.5', '2% Redemption Point (Lvl 3)', NULL, '2022-04-19 07:04:06', '2022-04-19 07:04:06'),
(47, '6123f764a746f91920fd9810b8d3d689', '3208f7e0641debe426e374a74ae3a607', 'mike1.5', 'b41fca733e4f10f28827335c631b0515', 'MLNg', '52.5', '6% Redemption Point (Lvl 4)', NULL, '2022-04-19 07:04:06', '2022-04-19 07:04:06'),
(48, '6123f764a746f91920fd9810b8d3d689', '3208f7e0641debe426e374a74ae3a607', 'mike1.5', '1d3533310d46d70c3802b8f1efc391dc', 'LH Chua', '52.5', '6% Redemption Point (Lvl 5)', NULL, '2022-04-19 07:04:06', '2022-04-19 07:04:06'),
(49, '6123f764a746f91920fd9810b8d3d689', '3208f7e0641debe426e374a74ae3a607', 'mike1.5', 'ba8592381362dadc1fc9b60257bf19bb', 'ireneyeoh', '52.5', '6% Redemption Point (Lvl 6)', NULL, '2022-04-19 07:04:06', '2022-04-19 07:04:06'),
(50, '6123f764a746f91920fd9810b8d3d689', '3208f7e0641debe426e374a74ae3a607', 'mike1.5', 'a38875a5fd6dbd97f903822d0b3f64ff', 'PS Ng', '52.5', '6% Redemption Point (Lvl 7)', NULL, '2022-04-19 07:04:06', '2022-04-19 07:04:06'),
(51, '5edbd69090fa7035e85fa4134eca0efa', 'b5dd5d41441804b08323a38685c348a5', 'user', '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '62.5', '10% Sales Commission (Lvl 1)', NULL, '2022-04-19 07:04:08', '2022-04-19 07:04:08'),
(52, '5edbd69090fa7035e85fa4134eca0efa', 'b5dd5d41441804b08323a38685c348a5', 'user', '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '12.5', '2% Redemption Point (Lvl 1)', NULL, '2022-04-19 07:04:08', '2022-04-19 07:04:08'),
(53, '5edbd69090fa7035e85fa4134eca0efa', 'b5dd5d41441804b08323a38685c348a5', 'user', 'd357f663ae35b716359741bb217e7157', 'ZKChua', '50', '8% Sales Commission (Lvl 2)', NULL, '2022-04-19 07:04:08', '2022-04-19 07:04:08'),
(54, '5edbd69090fa7035e85fa4134eca0efa', 'b5dd5d41441804b08323a38685c348a5', 'user', 'd357f663ae35b716359741bb217e7157', 'ZKChua', '12.5', '2% Redemption Point (Lvl 2)', NULL, '2022-04-19 07:04:08', '2022-04-19 07:04:08'),
(55, '5edbd69090fa7035e85fa4134eca0efa', 'b5dd5d41441804b08323a38685c348a5', 'user', 'b41fca733e4f10f28827335c631b0515', 'MLNg', '37.5', '6% Sales Commission (Lvl 3)', NULL, '2022-04-19 07:04:08', '2022-04-19 07:04:08'),
(56, '5edbd69090fa7035e85fa4134eca0efa', 'b5dd5d41441804b08323a38685c348a5', 'user', 'b41fca733e4f10f28827335c631b0515', 'MLNg', '12.5', '2% Redemption Point (Lvl 3)', NULL, '2022-04-19 07:04:08', '2022-04-19 07:04:08'),
(57, '5edbd69090fa7035e85fa4134eca0efa', 'b5dd5d41441804b08323a38685c348a5', 'user', '1d3533310d46d70c3802b8f1efc391dc', 'LH Chua', '37.5', '6% Redemption Point (Lvl 4)', NULL, '2022-04-19 07:04:08', '2022-04-19 07:04:08'),
(58, '5edbd69090fa7035e85fa4134eca0efa', 'b5dd5d41441804b08323a38685c348a5', 'user', 'ba8592381362dadc1fc9b60257bf19bb', 'ireneyeoh', '37.5', '6% Redemption Point (Lvl 5)', NULL, '2022-04-19 07:04:08', '2022-04-19 07:04:08'),
(59, '5edbd69090fa7035e85fa4134eca0efa', 'b5dd5d41441804b08323a38685c348a5', 'user', 'a38875a5fd6dbd97f903822d0b3f64ff', 'PS Ng', '37.5', '6% Redemption Point (Lvl 6)', NULL, '2022-04-19 07:04:08', '2022-04-19 07:04:08'),
(60, '5edbd69090fa7035e85fa4134eca0efa', 'b5dd5d41441804b08323a38685c348a5', 'user', 'd0f951ec5bdbc848c52f846fb973d6da', 'YeohShaun', '37.5', '6% Redemption Point (Lvl 7)', NULL, '2022-04-19 07:04:08', '2022-04-19 07:04:08'),
(61, 'a68fbe04f13c6d9e906bcb0e422cd0a11644487798', 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', 'b5dd5d41441804b08323a38685c348a5', 'user', '50', '10% Sales Commission (Lvl 1)', NULL, '2022-04-19 07:04:09', '2022-04-19 07:04:09'),
(62, 'a68fbe04f13c6d9e906bcb0e422cd0a11644487798', 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', 'b5dd5d41441804b08323a38685c348a5', 'user', '10', '2% Redemption Point (Lvl 1)', NULL, '2022-04-19 07:04:09', '2022-04-19 07:04:09'),
(63, 'a68fbe04f13c6d9e906bcb0e422cd0a11644487798', 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '40', '8% Sales Commission (Lvl 2)', NULL, '2022-04-19 07:04:09', '2022-04-19 07:04:09'),
(64, 'a68fbe04f13c6d9e906bcb0e422cd0a11644487798', 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '10', '2% Redemption Point (Lvl 2)', NULL, '2022-04-19 07:04:09', '2022-04-19 07:04:09'),
(65, 'a68fbe04f13c6d9e906bcb0e422cd0a11644487798', 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', 'd357f663ae35b716359741bb217e7157', 'ZKChua', '30', '6% Sales Commission (Lvl 3)', NULL, '2022-04-19 07:04:09', '2022-04-19 07:04:09'),
(66, 'a68fbe04f13c6d9e906bcb0e422cd0a11644487798', 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', 'd357f663ae35b716359741bb217e7157', 'ZKChua', '10', '2% Redemption Point (Lvl 3)', NULL, '2022-04-19 07:04:09', '2022-04-19 07:04:09'),
(67, 'a68fbe04f13c6d9e906bcb0e422cd0a11644487798', 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', 'b41fca733e4f10f28827335c631b0515', 'MLNg', '30', '6% Redemption Point (Lvl 4)', NULL, '2022-04-19 07:04:09', '2022-04-19 07:04:09'),
(68, 'a68fbe04f13c6d9e906bcb0e422cd0a11644487798', 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', '1d3533310d46d70c3802b8f1efc391dc', 'LH Chua', '30', '6% Redemption Point (Lvl 5)', NULL, '2022-04-19 07:04:09', '2022-04-19 07:04:09'),
(69, 'a68fbe04f13c6d9e906bcb0e422cd0a11644487798', 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', 'ba8592381362dadc1fc9b60257bf19bb', 'ireneyeoh', '30', '6% Redemption Point (Lvl 6)', NULL, '2022-04-19 07:04:09', '2022-04-19 07:04:09'),
(70, 'a68fbe04f13c6d9e906bcb0e422cd0a11644487798', 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', 'a38875a5fd6dbd97f903822d0b3f64ff', 'PS Ng', '30', '6% Redemption Point (Lvl 7)', NULL, '2022-04-19 07:04:09', '2022-04-19 07:04:09'),
(71, '1a4e79676f248f1d64e84dba27e4cf18', '2033d3489f3699966be131b98e2ac71a', 'Chee sk', '21013dbe0b7a0308fb41984bce5f3984', 'andylimch', '100', '10% Sales Commission (Lvl 1)', NULL, '2022-04-19 07:04:11', '2022-04-19 07:04:11'),
(72, '1a4e79676f248f1d64e84dba27e4cf18', '2033d3489f3699966be131b98e2ac71a', 'Chee sk', '21013dbe0b7a0308fb41984bce5f3984', 'andylimch', '20', '2% Redemption Point (Lvl 1)', NULL, '2022-04-19 07:04:11', '2022-04-19 07:04:11'),
(73, '1a4e79676f248f1d64e84dba27e4cf18', '2033d3489f3699966be131b98e2ac71a', 'Chee sk', 'd0f951ec5bdbc848c52f846fb973d6da', 'YeohShaun', '80', '8% Sales Commission (Lvl 2)', NULL, '2022-04-19 07:04:11', '2022-04-19 07:04:11'),
(74, '1a4e79676f248f1d64e84dba27e4cf18', '2033d3489f3699966be131b98e2ac71a', 'Chee sk', 'd0f951ec5bdbc848c52f846fb973d6da', 'YeohShaun', '20', '2% Redemption Point (Lvl 2)', NULL, '2022-04-19 07:04:11', '2022-04-19 07:04:11'),
(75, '1a4e79676f248f1d64e84dba27e4cf18', '2033d3489f3699966be131b98e2ac71a', 'Chee sk', '70691b70ee884b1439e8a73ce33c0eef', 'company', '60', '6% Sales Commission (Lvl 3)', NULL, '2022-04-19 07:04:11', '2022-04-19 07:04:11'),
(76, '1a4e79676f248f1d64e84dba27e4cf18', '2033d3489f3699966be131b98e2ac71a', 'Chee sk', '70691b70ee884b1439e8a73ce33c0eef', 'company', '20', '2% Redemption Point (Lvl 3)', NULL, '2022-04-19 07:04:11', '2022-04-19 07:04:11'),
(77, '1a4e79676f248f1d64e84dba27e4cf18', '2033d3489f3699966be131b98e2ac71a', 'Chee sk', 'c7b20dbc8b39d50703c15594073ce731', 'Reserve06', '60', '6% Redemption Point (Lvl 4)', NULL, '2022-04-19 07:04:11', '2022-04-19 07:04:11'),
(78, '1a4e79676f248f1d64e84dba27e4cf18', '2033d3489f3699966be131b98e2ac71a', 'Chee sk', '6f687ff16c5606ddae9cd8616435a401', 'Reserve05', '60', '6% Redemption Point (Lvl 5)', NULL, '2022-04-19 07:04:11', '2022-04-19 07:04:11'),
(79, '1a4e79676f248f1d64e84dba27e4cf18', '2033d3489f3699966be131b98e2ac71a', 'Chee sk', 'f79e2c7602cf0102e57dfdfcfcac0f00', 'Reserve04', '60', '6% Redemption Point (Lvl 6)', NULL, '2022-04-19 07:04:11', '2022-04-19 07:04:11'),
(80, '1a4e79676f248f1d64e84dba27e4cf18', '2033d3489f3699966be131b98e2ac71a', 'Chee sk', '07b610a86ed7dc1a0cf9f484057066b4', 'Reserve03', '60', '6% Redemption Point (Lvl 7)', NULL, '2022-04-19 07:04:11', '2022-04-19 07:04:11'),
(81, 'f7d8c90ea22b01b15b57d269aa68594b', 'b5dd5d41441804b08323a38685c348a5', 'user', '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '150', '10% Sales Commission (Lvl 1)', NULL, '2022-04-19 07:04:12', '2022-04-19 07:04:12'),
(82, 'f7d8c90ea22b01b15b57d269aa68594b', 'b5dd5d41441804b08323a38685c348a5', 'user', '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '30', '2% Redemption Point (Lvl 1)', NULL, '2022-04-19 07:04:12', '2022-04-19 07:04:12'),
(83, 'f7d8c90ea22b01b15b57d269aa68594b', 'b5dd5d41441804b08323a38685c348a5', 'user', 'd357f663ae35b716359741bb217e7157', 'ZKChua', '120', '8% Sales Commission (Lvl 2)', NULL, '2022-04-19 07:04:12', '2022-04-19 07:04:12'),
(84, 'f7d8c90ea22b01b15b57d269aa68594b', 'b5dd5d41441804b08323a38685c348a5', 'user', 'd357f663ae35b716359741bb217e7157', 'ZKChua', '30', '2% Redemption Point (Lvl 2)', NULL, '2022-04-19 07:04:12', '2022-04-19 07:04:12'),
(85, 'f7d8c90ea22b01b15b57d269aa68594b', 'b5dd5d41441804b08323a38685c348a5', 'user', 'b41fca733e4f10f28827335c631b0515', 'MLNg', '90', '6% Sales Commission (Lvl 3)', NULL, '2022-04-19 07:04:12', '2022-04-19 07:04:12'),
(86, 'f7d8c90ea22b01b15b57d269aa68594b', 'b5dd5d41441804b08323a38685c348a5', 'user', 'b41fca733e4f10f28827335c631b0515', 'MLNg', '30', '2% Redemption Point (Lvl 3)', NULL, '2022-04-19 07:04:12', '2022-04-19 07:04:12'),
(87, 'f7d8c90ea22b01b15b57d269aa68594b', 'b5dd5d41441804b08323a38685c348a5', 'user', '1d3533310d46d70c3802b8f1efc391dc', 'LH Chua', '90', '6% Redemption Point (Lvl 4)', NULL, '2022-04-19 07:04:12', '2022-04-19 07:04:12'),
(88, 'f7d8c90ea22b01b15b57d269aa68594b', 'b5dd5d41441804b08323a38685c348a5', 'user', 'ba8592381362dadc1fc9b60257bf19bb', 'ireneyeoh', '90', '6% Redemption Point (Lvl 5)', NULL, '2022-04-19 07:04:12', '2022-04-19 07:04:12'),
(89, 'f7d8c90ea22b01b15b57d269aa68594b', 'b5dd5d41441804b08323a38685c348a5', 'user', 'a38875a5fd6dbd97f903822d0b3f64ff', 'PS Ng', '90', '6% Redemption Point (Lvl 6)', NULL, '2022-04-19 07:04:12', '2022-04-19 07:04:12'),
(90, 'f7d8c90ea22b01b15b57d269aa68594b', 'b5dd5d41441804b08323a38685c348a5', 'user', 'd0f951ec5bdbc848c52f846fb973d6da', 'YeohShaun', '90', '6% Redemption Point (Lvl 7)', NULL, '2022-04-19 07:04:12', '2022-04-19 07:04:12'),
(91, 'e9140833d49ab185007f6b36e6c98ba71644653117', '0dbdc899bc89b530c7a17012d59b8644', 'mimiung', '515d91b5b51ed0ff8c0a4c5deaa95c3c', 'NickNai', '100', '10% Sales Commission (Lvl 1)', NULL, '2022-04-19 07:04:13', '2022-04-19 07:04:13'),
(92, 'e9140833d49ab185007f6b36e6c98ba71644653117', '0dbdc899bc89b530c7a17012d59b8644', 'mimiung', '515d91b5b51ed0ff8c0a4c5deaa95c3c', 'NickNai', '20', '2% Redemption Point (Lvl 1)', NULL, '2022-04-19 07:04:13', '2022-04-19 07:04:13'),
(93, 'e9140833d49ab185007f6b36e6c98ba71644653117', '0dbdc899bc89b530c7a17012d59b8644', 'mimiung', '70691b70ee884b1439e8a73ce33c0eef', 'company', '80', '8% Sales Commission (Lvl 2)', NULL, '2022-04-19 07:04:13', '2022-04-19 07:04:13'),
(94, 'e9140833d49ab185007f6b36e6c98ba71644653117', '0dbdc899bc89b530c7a17012d59b8644', 'mimiung', '70691b70ee884b1439e8a73ce33c0eef', 'company', '20', '2% Redemption Point (Lvl 2)', NULL, '2022-04-19 07:04:13', '2022-04-19 07:04:13'),
(95, 'e9140833d49ab185007f6b36e6c98ba71644653117', '0dbdc899bc89b530c7a17012d59b8644', 'mimiung', 'c7b20dbc8b39d50703c15594073ce731', 'Reserve06', '60', '6% Sales Commission (Lvl 3)', NULL, '2022-04-19 07:04:13', '2022-04-19 07:04:13'),
(96, 'e9140833d49ab185007f6b36e6c98ba71644653117', '0dbdc899bc89b530c7a17012d59b8644', 'mimiung', 'c7b20dbc8b39d50703c15594073ce731', 'Reserve06', '20', '2% Redemption Point (Lvl 3)', NULL, '2022-04-19 07:04:13', '2022-04-19 07:04:13'),
(97, 'e9140833d49ab185007f6b36e6c98ba71644653117', '0dbdc899bc89b530c7a17012d59b8644', 'mimiung', '6f687ff16c5606ddae9cd8616435a401', 'Reserve05', '60', '6% Redemption Point (Lvl 4)', NULL, '2022-04-19 07:04:13', '2022-04-19 07:04:13'),
(98, 'e9140833d49ab185007f6b36e6c98ba71644653117', '0dbdc899bc89b530c7a17012d59b8644', 'mimiung', 'f79e2c7602cf0102e57dfdfcfcac0f00', 'Reserve04', '60', '6% Redemption Point (Lvl 5)', NULL, '2022-04-19 07:04:13', '2022-04-19 07:04:13'),
(99, 'e9140833d49ab185007f6b36e6c98ba71644653117', '0dbdc899bc89b530c7a17012d59b8644', 'mimiung', '07b610a86ed7dc1a0cf9f484057066b4', 'Reserve03', '60', '6% Redemption Point (Lvl 6)', NULL, '2022-04-19 07:04:13', '2022-04-19 07:04:13'),
(100, 'e9140833d49ab185007f6b36e6c98ba71644653117', '0dbdc899bc89b530c7a17012d59b8644', 'mimiung', '1fc0abce45843733bd5aa6ca6886ae7f', 'Reserve02', '60', '6% Redemption Point (Lvl 7)', NULL, '2022-04-19 07:04:13', '2022-04-19 07:04:13'),
(101, '1c7c566372eeac2360374f8ac00e80d31643193205', '1c7c566372eeac2360374f8ac00e80d3', 'lim', '1bb3266cd1c0bf3fa22c06e6a9eb892d', 'Joseph', '67.5', '10% Sales Commission (Lvl 1)', NULL, '2022-04-19 07:04:16', '2022-04-19 07:04:16'),
(102, '1c7c566372eeac2360374f8ac00e80d31643193205', '1c7c566372eeac2360374f8ac00e80d3', 'lim', '1bb3266cd1c0bf3fa22c06e6a9eb892d', 'Joseph', '13.5', '2% Redemption Point (Lvl 1)', NULL, '2022-04-19 07:04:16', '2022-04-19 07:04:16'),
(103, '1c7c566372eeac2360374f8ac00e80d31643193205', '1c7c566372eeac2360374f8ac00e80d3', 'lim', '70691b70ee884b1439e8a73ce33c0eef', 'company', '54', '8% Sales Commission (Lvl 2)', NULL, '2022-04-19 07:04:16', '2022-04-19 07:04:16'),
(104, '1c7c566372eeac2360374f8ac00e80d31643193205', '1c7c566372eeac2360374f8ac00e80d3', 'lim', '70691b70ee884b1439e8a73ce33c0eef', 'company', '13.5', '2% Redemption Point (Lvl 2)', NULL, '2022-04-19 07:04:16', '2022-04-19 07:04:16'),
(105, '1c7c566372eeac2360374f8ac00e80d31643193205', '1c7c566372eeac2360374f8ac00e80d3', 'lim', 'c7b20dbc8b39d50703c15594073ce731', 'Reserve06', '40.5', '6% Sales Commission (Lvl 3)', NULL, '2022-04-19 07:04:16', '2022-04-19 07:04:16'),
(106, '1c7c566372eeac2360374f8ac00e80d31643193205', '1c7c566372eeac2360374f8ac00e80d3', 'lim', 'c7b20dbc8b39d50703c15594073ce731', 'Reserve06', '13.5', '2% Redemption Point (Lvl 3)', NULL, '2022-04-19 07:04:16', '2022-04-19 07:04:16'),
(107, '1c7c566372eeac2360374f8ac00e80d31643193205', '1c7c566372eeac2360374f8ac00e80d3', 'lim', '6f687ff16c5606ddae9cd8616435a401', 'Reserve05', '40.5', '6% Redemption Point (Lvl 4)', NULL, '2022-04-19 07:04:16', '2022-04-19 07:04:16'),
(108, '1c7c566372eeac2360374f8ac00e80d31643193205', '1c7c566372eeac2360374f8ac00e80d3', 'lim', 'f79e2c7602cf0102e57dfdfcfcac0f00', 'Reserve04', '40.5', '6% Redemption Point (Lvl 5)', NULL, '2022-04-19 07:04:16', '2022-04-19 07:04:16'),
(109, '1c7c566372eeac2360374f8ac00e80d31643193205', '1c7c566372eeac2360374f8ac00e80d3', 'lim', '07b610a86ed7dc1a0cf9f484057066b4', 'Reserve03', '40.5', '6% Redemption Point (Lvl 6)', NULL, '2022-04-19 07:04:16', '2022-04-19 07:04:16'),
(110, '1c7c566372eeac2360374f8ac00e80d31643193205', '1c7c566372eeac2360374f8ac00e80d3', 'lim', '1fc0abce45843733bd5aa6ca6886ae7f', 'Reserve02', '40.5', '6% Redemption Point (Lvl 7)', NULL, '2022-04-19 07:04:16', '2022-04-19 07:04:16'),
(111, 'a4f0be4e415f65008164d5478ea95628', 'be4dfa404b72988cbfa6b931fa2b25ab', 'Josie', 'ff554b0ddaed52fdb196486b6875821b', 'shuennchi2020', '150', '10% Sales Commission (Lvl 1)', NULL, '2022-04-19 07:04:18', '2022-04-19 07:04:18'),
(112, 'a4f0be4e415f65008164d5478ea95628', 'be4dfa404b72988cbfa6b931fa2b25ab', 'Josie', 'ff554b0ddaed52fdb196486b6875821b', 'shuennchi2020', '30', '2% Redemption Point (Lvl 1)', NULL, '2022-04-19 07:04:18', '2022-04-19 07:04:18'),
(113, 'a4f0be4e415f65008164d5478ea95628', 'be4dfa404b72988cbfa6b931fa2b25ab', 'Josie', 'ecbbf12d6997927fd668dca72dbf6092', 'junwei', '120', '8% Sales Commission (Lvl 2)', NULL, '2022-04-19 07:04:18', '2022-04-19 07:04:18'),
(114, 'a4f0be4e415f65008164d5478ea95628', 'be4dfa404b72988cbfa6b931fa2b25ab', 'Josie', 'ecbbf12d6997927fd668dca72dbf6092', 'junwei', '30', '2% Redemption Point (Lvl 2)', NULL, '2022-04-19 07:04:18', '2022-04-19 07:04:18'),
(115, 'a4f0be4e415f65008164d5478ea95628', 'be4dfa404b72988cbfa6b931fa2b25ab', 'Josie', '1bb3266cd1c0bf3fa22c06e6a9eb892d', 'Joseph', '90', '6% Sales Commission (Lvl 3)', NULL, '2022-04-19 07:04:18', '2022-04-19 07:04:18'),
(116, 'a4f0be4e415f65008164d5478ea95628', 'be4dfa404b72988cbfa6b931fa2b25ab', 'Josie', '1bb3266cd1c0bf3fa22c06e6a9eb892d', 'Joseph', '30', '2% Redemption Point (Lvl 3)', NULL, '2022-04-19 07:04:18', '2022-04-19 07:04:18'),
(117, 'a4f0be4e415f65008164d5478ea95628', 'be4dfa404b72988cbfa6b931fa2b25ab', 'Josie', '70691b70ee884b1439e8a73ce33c0eef', 'company', '90', '6% Redemption Point (Lvl 4)', NULL, '2022-04-19 07:04:18', '2022-04-19 07:04:18'),
(118, 'a4f0be4e415f65008164d5478ea95628', 'be4dfa404b72988cbfa6b931fa2b25ab', 'Josie', 'c7b20dbc8b39d50703c15594073ce731', 'Reserve06', '90', '6% Redemption Point (Lvl 5)', NULL, '2022-04-19 07:04:18', '2022-04-19 07:04:18'),
(119, 'a4f0be4e415f65008164d5478ea95628', 'be4dfa404b72988cbfa6b931fa2b25ab', 'Josie', '6f687ff16c5606ddae9cd8616435a401', 'Reserve05', '90', '6% Redemption Point (Lvl 6)', NULL, '2022-04-19 07:04:18', '2022-04-19 07:04:18'),
(120, 'a4f0be4e415f65008164d5478ea95628', 'be4dfa404b72988cbfa6b931fa2b25ab', 'Josie', 'f79e2c7602cf0102e57dfdfcfcac0f00', 'Reserve04', '90', '6% Redemption Point (Lvl 7)', NULL, '2022-04-19 07:04:18', '2022-04-19 07:04:18'),
(121, 'b5dd5d41441804b08323a38685c348a51644391778', 'b5dd5d41441804b08323a38685c348a5', 'user', '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '80', '10% Sales Commission (Lvl 1)', NULL, '2022-04-19 07:04:20', '2022-04-19 07:04:20'),
(122, 'b5dd5d41441804b08323a38685c348a51644391778', 'b5dd5d41441804b08323a38685c348a5', 'user', '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '16', '2% Redemption Point (Lvl 1)', NULL, '2022-04-19 07:04:20', '2022-04-19 07:04:20'),
(123, 'b5dd5d41441804b08323a38685c348a51644391778', 'b5dd5d41441804b08323a38685c348a5', 'user', 'd357f663ae35b716359741bb217e7157', 'ZKChua', '64', '8% Sales Commission (Lvl 2)', NULL, '2022-04-19 07:04:20', '2022-04-19 07:04:20'),
(124, 'b5dd5d41441804b08323a38685c348a51644391778', 'b5dd5d41441804b08323a38685c348a5', 'user', 'd357f663ae35b716359741bb217e7157', 'ZKChua', '16', '2% Redemption Point (Lvl 2)', NULL, '2022-04-19 07:04:20', '2022-04-19 07:04:20'),
(125, 'b5dd5d41441804b08323a38685c348a51644391778', 'b5dd5d41441804b08323a38685c348a5', 'user', 'b41fca733e4f10f28827335c631b0515', 'MLNg', '48', '6% Sales Commission (Lvl 3)', NULL, '2022-04-19 07:04:20', '2022-04-19 07:04:20'),
(126, 'b5dd5d41441804b08323a38685c348a51644391778', 'b5dd5d41441804b08323a38685c348a5', 'user', 'b41fca733e4f10f28827335c631b0515', 'MLNg', '16', '2% Redemption Point (Lvl 3)', NULL, '2022-04-19 07:04:20', '2022-04-19 07:04:20'),
(127, 'b5dd5d41441804b08323a38685c348a51644391778', 'b5dd5d41441804b08323a38685c348a5', 'user', '1d3533310d46d70c3802b8f1efc391dc', 'LH Chua', '48', '6% Redemption Point (Lvl 4)', NULL, '2022-04-19 07:04:20', '2022-04-19 07:04:20'),
(128, 'b5dd5d41441804b08323a38685c348a51644391778', 'b5dd5d41441804b08323a38685c348a5', 'user', 'ba8592381362dadc1fc9b60257bf19bb', 'ireneyeoh', '48', '6% Redemption Point (Lvl 5)', NULL, '2022-04-19 07:04:20', '2022-04-19 07:04:20'),
(129, 'b5dd5d41441804b08323a38685c348a51644391778', 'b5dd5d41441804b08323a38685c348a5', 'user', 'a38875a5fd6dbd97f903822d0b3f64ff', 'PS Ng', '48', '6% Redemption Point (Lvl 6)', NULL, '2022-04-19 07:04:20', '2022-04-19 07:04:20'),
(130, 'b5dd5d41441804b08323a38685c348a51644391778', 'b5dd5d41441804b08323a38685c348a5', 'user', 'd0f951ec5bdbc848c52f846fb973d6da', 'YeohShaun', '48', '6% Redemption Point (Lvl 7)', NULL, '2022-04-19 07:04:20', '2022-04-19 07:04:20');

-- --------------------------------------------------------

--
-- Table structure for table `bonus_sales_or_rebate`
--

CREATE TABLE `bonus_sales_or_rebate` (
  `id` int(11) NOT NULL,
  `order_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `receiver_uid` varchar(255) DEFAULT NULL,
  `receiver` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `bonus_type` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bonus_sales_or_rebate`
--

INSERT INTO `bonus_sales_or_rebate` (`id`, `order_uid`, `uid`, `username`, `receiver_uid`, `receiver`, `amount`, `bonus_type`, `status`, `date_created`, `date_updated`) VALUES
(1, '5edbd69090fa7035e85fa4134eca0efa', 'dce894fc550ee7ee52e9a13fb6fa07ff', NULL, '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '25', '20% First Purchase Commission (VFDP30s)', NULL, '2022-04-19 07:04:24', '2022-04-19 07:04:24'),
(2, '5edbd69090fa7035e85fa4134eca0efa', 'dce894fc550ee7ee52e9a13fb6fa07ff', NULL, '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '2.5', '2% Redemption Point (VFDP30s)', NULL, '2022-04-19 07:04:24', '2022-04-19 07:04:24'),
(3, '5edbd69090fa7035e85fa4134eca0efa', '26957cf0df016805c7586de2ab1be69a', NULL, 'b5dd5d41441804b08323a38685c348a5', 'user', '225', '20% Rebate (VFDP30s)', NULL, '2022-04-19 07:04:24', '2022-04-19 07:04:24'),
(4, '5edbd69090fa7035e85fa4134eca0efa', '26957cf0df016805c7586de2ab1be69a', NULL, 'b5dd5d41441804b08323a38685c348a5', 'user', '22.5', '2% Redemption Point (VFDP30s)', NULL, '2022-04-19 07:04:24', '2022-04-19 07:04:24'),
(5, '6f844ad00ada79c1547f5f839d92c5eb', '6faa35e335df67f0da627cc234e15240', NULL, 'b9dd1080282915d3256dbda4b4b93960', 'weitsong', '25', '20% First Purchase Commission (VFDP30s)', NULL, '2022-04-19 07:04:24', '2022-04-19 07:04:24'),
(6, '6f844ad00ada79c1547f5f839d92c5eb', '6faa35e335df67f0da627cc234e15240', NULL, 'b9dd1080282915d3256dbda4b4b93960', 'weitsong', '2.5', '2% Redemption Point (VFDP30s)', NULL, '2022-04-19 07:04:24', '2022-04-19 07:04:24'),
(7, '6f844ad00ada79c1547f5f839d92c5eb', 'b33a0b4fcb760c357b6390d780a24509', NULL, 'd0a0661528e10beed710f467137e1b97', 'Bklim0805', '75', '20% Rebate (VFDP30s)', NULL, '2022-04-19 07:04:24', '2022-04-19 07:04:24'),
(8, '6f844ad00ada79c1547f5f839d92c5eb', 'b33a0b4fcb760c357b6390d780a24509', NULL, 'd0a0661528e10beed710f467137e1b97', 'Bklim0805', '7.5', '2% Redemption Point (VFDP30s)', NULL, '2022-04-19 07:04:24', '2022-04-19 07:04:24'),
(9, '6123f764a746f91920fd9810b8d3d689', '5bba8db4c11e20c9352300bf694f528d', NULL, 'b5dd5d41441804b08323a38685c348a5', 'user', '25', '20% First Purchase Commission (VFDP30s)', NULL, '2022-04-19 07:04:24', '2022-04-19 07:04:24'),
(10, '6123f764a746f91920fd9810b8d3d689', '5bba8db4c11e20c9352300bf694f528d', NULL, 'b5dd5d41441804b08323a38685c348a5', 'user', '2.5', '2% Redemption Point (VFDP30s)', NULL, '2022-04-19 07:04:24', '2022-04-19 07:04:24'),
(11, '6123f764a746f91920fd9810b8d3d689', 'ec0ca23fcbd7802d527937952c0d9980', NULL, '3208f7e0641debe426e374a74ae3a607', 'mike1.5', '50', '20% Rebate (VFDP30s)', NULL, '2022-04-19 07:04:24', '2022-04-19 07:04:24'),
(12, '6123f764a746f91920fd9810b8d3d689', 'ec0ca23fcbd7802d527937952c0d9980', NULL, '3208f7e0641debe426e374a74ae3a607', 'mike1.5', '5', '2% Redemption Point (VFDP30s)', NULL, '2022-04-19 07:04:24', '2022-04-19 07:04:24'),
(13, 'c3263bccfb131fa6a68b8f98f7320b311649825531', '10927f1113c7a81dfd1f2e6b70d37fe8', NULL, 'b5dd5d41441804b08323a38685c348a5', 'user', '25', '20% First Purchase Commission (VFDP30s)', NULL, '2022-04-19 07:04:24', '2022-04-19 07:04:24'),
(14, 'c3263bccfb131fa6a68b8f98f7320b311649825531', '10927f1113c7a81dfd1f2e6b70d37fe8', NULL, 'b5dd5d41441804b08323a38685c348a5', 'user', '2.5', '2% Redemption Point (VFDP30s)', NULL, '2022-04-19 07:04:24', '2022-04-19 07:04:24'),
(15, 'c3263bccfb131fa6a68b8f98f7320b311649825531', '7aee072305c0c7138ed41e233a65bcdf', NULL, 'c3263bccfb131fa6a68b8f98f7320b31', 'user two', '25', '20% Rebate (VFDP30s)', NULL, '2022-04-19 07:04:24', '2022-04-19 07:04:24'),
(16, 'c3263bccfb131fa6a68b8f98f7320b311649825531', '7aee072305c0c7138ed41e233a65bcdf', NULL, 'c3263bccfb131fa6a68b8f98f7320b31', 'user two', '2.5', '2% Redemption Point (VFDP30s)', NULL, '2022-04-19 07:04:24', '2022-04-19 07:04:24'),
(17, '1c7c566372eeac2360374f8ac00e80d31643193205', '25e96327ccdf97558b4d05cea22cf3e4', NULL, '1bb3266cd1c0bf3fa22c06e6a9eb892d', 'Joseph', '45', '20% First Purchase Commission (VFDP60s)', NULL, '2022-04-19 07:04:27', '2022-04-19 07:04:27'),
(18, '1c7c566372eeac2360374f8ac00e80d31643193205', '25e96327ccdf97558b4d05cea22cf3e4', NULL, '1bb3266cd1c0bf3fa22c06e6a9eb892d', 'Joseph', '4.5', '2% Redemption Point (VFDP60s)', NULL, '2022-04-19 07:04:27', '2022-04-19 07:04:27'),
(19, '1c7c566372eeac2360374f8ac00e80d31643193205', '73c354b810127754b6bc177712f14a9d', NULL, '1c7c566372eeac2360374f8ac00e80d3', 'lim', '90', '20% Rebate (VFDP60s)', NULL, '2022-04-19 07:04:27', '2022-04-19 07:04:27'),
(20, '1c7c566372eeac2360374f8ac00e80d31643193205', '73c354b810127754b6bc177712f14a9d', NULL, '1c7c566372eeac2360374f8ac00e80d3', 'lim', '9', '2% Redemption Point (VFDP60s)', NULL, '2022-04-19 07:04:27', '2022-04-19 07:04:27'),
(21, 'e478e1612e88658eed0971432662cf54', 'eb79c1f95c0744fb65d417569ad3ceac', NULL, '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '45', '20% First Purchase Commission (VFDP60s)', NULL, '2022-04-19 07:04:27', '2022-04-19 07:04:27'),
(22, 'e478e1612e88658eed0971432662cf54', 'eb79c1f95c0744fb65d417569ad3ceac', NULL, '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '4.5', '2% Redemption Point (VFDP60s)', NULL, '2022-04-19 07:04:27', '2022-04-19 07:04:27'),
(23, 'e478e1612e88658eed0971432662cf54', 'ea1a24b94d9826e44ec577704e907394', NULL, 'b5dd5d41441804b08323a38685c348a5', 'user', '90', '20% Rebate (VFDP60s)', NULL, '2022-04-19 07:04:27', '2022-04-19 07:04:27'),
(24, 'e478e1612e88658eed0971432662cf54', 'ea1a24b94d9826e44ec577704e907394', NULL, 'b5dd5d41441804b08323a38685c348a5', 'user', '9', '2% Redemption Point (VFDP60s)', NULL, '2022-04-19 07:04:27', '2022-04-19 07:04:27'),
(25, '1a4e79676f248f1d64e84dba27e4cf18', '4057f5324ae73b38c3fd06f875845dbf', NULL, '21013dbe0b7a0308fb41984bce5f3984', 'andylimch', '100', '20% First Purchase Commission (VFDP150s)', NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29'),
(26, '1a4e79676f248f1d64e84dba27e4cf18', '4057f5324ae73b38c3fd06f875845dbf', NULL, '21013dbe0b7a0308fb41984bce5f3984', 'andylimch', '10', '2% Redemption Point (VFDP150s)', NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29'),
(27, '1a4e79676f248f1d64e84dba27e4cf18', '8ad42afc9ae9ec09f5d13aa11bcd20e0', NULL, '2033d3489f3699966be131b98e2ac71a', 'Chee sk', '100', '20% Rebate (VFDP150s)', NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29'),
(28, '1a4e79676f248f1d64e84dba27e4cf18', '8ad42afc9ae9ec09f5d13aa11bcd20e0', NULL, '2033d3489f3699966be131b98e2ac71a', 'Chee sk', '10', '2% Redemption Point (VFDP150s)', NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29'),
(29, 'a4f0be4e415f65008164d5478ea95628', '822aef887d585e63cecdfd8a39fb23a5', NULL, 'ff554b0ddaed52fdb196486b6875821b', 'shuennchi2020', '100', '20% First Purchase Commission (VFDP150s)', NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29'),
(30, 'a4f0be4e415f65008164d5478ea95628', '822aef887d585e63cecdfd8a39fb23a5', NULL, 'ff554b0ddaed52fdb196486b6875821b', 'shuennchi2020', '10', '2% Redemption Point (VFDP150s)', NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29'),
(31, 'a4f0be4e415f65008164d5478ea95628', '3c4c53a353fe260af7d12b7012dd6e2a', NULL, 'be4dfa404b72988cbfa6b931fa2b25ab', 'Josie', '200', '20% Rebate (VFDP150s)', NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29'),
(32, 'a4f0be4e415f65008164d5478ea95628', '3c4c53a353fe260af7d12b7012dd6e2a', NULL, 'be4dfa404b72988cbfa6b931fa2b25ab', 'Josie', '20', '2% Redemption Point (VFDP150s)', NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29'),
(33, 'f7d8c90ea22b01b15b57d269aa68594b', 'f74e960258d1aab964ca8f7e13cf6a74', NULL, '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '100', '20% First Purchase Commission (VFDP150s)', NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29'),
(34, 'f7d8c90ea22b01b15b57d269aa68594b', 'f74e960258d1aab964ca8f7e13cf6a74', NULL, '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '10', '2% Redemption Point (VFDP150s)', NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29'),
(35, 'f7d8c90ea22b01b15b57d269aa68594b', 'be5f686d73aa1b2a590aeaf469e3155a', NULL, 'b5dd5d41441804b08323a38685c348a5', 'user', '1400', '20% Rebate (VFDP150s)', NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29'),
(36, 'f7d8c90ea22b01b15b57d269aa68594b', 'be5f686d73aa1b2a590aeaf469e3155a', NULL, 'b5dd5d41441804b08323a38685c348a5', 'user', '140', '2% Redemption Point (VFDP150s)', NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29'),
(37, 'e9140833d49ab185007f6b36e6c98ba71644653117', '5f33bb86b5e6e5cba3b418fa59e5cc58', NULL, '515d91b5b51ed0ff8c0a4c5deaa95c3c', 'NickNai', '100', '20% First Purchase Commission (VFDP150s)', NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29'),
(38, 'e9140833d49ab185007f6b36e6c98ba71644653117', '5f33bb86b5e6e5cba3b418fa59e5cc58', NULL, '515d91b5b51ed0ff8c0a4c5deaa95c3c', 'NickNai', '10', '2% Redemption Point (VFDP150s)', NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29'),
(39, 'e9140833d49ab185007f6b36e6c98ba71644653117', '8af5714def4960b499f061fab68a217a', NULL, '0dbdc899bc89b530c7a17012d59b8644', 'mimiung', '100', '20% Rebate (VFDP150s)', NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29'),
(40, 'e9140833d49ab185007f6b36e6c98ba71644653117', '8af5714def4960b499f061fab68a217a', NULL, '0dbdc899bc89b530c7a17012d59b8644', 'mimiung', '10', '2% Redemption Point (VFDP150s)', NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29'),
(41, 'a68fbe04f13c6d9e906bcb0e422cd0a11644487798', '53a925b6854b817e010fc6c7bdf98729', NULL, 'b5dd5d41441804b08323a38685c348a5', 'user', '100', '20% First Purchase Commission (VFDP150s)', NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29'),
(42, 'a68fbe04f13c6d9e906bcb0e422cd0a11644487798', '53a925b6854b817e010fc6c7bdf98729', NULL, 'b5dd5d41441804b08323a38685c348a5', 'user', '10', '2% Redemption Point (VFDP150s)', NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29'),
(43, 'a68fbe04f13c6d9e906bcb0e422cd0a11644487798', '09e54d94f57a544fa2f67e57a4cebb84', NULL, 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', '100', '20% Rebate (VFDP150s)', NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29'),
(44, 'a68fbe04f13c6d9e906bcb0e422cd0a11644487798', '09e54d94f57a544fa2f67e57a4cebb84', NULL, 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', '10', '2% Redemption Point (VFDP150s)', NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29');

-- --------------------------------------------------------

--
-- Table structure for table `bonus_star`
--

CREATE TABLE `bonus_star` (
  `id` int(11) NOT NULL,
  `order_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `receiver_uid` varchar(255) DEFAULT NULL,
  `receiver` varchar(255) DEFAULT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `bonus_type` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bonus_star`
--

INSERT INTO `bonus_star` (`id`, `order_uid`, `uid`, `username`, `receiver_uid`, `receiver`, `user_type`, `amount`, `bonus_type`, `status`, `date_created`, `date_updated`) VALUES
(1, NULL, 'd1d4d3657283decd47b1a8fd283094b0', NULL, '70691b70ee884b1439e8a73ce33c0eef', 'company', '2', '14850', 'Star Agent Achievement 2%', 'Claim', '2022-04-19 07:04:32', '2022-04-19 07:11:55'),
(2, NULL, '2dcb541a42f2acd91036c5dbf93098a5', NULL, 'd0f951ec5bdbc848c52f846fb973d6da', 'YeohShaun', '1', '9275', 'Star Agent Achievement 2%', 'Claim', '2022-04-19 07:04:32', '2022-04-19 07:11:55'),
(3, NULL, '8b9c794ef36bc640c278f1bb7c3e5d12', NULL, 'a38875a5fd6dbd97f903822d0b3f64ff', 'PS Ng', '1', '8275', 'Star Agent Achievement 2%', 'Claim', '2022-04-19 07:04:33', '2022-04-19 07:11:55'),
(4, NULL, 'f0eebd2f6b5ef29535113a96187f4172', NULL, 'ba8592381362dadc1fc9b60257bf19bb', 'ireneyeoh', '1', '8275', 'Star Agent Achievement 2%', 'Claim', '2022-04-19 07:04:36', '2022-04-19 07:11:55'),
(5, NULL, '1157850d73e58c3b94a243160d30da4b', NULL, '1d3533310d46d70c3802b8f1efc391dc', 'LH Chua', '1', '8275', 'Star Agent Achievement 2%', 'Claim', '2022-04-19 07:04:41', '2022-04-19 07:11:55'),
(6, NULL, '0b73d17008d54aa08cf36f2423fb98f4', NULL, 'b41fca733e4f10f28827335c631b0515', 'MLNg', '1', '8275', 'Star Agent Achievement 2%', 'Claim', '2022-04-19 07:04:42', '2022-04-19 07:11:55'),
(7, NULL, '57db6f98b459370bc20ed0c4ee595ea0', NULL, 'd357f663ae35b716359741bb217e7157', 'ZKChua', '1', '8275', 'Star Agent Achievement 2%', 'Claim', '2022-04-19 07:04:43', '2022-04-19 07:11:55'),
(8, NULL, '5fa90f280ba2ea7f49f783072037704a', NULL, '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '1', '8275', 'Star Agent Achievement 2%', 'Claim', '2022-04-19 07:04:43', '2022-04-19 07:11:55'),
(9, NULL, 'bfd58f7736f42fea98eb4537e82c9b55', NULL, 'b5dd5d41441804b08323a38685c348a5', 'user', '1', '8275', 'Star Agent Achievement 2%', 'Claim', '2022-04-19 07:04:43', '2022-04-19 07:11:55');

-- --------------------------------------------------------

--
-- Table structure for table `bonus_star_claim`
--

CREATE TABLE `bonus_star_claim` (
  `id` int(11) NOT NULL,
  `order_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `receiver_uid` varchar(255) DEFAULT NULL,
  `receiver` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `bonus_type` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bonus_star_claim`
--

INSERT INTO `bonus_star_claim` (`id`, `order_uid`, `uid`, `username`, `receiver_uid`, `receiver`, `amount`, `bonus_type`, `status`, `date_created`, `date_updated`) VALUES
(1, NULL, 'ae953759ff0143689813f8dd93b3dc84', NULL, 'd0f951ec5bdbc848c52f846fb973d6da', 'YeohShaun', '3712.5', 'Star Agent Achievement 2% RM29700', 'Claim', '2022-04-19 07:11:55', '2022-04-19 07:11:55'),
(2, NULL, 'ed4df2a46340c6c88fcb2772ef41b77a', NULL, 'a38875a5fd6dbd97f903822d0b3f64ff', 'PS Ng', '3712.5', 'Star Agent Achievement 2% RM29700', 'Claim', '2022-04-19 07:11:55', '2022-04-19 07:11:55'),
(3, NULL, '68df567f2e528668d8dc86257903cc7e', NULL, 'ba8592381362dadc1fc9b60257bf19bb', 'ireneyeoh', '3712.5', 'Star Agent Achievement 2% RM29700', 'Claim', '2022-04-19 07:11:55', '2022-04-19 07:11:55'),
(4, NULL, 'a361a2fe68c7faf977dc95459e8533c1', NULL, '1d3533310d46d70c3802b8f1efc391dc', 'LH Chua', '3712.5', 'Star Agent Achievement 2% RM29700', 'Claim', '2022-04-19 07:11:55', '2022-04-19 07:11:55'),
(5, NULL, '02c264a178b67d977517cfe57c3f532b', NULL, 'b41fca733e4f10f28827335c631b0515', 'MLNg', '3712.5', 'Star Agent Achievement 2% RM29700', 'Claim', '2022-04-19 07:11:55', '2022-04-19 07:11:55'),
(6, NULL, '68bc61f70e1358dc88f94e8b584ee598', NULL, 'd357f663ae35b716359741bb217e7157', 'ZKChua', '3712.5', 'Star Agent Achievement 2% RM29700', 'Claim', '2022-04-19 07:11:55', '2022-04-19 07:11:55'),
(7, NULL, '527054c655a9f56fa26c632008e4b2ec', NULL, '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', '3712.5', 'Star Agent Achievement 2% RM29700', 'Claim', '2022-04-19 07:11:55', '2022-04-19 07:11:55'),
(8, NULL, '494c8af959983d72c0d7a1bf61aff22d', NULL, 'b5dd5d41441804b08323a38685c348a5', 'user', '3712.5', 'Star Agent Achievement 2% RM29700', 'Claim', '2022-04-19 07:11:55', '2022-04-19 07:11:55');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id` bigint(20) NOT NULL,
  `level` varchar(255) DEFAULT NULL,
  `sales_commission` varchar(255) DEFAULT NULL,
  `redemption_point` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id`, `level`, `sales_commission`, `redemption_point`, `date_created`, `date_updated`) VALUES
(1, '1', '10', '2', '2022-01-26 09:16:36', '2022-01-26 09:16:36'),
(2, '2', '8', '2', '2022-01-26 09:16:36', '2022-01-26 09:16:36'),
(3, '3', '6', '2', '2022-01-26 09:17:13', '2022-01-26 09:17:13'),
(4, '4', '0', '6', '2022-01-26 09:17:13', '2022-01-26 09:17:13'),
(5, '5', '0', '6', '2022-01-26 09:17:28', '2022-01-26 09:17:28'),
(6, '6', '0', '6', '2022-01-26 09:17:28', '2022-01-26 09:17:28'),
(7, '7', '0', '6', '2022-01-26 09:17:41', '2022-01-26 09:17:41');

-- --------------------------------------------------------

--
-- Table structure for table `livestream`
--

CREATE TABLE `livestream` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `channel` text DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `time_start` varchar(255) DEFAULT NULL,
  `time_end` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `livestream`
--

INSERT INTO `livestream` (`id`, `uid`, `user_uid`, `username`, `channel`, `date`, `time_start`, `time_end`, `status`, `date_created`, `date_updated`) VALUES
(1, 'ea39f2c611861723cab7a63a95f71368', 'testing123', 'testing123 live', 'utube live', '2021-12-25', '12:28', '15:28', 'Delete', '2021-12-06 04:31:18', '2021-12-06 04:31:18'),
(2, '3226c5438b31d87b8d564e97ac5e0c1d', 'testing123', 'livestream 123 edit', 'vtube edit', '2022-02-06', '16:30', '17:50', 'Delete', '2022-01-24 06:56:39', '2022-01-24 06:56:39');

-- --------------------------------------------------------

--
-- Table structure for table `ordering_list`
--

CREATE TABLE `ordering_list` (
  `id` bigint(20) NOT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `product_uid` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `main_product_uid` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `original_price` varchar(255) DEFAULT NULL,
  `final_price` varchar(255) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `totalPrice` varchar(255) DEFAULT NULL,
  `product_value` varchar(255) DEFAULT NULL,
  `redemption_point` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ordering_list`
--

INSERT INTO `ordering_list` (`id`, `user_uid`, `product_uid`, `product_name`, `main_product_uid`, `order_id`, `quantity`, `original_price`, `final_price`, `discount`, `totalPrice`, `product_value`, `redemption_point`, `status`, `date_created`, `date_updated`) VALUES
(1, 'b5dd5d41441804b08323a38685c348a5', '35117b1cb67c78efa860c4882c2e5745', 'Verju Foryn Deer Placenta (30s)', NULL, NULL, '10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-19 07:04:24', '2022-04-19 07:04:24'),
(2, 'd0a0661528e10beed710f467137e1b97', '35117b1cb67c78efa860c4882c2e5745', 'Verju Foryn Deer Placenta (30s)', NULL, NULL, '4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-19 07:04:24', '2022-04-19 07:04:24'),
(3, '3208f7e0641debe426e374a74ae3a607', '35117b1cb67c78efa860c4882c2e5745', 'Verju Foryn Deer Placenta (30s)', NULL, NULL, '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-19 07:04:24', '2022-04-19 07:04:24'),
(4, 'c3263bccfb131fa6a68b8f98f7320b31', '35117b1cb67c78efa860c4882c2e5745', 'Verju Foryn Deer Placenta (30s)', NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-19 07:04:24', '2022-04-19 07:04:24'),
(5, '1c7c566372eeac2360374f8ac00e80d3', '547297ffdb9f49d6e5ff1db5208f2530', 'Verju Foryn Deer Placenta (60s)', NULL, NULL, '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-19 07:04:27', '2022-04-19 07:04:27'),
(6, 'b5dd5d41441804b08323a38685c348a5', '547297ffdb9f49d6e5ff1db5208f2530', 'Verju Foryn Deer Placenta (60s)', NULL, NULL, '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-19 07:04:27', '2022-04-19 07:04:27'),
(7, '2033d3489f3699966be131b98e2ac71a', '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150s)', NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29'),
(8, 'be4dfa404b72988cbfa6b931fa2b25ab', '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150s)', NULL, NULL, '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29'),
(9, 'b5dd5d41441804b08323a38685c348a5', '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150s)', NULL, NULL, '15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29'),
(10, '0dbdc899bc89b530c7a17012d59b8644', '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150s)', NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29'),
(11, 'a68fbe04f13c6d9e906bcb0e422cd0a1', '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150s)', NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-19 07:04:29', '2022-04-19 07:04:29');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `address_one` varchar(2500) DEFAULT NULL,
  `address_two` varchar(2500) DEFAULT NULL,
  `address_three` varchar(2500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `subtotal` decimal(50,0) DEFAULT NULL,
  `total` decimal(50,0) DEFAULT NULL COMMENT 'include postage fees',
  `product_value` varchar(255) DEFAULT NULL,
  `redemption_point` varchar(255) DEFAULT NULL,
  `bonus_status` varchar(255) DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_amount` int(255) DEFAULT NULL,
  `payment_reference` varchar(255) DEFAULT NULL,
  `payment_status` varchar(200) DEFAULT NULL COMMENT 'pending, accepted/completed, rejected, NULL = nothing',
  `shipping_status` varchar(200) DEFAULT NULL COMMENT 'pending, shipped, refunded',
  `shipping_method` varchar(200) DEFAULT NULL,
  `shipping_date` date DEFAULT NULL,
  `tracking_number` varchar(200) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `receipt` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `uid`, `name`, `contact`, `email`, `address_one`, `address_two`, `address_three`, `city`, `postcode`, `state`, `country`, `subtotal`, `total`, `product_value`, `redemption_point`, `bonus_status`, `payment_method`, `payment_amount`, `payment_reference`, `payment_status`, `shipping_status`, `shipping_method`, `shipping_date`, `tracking_number`, `remark`, `receipt`, `date_created`, `date_updated`) VALUES
(1, 'b5dd5d41441804b08323a38685c348a51643192776', 'b5dd5d41441804b08323a38685c348a5', 'Kara', '01110391103', NULL, '3, DC Road 03', NULL, NULL, 'Central  Park', '1103', 'CPPD', 'USDC', '6000', NULL, '3000', '3000', 'Clear', 'BILLPLZ', NULL, '5oid1gpn', 'Approved', 'Pending', NULL, NULL, NULL, '04/2022', '', '2022-01-26 10:26:17', '2022-04-19 07:04:44'),
(2, '1c7c566372eeac2360374f8ac00e80d31643193205', '1c7c566372eeac2360374f8ac00e80d3', 'Oliver', '01110391101', NULL, '1, DC Road 01', NULL, NULL, 'Starling City', '1101', 'SCPD', 'USDC', '1350', NULL, '675', '675', 'Clear', 'BILLPLZ', NULL, 'r6lo0phr', 'Approved', 'Pending', NULL, NULL, NULL, '04/2022', '', '2022-01-26 10:33:47', '2022-04-19 07:04:44'),
(3, 'b5dd5d41441804b08323a38685c348a51644391778', 'b5dd5d41441804b08323a38685c348a5', 'Kara', '01110391103', NULL, '3, DC Road 03', NULL, NULL, 'Central  Park', '1103', 'CPPD', 'USDC', '1600', NULL, '800', '800', 'Clear', 'BILLPLZ', NULL, 'sh5tkzpw', 'Approved', 'Pending', NULL, NULL, NULL, '04/2022', '', '2022-02-09 07:29:59', '2022-04-19 07:04:44'),
(4, 'a68fbe04f13c6d9e906bcb0e422cd0a11644478288', 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'abc', '251', NULL, 'asdsd', NULL, NULL, 'abc', '1513', 'abc', 'acdw', '1000', NULL, '500', '500', 'Clear', 'BILLPLZ', NULL, 'ugtn117p', 'Approved', 'Pending', NULL, NULL, NULL, '04/2022', '', '2022-02-10 07:31:30', '2022-04-19 07:04:44'),
(5, 'a68fbe04f13c6d9e906bcb0e422cd0a11644487798', 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'abc', '251', NULL, 'asdsd', NULL, NULL, 'abc', '1513', 'abc', 'acdw', '1000', NULL, '500', '500', 'Clear', 'BILLPLZ', NULL, 'itpt7pmm', 'Approved', 'Pending', NULL, NULL, NULL, '04/2022', '', '2022-02-10 10:10:02', '2022-04-19 07:04:44'),
(6, 'e9140833d49ab185007f6b36e6c98ba71644653117', '0dbdc899bc89b530c7a17012d59b8644', '3', '3', NULL, '3', NULL, NULL, '3', '3', 's', '3', '2000', NULL, '1000', '1000', 'Clear', 'BILLPLZ', NULL, 'x0lajguk', 'Approved', 'Pending', NULL, NULL, NULL, '04/2022', '', '2022-02-12 08:05:19', '2022-04-19 07:04:44'),
(8, '6f844ad00ada79c1547f5f839d92c5eb', 'd0a0661528e10beed710f467137e1b97', 'Bklim0805', 'dasd', NULL, 'asd', NULL, NULL, 'df', 'sdf', 'sdf', 'sd', '4800', NULL, '2400', '2400', 'Clear', 'BILLPLZ', NULL, '2en9hs3k', 'Approved', 'Pending', NULL, NULL, NULL, '04/2022', '', '2022-02-15 04:47:34', '2022-04-19 07:04:44'),
(10, '1a4e79676f248f1d64e84dba27e4cf18', '2033d3489f3699966be131b98e2ac71a', 'Chee sk', '01110391103', NULL, '3, DC Road 03', NULL, NULL, 'Central  Park', '1103', 'CPPD', 'USDC', '2000', NULL, '1000', '1000', 'Clear', 'BILLPLZ', NULL, '0ehbcjoq', 'Approved', 'Pending', NULL, NULL, NULL, '04/2022', '', '2022-02-17 07:01:09', '2022-04-19 07:04:44'),
(11, 'a4f0be4e415f65008164d5478ea95628', 'be4dfa404b72988cbfa6b931fa2b25ab', 'Josie', '0124455772', NULL, '3, Jln Asd, Tmn Zxc', NULL, NULL, 'BLP3', '12377', 'PG', 'MAS', '3000', NULL, '1500', '1500', 'Clear', 'BILLPLZ', NULL, 'nfqkoprm', 'Approved', 'Pending', NULL, NULL, NULL, '04/2022', '', '2022-02-17 09:00:10', '2022-04-19 07:04:44'),
(17, 'c3263bccfb131fa6a68b8f98f7320b311649825531', 'c3263bccfb131fa6a68b8f98f7320b31', 'user two', '0124566523', NULL, '2, Jln Txs', NULL, NULL, 'Alor Star', '45645', 'KDH', 'Mas', '950', NULL, '475', '475', 'Clear', 'BILLPLZ', NULL, 'vniuq3si', 'Approved', 'Pending', NULL, NULL, NULL, '04/2022', '', '2022-04-13 04:54:14', '2022-04-19 07:04:44'),
(18, '5edbd69090fa7035e85fa4134eca0efa', 'b5dd5d41441804b08323a38685c348a5', 'User One', '0124455665', NULL, '101, Jln User One', NULL, NULL, 'BPL', '10110', 'PG', 'MAS', '1250', NULL, '625', '625', 'Clear', 'BILLPLZ', NULL, 'dodh4axu', 'Approved', 'Pending', NULL, NULL, NULL, '04/2022', '', '2022-04-18 06:27:37', '2022-04-19 07:04:44'),
(19, '6123f764a746f91920fd9810b8d3d689', '3208f7e0641debe426e374a74ae3a607', 'Mike 1.5', '0121515115', NULL, '15 Jln Mike', NULL, NULL, 'Tmn Mike15', '11515', 'KDH', 'MAS', '1750', NULL, '875', '875', 'Clear', 'BILLPLZ', NULL, 'g3p2l8za', 'Approved', 'Pending', NULL, NULL, NULL, '04/2022', '', '2022-04-18 08:16:18', '2022-04-19 07:04:44'),
(20, 'f7d8c90ea22b01b15b57d269aa68594b', 'b5dd5d41441804b08323a38685c348a5', 'User One', '0124455665', NULL, '101, Jln User One', NULL, NULL, 'BPL', '10110', 'PG', 'MAS', '3000', NULL, '1500', '1500', 'Clear', 'BILLPLZ', NULL, 'hjghs8hf', 'Approved', 'Pending', NULL, NULL, NULL, '04/2022', '', '2022-04-18 08:18:46', '2022-04-19 07:04:44');

-- --------------------------------------------------------

--
-- Table structure for table `order_list`
--

CREATE TABLE `order_list` (
  `id` bigint(20) NOT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `product_uid` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `main_product_uid` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `original_price` varchar(255) DEFAULT NULL,
  `final_price` varchar(255) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `totalPrice` varchar(255) DEFAULT NULL,
  `product_value` varchar(255) DEFAULT NULL,
  `redemption_point` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_list`
--

INSERT INTO `order_list` (`id`, `user_uid`, `product_uid`, `product_name`, `main_product_uid`, `order_id`, `quantity`, `original_price`, `final_price`, `discount`, `totalPrice`, `product_value`, `redemption_point`, `status`, `date_created`, `date_updated`) VALUES
(1, 'b5dd5d41441804b08323a38685c348a5', '35117b1cb67c78efa860c4882c2e5745', 'Verju Foryn Deer Placenta (30’s)', '35117b1cb67c78efa860c4882c2e5745', 'b5dd5d41441804b08323a38685c348a51643192776', '4', '250', NULL, NULL, '1000', '500', '500', 'Claim', '2022-01-26 10:26:16', '2022-04-19 07:04:24'),
(2, 'b5dd5d41441804b08323a38685c348a5', '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150’s)', '712e761968f09667885752f04d48c77f', 'b5dd5d41441804b08323a38685c348a51643192776', '5', '1000', NULL, NULL, '5000', '2500', '2500', 'Claim', '2022-01-26 10:26:16', '2022-04-19 07:04:24'),
(3, '1c7c566372eeac2360374f8ac00e80d3', '547297ffdb9f49d6e5ff1db5208f2530', 'Verju Foryn Deer Placenta (60’s)', '547297ffdb9f49d6e5ff1db5208f2530', '1c7c566372eeac2360374f8ac00e80d31643193205', '3', '450', NULL, NULL, '1350', '675', '675', 'Claim', '2022-01-26 10:33:25', '2022-04-19 07:04:27'),
(4, 'b5dd5d41441804b08323a38685c348a5', '35117b1cb67c78efa860c4882c2e5745', 'Verju Foryn Deer Placenta (30’s)', '35117b1cb67c78efa860c4882c2e5745', 'b5dd5d41441804b08323a38685c348a51644391778', '1', '250', NULL, NULL, '250', '125', '125', 'Claim', '2022-02-09 07:29:38', '2022-04-19 07:04:24'),
(5, 'b5dd5d41441804b08323a38685c348a5', '547297ffdb9f49d6e5ff1db5208f2530', 'Verju Foryn Deer Placenta (60’s)', '547297ffdb9f49d6e5ff1db5208f2530', 'b5dd5d41441804b08323a38685c348a51644391778', '3', '450', NULL, NULL, '1350', '675', '675', 'Claim', '2022-02-09 07:29:38', '2022-04-19 07:04:24'),
(6, 'a68fbe04f13c6d9e906bcb0e422cd0a1', '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150’s)', '712e761968f09667885752f04d48c77f', 'a68fbe04f13c6d9e906bcb0e422cd0a11644478288', '1', '1000', NULL, NULL, '1000', '500', '500', 'Claim', '2022-02-10 07:31:28', '2022-04-19 07:04:29'),
(7, 'a68fbe04f13c6d9e906bcb0e422cd0a1', '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150’s)', '712e761968f09667885752f04d48c77f', 'a68fbe04f13c6d9e906bcb0e422cd0a11644487798', '1', '1000', NULL, NULL, '1000', '500', '500', 'Claim', '2022-02-10 10:09:58', '2022-04-19 07:04:29'),
(8, '0dbdc899bc89b530c7a17012d59b8644', '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150’s)', '712e761968f09667885752f04d48c77f', 'e9140833d49ab185007f6b36e6c98ba71644653117', '2', '1000', NULL, NULL, '2000', '1000', '1000', 'Claim', '2022-02-12 08:05:17', '2022-04-19 07:04:29'),
(9, 'b5dd5d41441804b08323a38685c348a5', '35117b1cb67c78efa860c4882c2e5745', 'Verju Foryn Deer Placenta (30’s)', '35117b1cb67c78efa860c4882c2e5745', 'b5dd5d41441804b08323a38685c348a51644851155', '2', '250', NULL, NULL, '500', '250', '250', 'Pending', '2022-02-14 15:05:55', '2022-02-14 15:05:55'),
(10, 'b5dd5d41441804b08323a38685c348a5', '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150’s)', '712e761968f09667885752f04d48c77f', 'b5dd5d41441804b08323a38685c348a51644851155', '4', '1000', NULL, NULL, '4000', '2000', '2000', 'Pending', '2022-02-14 15:05:55', '2022-02-14 15:05:55'),
(11, 'b5dd5d41441804b08323a38685c348a5', '547297ffdb9f49d6e5ff1db5208f2530', 'Verju Foryn Deer Placenta (60’s)', '547297ffdb9f49d6e5ff1db5208f2530', 'b5dd5d41441804b08323a38685c348a51644851155', '3', '450', NULL, NULL, '1350', '675', '675', 'Pending', '2022-02-14 15:05:55', '2022-02-14 15:05:55'),
(12, 'b5dd5d41441804b08323a38685c348a5', '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150’s)', '712e761968f09667885752f04d48c77f', 'b5dd5d41441804b08323a38685c348a51644851155', '4', '1000', NULL, NULL, '4000', '2000', '2000', 'Pending', '2022-02-14 15:05:55', '2022-02-14 15:05:55'),
(13, 'b5dd5d41441804b08323a38685c348a5', '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150’s)', '712e761968f09667885752f04d48c77f', 'b5dd5d41441804b08323a38685c348a51644851155', '4', '1000', NULL, NULL, '4000', '2000', '2000', 'Pending', '2022-02-14 15:05:55', '2022-02-14 15:05:55'),
(14, 'b5dd5d41441804b08323a38685c348a5', '35117b1cb67c78efa860c4882c2e5745', 'Verju Foryn Deer Placenta (30’s)', '35117b1cb67c78efa860c4882c2e5745', 'b5dd5d41441804b08323a38685c348a51644851155', '2', '250', NULL, NULL, '500', '250', '250', 'Pending', '2022-02-14 15:05:55', '2022-02-14 15:05:55'),
(15, 'b5dd5d41441804b08323a38685c348a5', '547297ffdb9f49d6e5ff1db5208f2530', 'Verju Foryn Deer Placenta (60’s)', '547297ffdb9f49d6e5ff1db5208f2530', 'b5dd5d41441804b08323a38685c348a51644851155', '3', '450', NULL, NULL, '1350', '675', '675', 'Pending', '2022-02-14 15:05:55', '2022-02-14 15:05:55'),
(16, 'b5dd5d41441804b08323a38685c348a5', '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150’s)', '712e761968f09667885752f04d48c77f', 'b5dd5d41441804b08323a38685c348a51644851155', '4', '1000', NULL, NULL, '4000', '2000', '2000', 'Pending', '2022-02-14 15:05:55', '2022-02-14 15:05:55'),
(17, 'b5dd5d41441804b08323a38685c348a5', '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150’s)', '712e761968f09667885752f04d48c77f', 'b5dd5d41441804b08323a38685c348a51644851496', '4', '1000', NULL, NULL, '4000', '2000', '2000', 'Claim', '2022-02-14 15:11:36', '2022-04-19 07:04:29'),
(18, 'b5dd5d41441804b08323a38685c348a5', '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150’s)', '712e761968f09667885752f04d48c77f', 'b5dd5d41441804b08323a38685c348a51644851496', '4', '1000', NULL, NULL, '4000', '2000', '2000', 'Claim', '2022-02-14 15:11:36', '2022-04-19 07:04:29'),
(19, 'b5dd5d41441804b08323a38685c348a5', '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150’s)', '712e761968f09667885752f04d48c77f', 'b5dd5d41441804b08323a38685c348a51644851496', '4', '1000', NULL, NULL, '4000', '2000', '2000', 'Claim', '2022-02-14 15:11:36', '2022-04-19 07:04:29'),
(20, 'd0a0661528e10beed710f467137e1b97', '35117b1cb67c78efa860c4882c2e5745', 'Verju Foryn Deer Placenta (30’s)', '35117b1cb67c78efa860c4882c2e5745', '6f844ad00ada79c1547f5f839d92c5eb', '4', '250', NULL, NULL, '1000', '500', '500', 'Claim', '2022-02-15 04:47:34', '2022-04-19 07:04:24'),
(21, 'd0a0661528e10beed710f467137e1b97', '547297ffdb9f49d6e5ff1db5208f2530', 'Verju Foryn Deer Placenta (60’s)', '547297ffdb9f49d6e5ff1db5208f2530', '6f844ad00ada79c1547f5f839d92c5eb', '4', '450', NULL, NULL, '1800', '900', '900', 'Claim', '2022-02-15 04:47:35', '2022-04-19 07:04:24'),
(22, 'd0a0661528e10beed710f467137e1b97', '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150’s)', '712e761968f09667885752f04d48c77f', '6f844ad00ada79c1547f5f839d92c5eb', '2', '1000', NULL, NULL, '2000', '1000', '1000', 'Claim', '2022-02-15 04:47:35', '2022-04-19 07:04:24'),
(23, 'b5dd5d41441804b08323a38685c348a5', '547297ffdb9f49d6e5ff1db5208f2530', 'Verju Foryn Deer Placenta (60’s)', '547297ffdb9f49d6e5ff1db5208f2530', 'e478e1612e88658eed0971432662cf54', '3', '450', NULL, NULL, '1350', '675', '675', 'Claim', '2022-02-15 07:17:46', '2022-04-19 07:04:27'),
(24, 'b5dd5d41441804b08323a38685c348a5', '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150’s)', '712e761968f09667885752f04d48c77f', 'e478e1612e88658eed0971432662cf54', '7', '1000', NULL, NULL, '7000', '3500', '3500', 'Claim', '2022-02-15 07:17:46', '2022-04-19 07:04:27'),
(25, '2033d3489f3699966be131b98e2ac71a', '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150’s)', '712e761968f09667885752f04d48c77f', '1a4e79676f248f1d64e84dba27e4cf18', '2', '1000', NULL, NULL, '2000', '1000', '1000', 'Claim', '2022-02-17 07:01:09', '2022-04-19 07:04:29'),
(26, 'be4dfa404b72988cbfa6b931fa2b25ab', '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150’s)', '712e761968f09667885752f04d48c77f', 'a4f0be4e415f65008164d5478ea95628', '3', '1000', NULL, NULL, '3000', '1500', '1500', 'Claim', '2022-02-17 09:00:10', '2022-04-19 07:04:29'),
(38, 'c3263bccfb131fa6a68b8f98f7320b31', '35117b1cb67c78efa860c4882c2e5745', 'Verju Foryn Deer Placenta (30’s)', '35117b1cb67c78efa860c4882c2e5745', 'c3263bccfb131fa6a68b8f98f7320b311649825531', '2', '250', NULL, NULL, '500', '250', '250', 'Claim', '2022-04-13 04:52:11', '2022-04-19 07:04:24'),
(39, 'c3263bccfb131fa6a68b8f98f7320b31', '547297ffdb9f49d6e5ff1db5208f2530', 'Verju Foryn Deer Placenta (60’s)', '547297ffdb9f49d6e5ff1db5208f2530', 'c3263bccfb131fa6a68b8f98f7320b311649825531', '1', '450', NULL, NULL, '450', '225', '225', 'Claim', '2022-04-13 04:52:11', '2022-04-19 07:04:24'),
(40, 'b5dd5d41441804b08323a38685c348a5', '35117b1cb67c78efa860c4882c2e5745', 'Verju Foryn Deer Placenta (30’s)', '35117b1cb67c78efa860c4882c2e5745', '5edbd69090fa7035e85fa4134eca0efa', '5', '250', NULL, NULL, '1250', '625', '625', 'Claim', '2022-04-18 06:27:37', '2022-04-19 07:04:24'),
(41, '3208f7e0641debe426e374a74ae3a607', '35117b1cb67c78efa860c4882c2e5745', 'Verju Foryn Deer Placenta (30’s)', '35117b1cb67c78efa860c4882c2e5745', '6123f764a746f91920fd9810b8d3d689', '3', '250', NULL, NULL, '750', '375', '375', 'Claim', '2022-04-18 08:16:18', '2022-04-19 07:04:24'),
(42, '3208f7e0641debe426e374a74ae3a607', '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150’s)', '712e761968f09667885752f04d48c77f', '6123f764a746f91920fd9810b8d3d689', '1', '1000', NULL, NULL, '1000', '500', '500', 'Claim', '2022-04-18 08:16:18', '2022-04-19 07:04:24'),
(43, 'b5dd5d41441804b08323a38685c348a5', '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150’s)', '712e761968f09667885752f04d48c77f', 'f7d8c90ea22b01b15b57d269aa68594b', '3', '1000', NULL, NULL, '3000', '1500', '1500', 'Claim', '2022-04-18 08:18:46', '2022-04-19 07:04:29');

-- --------------------------------------------------------

--
-- Table structure for table `preorder_list`
--

CREATE TABLE `preorder_list` (
  `id` bigint(20) NOT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `product_uid` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `main_product_uid` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `original_price` varchar(255) DEFAULT NULL,
  `final_price` varchar(255) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `totalPrice` varchar(255) DEFAULT NULL,
  `product_value` varchar(255) DEFAULT NULL,
  `redemption_point` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `preorder_list`
--

INSERT INTO `preorder_list` (`id`, `user_uid`, `product_uid`, `product_name`, `main_product_uid`, `order_id`, `quantity`, `original_price`, `final_price`, `discount`, `totalPrice`, `product_value`, `redemption_point`, `status`, `date_created`, `date_updated`) VALUES
(1, 'c3263bccfb131fa6a68b8f98f7320b31', '35117b1cb67c78efa860c4882c2e5745', 'Verju Foryn Deer Placenta (30’s)', '35117b1cb67c78efa860c4882c2e5745', NULL, '2', '250', NULL, NULL, '500', '250', '250', 'Sold', '2022-04-13 04:52:01', '2022-04-13 04:52:11'),
(2, 'c3263bccfb131fa6a68b8f98f7320b31', '547297ffdb9f49d6e5ff1db5208f2530', 'Verju Foryn Deer Placenta (60’s)', '547297ffdb9f49d6e5ff1db5208f2530', NULL, '1', '450', NULL, NULL, '450', '225', '225', 'Sold', '2022-04-13 04:52:05', '2022-04-13 04:52:11');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `product_value` varchar(255) DEFAULT NULL,
  `redemption_point` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `description_two` text DEFAULT NULL,
  `keyword_one` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `uid`, `name`, `code`, `price`, `product_value`, `redemption_point`, `status`, `description`, `description_two`, `keyword_one`, `image_one`, `image_two`, `date_created`, `date_updated`) VALUES
(1, '35117b1cb67c78efa860c4882c2e5745', 'Verju Foryn Deer Placenta (30’s)', '10VFDP3001-01-01', '250', '125', '125', 'Available', 'Description for Verju Foryn Deer Placenta (30’s)', NULL, NULL, '30s.PNG', NULL, '2022-01-24 06:23:56', '2022-02-17 09:09:57'),
(2, '547297ffdb9f49d6e5ff1db5208f2530', 'Verju Foryn Deer Placenta (60’s)', '10VFDP3002-01-01', '450', '225', '225', 'Available', 'Description for Verju Foryn Deer Placenta (60’s)', NULL, NULL, '60s.PNG', NULL, '2022-01-24 06:47:09', '2022-02-17 09:10:09'),
(3, '712e761968f09667885752f04d48c77f', 'Verju Foryn Deer Placenta (150’s)', '10VFDP3005-01-01', '1000', '500', '500', 'Available', 'Description for Verju Foryn Deer Placenta (150’s)', NULL, NULL, '150s.PNG', NULL, '2022-01-24 08:19:32', '2022-02-17 09:10:13');

-- --------------------------------------------------------

--
-- Table structure for table `referral_history`
--

CREATE TABLE `referral_history` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `current_level` int(100) NOT NULL,
  `current_status` varchar(255) DEFAULT 'Member',
  `order_status` varchar(255) DEFAULT NULL,
  `top_referrer_id` varchar(100) NOT NULL COMMENT 'the topmost person''s uid',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `referral_history`
--

INSERT INTO `referral_history` (`id`, `referrer_id`, `referral_id`, `referral_name`, `current_level`, `current_status`, `order_status`, `top_referrer_id`, `date_created`, `date_updated`) VALUES
(1, 'c7b20dbc8b39d50703c15594073ce731', '70691b70ee884b1439e8a73ce33c0eef', 'company', 0, 'Manager', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2020-03-11 01:50:18', '2021-01-31 04:18:34'),
(2, '70691b70ee884b1439e8a73ce33c0eef', '1bb3266cd1c0bf3fa22c06e6a9eb892d', 'Joseph', 1, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2020-12-28 12:13:39', '2021-01-12 03:56:56'),
(3, '1bb3266cd1c0bf3fa22c06e6a9eb892d', 'ecbbf12d6997927fd668dca72dbf6092', 'junwei', 2, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2020-12-28 12:15:39', '2021-01-12 03:56:58'),
(4, '70691b70ee884b1439e8a73ce33c0eef', 'd0f951ec5bdbc848c52f846fb973d6da', 'YeohShaun', 1, 'Senior Manager', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2020-12-28 13:06:10', '2021-02-26 14:00:03'),
(5, '70691b70ee884b1439e8a73ce33c0eef', '267cd1469afa7e914a78672d7b3549b8', 'KentChiam', 1, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2020-12-28 13:12:44', '2021-01-12 03:57:01'),
(6, '70691b70ee884b1439e8a73ce33c0eef', '515d91b5b51ed0ff8c0a4c5deaa95c3c', 'NickNai', 1, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2020-12-28 13:15:28', '2021-01-12 03:57:02'),
(7, 'd0f951ec5bdbc848c52f846fb973d6da', 'a38875a5fd6dbd97f903822d0b3f64ff', 'PS Ng', 2, 'Manager', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2020-12-28 15:22:30', '2021-01-13 14:15:02'),
(8, 'd0f951ec5bdbc848c52f846fb973d6da', '21013dbe0b7a0308fb41984bce5f3984', 'andylimch', 2, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2020-12-29 06:25:20', '2020-12-29 06:25:20'),
(9, '21013dbe0b7a0308fb41984bce5f3984', '2033d3489f3699966be131b98e2ac71a', 'Chee sk', 3, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2020-12-29 06:49:02', '2020-12-29 06:49:02'),
(10, '1bb3266cd1c0bf3fa22c06e6a9eb892d', '0b5721d2ddb5d5a43fe76b1222865475', 'BennyTan', 2, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2020-12-30 11:23:30', '2020-12-30 11:23:30'),
(11, '1bb3266cd1c0bf3fa22c06e6a9eb892d', '9d78bb8f8b39590eafc90aa2368aaa6c', 'K3lly1007', 2, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2020-12-30 12:07:50', '2021-01-12 04:38:18'),
(12, '1bb3266cd1c0bf3fa22c06e6a9eb892d', 'f7c777efae811767f1fc52d2a8faf67c', 'Grextce', 2, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2020-12-30 12:16:04', '2020-12-30 12:16:04'),
(13, 'f7c777efae811767f1fc52d2a8faf67c', '2aff28e8bfa7e1979df21e34f80b925e', 'Tan Boon Wan', 3, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2020-12-30 12:19:50', '2020-12-30 12:19:50'),
(14, '2aff28e8bfa7e1979df21e34f80b925e', 'aba74f5b29521b37630114d5b6bfad7c', 'Gina Tan', 4, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2020-12-30 12:25:30', '2020-12-30 12:25:30'),
(15, '1bb3266cd1c0bf3fa22c06e6a9eb892d', 'be94e953ef428a1f7b91b4c5913508c8', 'Yvonne Lee', 2, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2020-12-30 12:34:49', '2020-12-30 12:34:49'),
(16, '2aff28e8bfa7e1979df21e34f80b925e', '0e5aab1c9b188c04fa613d65e2c73d94', 'JoanneChong', 4, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2020-12-30 12:36:45', '2020-12-30 12:36:45'),
(17, '2aff28e8bfa7e1979df21e34f80b925e', 'ae8432247d87c00e3ed8e7d95f3baed4', 'tommyteh01', 4, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2020-12-30 13:07:26', '2021-01-12 04:38:10'),
(18, 'a38875a5fd6dbd97f903822d0b3f64ff', 'c3ba72d09db3af72a6958db12a4b5663', 'perdana73', 3, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2020-12-31 04:46:17', '2020-12-31 04:46:17'),
(19, 'a38875a5fd6dbd97f903822d0b3f64ff', 'ba8592381362dadc1fc9b60257bf19bb', 'ireneyeoh', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2020-12-31 11:18:14', '2021-01-12 04:38:16'),
(20, 'ba8592381362dadc1fc9b60257bf19bb', '9a5a0f74bb189acb864e365fe57ce7ad', 'CGGoh', 4, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-01 03:28:52', '2021-01-01 03:28:52'),
(21, 'ba8592381362dadc1fc9b60257bf19bb', 'ec8505287e6a2b5abf048ade05506d32', 'stchoy', 4, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-01 05:05:55', '2021-01-01 05:05:55'),
(22, '1bb3266cd1c0bf3fa22c06e6a9eb892d', '168173637a544e85720b9fe54aa71b7b', 'hdk64042', 2, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-02 05:41:40', '2021-01-02 05:41:40'),
(23, '0b5721d2ddb5d5a43fe76b1222865475', 'c411c4c8c3a6607c019408bc9e122915', 'Seng Aun', 3, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-04 09:37:01', '2021-01-04 09:37:01'),
(24, '1bb3266cd1c0bf3fa22c06e6a9eb892d', '1c7c566372eeac2360374f8ac00e80d3', 'lim', 2, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-04 10:28:00', '2021-01-04 10:28:00'),
(25, 'a38875a5fd6dbd97f903822d0b3f64ff', 'bdf1f1cf7d52592de7d2f2e517a8eeb0', 'YCTAN', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-05 12:19:56', '2021-01-13 11:20:22'),
(26, 'd0f951ec5bdbc848c52f846fb973d6da', '8b375cf7a0c532458ce51e8a9935ba8b', 'Andy koay', 2, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-07 06:19:30', '2021-01-30 09:27:43'),
(27, 'ba8592381362dadc1fc9b60257bf19bb', '1d3533310d46d70c3802b8f1efc391dc', 'LH Chua', 4, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-07 08:59:50', '2021-01-12 04:38:27'),
(28, 'ecbbf12d6997927fd668dca72dbf6092', 'ff554b0ddaed52fdb196486b6875821b', 'shuennchi2020', 3, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-07 09:33:59', '2021-01-07 09:33:59'),
(29, 'ff554b0ddaed52fdb196486b6875821b', 'be4dfa404b72988cbfa6b931fa2b25ab', 'Josie', 4, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-07 09:52:38', '2021-01-07 09:52:38'),
(30, '9d78bb8f8b39590eafc90aa2368aaa6c', '9737ef340ac451a52b391fcd28955bf3', 'Louis', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-07 11:39:30', '2021-01-12 04:38:20'),
(31, '9d78bb8f8b39590eafc90aa2368aaa6c', '85a7e481a2c0b0f322716319333f5630', 'Jacob tan', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-08 02:36:14', '2021-01-12 04:38:22'),
(32, '1bb3266cd1c0bf3fa22c06e6a9eb892d', '22b1d1026edc5ff2437149f8b0bfe860', 'chiam', 2, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-08 04:27:35', '2021-01-08 04:27:35'),
(33, 'ba8592381362dadc1fc9b60257bf19bb', 'b4d548a8b9bf1ccc3aa0c72b4e857e78', 'iyen1712', 4, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-08 06:40:17', '2021-01-12 04:38:25'),
(34, '1d3533310d46d70c3802b8f1efc391dc', 'b41fca733e4f10f28827335c631b0515', 'MLNg', 5, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-08 08:34:16', '2021-01-12 04:38:29'),
(35, 'b41fca733e4f10f28827335c631b0515', 'd357f663ae35b716359741bb217e7157', 'ZKChua', 6, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-08 09:28:57', '2021-01-12 04:38:31'),
(36, 'd357f663ae35b716359741bb217e7157', '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', 7, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-08 09:38:15', '2021-01-12 04:38:33'),
(37, '8d115e44db8169a60fd8e70a46bc95b0', 'b5dd5d41441804b08323a38685c348a5', 'YJChua', 8, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-08 10:09:17', '2021-01-12 04:38:35'),
(38, 'a38875a5fd6dbd97f903822d0b3f64ff', '62e7d178f94e7df56a5b3a5b02c5dbb0', 'Kim Ng', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-08 10:20:23', '2021-01-12 04:38:44'),
(39, 'a38875a5fd6dbd97f903822d0b3f64ff', 'fffbfd13c8805f195b050d107acfd7ed', 'Woon', 3, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-08 12:08:31', '2021-01-08 12:08:31'),
(40, '62e7d178f94e7df56a5b3a5b02c5dbb0', 'a8838737e4b0bc22f5fa988791069bf0', 'YeeYap', 4, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-09 12:31:45', '2021-01-12 13:58:03'),
(41, '9d78bb8f8b39590eafc90aa2368aaa6c', 'b419e184888fd00ca338db792cd24f8d', 'Moon1977', 3, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-10 12:22:09', '2021-01-10 12:22:09'),
(42, 'a38875a5fd6dbd97f903822d0b3f64ff', '7f0587559bc62b00a4cdac2059fd5e76', 'Raymond tan', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-10 12:29:26', '2021-01-12 13:58:13'),
(43, 'b419e184888fd00ca338db792cd24f8d', '494cdc6a441611ddc100cf0cff5f2a52', 'Dan681225', 4, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-10 12:32:42', '2021-01-10 12:32:42'),
(44, '85a7e481a2c0b0f322716319333f5630', '13516b89e887e7a2ff998679f65bf5a6', 'Voodokit', 4, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-10 15:49:23', '2021-01-12 16:23:22'),
(45, 'd0f951ec5bdbc848c52f846fb973d6da', 'e62e502c7699fadd63611bef06ff9605', 'AnnieLim', 2, 'Manager', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-11 09:36:32', '2021-02-10 14:15:02'),
(46, '267cd1469afa7e914a78672d7b3549b8', '2556ee6809b71eb935c07e9520b68005', 'Yoyokok', 2, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-11 09:50:42', '2021-01-11 09:50:42'),
(47, '515d91b5b51ed0ff8c0a4c5deaa95c3c', '0dbdc899bc89b530c7a17012d59b8644', 'mimiung', 2, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-11 10:08:48', '2021-01-11 10:08:48'),
(48, '2556ee6809b71eb935c07e9520b68005', '2f480ac7fbbdc9e3d455185992d0ee55', 'AnnLim', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-11 15:04:31', '2021-01-12 16:33:51'),
(49, 'ff554b0ddaed52fdb196486b6875821b', '305db2cdccd8b9f1703777ef9f6d64e6', 'Feiweng', 4, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-12 03:46:13', '2021-01-12 03:46:13'),
(50, '70691b70ee884b1439e8a73ce33c0eef', 'b629870d94417ff17a8d0086a932d70c', 'Reserve1', 1, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-12 04:18:11', '2021-01-12 04:18:38'),
(51, '70691b70ee884b1439e8a73ce33c0eef', '0de594c4216ddc7bd4eb6602849e19f5', 'Reserve2', 1, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-12 04:18:23', '2021-01-12 04:18:40'),
(52, '70691b70ee884b1439e8a73ce33c0eef', 'b63fad38f8ee572b4598ef10f12c4e41', 'Reserve3', 1, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-12 04:18:25', '2021-01-12 04:18:42'),
(53, 'ff554b0ddaed52fdb196486b6875821b', '31d34933763feed3a083315fa7de2ad2', 'albertgan', 4, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-12 09:39:26', '2021-01-12 09:39:26'),
(54, '2aff28e8bfa7e1979df21e34f80b925e', 'cd972b2f76411c015ef200363b26a264', 'Jordan koay', 4, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-12 10:21:25', '2021-01-12 10:21:25'),
(55, 'a38875a5fd6dbd97f903822d0b3f64ff', 'e4c8294350f46de003f8fda3d4464f85', 'penglong5', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-12 13:08:16', '2021-01-12 13:57:21'),
(56, '13516b89e887e7a2ff998679f65bf5a6', '6825c96bc70c2f3ef08ee01168a94b09', 'Collyn323', 5, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-12 17:05:23', '2021-01-17 11:54:48'),
(57, 'ff554b0ddaed52fdb196486b6875821b', '380a20d3a04a5f2aae722b06365d4103', 'liangkhenglo', 4, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-12 19:45:12', '2021-01-12 19:45:12'),
(58, '13516b89e887e7a2ff998679f65bf5a6', 'da95755224aa6650687b49e22d77bcce', 'Ronnie1113', 5, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-13 02:42:15', '2021-01-13 05:01:15'),
(59, '85a7e481a2c0b0f322716319333f5630', '04e1b4c1ffc641bd6eedb4d3cbd54bb8', 'Reenako', 4, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-13 02:43:51', '2021-01-15 10:48:48'),
(60, 'a38875a5fd6dbd97f903822d0b3f64ff', '1bdcbeffc71fc82ba8266a4c6d883be9', 'YH Ng', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-13 09:59:32', '2021-01-13 10:20:30'),
(61, 'a38875a5fd6dbd97f903822d0b3f64ff', '3ea9484b8c6cbf0db0f6c53babde3af7', 'NicoleYang', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-13 13:48:24', '2021-01-13 14:10:03'),
(62, 'ba8592381362dadc1fc9b60257bf19bb', 'f424ef5cb82a798f65826c21932c3daf', 'skleong', 4, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-14 13:06:17', '2021-01-14 13:06:17'),
(63, 'd0f951ec5bdbc848c52f846fb973d6da', '603daaa6ce6a094a618f01565a7a83be', 'Esther', 2, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-14 14:48:14', '2021-01-15 10:47:55'),
(64, '2556ee6809b71eb935c07e9520b68005', '8da8b4fe873a1dcc57090752775bc846', 'sinkee', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-15 10:13:03', '2021-01-15 10:48:36'),
(65, '267cd1469afa7e914a78672d7b3549b8', '0fa06fb4318ce5e01960080c008f6bef', 'shiuan', 2, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-15 11:30:54', '2021-01-15 11:46:16'),
(66, 'd0f951ec5bdbc848c52f846fb973d6da', '7653ff946b8d868479a660f8424a3368', 'Vostron', 2, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-16 03:41:43', '2021-01-20 14:56:45'),
(67, 'd0f951ec5bdbc848c52f846fb973d6da', 'ce3e65bdf1c5747cf04be30f30c37281', 'Fennielim', 2, 'Manager', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-16 04:05:23', '2021-02-26 13:55:01'),
(68, '267cd1469afa7e914a78672d7b3549b8', 'b50d9395e8a9606bf53a12eb11494308', 'ho shu', 2, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-16 06:48:38', '2021-01-16 06:48:38'),
(69, '267cd1469afa7e914a78672d7b3549b8', 'b9dd1080282915d3256dbda4b4b93960', 'weitsong', 2, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-16 12:44:48', '2021-01-16 13:11:02'),
(70, 'd0f951ec5bdbc848c52f846fb973d6da', '6d8d575ee828b6ce80d791633d84a772', 'VinceC', 2, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-17 03:12:20', '2021-01-23 09:12:54'),
(71, '515d91b5b51ed0ff8c0a4c5deaa95c3c', 'fce7a3684792f5beda7cd1d438d6997f', 'avelyntoh', 2, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-17 05:23:04', '2021-01-17 05:23:04'),
(72, 'd0f951ec5bdbc848c52f846fb973d6da', '16c61d5aae8d110ae3b6492b1bb9270a', 'Parames41', 2, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-17 14:14:48', '2021-01-17 14:14:48'),
(73, 'a38875a5fd6dbd97f903822d0b3f64ff', 'a6e56a53f9352e83354674b5ac15844f', 'Unix', 3, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-18 05:39:44', '2021-01-18 05:39:44'),
(74, 'd0f951ec5bdbc848c52f846fb973d6da', 'adac02200cb152176a295d0aa598ca7d', 'jackyeesj', 2, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-19 13:46:50', '2021-01-19 13:46:50'),
(75, 'd0f951ec5bdbc848c52f846fb973d6da', '216537ee4919953baf68cba1e4b4eb99', 'Venice Lim', 2, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-24 08:16:56', '2021-01-25 15:25:12'),
(76, 'e62e502c7699fadd63611bef06ff9605', 'bb241886cda8524cbb388c75b50682a2', 'tehalan88', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-25 04:43:02', '2021-01-25 15:46:34'),
(77, '267cd1469afa7e914a78672d7b3549b8', 'e061c363c9824601c8b7df04ca426f4d', 'Siow Yen', 2, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-27 09:03:54', '2021-01-27 13:59:30'),
(78, 'd0f951ec5bdbc848c52f846fb973d6da', 'f7327ffac161db12503a28dde3b9fb0a', 'PHMOK2021', 2, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-27 12:12:46', '2021-01-27 15:01:32'),
(79, 'e62e502c7699fadd63611bef06ff9605', 'ef571161fb416ee8d1b79a873931b60c', 'weikang22', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-28 02:50:12', '2021-01-30 09:27:13'),
(80, 'e62e502c7699fadd63611bef06ff9605', 'dc305083f182ccfff4f972738e931cd8', 'heng664461', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-01-30 02:02:34', '2021-02-01 02:38:20'),
(82, '6f687ff16c5606ddae9cd8616435a401', 'c7b20dbc8b39d50703c15594073ce731', 'Reserve06', 0, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-31 04:16:59', '2021-01-31 04:18:26'),
(83, 'f79e2c7602cf0102e57dfdfcfcac0f00', '6f687ff16c5606ddae9cd8616435a401', 'Reserve05', 0, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-31 04:17:04', '2021-01-31 04:18:21'),
(84, '07b610a86ed7dc1a0cf9f484057066b4', 'f79e2c7602cf0102e57dfdfcfcac0f00', 'Reserve04', 0, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-31 04:17:09', '2021-01-31 04:18:15'),
(85, '1fc0abce45843733bd5aa6ca6886ae7f', '07b610a86ed7dc1a0cf9f484057066b4', 'Reserve03', 0, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-31 04:17:15', '2021-01-31 04:18:10'),
(86, 'f37f95eef40574604133df0fb13d45d3', '1fc0abce45843733bd5aa6ca6886ae7f', 'Reserve02', 0, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-31 04:17:20', '2021-01-31 04:18:03'),
(87, '70691b70ee884b1439e8a73ce33c0eef', 'f37f95eef40574604133df0fb13d45d3', 'Reserve01', 0, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-01-31 04:17:25', '2022-02-10 06:16:19'),
(88, 'e061c363c9824601c8b7df04ca426f4d', 'c230fcd66070a675120a862b20bf5ba3', 'khoo wei boon', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-02-01 01:53:44', '2021-02-01 01:59:58'),
(89, 'dc305083f182ccfff4f972738e931cd8', 'ffc972deb0da39f8c212c03545cc630e', 'khiang928999', 4, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-02-01 02:14:43', '2021-02-01 13:44:41'),
(90, 'e62e502c7699fadd63611bef06ff9605', 'f5effae34bca678714b70c3b3f242dcc', 'Sealwan', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-02-01 07:32:20', '2021-02-01 13:45:09'),
(91, 'ce3e65bdf1c5747cf04be30f30c37281', '551a9c6a0e9024722081bf498aeb4139', 'Kentng', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-02-03 12:41:56', '2021-02-03 15:28:17'),
(92, 'dc305083f182ccfff4f972738e931cd8', '800c92a46d2a5644e73b15c3433abc11', 'AppleHeng', 4, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-02-04 13:56:21', '2021-02-04 14:23:28'),
(93, '515d91b5b51ed0ff8c0a4c5deaa95c3c', '139a6ccf8cf334db4bbd7fd091c2342c', 'FishYee95', 2, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-02-05 15:06:00', '2021-02-07 11:31:23'),
(94, 'e62e502c7699fadd63611bef06ff9605', '55fc16ed102e2e4e39cc0955a94358c1', 'Shaun2007', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-02-07 09:50:43', '2021-02-07 10:17:01'),
(95, 'e62e502c7699fadd63611bef06ff9605', 'c50f508a32116d5c5886aa011c9041ea', 'SLChoo', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-02-08 15:32:15', '2021-02-10 14:12:04'),
(96, 'e62e502c7699fadd63611bef06ff9605', '455e98a9858a6ca9ce5487793ea3b727', 'Chia Yi', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-02-10 11:42:55', '2021-02-10 14:12:07'),
(97, 'ce3e65bdf1c5747cf04be30f30c37281', 'd78fb060a5cb21715f260615ee6602f5', 'Frankyng', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-02-15 13:41:26', '2021-02-15 15:05:00'),
(98, 'ce3e65bdf1c5747cf04be30f30c37281', 'da48c3f17ad440f9b9eeba22188a4033', 'NgohSL', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-02-15 13:50:44', '2021-02-26 13:32:02'),
(99, 'ce3e65bdf1c5747cf04be30f30c37281', '1f0dcba6737c4f4268924c8f307a8060', 'Samng', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-02-15 13:57:42', '2021-02-15 15:05:04'),
(100, '1bb3266cd1c0bf3fa22c06e6a9eb892d', 'a8e5dc692e5885b09153c52f4cc5fea3', 'Samanta', 2, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-02-16 08:57:58', '2021-02-21 14:33:21'),
(101, 'a8e5dc692e5885b09153c52f4cc5fea3', 'ff2dd6f80318e02f79fc648780b542d1', 'Serlynnliew86', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-02-16 09:30:36', '2021-02-21 14:33:17'),
(102, 'ce3e65bdf1c5747cf04be30f30c37281', '7dacd6959cc2d2c9773d87103da5f93a', 'Eunice', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-02-21 02:09:53', '2021-02-21 14:33:24'),
(103, 'ce3e65bdf1c5747cf04be30f30c37281', 'f1bd6ee1c506b7d5e3729d94afcacd4b', 'Kia', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-02-21 02:18:21', '2021-02-21 14:05:53'),
(104, '455e98a9858a6ca9ce5487793ea3b727', 'f37c1aacc6dcadebd51ce04293810fa7', 'Teh Minnie', 4, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-02-21 12:51:59', '2021-02-21 14:06:55'),
(105, 'e62e502c7699fadd63611bef06ff9605', '534ee6bd02ea0f03a35994863ff4248a', 'Ivy', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-02-21 15:05:02', '2021-02-25 14:55:36'),
(106, '534ee6bd02ea0f03a35994863ff4248a', 'bfc365bfa23a58ed721fe4aca704e922', 'cheesc', 4, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-02-23 05:42:19', '2021-02-25 14:55:51'),
(107, '9d78bb8f8b39590eafc90aa2368aaa6c', '2d934a03c162b5e06559e1c723c58d2e', 'Shangqian1120', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-02-23 10:57:58', '2021-02-23 15:15:47'),
(108, '2d934a03c162b5e06559e1c723c58d2e', '90db509479cafe32422779b0a81d0064', 'Enggim1030', 4, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-02-23 11:15:48', '2021-02-28 15:46:45'),
(109, '534ee6bd02ea0f03a35994863ff4248a', 'ed9174a5cfb0fc8325f4e5507036e0b7', 'Limph', 4, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-02-23 13:59:34', '2021-02-25 14:55:45'),
(110, 'ce3e65bdf1c5747cf04be30f30c37281', 'abf3698d51034b6f24cae9f2fcfabb08', 'NgohSF', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-02-26 12:01:02', '2021-02-26 13:51:59'),
(111, '55fc16ed102e2e4e39cc0955a94358c1', '08412aab45cf5ee7fa7e0da25ce46ab8', 'SFNGOH', 4, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-03-03 15:04:18', '2021-03-03 15:31:04'),
(112, 'b9dd1080282915d3256dbda4b4b93960', 'd0a0661528e10beed710f467137e1b97', 'Bklim0805', 3, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-03-05 06:44:44', '2021-03-10 13:57:01'),
(113, '515d91b5b51ed0ff8c0a4c5deaa95c3c', '5c7309824e28d6b883c727d4b7c92be3', 'KL NAI', 2, 'Member', 'YES', '70691b70ee884b1439e8a73ce33c0eef', '2021-03-10 13:48:55', '2021-03-10 13:57:34'),
(114, '534ee6bd02ea0f03a35994863ff4248a', '74294b0c9e8c56e33888cef954af948c', 'Leekh', 4, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-03-16 09:46:52', '2021-03-16 09:46:52'),
(115, '9d78bb8f8b39590eafc90aa2368aaa6c', '901ab8d882636fc5f04e086d767f62bc', 'Eena0405', 3, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-03-19 16:53:12', '2021-03-19 16:53:12'),
(116, 'ecbbf12d6997927fd668dca72dbf6092', 'f4e123e3b81f131140758f48a894d51e', 'junwei2', 3, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2021-03-22 06:09:05', '2021-03-22 06:09:05'),
(117, 'f37f95eef40574604133df0fb13d45d3', 'd884d3b3a1987061fcf4e1b80c0a9241', 'tester', 1, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2022-02-08 04:09:04', '2022-02-08 04:09:04'),
(118, 'b5dd5d41441804b08323a38685c348a5', 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', 9, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2022-02-10 05:45:09', '2022-02-10 05:45:09'),
(119, 'b5dd5d41441804b08323a38685c348a5', '3208f7e0641debe426e374a74ae3a607', 'mike1.5', 9, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2022-02-10 06:57:11', '2022-02-10 06:57:11'),
(120, 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'e9140833d49ab185007f6b36e6c98ba7', 'mike2', 10, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2022-02-10 06:57:57', '2022-02-10 06:57:57'),
(121, 'e9140833d49ab185007f6b36e6c98ba7', 'a7e3cdba2b585f5e4fb622599bb325eb', 'mike3', 11, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2022-02-10 06:58:28', '2022-02-10 06:58:28'),
(122, '3208f7e0641debe426e374a74ae3a607', '99a995b1f70685db86d72a4be10559e9', 'mike2.5', 10, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2022-02-10 06:59:28', '2022-02-10 06:59:28'),
(123, '99a995b1f70685db86d72a4be10559e9', 'f852301f0d6f70cbbe689d298c9a0d29', 'mike3.5', 11, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2022-02-10 06:59:47', '2022-02-10 06:59:47'),
(124, 'a7e3cdba2b585f5e4fb622599bb325eb', 'a59fb29f14526bccb6f723e00138b41a', 'mike4', 12, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2022-02-13 16:38:27', '2022-02-13 16:38:27'),
(125, 'b5dd5d41441804b08323a38685c348a5', 'c3263bccfb131fa6a68b8f98f7320b31', 'user two', 9, 'Member', NULL, '70691b70ee884b1439e8a73ce33c0eef', '2022-04-13 04:58:56', '2022-04-13 04:58:56');

-- --------------------------------------------------------

--
-- Table structure for table `sales_commission`
--

CREATE TABLE `sales_commission` (
  `id` int(11) NOT NULL,
  `order_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `receiver_uid` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `rank` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sales_commission`
--

INSERT INTO `sales_commission` (`id`, `order_uid`, `uid`, `username`, `receiver_uid`, `level`, `amount`, `rank`, `status`, `date_created`, `date_updated`) VALUES
(1, 'b5dd5d41441804b08323a38685c348a51643192776', '24cbabaa3fdfe77f43bf847c7270ebb9', 'ZKChua', 'd357f663ae35b716359741bb217e7157', '6', '900', 'Agent', NULL, '2022-04-19 07:03:56', '2022-04-19 07:03:56'),
(2, 'a68fbe04f13c6d9e906bcb0e422cd0a11644478288', '0a62135ab801668c6dedf74e77ff1666', 'ZKChua', 'd357f663ae35b716359741bb217e7157', '6', '150', 'Agent', NULL, '2022-04-19 07:03:59', '2022-04-19 07:03:59'),
(3, 'c3263bccfb131fa6a68b8f98f7320b311649825531', '03492b779325e14503379d0ddf609e3f', 'ZKChua', 'd357f663ae35b716359741bb217e7157', '6', '142.5', 'Agent', NULL, '2022-04-19 07:04:03', '2022-04-19 07:04:03'),
(4, '6123f764a746f91920fd9810b8d3d689', '172c03f21e06bedcd03596ac9a2ccd92', 'ZKChua', 'd357f663ae35b716359741bb217e7157', '6', '262.5', 'Agent', NULL, '2022-04-19 07:04:06', '2022-04-19 07:04:06'),
(5, '5edbd69090fa7035e85fa4134eca0efa', '9261d04a0a3f40c6f2a8c2279d443a06', 'ZKChua', 'd357f663ae35b716359741bb217e7157', '6', '187.5', 'Agent', NULL, '2022-04-19 07:04:08', '2022-04-19 07:04:08'),
(6, 'a68fbe04f13c6d9e906bcb0e422cd0a11644487798', '7c3195b1f2474c20be9aac0cae3f5749', 'ZKChua', 'd357f663ae35b716359741bb217e7157', '6', '150', 'Agent', NULL, '2022-04-19 07:04:10', '2022-04-19 07:04:10'),
(7, '1a4e79676f248f1d64e84dba27e4cf18', '2596f0749fe9b352317362a9e36e7a29', 'YeohShaun', 'd0f951ec5bdbc848c52f846fb973d6da', '1', '300', 'Agent', NULL, '2022-04-19 07:04:11', '2022-04-19 07:04:11'),
(8, 'f7d8c90ea22b01b15b57d269aa68594b', 'c5856f6a2466ba82be9af8f3cd6bf252', 'ZKChua', 'd357f663ae35b716359741bb217e7157', '6', '450', 'Agent', NULL, '2022-04-19 07:04:12', '2022-04-19 07:04:12'),
(9, 'b5dd5d41441804b08323a38685c348a51644391778', 'b23111b8aba41417ce3874424426fc9f', 'ZKChua', 'd357f663ae35b716359741bb217e7157', '6', '240', 'Agent', NULL, '2022-04-19 07:04:20', '2022-04-19 07:04:20');

-- --------------------------------------------------------

--
-- Table structure for table `sales_commission_backup`
--

CREATE TABLE `sales_commission_backup` (
  `id` int(11) NOT NULL,
  `order_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `receiver_uid` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `rank` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sales_commission_backup`
--

INSERT INTO `sales_commission_backup` (`id`, `order_uid`, `uid`, `username`, `receiver_uid`, `level`, `amount`, `rank`, `status`, `date_created`, `date_updated`) VALUES
(1, 'b5dd5d41441804b08323a38685c348a51643192776', '24cbabaa3fdfe77f43bf847c7270ebb9', 'ZKChua', 'd357f663ae35b716359741bb217e7157', '6', '900', 'Agent', NULL, '2022-04-19 07:03:56', '2022-04-19 07:03:56'),
(2, 'b5dd5d41441804b08323a38685c348a51643192776', '05d5d82938ca5e8556c5997d5be62cb8', 'LH Chua', '1d3533310d46d70c3802b8f1efc391dc', '4', '900', 'Agent', NULL, '2022-04-19 07:03:56', '2022-04-19 07:03:56'),
(3, 'b5dd5d41441804b08323a38685c348a51643192776', '10afb0ece2b72efc4654d5003f180b38', 'PS Ng', 'a38875a5fd6dbd97f903822d0b3f64ff', '2', '900', 'Agent', NULL, '2022-04-19 07:03:56', '2022-04-19 07:03:56'),
(4, 'b5dd5d41441804b08323a38685c348a51643192776', 'e8abbd64ea4f2978a4cc5060227925f5', 'YeohShaun', 'd0f951ec5bdbc848c52f846fb973d6da', '1', '900', 'Agent', NULL, '2022-04-19 07:03:56', '2022-04-19 07:03:56'),
(5, 'a68fbe04f13c6d9e906bcb0e422cd0a11644478288', '0a62135ab801668c6dedf74e77ff1666', 'ZKChua', 'd357f663ae35b716359741bb217e7157', '6', '150', 'Agent', NULL, '2022-04-19 07:03:59', '2022-04-19 07:03:59'),
(6, 'a68fbe04f13c6d9e906bcb0e422cd0a11644478288', '59378d505d54af0a27c6e15ce83c7aea', 'LH Chua', '1d3533310d46d70c3802b8f1efc391dc', '4', '150', 'Agent', NULL, '2022-04-19 07:03:59', '2022-04-19 07:03:59'),
(7, 'a68fbe04f13c6d9e906bcb0e422cd0a11644478288', '424c18d15d652569f0e524337c924ffb', 'PS Ng', 'a38875a5fd6dbd97f903822d0b3f64ff', '2', '150', 'Agent', NULL, '2022-04-19 07:03:59', '2022-04-19 07:03:59'),
(8, 'a68fbe04f13c6d9e906bcb0e422cd0a11644478288', '9955a189418c99fd0315cfe736440168', 'YeohShaun', 'd0f951ec5bdbc848c52f846fb973d6da', '1', '150', 'Agent', NULL, '2022-04-19 07:03:59', '2022-04-19 07:03:59'),
(9, 'c3263bccfb131fa6a68b8f98f7320b311649825531', '03492b779325e14503379d0ddf609e3f', 'ZKChua', 'd357f663ae35b716359741bb217e7157', '6', '142.5', 'Agent', NULL, '2022-04-19 07:04:03', '2022-04-19 07:04:03'),
(10, 'c3263bccfb131fa6a68b8f98f7320b311649825531', 'a84056d480a75893e38b6ed0f88074cf', 'LH Chua', '1d3533310d46d70c3802b8f1efc391dc', '4', '142.5', 'Agent', NULL, '2022-04-19 07:04:03', '2022-04-19 07:04:03'),
(11, 'c3263bccfb131fa6a68b8f98f7320b311649825531', 'ed2f315b48d4cc23afe6c343d208fbbb', 'PS Ng', 'a38875a5fd6dbd97f903822d0b3f64ff', '2', '142.5', 'Agent', NULL, '2022-04-19 07:04:03', '2022-04-19 07:04:03'),
(12, 'c3263bccfb131fa6a68b8f98f7320b311649825531', 'af5b409516cc2c617222ed8ee1fa18c6', 'YeohShaun', 'd0f951ec5bdbc848c52f846fb973d6da', '1', '142.5', 'Agent', NULL, '2022-04-19 07:04:03', '2022-04-19 07:04:03'),
(13, '6123f764a746f91920fd9810b8d3d689', '172c03f21e06bedcd03596ac9a2ccd92', 'ZKChua', 'd357f663ae35b716359741bb217e7157', '6', '262.5', 'Agent', NULL, '2022-04-19 07:04:06', '2022-04-19 07:04:06'),
(14, '6123f764a746f91920fd9810b8d3d689', 'f7e3162ea608db9c361bddb5568a260d', 'LH Chua', '1d3533310d46d70c3802b8f1efc391dc', '4', '262.5', 'Agent', NULL, '2022-04-19 07:04:06', '2022-04-19 07:04:06'),
(15, '6123f764a746f91920fd9810b8d3d689', '4394d6ffd21b672c5684caa135b7cba6', 'PS Ng', 'a38875a5fd6dbd97f903822d0b3f64ff', '2', '262.5', 'Agent', NULL, '2022-04-19 07:04:06', '2022-04-19 07:04:06'),
(16, '6123f764a746f91920fd9810b8d3d689', 'd4c21e0a681d77190b686cdd7e33535b', 'YeohShaun', 'd0f951ec5bdbc848c52f846fb973d6da', '1', '262.5', 'Agent', NULL, '2022-04-19 07:04:06', '2022-04-19 07:04:06'),
(17, '5edbd69090fa7035e85fa4134eca0efa', '9261d04a0a3f40c6f2a8c2279d443a06', 'ZKChua', 'd357f663ae35b716359741bb217e7157', '6', '187.5', 'Agent', NULL, '2022-04-19 07:04:08', '2022-04-19 07:04:08'),
(18, '5edbd69090fa7035e85fa4134eca0efa', 'f73e6f3252a85d1daabd937f597479b7', 'LH Chua', '1d3533310d46d70c3802b8f1efc391dc', '4', '187.5', 'Agent', NULL, '2022-04-19 07:04:08', '2022-04-19 07:04:08'),
(19, '5edbd69090fa7035e85fa4134eca0efa', 'c36b3b98ff28240f2e867d9664577975', 'PS Ng', 'a38875a5fd6dbd97f903822d0b3f64ff', '2', '187.5', 'Agent', NULL, '2022-04-19 07:04:08', '2022-04-19 07:04:08'),
(20, '5edbd69090fa7035e85fa4134eca0efa', '7237af5930e5e2e0df70e602750f76f9', 'YeohShaun', 'd0f951ec5bdbc848c52f846fb973d6da', '1', '187.5', 'Agent', NULL, '2022-04-19 07:04:08', '2022-04-19 07:04:08'),
(21, 'a68fbe04f13c6d9e906bcb0e422cd0a11644487798', '7c3195b1f2474c20be9aac0cae3f5749', 'ZKChua', 'd357f663ae35b716359741bb217e7157', '6', '150', 'Agent', NULL, '2022-04-19 07:04:10', '2022-04-19 07:04:10'),
(22, 'a68fbe04f13c6d9e906bcb0e422cd0a11644487798', '7188272c49833291068b6578b27172f6', 'LH Chua', '1d3533310d46d70c3802b8f1efc391dc', '4', '150', 'Agent', NULL, '2022-04-19 07:04:10', '2022-04-19 07:04:10'),
(23, 'a68fbe04f13c6d9e906bcb0e422cd0a11644487798', '803468ccf28c60510466607e2887cc7d', 'PS Ng', 'a38875a5fd6dbd97f903822d0b3f64ff', '2', '150', 'Agent', NULL, '2022-04-19 07:04:10', '2022-04-19 07:04:10'),
(24, 'a68fbe04f13c6d9e906bcb0e422cd0a11644487798', '3dd1eb1aec6ed8803aca43c94588cf4e', 'YeohShaun', 'd0f951ec5bdbc848c52f846fb973d6da', '1', '150', 'Agent', NULL, '2022-04-19 07:04:10', '2022-04-19 07:04:10'),
(25, '1a4e79676f248f1d64e84dba27e4cf18', '2596f0749fe9b352317362a9e36e7a29', 'YeohShaun', 'd0f951ec5bdbc848c52f846fb973d6da', '1', '300', 'Agent', NULL, '2022-04-19 07:04:11', '2022-04-19 07:04:11'),
(26, 'f7d8c90ea22b01b15b57d269aa68594b', 'c5856f6a2466ba82be9af8f3cd6bf252', 'ZKChua', 'd357f663ae35b716359741bb217e7157', '6', '450', 'Agent', NULL, '2022-04-19 07:04:12', '2022-04-19 07:04:12'),
(27, 'f7d8c90ea22b01b15b57d269aa68594b', '8672379693cb27a78ff82b79dfdc9930', 'LH Chua', '1d3533310d46d70c3802b8f1efc391dc', '4', '450', 'Agent', NULL, '2022-04-19 07:04:12', '2022-04-19 07:04:12'),
(28, 'f7d8c90ea22b01b15b57d269aa68594b', '9ac29b8bccef523b5d0f9994b40f4d42', 'PS Ng', 'a38875a5fd6dbd97f903822d0b3f64ff', '2', '450', 'Agent', NULL, '2022-04-19 07:04:12', '2022-04-19 07:04:12'),
(29, 'f7d8c90ea22b01b15b57d269aa68594b', '4d98c5c3a758d056616b381085c36b4e', 'YeohShaun', 'd0f951ec5bdbc848c52f846fb973d6da', '1', '450', 'Agent', NULL, '2022-04-19 07:04:12', '2022-04-19 07:04:12'),
(30, 'b5dd5d41441804b08323a38685c348a51644391778', 'b23111b8aba41417ce3874424426fc9f', 'ZKChua', 'd357f663ae35b716359741bb217e7157', '6', '240', 'Agent', NULL, '2022-04-19 07:04:20', '2022-04-19 07:04:20'),
(31, 'b5dd5d41441804b08323a38685c348a51644391778', 'fbbcf47c64783a6bef79eb39d09e7eec', 'LH Chua', '1d3533310d46d70c3802b8f1efc391dc', '4', '240', 'Agent', NULL, '2022-04-19 07:04:20', '2022-04-19 07:04:20'),
(32, 'b5dd5d41441804b08323a38685c348a51644391778', '31cb58efde3e61bf9bd3ac50b60996b9', 'PS Ng', 'a38875a5fd6dbd97f903822d0b3f64ff', '2', '240', 'Agent', NULL, '2022-04-19 07:04:20', '2022-04-19 07:04:20'),
(33, 'b5dd5d41441804b08323a38685c348a51644391778', '05774130a37a0ad2a8ca54dbbd9274f0', 'YeohShaun', 'd0f951ec5bdbc848c52f846fb973d6da', '1', '240', 'Agent', NULL, '2022-04-19 07:04:20', '2022-04-19 07:04:20');

-- --------------------------------------------------------

--
-- Table structure for table `transfer_record`
--

CREATE TABLE `transfer_record` (
  `id` int(11) NOT NULL,
  `transaction_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `transfer_to` varchar(255) DEFAULT NULL,
  `transfer_type` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `receiver_uid` varchar(255) DEFAULT NULL,
  `receiver` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'PENDING',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `upgrading`
--

CREATE TABLE `upgrading` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `remark` text DEFAULT NULL,
  `edit_by` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `upgrading`
--

INSERT INTO `upgrading` (`id`, `uid`, `user_uid`, `ip_address`, `remark`, `edit_by`, `date_created`, `date_updated`) VALUES
(1, '12c05d6a205b3e023aafd4c99eaebb53', '70691b70ee884b1439e8a73ce33c0eef', '161.142.245.181', NULL, 'a7bd724138f51e77ac86dc66b64bfdcb', '2022-02-10 05:43:48', '2022-02-10 05:43:48');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `icno` varchar(255) DEFAULT NULL,
  `password` char(64) DEFAULT NULL,
  `salt` char(64) DEFAULT NULL,
  `birth_date` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `zipcode` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_acc_number` varchar(255) DEFAULT NULL,
  `bank_acc_name` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `rank` varchar(255) DEFAULT 'Member',
  `achieve_rank` varchar(255) DEFAULT NULL,
  `first_order` varchar(255) DEFAULT '0',
  `order_status` varchar(255) DEFAULT NULL,
  `sales_commission` decimal(20,2) NOT NULL,
  `redemption_point` decimal(20,2) NOT NULL,
  `sales_commission_month` decimal(20,2) NOT NULL,
  `redemption_point_month` decimal(20,2) NOT NULL,
  `wallet` decimal(20,2) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `fullname`, `firstname`, `lastname`, `icno`, `password`, `salt`, `birth_date`, `country`, `phone_no`, `address`, `zipcode`, `state`, `bank_name`, `bank_acc_number`, `bank_acc_name`, `login_type`, `user_type`, `rank`, `achieve_rank`, `first_order`, `order_status`, `sales_commission`, `redemption_point`, `sales_commission_month`, `redemption_point_month`, `wallet`, `date_created`, `date_updated`) VALUES
(1, 'a7bd724138f51e77ac86dc66b64bfdcb', 'admin', 'admin@gmail.com', NULL, 'admin', 'admin h2o', NULL, '23eb111bfed87d266953601d3c287f55c324b0788452919232a1222ae036a823', '0dfee12f6549614366e94a959e057af1f5ce4d06', NULL, 'Malaysia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2020-03-11 01:40:01', '2020-11-18 07:44:38'),
(2, '70691b70ee884b1439e8a73ce33c0eef', 'company', 'companyacc@gmail.com', NULL, 'company', 'company h2o', NULL, '23eb111bfed87d266953601d3c287f55c324b0788452919232a1222ae036a823', '0dfee12f6549614366e94a959e057af1f5ce4d06', NULL, 'Malaysia', NULL, NULL, NULL, NULL, 'UOB', '2653055171', 'Hygenie (M) Sdn Bhd', 1, 1, 'Member', NULL, '0', NULL, '338.00', '191.50', '0.00', '0.00', '0.00', '2020-03-11 09:23:46', '2022-04-19 07:04:18'),
(3, '7ea1cc3effbf37e0381104ce39aa06e5', 'mex', 'mex@gmail.com', NULL, 'mex', 'mex h2o', NULL, '0ba9793c54e6f630d55390cd85f3bef8878c4311f83e886f12ed5f60e1f2e95e', '53088dfb56d409d76d9c908c094d77def297d116', NULL, 'Malaysia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2020-04-15 09:36:15', '2020-11-09 07:49:48'),
(4, '1bb3266cd1c0bf3fa22c06e6a9eb892d', 'Joseph', 'josephkoay@yahoo.com', NULL, 'Peng Hooi', 'Koay', '680125075199', '00acabb1f570f5b13e6d0bba4fad20f72b86a170c8e62f43999fedadfa3b860b', 'd7442113b1989271d79c91a58fae231eb6ba2387', '1968-01-25', 'Malaysia', '0124778232', '4-10-2 flamingo series solok gangsa', '11600', '-', 'Hong Leong Bank', '29350142994', 'Koay Peng Hooi', 1, 1, 'Member', NULL, '0', NULL, '202.50', '48.00', '0.00', '0.00', '0.00', '2020-12-28 12:13:39', '2022-04-19 07:04:27'),
(5, 'ecbbf12d6997927fd668dca72dbf6092', 'junwei', 'junwei0915@hotmail.com', NULL, 'Jun Wei', 'Chng', '870915355117', 'ec56c54ba4829c88f32850f5746e6a8edc65b1850618c3e03bae91c4f8c5e826', '6246be97a706e9641731d83f35a11de485e36da2', '1987-09-15', 'Malaysia', '0149110459', '46 jalan hamilton', '11600', '-', 'Maybank', '157072432817', 'Chng Jun Wei', 1, 1, 'Member', NULL, '0', NULL, '120.00', '30.00', '0.00', '0.00', '0.00', '2020-12-28 12:15:39', '2022-04-19 07:04:18'),
(6, 'd0f951ec5bdbc848c52f846fb973d6da', 'YeohShaun', 'joshuahcyeoh@gmail.com', NULL, 'Yeoh', 'Chong', '670527075619', 'cb9f59336e80d27c82f9c9c774e4c97ee653b3928af2349d05fe20250809fbf3', '6d33050cf081bc2dbc9c825e1b77487c39770db5', '1967-05-27', 'Malaysia', '0102192717', '27U, Jalan Ayer Itam', '11500', '-', 'MayBank', '157139629204', 'Yeoh Hooi Chong', 1, 1, 'Agent', NULL, '0', NULL, '4092.50', '375.50', '0.00', '0.00', '0.00', '2020-12-28 13:06:10', '2022-04-19 07:11:55'),
(7, '267cd1469afa7e914a78672d7b3549b8', 'KentChiam', 'chiam2000@gmail.com', NULL, 'Chin Hooi', 'Chiam', '680125075105', '8189cf33d7b9f64fd0d4752842d5f042f3368274e07bd65e729c812bb2fcae34', 'ce26beecdd68333e060bee725196a548f9d54fa7', '1968-01-25', 'Malaysia', '0124112000', '266A Jalan Macalister', '10400', '-', 'Maybank', '107161227004', 'Chiam Chin Hooi', 1, 1, 'Member', NULL, '0', NULL, '192.00', '48.00', '0.00', '0.00', '0.00', '2020-12-28 13:12:44', '2022-04-19 07:04:01'),
(8, '515d91b5b51ed0ff8c0a4c5deaa95c3c', 'NickNai', 'nicknai83@gmail.com', NULL, 'Keat Hwa AL Nai Ron', 'Nai', '830609085343', 'e736d43193d6c1a1307ce12c18518cbc897261db848cc8f68ea04d4aabae9ee6', 'dba07c9773d505250eec17903e3aeaa379d5bf83', '1983-06-09', 'Malaysia', '0164870888', '8-21-6 Jay Series Jalan Gangsa Greenlane Heights', '11600', '-', 'Maybank', '107424132939', 'Nai Keat Hwa AL Nai Ron', 1, 1, 'Member', NULL, '0', NULL, '200.00', '30.00', '0.00', '0.00', '0.00', '2020-12-28 13:15:28', '2022-04-19 07:04:29'),
(9, 'a38875a5fd6dbd97f903822d0b3f64ff', 'PS Ng', 'Pengsiongng@gmail.com', NULL, 'Peng Siong', 'Ng', '671107045469', '42b4c28c6af5e9c9204377c21b0c5dcfddddbcfe12b60e7c9862ad98f3ab87c1', '2735ab6986e48ab9081ee08128a30fec0d2f7458', '1967-11-07', 'Malaysia', '0124277600', 'Dua Villas. 22A. Lintang Rajawali 5', '11900', '-', 'CIMB', '7019725958', 'NG PENG SIONG', 1, 1, 'Agent', NULL, '0', NULL, '3712.50', '496.50', '0.00', '0.00', '0.00', '2020-12-28 15:22:30', '2022-04-19 07:11:55'),
(10, '21013dbe0b7a0308fb41984bce5f3984', 'andylimch', 'andylimch32@gmail.com', NULL, 'Andy Cheng Hoe', 'Lim', '800726075315', '1ff9e3a548c2e32b0fbb8e7179ab56a396a9d99f42847eb183a1f7d1d8f63a7b', '739202cf9b6c6c355bed83c8f618c22bc2a19c42', '1980-07-26', 'Malaysia', '60124102232', '1-06-3A Berjaya Court Gerbang Berjaya', '10350', '-', 'Public Bank', '4414955824', 'Andy Lim Cheng Hoe', 1, 1, 'Member', NULL, '0', NULL, '200.00', '30.00', '0.00', '0.00', '0.00', '2020-12-29 06:25:20', '2022-04-19 07:04:29'),
(11, '2033d3489f3699966be131b98e2ac71a', 'Chee sk', 'skchee1978@gmail.com', NULL, 'CHEE', 'Soon kok', '780927025001', '6ec2068b4bffb78a882c347d5b3caf005c7e20a7e02119f0764b2bde1d843f23', '270fc09d1d22ede56403e719f4ca12dcca244978', '1978-09-27', 'Malaysia', '0164228050', 'No 3, lorong perdana 1 taman perdana', '14100', '-', 'Public bank', '4509132934', 'Chee Soon kok', 1, 1, 'Member', NULL, '0', NULL, '100.00', '10.00', '0.00', '0.00', '0.00', '2020-12-29 06:49:02', '2022-04-19 07:04:29'),
(12, '0b5721d2ddb5d5a43fe76b1222865475', 'BennyTan', 'superartevent@gmail.com', NULL, 'Beng Chuan', 'Tan', '601030075171', '3790afe61de3cb82c4da799400d4b29d94139b6f11b4dfa0756f804e6ff626e2', 'eea32dc8eca429434fec3ce1a9c8356d66c887cb', '1960-10-30', 'Malaysia', '012-4772683', '43 Cangkat Dumbar', '11600', '-', 'Maybank', '507189401515', 'Tan Beng Chuan', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2020-12-30 11:23:30', '2021-01-04 09:37:01'),
(13, '9d78bb8f8b39590eafc90aa2368aaa6c', 'K3lly1007', 'taneechee@gmail.com', NULL, 'Tan', 'Ee Chee', '921007075504', 'c66bdf46a2428eeb25d9a92338d17fd541ce5f02a9c42c85cbea3bd2230b9504', '5942d2f1ba93a5584d22d83a8954b9b06452407c', '1992-10-07', 'Malaysia', '01151093745', 'Ee Chee', '11600', '-', 'Public Bank', '4521706902', 'Tan Ee Chee', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2020-12-30 12:07:50', '2021-03-19 16:53:12'),
(14, 'f7c777efae811767f1fc52d2a8faf67c', 'Grextce', 'qutebear@yahoo.com', NULL, 'Yinn Nien', 'Tan', '810428075148', 'c275672dce1d8c040a03926698fd66e08b0095085c533c0b834df9e11e4400a2', '636f51e578287a01618503bcb86f761c7051b13d', '2020-04-28', 'Malaysia', '0124712322', '31,', '11600', '-', 'CIMB BANK', '7021227882', 'TAN YINN NIEN', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2020-12-30 12:16:04', '2020-12-30 12:19:50'),
(15, '2aff28e8bfa7e1979df21e34f80b925e', 'Tan Boon Wan', 'tys1bct@gmail.com', NULL, 'Boon Wan', 'Tan', '560406075711', '49e7c8dfca1bb34cdbca056c2fd805bec38289402e564e13ad13df29c5b3c0f6', '516c277d7419da5f2c6836ea08eb5a57fe86f60b', '1956-04-06', 'Malaysia', '0174375505', '31,', '11600', '-', 'Hong Leong Bank', '29350142987', 'Tan Boon Wan', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2020-12-30 12:19:50', '2021-01-12 10:21:25'),
(16, 'aba74f5b29521b37630114d5b6bfad7c', 'Gina Tan', 'ginatan_90@hotmail.com', NULL, 'Yinn Syuan', 'Tan', '900911075198', '0a1cd16bad9556ddd26b9ed64fe9401a40675c1a130aa1d7f074efe481a8140b', 'de5b3af6495fcfd0bbf052a61af2518bb93821bf', '1990-09-11', 'Malaysia', '0164232945', '31,', '11600', '-', 'Maybank', '‭157399003630‬', 'TAN YINN SYUAN', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2020-12-30 12:25:30', '2020-12-30 12:27:17'),
(17, 'be94e953ef428a1f7b91b4c5913508c8', 'Yvonne Lee', 'yvonne551968@hotmail.com', NULL, 'Lee', 'Siew Hooi', '680505075596', 'dda58752d01f28f738b8ad8b08678b931591805fa89f7bf7491d9cbcfe5ea0f3', '4ae32f7e9be2bcc855aa6716212c58d3f4e6353d', '1968-05-05', 'Malaysia', '+6012-9440188', 'Desa Delima 1A-08-16, Lorong Semarak Api 2,', '11500', '-', 'Maybank', '557335063480', 'Lee Siew Hooi', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2020-12-30 12:34:49', '2020-12-30 13:38:19'),
(18, '0e5aab1c9b188c04fa613d65e2c73d94', 'JoanneChong', 'joanneyean518@gmail.com', NULL, 'Kook Yean', 'Chong', '740812075512', 'd25fe46aed0c6fa1f045a1179218c92cbd8c0939ad4f3426c8d126e98eb2a92c', 'a3379ba227b0c4de70ade0e31fc9c5be5c73a95a', '1974-08-12', 'Malaysia', '0124801518', '21-5-2, Jalan Rawang', '10460', '-', 'Public Bank', '4-9104211-24', 'Chong Kook Yean', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2020-12-30 12:36:45', '2020-12-30 12:39:04'),
(19, 'ae8432247d87c00e3ed8e7d95f3baed4', 'tommyteh01', 'tommyteh01@gmail.com', NULL, 'Tommy', 'Teh', '580411075327', '58e92d8237c706a50d9f2fa33d6cd450d1ba1b13e4f525cbd5ea3a1c14230a42', '8b3b49d220547244f600c767938ab1e5d5c2476e', '1958-04-11', 'Malaysia', '0194701662', '5-5-18 Lengkok Nipah', '11900', '-', 'Maybank', '007134702908', 'Tommy Teh Guan Seng', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2020-12-30 13:07:26', '2021-01-12 04:38:10'),
(20, 'c3ba72d09db3af72a6958db12a4b5663', 'perdana73', 'yahyapenang@yahoo.com', NULL, 'Yahya', 'Musa', '731201065249', 'a8cd9b7b3f9e751fedfb92baf208b409d69f1adc3cb781cc655dd82c3cab6906', '7953f778c69d0a80bf02cb21a4773db79be71446', '1973-12-01', 'Malaysia', '0184003225', '1122 mk 11 Simpang Empat Permatang Buloh', '13200', '-', NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2020-12-31 04:46:17', '2020-12-31 04:49:14'),
(21, 'ba8592381362dadc1fc9b60257bf19bb', 'ireneyeoh', 'ceireneyeoh@gmail.com', NULL, 'Irene', 'Yeoh', '611225075226', 'cdfdd1477e6d1a2c30d9fafe9cfb86612d15c27312540e3318fb5425b7668657', 'c25d10ae787d7a680ba93896f3e97b008908f5c3', '1961-12-25', 'Malaysia', '0194238186', '3-45 (Tanjung Villa) Jalan Bunga Tongkeng', '11200', '-', 'CIMB BANK', '7019386642', 'YEOH CHENG EAN', 1, 1, 'Member', NULL, '0', NULL, '3712.50', '496.50', '0.00', '0.00', '0.00', '2020-12-31 11:18:14', '2022-04-19 07:11:55'),
(22, '9a5a0f74bb189acb864e365fe57ce7ad', 'CGGoh', 'Cggoh425@gmail.com', NULL, 'CG', 'Goh', '740619075289', '7796cccd439de50718cd43f3807ab4f5024a30cd2c8d19b19c9f004741411866', '1e0d6962e3fcf1f6d63047406c47926d5990dc58', '1974-06-19', 'Malaysia', '0124075237', '46, lorong bayu mutiara 6, taman bayu mutiara, built mertajam, penang', '14000', '-', 'Maybank', '107135058414', 'Goh choo ghee', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-01 03:28:52', '2021-01-01 03:30:34'),
(23, 'ec8505287e6a2b5abf048ade05506d32', 'stchoy', 'stchoy99@gmail.com', NULL, 'Steven', 'Choy Kim Theam', '710516075289', '541147b6f0f6ac557571a037fb4d85761f9032b9bb31f1feed109918f54f1d7a', '539c3039f40a5ea5ef864c352ad7c290ae64ccaa', '1971-05-16', 'Malaysia', '0124918866', '26-2-2 Sri Emas', '10350', '-', 'Public Bank', '4412381610', 'Steven Choy Kim Theam', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-01 05:05:55', '2021-01-01 05:12:27'),
(24, '168173637a544e85720b9fe54aa71b7b', 'hdk64042', 'sylvia_hee@hotmail.com', NULL, 'Hee', 'Dee Koon', '640402075650', '2619a0ff444aa4d57c26c48b4ad0966d1028608dad9d70d421724f7bacf438ca', 'c0a7ebb691c39f6957b5b2e331471f6c5b1b2d36', '1964-04-02', 'Malaysia', '0162155558', '29 Lintang Lembah Permai 12', '11200', '-', 'Hong Leong Bank Berhad', '05300104003', 'Hee Dee Koon', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-02 05:41:40', '2021-01-09 12:46:17'),
(25, 'c411c4c8c3a6607c019408bc9e122915', 'Seng Aun', 'ongsengaun@gmail.com', NULL, 'Seng aun', 'Ong', '651028075039', 'b673ee8adf1ed3643a0d0df6848ac959e1ef6178b8c960dadfbe8e1496ec9d17', '9b46d9198ec8b9c772ab4b1bc7648fb4bc4ffd4d', '2021-01-04', 'Malaysia', '60124722292', 'No 135 A  Taman Vokasional Mk Kuah', '07000', '-', 'MB', '12073050508', 'ong seng aun', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-04 09:37:01', '2021-01-10 02:50:12'),
(26, '1c7c566372eeac2360374f8ac00e80d3', 'lim', 'Abc@123.com', NULL, 'Lim', 'Guan', '123456087072', '23eb111bfed87d266953601d3c287f55c324b0788452919232a1222ae036a823', '0dfee12f6549614366e94a959e057af1f5ce4d06', '2021-01-04', 'Malaysia', '0124557842', '123', '14000', '-', NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '90.00', '9.00', '0.00', '0.00', '0.00', '2021-01-04 10:28:00', '2022-04-19 07:04:27'),
(27, 'bdf1f1cf7d52592de7d2f2e517a8eeb0', 'YCTAN', 'yeechurd@gmail.com', NULL, 'Yee Churd', 'Tan', '780215075713', '1d6d854e285907f909c9636c17b3a0232294b51db854ef0f6252300eaf400f09', 'b05584899d010fc5fe8e5a6c7c564e3a71d71150', '1978-02-15', 'Malaysia', '0124237155', 'No.1, Lintang Delima Satu,', '11700', '-', 'PUBLIC BANK', '3149917515', 'TAN YEE CHURD', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-05 12:19:56', '2021-01-13 11:20:22'),
(28, '8b375cf7a0c532458ce51e8a9935ba8b', 'Andy koay', 'Wckoay@yahoo.com', NULL, 'Koay', 'Wen chin', '840124-07-5285', '6ca759b390039f530ebf4460cee0235d4c67122ac33534aaa84e464a01f8157e', 'ae46830a7c765bff113e8a21f4ad1860b138aa79', '1984-01-24', 'Malaysia', '0125716468', '9-4-6,bukit awana,', '11060', '-', 'Cimb', '7020703329', 'Koay wen chin', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-07 06:19:30', '2021-01-30 09:27:43'),
(29, '1d3533310d46d70c3802b8f1efc391dc', 'LH Chua', 'lh_chua@eca.com.my', NULL, 'Chua', 'Lye Hock', '720219075281', '080b55fd1ede9a9df810a239b7426b8181a4e6087b4b7cfd1b468e851ac82f5b', 'fdd9e5c5acafb70a28909d7ad41e89941659db77', '1972-02-19', 'Malaysia', '0124833203', 'No 17, Lorong Bukit Juru 7,', '14100', '-', 'Public Bank Berhad', '3204818718', 'Chua Lye Hock', 1, 1, 'Agent', NULL, '0', NULL, '3712.50', '496.50', '0.00', '0.00', '0.00', '2021-01-07 08:59:50', '2022-04-19 07:11:55'),
(30, 'ff554b0ddaed52fdb196486b6875821b', 'shuennchi2020', 'limshuennchi2020@gmail.com', NULL, 'Shuenn Chi', 'Lim', '811204075215', 'c69c614aa50c3567ae37235264bf1d6c5e07150de9576c112d5f8cc47d482dba', '3e358928f9c2fc87c4bbb5ba941bcacc7a8e0c0a', '1981-12-04', 'Malaysia', '0145212020', '8A-20-2,Persiaran Gurney,', '10250', '-', 'Ambank', '820070001653', 'Lim Shuenn Chi', 1, 1, 'Member', NULL, '0', NULL, '250.00', '40.00', '0.00', '0.00', '0.00', '2021-01-07 09:33:59', '2022-04-19 07:04:29'),
(31, 'be4dfa404b72988cbfa6b931fa2b25ab', 'Josie', 'loh.s.h.2263@gmail.com', NULL, 'Loh', 'siew heong', '760126095044', 'aabf4915f3d163376859a1389cdf2fd8a1af7b58dcfa70fa8e905bc1ffa89989', '2b6eb07b0c6756405ced49b3c4472dce36eb9c47', '1976-01-26', 'Malaysia', '0164732263', '11B-33-H,persiaran halia 3,mont residences,10470 tanjong tokong penang', '10470', '-', 'Cimb', '7631956745', 'loh siew heong', 1, 1, 'Member', NULL, '0', NULL, '200.00', '20.00', '0.00', '0.00', '0.00', '2021-01-07 09:52:38', '2022-04-19 07:04:29'),
(32, '9737ef340ac451a52b391fcd28955bf3', 'Louis', 'louis.ja2005@gmail.com', NULL, 'Louis', 'Lim', '650207086441', '1b19e85a8e0db9a8217b0221980b72fde195047633cc4d9b26677b9afe4a1c11', '8f316da9a8e1dba60016b229fec8dbd64183af73', '1965-02-07', 'Malaysia', '0174778011', '21-1-2 Graceland Villa', '11950', '-', 'CIMB', '7019634727', 'Lim Kok Leong', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-07 11:39:30', '2021-01-12 04:38:20'),
(33, '85a7e481a2c0b0f322716319333f5630', 'Jacob tan', 'Baofeng0815@gmail.com', NULL, 'Ee tze', 'Tan', '910815075322', 'd234e568e90c76655883a004f74f292cd6277da4843a47d5d8d6b54070666b25', '31288401041cc93de923299b915eec3a70af394c', '1991-08-15', 'Malaysia', '0164488093', '22 solok slim', '11600', '-', 'Public bank', '4515630111', 'Tan ee tze', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-08 02:36:14', '2021-01-31 04:14:07'),
(34, '22b1d1026edc5ff2437149f8b0bfe860', 'chiam', 'chongcheanlee@yahoo.com', NULL, 'Chiam', 'Lee', '123341324141', 'e213bd53a8223d26711ef57b678f8b4cfc598e171709517102dafef1d6ecddec', 'c642ee59c20ab0abf72f486ddcd1d81efa04a70f', '1972-01-19', 'Malaysia', '0184679715', 'Qwer,12', '14000', '-', 'Maybank', '1234567890123', 'Chiam ah teck', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-08 04:27:35', '2021-01-08 04:31:24'),
(35, 'b4d548a8b9bf1ccc3aa0c72b4e857e78', 'iyen1712', 'changi_yen@yahoo.com', NULL, 'I Yen', 'Chang', '791217115225', '3b2fb14816581ade6de12faaf8477eb05de8cf04fae30c6d122f3db0c6bbe2dd', '79c1d865d1495d4e4fa09cdfcee247a6fdbeaf60', '1979-12-17', 'Malaysia', '0126366583', '10 lorong naluri indah,', '13500', '-', 'malayan banking berhad', '113014511114', 'CHANG I YEN', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-08 06:40:17', '2021-01-12 04:38:25'),
(36, 'b41fca733e4f10f28827335c631b0515', 'MLNg', 'ml_ng@eca.com.my', NULL, 'Ng', 'Mei Li', '760318075136', '31e4ef66ca51832f28bf0c54311a9cef0147bc7b18451869c1b2093202dcf4e7', '10f542fd386abe965888cd848de49a22741b4f48', '1976-03-18', 'Malaysia', '0124231203', 'No 17, Lorong Bukit Juru 7,', '14100', '-', 'Public Bank Berhad', '4178446724', 'Ng Mei Li', 1, 1, 'Member', NULL, '0', NULL, '4068.00', '259.50', '0.00', '0.00', '0.00', '2021-01-08 08:34:16', '2022-04-19 07:11:55'),
(37, 'd357f663ae35b716359741bb217e7157', 'ZKChua', 'ml_ng@eca.com.my', NULL, 'Chua', 'Zhi Kian', '060205070585', '42fe4e9f8fc4c3ae5f5ce95a2233189a27ed38442dc1e23979f11ea16bb3c2d7', 'b1e84eddfacdb39f00df91b7db7694fc1a280d7d', '2006-02-05', 'Malaysia', '0192636288', 'No 17, Lorong Bukit Juru 7,', '14100', '-', 'UOB', '1233054289', 'Chua Zhi Kian', 1, 1, 'Agent', NULL, '0', NULL, '6810.00', '165.50', '0.00', '0.00', '0.00', '2021-01-08 09:28:57', '2022-04-19 07:11:55'),
(38, '8d115e44db8169a60fd8e70a46bc95b0', 'RYChua', 'ml_ng@eca.com.my', NULL, 'Chua', 'Ren Yang', '081107071149', '1b3d32721833f4cdd0c3977adcb4840ba98426858167406741f64ad683c45303', '222f3e9f0ea50a6a3854b344b870b5a842fbe79c', '2008-11-07', 'Malaysia', '0192320288', 'No 17, Lorong Bukit Juru 7,', '14100', '-', 'Public Bank Berhad', '4178446724', 'Ng Mei Li', 1, 1, 'Member', NULL, '0', NULL, '4663.00', '182.50', '0.00', '0.00', '0.00', '2021-01-08 09:38:15', '2022-04-19 07:11:55'),
(39, 'b5dd5d41441804b08323a38685c348a5', 'user', 'ml_ng@eca.com.my', NULL, 'Chua', 'Yan Jun', '091123070823', '23eb111bfed87d266953601d3c287f55c324b0788452919232a1222ae036a823', '0dfee12f6549614366e94a959e057af1f5ce4d06', '2009-11-23', 'Malaysia', '0124231203', 'No 17, Lorong Bukit Juru 7,', '14100', '-', 'Public Bank Berhad', '4178446724', 'Ng Mei Li', 1, 1, 'Member', NULL, '0', NULL, '5812.50', '233.50', '0.00', '0.00', '0.00', '2021-01-08 10:09:17', '2022-04-19 07:11:55'),
(40, '62e7d178f94e7df56a5b3a5b02c5dbb0', 'Kim Ng', 'Ngkuikim@hotmail.com', NULL, 'Kui Kim', 'Ng', '730324015052', 'da5994e41746267f7687117ea5e6a36417e03f2075920dc2f6e5ac26d1048321', 'b733d179367dae7dd8a59ead85f1cbe5a652531c', '1973-03-24', 'Malaysia', '+60167834018', '9 Jalan Tasek 55', '81750', '-', 'Public bank', '4-6297778-13', 'NG KUI KIM', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-08 10:20:23', '2021-01-31 04:14:07'),
(41, 'fffbfd13c8805f195b050d107acfd7ed', 'Woon', 'woonchinchai@hotmail.com', NULL, 'Chin Chai', 'Woon', '711004015615', '6097b59e8a33a9c6a59989edeaf22571acc5b76b77922d1e0fecae684326bc87', 'd0f8b24f369bd7a4c1dafed0a2b7fc1a74d96aa6', '1971-10-04', 'Malaysia', '+60167834018', '9 Jalan Tasek 55', '81750', '-', NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-08 12:08:31', '2021-01-08 12:08:58'),
(42, 'a8838737e4b0bc22f5fa988791069bf0', 'YeeYap', 'Pengsiongng@gmail.com', NULL, 'Siok Ling', 'Yap', '830109145848', 'd8e6f516f625cc393ce51fc491e5e4602723788aca7a30380779068e28b371e0', '76468fb7af7605dfbd2f206553fb816794ebfb8e', '1983-01-09', 'Malaysia', '0123969775', 'A-13A-13, Monte Bayu Condominium', '56100', '-', 'Public bank', '4934301719', 'Yap Siok Ling', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-09 12:31:45', '2021-01-12 13:58:03'),
(43, 'b419e184888fd00ca338db792cd24f8d', 'Moon1977', 'sgk2024@gmail.com', NULL, 'Sg', 'Khng', '770731075622', '6562c1eef85a2af6c38847a069ae6b6bb38c36c6f6a81e497f726f792fc760bc', '8ba3058afcfc763a091876036ed548144f93f548', '1977-07-31', 'Malaysia', '0124618019', 'D11-18 Flat Uda Tanjung Tokong', '10470', '-', 'Maybank', '10714322418', 'Khng Swee Guat', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-10 12:22:09', '2021-01-10 12:32:42'),
(44, '7f0587559bc62b00a4cdac2059fd5e76', 'Raymond tan', 'tan.chai@hotmail.com', NULL, 'Tan', 'Pick chai', '771113015635', '836e85644d1da448ceb5a370d71c225d4cac0d079b4b5bed5d20f3c66911c3b9', '35e7b15eaaa6d827d1dc61a7cfa30884da3ca9c4', '1977-11-13', 'Malaysia', '0136830886', 'No 10,Jalan Tiram 7, taman preindustrial Tiram,', '81800', '-', 'Rhb', '20118300037675', 'Tan pick chai', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-10 12:29:26', '2021-01-12 13:58:13'),
(45, '494cdc6a441611ddc100cf0cff5f2a52', 'Dan681225', 'Danyeoh68@gmail.com', NULL, 'Kwang Boo', 'Yeoh', '681225-07-5545', '69497540fdb1f9de453dd922b964bbef95af4b368bbb1db2aee1937a62ecde3b', '350a416bdf15dde0e816880b765d13d2a488c956', '1968-12-25', 'Malaysia', '0124268979', '26c-06-07 Taman Kristal Lengkok Erskine,10470 Georgetown,Penang', '1047', '-', 'Maybank', '107041063690', 'Yeoh Kwang Boo', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-10 12:32:42', '2021-01-10 12:34:48'),
(46, '13516b89e887e7a2ff998679f65bf5a6', 'Voodokit', 'Masonchen149@gmail.com', NULL, 'Han Chet', 'Tan', '890201-07-5437', 'c970985d71a88c0340e8901dc274187ab18b77f195c32c6cc29c0854de7b074f', 'dc803ec71d5a513b1e684b32c1154879c736c709', '1989-02-01', 'Malaysia', '0107178093', '22 Solok Slim', '11600', '-', 'Maybank', '107246079938', 'Maybank', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-10 15:49:23', '2021-01-31 04:14:07'),
(47, 'e62e502c7699fadd63611bef06ff9605', 'AnnieLim', 'joshuahcyeoh@gmail.com', NULL, 'Lim', 'Koon', '740117065072', '55dcaea1946c61b3ea63af0477e6743af873f86351ecacf360f6c4f0b083973c', '30e5a624e43f2d512d6b11bd19412cc46cdc613f', '1974-01-17', 'Malaysia', '0102061727', '27U, Jalan Ayer Itam', '11500', '-', 'Public Bank Berhad', '4878362100', 'Lim Loy Koon', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-11 09:36:32', '2021-03-08 13:06:16'),
(48, '2556ee6809b71eb935c07e9520b68005', 'Yoyokok', 'yoyokok8803@gmail.com', NULL, 'Wei Fang', 'Kok', '710901075074', 'ff68daaab884e56a195ad4eb819c016d0443a400ff37da78da98c78121698c45', 'dc32c8fbe557621ab7ec264f9c7fcfbe0c5e2ec4', '1991-09-01', 'Malaysia', '0124768444', '266A', '10400', '-', 'Maybank', '157054726790', 'Kok Wei Fang', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-11 09:50:42', '2021-01-15 10:13:03'),
(49, '0dbdc899bc89b530c7a17012d59b8644', 'mimiung', 'mimiangsb@gmail.com', NULL, 'Siew Bee', 'Ung', '610203075474', '4ffa815ed9bdca5c9728e45da2dc0448d7d4fa6122c9072c581b30d2bb37b7af', 'a65cd905de74ed9509128bd9b388cdc812f84e2f', '1961-02-03', 'Malaysia', '0164222323', '8-21-6 Jay Series', '11600', '-', 'Public', '6473650336', 'Ung Siew Bee', 1, 1, 'Member', NULL, '0', NULL, '100.00', '10.00', '0.00', '0.00', '0.00', '2021-01-11 10:08:48', '2022-04-19 07:04:29'),
(50, '2f480ac7fbbdc9e3d455185992d0ee55', 'AnnLim', 'annlimse11@gmail.com', NULL, 'Sok Ean', 'Lim', '700611075392', 'eb396ddff3bc89f9d56f4fc0cb9e66c56ed7137664373500e068f87f5c543a04', 'bf42bcb4823744404188073ea24c8a629ee98f35', '1970-06-11', 'Malaysia', '0124129813', '3 Lorong Cendana Emas 4', '14100', '-', 'Public Bank', '4341312501', 'Lim Sok Ean', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-11 15:04:31', '2021-01-12 16:33:51'),
(51, '305db2cdccd8b9f1703777ef9f6d64e6', 'Feiweng', 'feiweng1991@gmail.com', NULL, 'LOH', 'Kok YONG', '910706115057', '298373d66049df08ec5ca4f1490bf8f8571d8d940f92f7476c549574cbdb6f9c', '1285cfa827b025621dd4f6b18c5e2fe7f348970f', '1991-07-06', 'Malaysia', '0169329268', '2A-13-13A Arcoris SOHO', '50480', '-', NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-12 03:46:13', '2021-01-12 03:46:13'),
(52, 'b629870d94417ff17a8d0086a932d70c', 'Reserve1', 'Reserve1@gmail.com', NULL, 'Reserve1', 'Company', '1610425091', 'a350d9d29b2770d5154d092c7a045cf806d6a1bd207af542573f472678be4186', '70834a887edf2c2108bc29f325f0b5b4998354aa', '18-08-2008', 'SG', '01211559901', '123, ABC', '123333', '-', NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-12 04:18:11', '2021-01-12 05:32:05'),
(53, '0de594c4216ddc7bd4eb6602849e19f5', 'Reserve2', 'Reserve2@gmail.com', NULL, 'Reserve2', 'Company', '1610425103', 'cd6c457d5a0ebb12375362ce91e1bb98eb244acfea71f114721f7339a5ca7862', 'e3d347e8d7f43d62ce552a1ec6f74a2d50742fa1', '18-08-2008', 'SG', '01211559902', '123, ABC', '123333', '-', NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-12 04:18:23', '2021-01-12 05:32:09'),
(54, 'b63fad38f8ee572b4598ef10f12c4e41', 'Reserve3', 'Reserve3@gmail.com', NULL, 'Reserve3', 'Company', '1610425105', '700e26304a524be648ac7c598c2d3d3e7b24f572d1cb9a83d99a21bfb52cab57', '112828e7cdb2dcd8acf7b322df5ec8105648f994', '18-08-2008', 'SG', '01211559903', '123, ABC', '123333', '-', NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-12 04:18:25', '2021-01-12 05:32:11'),
(55, '31d34933763feed3a083315fa7de2ad2', 'albertgan', 'albertgsc@gmail.com', NULL, 'Gan', 'S C', '641102095005', 'cef91730f71ad455b94622325b99f8e591240acc45d6413e02b6667146473360', '3ebede2599a9e614b5126509b79d224f37f93a09', '1964-11-02', 'Malaysia', '0124989272', '2A-15-7 Bayswater Condo', '11700', '-', 'Maybank', '507161053731', 'Gan Sing Chaw', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-12 09:39:26', '2021-01-12 09:46:16'),
(56, 'cd972b2f76411c015ef200363b26a264', 'Jordan koay', 'koaypengseng,@gmail.com', NULL, 'Peng seng', 'Koay', '560820075063', 'bd5721dd1e383ec4ada5f620f37532cf4f90a35307f1fc25d7e844df9ab83b92', 'a6a64d3b1192d503db9e59227a553068ee61f904', '1956-08-20', 'Malaysia', '0135961554', '56, Jalan Thomas,Gulugor', '11700', '-', 'Maybank', '106026130609', 'Koay Peng Seng', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-12 10:21:25', '2021-01-12 10:37:44'),
(57, 'e4c8294350f46de003f8fda3d4464f85', 'penglong5', 'penglong5@gmail.com', NULL, 'NG', 'PENG LONG', '760729045281', '4e01db16c99b0373d11df8d77e93fc2fb56d8f73d62c0021dd11e048100d2ed5', '0d350a58d002f9dfe37e736c07a90e44ed0e648c', '1976-07-29', 'Malaysia', '0197739238', '6', '81100', '-', 'Maybank', '101208806664', 'Ng Peng Long', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-12 13:08:16', '2021-01-12 13:57:21'),
(58, '6825c96bc70c2f3ef08ee01168a94b09', 'Collyn323', 'charlie_hcw@hotmail.com', NULL, 'Chin Wooi', 'Ho', '910323075190', 'c95fac29f3c5bd5a1ad42c30f423917c0d658027d1dce583031cf7f43275e6c9', '8273478083de75e72bc646f9c091a366e4cafa4d', '1991-03-23', 'Malaysia', '017-4238323', '9，lengkok haji Ahmad bukit dumbar residence', '11600', '-', 'Maybank', '157380063602', 'Ho Chin Wooi', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-12 17:05:23', '2021-01-17 11:54:48'),
(59, '380a20d3a04a5f2aae722b06365d4103', 'liangkhenglo', 'liangkhenglo@yahoo.com', NULL, 'Lo', 'Liang Kheng', '640510075375', '1d050d7290b97f86361523d7f6edbafe05030b26452ae633fa556312e9ac943d', '6c98882070f897d384be581c00f076bbb6b10965', '1964-05-10', 'Malaysia', '0164419499', '12A Jalan Bayan Mutiara 3', '11700', '-', NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-12 19:45:12', '2021-01-12 19:50:51'),
(60, 'da95755224aa6650687b49e22d77bcce', 'Ronnie1113', 'sunrise3268@gmail.com', NULL, 'Lung', 'Wong', '891113-13-5860', '0b2fc106fbb86d7b2ea7bb58290620273662986b8cb5ce701ef83d9c0dd393d9', 'fe80510664e0f541e0d25fdf85369e5877d016f2', '1989-11-13', 'Malaysia', '01112401811', '20 Jalan Rejang 9B,96100 Sarikei Sarawak.', '96100', '-', 'Public Bank', '4744811424', 'Wong Lung', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-13 02:42:15', '2021-01-13 05:01:15'),
(61, '04e1b4c1ffc641bd6eedb4d3cbd54bb8', 'Reenako', 'rinako_34@hotmail.com', NULL, 'Xue Ni', 'Ooi', '920503075680', 'ad14fa1821bf856ca887a17138053f32276e825668b46e792da943e9bf661733', '46f0ca78de47804c8eda002c6f58c10fdd48ea10', '1992-05-03', 'Malaysia', '0165057634', '1-25-c Greenlane Park', '11600', '-', 'Hong leong bank', '23500009663', 'Ooi Xue Ni', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-13 02:43:51', '2021-01-15 10:48:48'),
(62, '1bdcbeffc71fc82ba8266a4c6d883be9', 'YH Ng', 'Ngyhan77@gmail.com', NULL, 'Yao Han', 'Ng', '960820017683', '7c503379f33212be59c9efa114311a19897618cb8326fb17714e6c488f3ac8d8', '8fb9a4cc963c805bb045f534288e0b815a2e33a0', '1996-08-20', 'Malaysia', '0164570957', 'Dua Villas. 22A. Lintang Rajawali 5', '11900', '-', 'CIMB', '7021485676', 'Ng Yao Han', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-13 09:59:32', '2021-01-13 10:20:30'),
(63, '3ea9484b8c6cbf0db0f6c53babde3af7', 'NicoleYang', 'Cfyong09@gmail.com', NULL, 'Chooi Foon', 'Yong', '680927105716', 'ec2c2999d3eb0e1e9ad25b9a79773f3f86e4a12ed6df3cc751640668009ad898', 'c418f7fc33710c606121884992a5315c7753ce45', '1968-09-27', 'Malaysia', '0124730902', 'Dua Villas. 22A. Lintang Rajawali 5', '11900', '-', 'Hong Leong', '14050070732', 'Yong Chooi Foon', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-13 13:48:24', '2021-01-13 14:10:03'),
(64, 'f424ef5cb82a798f65826c21932c3daf', 'skleong', 'skleong9119@gmail.com', NULL, 'Kim Siang', 'Leong', '741101026209', 'd87e5081f27eae815afeadd31f45c1299e88e55e761d4ca6e6e984974831b713', 'e141e6313eb70e6a61de248760a13ea45d8685c2', '1974-11-01', 'Malaysia', '0124980988', '23, Jalan Southbay 3, Resident@Southbay', '11960', '-', NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-14 13:06:17', '2021-01-14 13:06:17'),
(65, '603daaa6ce6a094a618f01565a7a83be', 'Esther', 'esther3992@live.com', NULL, 'Lee Way', 'Lim', '810801-06-5264', '674f27fc7b01c33c1318e91d6d0a925bf7c4eae006182e610bea4f00bdb47591', 'c8948a8ee00b00c98c9ee232a5193ece46b749da', '1981-08-01', 'Malaysia', '019-4496554', 'No 7, Lorong Shahzan IM 11', '25200 Kuantan Pahang', '-', 'CIMB BANK', '7068942363', 'LIM LEE WAY', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-14 14:48:14', '2021-01-15 10:47:55'),
(66, '8da8b4fe873a1dcc57090752775bc846', 'sinkee', 'sinkeekok@gmail.com', NULL, 'Sin Kee', 'Kok', '401018075009', '75a6deb97709adab9c5991be0f1b9aaa65723208ba567a9890c61da5c63d3a4a', 'f26db77db4846e055658493aa263ff95293a6b46', '1940-10-18', 'Malaysia', '0125211459', '42', '11700', '-', 'UOB', '1463039947', 'KOK WEI FANG', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-15 10:13:03', '2021-01-15 10:48:36'),
(67, '0fa06fb4318ce5e01960080c008f6bef', 'shiuan', 'yschiam97@gmail.com', NULL, 'Yee Shiuan', 'Chiam', '971219355036', 'c2d27319f07c72b902b7662f612f558a64d7f159f80a20a92b4f2da4398291df', '42e035ebc1c7ca3f223f1927d6868cbe79bedd43', '1997-12-19', 'Malaysia', '0124768444', '266A,Macalister Road', '10400', '-', 'Hong Leong Bank', '16351117354', 'Kok Wei Fang', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-15 11:30:54', '2021-01-15 11:48:19'),
(68, '7653ff946b8d868479a660f8424a3368', 'Vostron', 'joshuahcyeoh@gmail.com', NULL, 'Leong', 'Kim Siang', '670527075620', 'ef158126b523de6aaed38f72a34f58f92b23bf2624ac1769eba715d52006fcf7', 'db206c6ba5b1e09f850a8a6d5e76bd389c0ec16d', '1974-11-01', 'Malaysia', '60124980988', '27U, jalan Ayer Itam', '14500', '-', 'Public Bank Berhad', '6497772229', 'Leong Lim Siang', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-16 03:41:43', '2021-01-20 14:56:45'),
(69, 'ce3e65bdf1c5747cf04be30f30c37281', 'Fennielim', 'fennie227@gmail.com', NULL, 'LIM', 'LAI FUN', '750227-06-5072', '1c8d885e8fabb211bb6b1c832b70a6f4096fdae4673f8b6527941ba26f899cfa', '1886a1c1d5619ee38d1b97ac94e5023fd7143583', '1975-02-27', 'Malaysia', '+6014-9023020', 'No 33, Lorong Shahzan IM 1,Taman Shahzan,', '25200', '-', 'RHB', '10608500072717', 'LIM LAI FUN', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-16 04:05:23', '2021-03-08 13:06:16'),
(70, 'b50d9395e8a9606bf53a12eb11494308', 'ho shu', 'hoshu79@gmail.com.', NULL, 'shu', 'Ho', '790509075400', '12372f6c18c266bed33defaabbdef0ab34c935d6a10e75781f55e003dda778e1', 'a3eb93824e02ef7499a6b9099e4eee8c584204da', '1979-05-09', 'Malaysia', '012458483', '2-6-07 taman sinar pelangi', '11600', '-', 'Hong Leong Bank', '05451083138', 'Ho Shu', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-16 06:48:38', '2021-01-16 07:03:23'),
(71, 'b9dd1080282915d3256dbda4b4b93960', 'weitsong', 'kokwtsong@gmail.com', NULL, 'Wei Tsong', 'Kok', '700803075397', 'ae33384448b9558132a05e9735a91f2f185ff07053859605a4af351c3c03f7fd', '57f11575836dd6247105dc4cc46c207c96cc960f', '1970-08-03', 'Malaysia', '0124111884', '3-15-7 Taman Desa Relau', '11900', '-', 'Public Bank', '6859300305', 'KOK WEI TSONG', 1, 1, 'Member', NULL, '0', NULL, '265.00', '50.50', '0.00', '0.00', '0.00', '2021-01-16 12:44:48', '2022-04-19 07:04:24'),
(72, '6d8d575ee828b6ce80d791633d84a772', 'VinceC', 'vince_chanyc@hotmail.com', NULL, 'Chan', 'Yeen Cheng', '790102085981', 'c5c18570b938af63f63479ef0f01b981cde516e15b6fd74eca193ae546771e83', '8f871734d62761b29e216ab20734b9f1a5c16350', '1979-01-02', 'Malaysia', '60122963688', '37-1-6 Mutiara Court 1, Lorong Delima 20', '11700', '-', 'Maybank', '107096235145', 'CHAN YEEN CHENG', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-17 03:12:20', '2021-01-23 09:12:54'),
(73, 'fce7a3684792f5beda7cd1d438d6997f', 'avelyntoh', 'avelyn_toh@yahoo.com', NULL, 'Toh', 'Su Foon', '820107025376', '4b1f09651abd397d68537a1b8ed6039f117f9e97f2be579a158250d12d6bab20', 'f15b8c96170fe6734eb277857fe13f1c510d2d5e', '1982-01-07', 'Malaysia', '0124844221', 'No 770, Jalan Sentosa 3/8, Taman Seri Sentosa, Lunas, Kedah', '09600', '-', 'Maybank', '102111139715', 'Toh Su Foon', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-17 05:23:04', '2021-01-17 07:58:23'),
(74, '16c61d5aae8d110ae3b6492b1bb9270a', 'Parames41', 'parames.pwb@gmail.com', NULL, 'Paramesuvaran', 'Markandu', '620410015021', '7fecee2ca97d9c82a180580cba46218b3d0bb8301b19e3e13aa7533e7b63eb26', 'c6b4f276dd55aa428807afda43dfa4904bf5a4e4', '1962-04-18', 'Malaysia', '+84785102399', '2 jalan bayu20', '816750', '-', 'May bank', '101142076894', 'Paramesuvaran Markandu', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-17 14:14:48', '2021-01-17 14:22:46'),
(75, 'a6e56a53f9352e83354674b5ac15844f', 'Unix', 'unixwong@gmail.com', NULL, 'Chee Yai', 'Wong', '830126105135', '36bf96d505382f89672c452ebb3c7776fadea7a6beb622fd647cbecca6b43b14', '120cd81dc7617f86c8b642cada700279d533fba2', '1983-01-26', 'Malaysia', '6019-377 1566', '21, Jalan Bunga Kemboja 9,', '56100', '-', 'CIMB', '7059350495', 'WONG CHEE YAI', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-18 05:39:44', '2021-01-19 05:07:23'),
(76, 'adac02200cb152176a295d0aa598ca7d', 'jackyeesj', 'jackyeesj@gmail.com', NULL, 'Jack', 'Yee', '791010-10-5531', '722d0a83011960b4420dfca18a73277350ad9a7966c8daf1bee479392bfd5cdd', '77bcfc17398bc45f9e3de628378b35a29b75f24e', '1979-10-18', 'Malaysia', '0192281018', 'No 6 Jalan Salung 33/26', '40400', '-', NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-19 13:46:50', '2021-02-14 15:03:34'),
(77, '216537ee4919953baf68cba1e4b4eb99', 'Venice Lim', 'venicelim7989@gmail.com', NULL, 'Venice', 'Lim', '750411075668', 'e7384f308d276b3394df473df3caf9d4bcf5c0b9fd047cfe269b10b4e898832c', 'e31ccba71864c842518165fa7f37de469edee9ad', '1975-04-11', 'Malaysia', '0103657989', '1-2-5 Solok Lenggong', '11600', '-', 'BSN', '0710041000113654', 'Lim Seok Khim', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-24 08:16:56', '2021-01-25 15:25:12'),
(78, 'bb241886cda8524cbb388c75b50682a2', 'tehalan88', 'tehlan88@gmail.com', NULL, 'ALAN', 'TEH', '880805086031', '9d0da899b7986a47c842fea094d4dc6005ffd53c8234e61b62c94564647bc0c8', 'e95fb900b62082037f234f31639ab840caef617a', '1988-08-05', 'Malaysia', '0165660854', '47,LORONG BUKIT MINYAK 17', '14000', '-', 'MayBank', '158248376907', 'ALAN TEH JOO THYE', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-25 04:43:02', '2021-01-25 15:46:34'),
(79, 'e061c363c9824601c8b7df04ca426f4d', 'Siow Yen', 'miaowei7279@gmail.com', NULL, 'Goh', 'Siow Yen', '840303-10-5224', '931e7ba66a5471874c724a9446324fb29aefc31dd9518c22d17424c560fd0b23', '9781f2d20c87ccc95aa6a5d3ae2125caf0c8b079', '1984-03-03', 'Malaysia', '0163351071', 'No57,Jalan Dato Yusof Shahbudin 17', '41200', '-', 'Hong Leong Bank', '32700031164', 'Goh Siow Yen', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-27 09:03:54', '2021-03-08 13:06:16'),
(80, 'f7327ffac161db12503a28dde3b9fb0a', 'PHMOK2021', 'mokph2000@gmail.com', NULL, 'MOK', 'PIK HEONG', '761209065478', '3963f924bcb9dc6a24bef553989203c1f822076d565e54c6bbfd0f9bb7295320', '856c6f1d2462d90f66897c05c5124183c913b935', '1976-12-09', 'Malaysia', '0122271782', '29B-16-03, Golden Triangle, Jalan Paya Terubong', '11900', '-', 'Public Bank', '5002088932', 'Mok Pik Heong', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-27 12:12:46', '2021-02-09 14:38:32'),
(81, 'ef571161fb416ee8d1b79a873931b60c', 'weikang22', 'wei_kang22@yahoo.com', NULL, 'Wei Kang', 'Chin', '811222025043', '864922257b699b1276106661a7a5172c56af72ec17a3696c494dac974ec511e3', 'c37724c34b3e1499c516cfb34c7c9250e4528bd8', '1981-12-22', 'Malaysia', '0175603669', '20, Lorong Alma Ria 2,', '14000', '-', 'Public Bank Berhad', '4313116916', 'CHIN WEI KANG', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-28 02:50:12', '2021-01-30 09:27:13'),
(82, 'dc305083f182ccfff4f972738e931cd8', 'heng664461', 'chengchong.heng@gmail.com', NULL, 'heng', 'chong', '700228025261', '79404269acf6a5af2f900e307cd876f98461ab46ea77d0ce747ea918994c25cc', 'a686e2098eb390a6a3866a6f02787d62e7542ad7', '1970-02-28', 'Malaysia', '0164456990', 'NO18, JALAN JINTAN 2, TAMAN SIMPANG PERDANA, SIMPANG EMPAT, ALOR SETAR, KEDAH.', '06650', '-', 'May Bank Berhad', '552022600405', 'Heng Cheng Chong', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-30 02:02:34', '2021-03-08 13:06:16'),
(84, 'c7b20dbc8b39d50703c15594073ce731', 'Reserve06', 'Reserve06@gmail.com', NULL, 'Reserve06', 'RC', '1612066619', '1a5bd5945a7f6308b575ed6fa374398ce73324c2b47ce1e9b6667d2cd504ebf7', '0f5711c4c0d952c15128e9c872551938ad36762f', '18-08-2008', 'SG', '01212332106', '123, ABC', '123333', 'PG', NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '100.50', '327.50', '0.00', '0.00', '0.00', '2021-01-31 04:16:59', '2022-04-19 07:04:18'),
(85, '6f687ff16c5606ddae9cd8616435a401', 'Reserve05', 'Reserve05@gmail.com', NULL, 'Reserve05', 'RC', '1612066624', '8367dea8059dd08729b24bad1fd07cf6b1c66c389172c1ce36e8f3a1d7c6e225', 'e052f6ac74511d7d50ea94f53f1551edcd9c2f86', '18-08-2008', 'SG', '01212332105', '123, ABC', '123333', 'PG', NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '0.00', '394.50', '0.00', '0.00', '0.00', '2021-01-31 04:17:04', '2022-04-19 07:04:18'),
(86, 'f79e2c7602cf0102e57dfdfcfcac0f00', 'Reserve04', 'Reserve04@gmail.com', NULL, 'Reserve04', 'RC', '1612066629', '413a05195a51af88158dd92ac168e9b61cb4f73b7df15015a24e371f2268af1e', '72824f78e8783cf5e7f1170a20babb667caa0a25', '18-08-2008', 'SG', '01212332104', '123, ABC', '123333', 'PG', NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '0.00', '394.50', '0.00', '0.00', '0.00', '2021-01-31 04:17:09', '2022-04-19 07:04:18'),
(87, '07b610a86ed7dc1a0cf9f484057066b4', 'Reserve03', 'Reserve03@gmail.com', NULL, 'Reserve03', 'RC', '1612066635', 'b8d5b4ee717f8a5c489e3038f485df5c14f3d598ace7e5fb3066143a86375cb9', 'b47db767a914230f7c0fec42cab45a77d7b14d04', '18-08-2008', 'SG', '01212332103', '123, ABC', '123333', 'PG', NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '0.00', '304.50', '0.00', '0.00', '0.00', '2021-01-31 04:17:15', '2022-04-19 07:04:16'),
(88, '1fc0abce45843733bd5aa6ca6886ae7f', 'Reserve02', 'Reserve02@gmail.com', NULL, 'Reserve02', 'RC', '1612066640', 'b36ec8efec63dc1431805fabf736105d7df5fae6f2c192deeaa5bd97a5e62b73', '258ffe79a0e80f111b551db0254f8aecc12f7344', '18-08-2008', 'SG', '01212332102', '123, ABC', '123333', 'PG', NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '0.00', '100.50', '0.00', '0.00', '0.00', '2021-01-31 04:17:20', '2022-04-19 07:04:16'),
(89, 'f37f95eef40574604133df0fb13d45d3', 'Reserve01', 'Reserve01@gmail.com', NULL, 'Reserve01', 'RC', '1612066645', '2a1dce4f3c3fbaa5c9e704696744a1acb59b7bc852c3666b074bba85d4c323d5', '8d7d94d1885857a7a974055fa6e7a241a9a649a0', '18-08-2008', 'SG', '01212332101', '123, ABC', '123333', 'PG', NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-01-31 04:17:25', '2021-01-31 04:17:25'),
(90, 'c230fcd66070a675120a862b20bf5ba3', 'khoo wei boon', 'feonkhoo83@gmail.com', NULL, 'Wei Boon', 'Khoo', '830806105622', 'a22f9cf896e6aa48e6a6307560c3c9d76316eb70f1d290e62c4f8c1560415d0a', '875f80f3db313de41e3b52473b9f853988025178', '1983-08-06', 'Malaysia', '0193301996', '102 Jalan Batu Nilam 23', '41200', '-', 'Maybank', '112111104478', 'Khoo Wei Boon', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-02-01 01:53:44', '2021-02-01 01:59:58'),
(91, 'ffc972deb0da39f8c212c03545cc630e', 'khiang928999', 'hengchengkhiang@gmail.com', NULL, 'Heng', 'Khiang', '680802025595', 'd422f9cca7e80b42783a70c0573e369fe7c8bc185e4f0031d1da76cb129b94db', '4c654c59d3c95332fad1d8ae555a36c42500d992', '1968-08-02', 'Malaysia', '0164229289', 'No,19,Jalan Jintan2,Taman Simpany Perdana, Simpany Empat.', '06650', '-', 'May Bank', '502027111405', 'Heng Cheng Khiang', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-02-01 02:14:43', '2021-02-01 13:44:41'),
(92, 'f5effae34bca678714b70c3b3f242dcc', 'Sealwan', 'rkiew22@gmail.com', NULL, 'Ramon', 'Kiew', '651223025811', '514f42279a0d9c8b27e44f5e0b645a4bf949bd254fd898d5776f2f885bc436d8', 'c474cc3de9d3ad7335f82e533e71e483e495751b', '1965-12-23', 'Malaysia', '0124996609', '72, Lorong Rajawali 6, Tree Residency', '11900', '-', 'MayBank', '157139983638', 'Kiew Siew Wan', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-02-01 07:32:20', '2021-02-01 13:45:09'),
(93, '551a9c6a0e9024722081bf498aeb4139', 'Kentng', 'kent930322@gmail.com', NULL, 'Ng', 'Chee Hou', '930322065095', 'e793b973d6964a0027fb51beafcf7f456cc168a5a2d21ee9832f311af9930191', '2ef8e60a64f26b2750e7c695845ee0252de8ce95', '1993-03-22', 'Malaysia', '0164100942', '10B-2-3, LGK KELICAP FORESTVILLE', '11900', '-', 'HONG LEONG BANK', '33750115015', 'NG CHEE HOU', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-02-03 12:41:56', '2021-02-03 15:28:17'),
(94, '800c92a46d2a5644e73b15c3433abc11', 'AppleHeng', 'henglp.apple@gmail.com', NULL, 'Heng', 'Peng', '660428025182', '1df219ee8ae8904f92d6fb57254505d5e19f1beb379bf62caeeec4251e2771af', '6952f3594b262459156eebc3df147d93fa6fc11c', '1966-04-28', 'Malaysia', '0162801837', '64A, Jalan Ismail Ghaney, Kampung Kasipillay, Jalan Ipoh.', '51200', '-', 'May Bank Berhad', '552022600405', 'Heng Cheng Chong', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-02-04 13:56:21', '2021-02-04 14:23:28'),
(95, '139a6ccf8cf334db4bbd7fd091c2342c', 'FishYee95', 'fishyee95@hotmail.com', NULL, 'Sern Yee', 'Teoh', '951026075574', '1d1e869f5445775ba56ef7ffda171cc328f0215c82faec0a97344e851f0dba56', '826cc21519c371c5a90cd7adc5682b754f47c6bb', '1995-10-26', 'Malaysia', '0164044446', '1-2-6 Penhill Apartment', '11500', '-', 'Maybank', '157120211282', 'Teoh Sern Yee', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-02-05 15:06:00', '2021-02-07 11:31:23'),
(96, '55fc16ed102e2e4e39cc0955a94358c1', 'Shaun2007', 'joshuahcyeoh@gmail.com', NULL, 'Shaun', 'Yeoh', '070517070479', '26af796585f9139e0b03efd4b3c3d6a033f1191cfc2b56060893b9c8d57aede3', '070b4142e2a22b0b2aa28119a79970506df8f4cc', '2007-05-17', 'Malaysia', '0102021727', '27U, Jalan Air Itam', '11500', '-', 'May Bank Berhad', '157139629204', 'Yeoh Hooi Chong', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-02-07 09:50:43', '2021-03-08 13:06:16'),
(97, 'c50f508a32116d5c5886aa011c9041ea', 'SLChoo', 'joshuahcyeoh@gmail.com', NULL, 'SIM', 'Choo', '741101026200', '61e82461c11591aab9440035e222396f1a6e1e9e57466c4ad4c38cc23779e21b', 'cd3b48664b9ff936d9b677ba84cfbbe12be999f0', '1957-01-01', 'Malaysia', '0164823132', 'Block1-12-12 Taman Kang Har Tong,', '11600', '-', 'CIMB', '7629429574', 'Sim Lay Choo', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-02-08 15:32:15', '2021-02-10 14:12:04'),
(98, '455e98a9858a6ca9ce5487793ea3b727', 'Chia Yi', 'lawcy23@yahoo.com', NULL, 'Law', 'Law', '720821075516', 'd90711611e7815fcaf90471f60d4fccc8d047a3cc312bcf773369ce6fea1b829', '2e8d19c15ce88a97ae34d41309404f963cd9ece1', '1972-08-21', 'Malaysia', '0165595039', '6-19-06, Lintang Kampung Melayu 2,', '11500', '-', 'CIMB', '7021401298', 'Law Chia Yi', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-02-10 11:42:55', '2021-03-08 13:06:16'),
(99, 'd78fb060a5cb21715f260615ee6602f5', 'Frankyng', 'frankynh7240@gmail.com', NULL, 'NG', 'WAI HOU', '990615065229', '9b5e72b10d5dc1028aa78625466c07708d711c4dd7434d440ce599e98191c1a3', 'f9f1f3134de719cac80f8d03a94750570bb5d40a', '1999-06-15', 'Malaysia', '0165937240', '10B-2-3 LGK KELICAP', '11900', '-', 'RHB', '10608500072717', 'LIM LAI FUN', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-02-15 13:41:26', '2021-02-15 15:05:00'),
(100, 'da48c3f17ad440f9b9eeba22188a4033', 'NgohSL', 'fennie227@gmail.com', NULL, 'NGOH', 'SING LOONG', '641122065129', 'f0c3af619b0076173f32cb568e0839cb8b6150952351941fe6cea7f27ca91e44', '1a06c26d400181d7955904073d8a258c601dc70d', '1964-11-22', 'Malaysia', '0164863371', 'NO 38,JLN ECO CASCADIA 6/8', '81100', '-', 'RHB', '10608500072717', 'LIM LAI FUN', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-02-15 13:50:44', '2021-02-26 13:32:02'),
(101, '1f0dcba6737c4f4268924c8f307a8060', 'Samng', 'fennie227@gmail.com', NULL, 'NG', 'CHON KEONG', '680522085331', 'a41416079e543c4782d1ba16ebbd1d8bdaf6806b731e9aa58c72b324d1357727', '99bf2d0b4d5bf7ac35b5067deddd5a122a0a28b5', '1968-05-22', 'Malaysia', '0169867718', 'NO 33,LRG Shahzan IM 1,TMN SHAHZAN', '25200', '-', 'RHB', '10608500072717', 'LIM LAI FUN', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-02-15 13:57:42', '2021-02-15 15:05:04'),
(102, 'a8e5dc692e5885b09153c52f4cc5fea3', 'Samanta', 'info.ceneric@gmail.com', NULL, 'Chew Hoon', 'See', '771009-07-5902', 'fbd42ec79a9dfc536d4a8215f513f61f303f88b5faef08fdc132aa69137611f6', 'e5d22444de1b6450ddfb15e6535d1835ac6e7ebc', '1977-10-09', 'Malaysia', '0164408940', '1A-21-3A, Solok Tanjung', '11200', '-', 'PUBLIC BANK BERHAD', '4308139913', 'See Chew Hoon', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-02-16 08:57:58', '2021-03-08 13:06:16'),
(103, 'ff2dd6f80318e02f79fc648780b542d1', 'Serlynnliew86', 'serlynn.liew86@gmail.com', NULL, 'Liew', 'Jim Wah', '860331355608', 'c3fb3449d44d022d08d97fb222404c7fec5a1708fe8890f204856cba8862eeba', '997148b730b7536d1ba209d1921f678bfa049205', '1986-03-31', 'Malaysia', '0127221323', '1-09-06,GL Garden', '11060', '-', 'Public Bank', '4485461603', 'Liew Jim Wah', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-02-16 09:30:36', '2021-02-21 14:33:17'),
(104, '7dacd6959cc2d2c9773d87103da5f93a', 'Eunice', 'eunice_lyra99@yahoo.com', NULL, 'Chang', 'Wen', '990402077656', '8e7b8b50ea19ab5a16e6fdcc23afff7d23dfd8bdd963383eecfc354e508f4d11', '7122c767f6216f699aa76271ee8cc51e6676a19c', '1999-04-02', 'Malaysia', '0102998573', '3-45 (Tanjung Villa) Jalan Bunga Tongkeng', '11200', '-', 'CIMB', '7072682295', 'Chang Yoong Wen', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-02-21 02:09:53', '2021-02-21 14:33:24'),
(105, 'f1bd6ee1c506b7d5e3729d94afcacd4b', 'Kia', 'ckia51@yahoo.com', NULL, 'Chang', 'Kia', '560630075337', '6be13acfb387adbebb98ea290220fe51cbd81e209146e6483d0afa41197a1650', 'abb2a3b91a731dfa641560e3a4fa43033c4573e0', '1956-06-30', 'Malaysia', '0195694573', '3-45 (Tanjung Villa) Jalan Bunga Tongkeng', '11200', '-', 'CIMB', '7019386642', 'Chang Chee Kia', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-02-21 02:18:21', '2021-02-21 14:05:53');
INSERT INTO `user` (`id`, `uid`, `username`, `email`, `fullname`, `firstname`, `lastname`, `icno`, `password`, `salt`, `birth_date`, `country`, `phone_no`, `address`, `zipcode`, `state`, `bank_name`, `bank_acc_number`, `bank_acc_name`, `login_type`, `user_type`, `rank`, `achieve_rank`, `first_order`, `order_status`, `sales_commission`, `redemption_point`, `sales_commission_month`, `redemption_point_month`, `wallet`, `date_created`, `date_updated`) VALUES
(106, 'f37c1aacc6dcadebd51ce04293810fa7', 'Teh Minnie', 'minnieteh99@gmail.com', NULL, 'Minnie', 'Teh', '990306075706', '31de7524d9c2a92b3b3e2db03c65bd581a9005c43b2f61f0709959aaef17cc8e', '1af05b6a028fdc7c3c535526ec68ad1eb77715d7', '1999-03-06', 'Malaysia', '0165595039', '27U, jalan Ayer Itam', '11500', '-', 'OCBC Bank', '7302512036', 'Teh Min Nee', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-02-21 12:51:59', '2021-02-21 14:06:55'),
(107, '534ee6bd02ea0f03a35994863ff4248a', 'Ivy', 'Ivksc@hotmail.com', NULL, 'Sew Chia', 'Kho', '780813025062', '71548fcee1539d8ec36153dd176ffd87f8d19675aade4269beab1585cc25a5f2', '9c3a966b4e6aa8b28b60676f61f6bf5bbce4fe70', '1978-08-13', 'Malaysia', '0127212682', '3 Lorong Perdana 1', '14100', '-', 'RHB BANK BERHAD', '10702800862192', 'Kho Sew Chia', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-02-21 15:05:02', '2021-03-16 09:46:52'),
(108, 'bfc365bfa23a58ed721fe4aca704e922', 'cheesc', 'skchee78@yahoo.com', NULL, 'chee', 'siew sean', '740201-02-6324', '05d8f341f5a9dd5713c9aef0fb4438f196dfa49ffd576cce109a4f66fe9c0458', 'd42366cf8e6336813fb929fb8b02358491804e4f', '1974-02-01', 'Malaysia', '0195911013', '58G jalan bangglo lada', '09100', '-', 'public bank', '4509132934', 'chee soon kok', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-02-23 05:42:19', '2021-02-25 14:55:51'),
(109, '2d934a03c162b5e06559e1c723c58d2e', 'Shangqian1120', 'shangqiangoh@gmail.com', NULL, 'Shang Qian', 'Goh', '961120075537', 'eebae1b5d4f5abfbf2bb252d767aed259e29b9ddd90fbe97b386ce81dc85ef0d', '83cb82ab0768966f7dc3b757f1eaffc3a2477838', '1996-11-20', 'Malaysia', '0186658128', '33-2-8 Persiaran Sungai Emas', '11100', '-', 'Maybank', '157148270648', 'Goh Shang Qian', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-02-23 10:57:58', '2021-03-08 13:06:16'),
(110, '90db509479cafe32422779b0a81d0064', 'Enggim1030', 'Shangqiangoh@gmail.com', NULL, 'Eng Gim', 'Tan', '611030075410', '2631bb236f8cd869f24e84d9f62f5fb7bb54237ac5c855fa9bfa44c7f1937f2f', '684f65d248d8f5142d6693cb4198f9faf11eb686', '1961-10-30', 'Malaysia', '0164910735', '33-2-8 Persiaran Sungai Emas', '11100', '-', 'Maybank', '157148270648', 'Goh Shang Qian', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-02-23 11:15:48', '2021-02-28 15:46:45'),
(111, 'ed9174a5cfb0fc8325f4e5507036e0b7', 'Limph', 'Skchee1978@gmail.com', NULL, 'Lim', 'phaik chin', '480410-02-5020', '6ddcc07e239584c6e9a888e4e9847253ba5f5227a61ef048e3208d4faba23e5b', '1c0bb18b0ed506cfb819954b5a37f91b03c57270', '1948-04-10', 'Malaysia', '0164228050', '3 Lorong Perdana 1', '14100', '-', 'Public bank', '4606942933', 'Lim phain chin', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-02-23 13:59:34', '2021-02-25 14:55:45'),
(112, 'abf3698d51034b6f24cae9f2fcfabb08', 'NgohSF', 'fennie227@gmail.com', NULL, 'Ngoh', 'Sing Foong', '611022-06-5159', '76572e3c090f72a6272d7e27dab0cc8cb39597325c36258eb26330885a6d14d8', 'a1fdc509ac014d6dbbb83619621a6559f0474875', '1961-10-22', 'Malaysia', '012-2895878', 'BLK E-206,APARTMENT IDAMAN,JALAN PJU 10/1', '47830', '-', 'RHB', '10608500072717', 'LIM LAI FUN', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-02-26 12:01:02', '2021-02-26 13:51:59'),
(113, '08412aab45cf5ee7fa7e0da25ce46ab8', 'SFNGOH', 'yshaun69@gmail.com', NULL, 'Ngoh', 'Foong', '611022065159', '9390bb6429ff413902128aca3288c6248632874129392a21afb3cd14cd0c9c15', '87e05716fc9ac0d186f08528999a35e938b580e4', '1961-10-22', 'Malaysia', '0122895878', 'BLK E-206 APARTMENT IDAMAN JALAN PJU 10/1 DAMANSARA DAMAI 47830 PETALING JAYA, SELANGOR', '47830', '-', 'PUBLIC ISLAMIC BANK', '4-9488415-21', 'Ngoh Sing Foong', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-03-03 15:04:18', '2021-03-03 15:31:04'),
(114, 'd0a0661528e10beed710f467137e1b97', 'Bklim0805', 'bklim991@gmail.com', NULL, 'Boon Keow', 'Lim', '700805075120', '51659865f6f620be66d3e59fbd4c641befde7a66c74d91583053a10ac2eeade1', '8e029a90e75e0402cd34acc3de96cdc586cd36b3', '1970-08-05', 'Malaysia', '0129397893', 'M-08-06 Desa Permai Indah', '11700', '-', 'AmBank', '8881002951839', 'Lim Boon Keow', 1, 1, 'Member', NULL, '0', NULL, '75.00', '7.50', '0.00', '0.00', '0.00', '2021-03-05 06:44:44', '2022-04-19 07:04:24'),
(115, '5c7309824e28d6b883c727d4b7c92be3', 'KL NAI', 'nick_nai83@yahoo.com', NULL, 'Keat Ling', 'Nai', '820131075437', '8a53ac9414c23555ccd2e14407f96dfc49365a68b303f4892611a9c3ac3d6cf7', '6f8c488246a15206c00a1732f0c3a599b7c98792', '1982-01-31', 'Malaysia', '0124189683', '25 Lorong Kijang 1', '09600', '-', 'CIMB', '7064649199', 'NAI KEAT HWA A/L NAI RON', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-03-10 13:48:55', '2021-03-10 13:57:34'),
(116, '74294b0c9e8c56e33888cef954af948c', 'Leekh', 'khlee7@gmail.com', NULL, 'Lee', 'Kh', '801007075305', '1353afee38d0bb133014776066b19e9c92657bc6bd893aff56c242025e91a20a', '7c8b5040ff364ce42b7f9cc8f0900149c2b18607', '1980-10-07', 'Malaysia', '0124548590', '1905', '11900', '-', 'maybank', '107246049720', 'lee kin hin', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-03-16 09:46:52', '2021-03-17 07:33:15'),
(117, '901ab8d882636fc5f04e086d767f62bc', 'Eena0405', 'miyci_xlove@hotmail.com', NULL, 'Eena', 'Koh', '9104051-07-5418', 'ecea53f037b329c1121775205c23d3774c101d441f9ca659f229ff6c8beb0874', '3508600e8f63988d55867df8a0acbb07a1cb6621', '1991-04-05', 'Malaysia', '01151093745', '33-15-13 Idaman Iris Apartment,Lilian Sungai Ara,', '11900', '-', 'Public Bank', '4536923328', 'Eena Koh', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-03-19 16:53:12', '2021-03-19 16:54:48'),
(118, 'f4e123e3b81f131140758f48a894d51e', 'junwei2', 'junwei0915@gmail.com', NULL, 'Ch&#039;ng', 'wei', '664464346', '1be0ff0443b752f0a3ed70c3f25876f418b519f82b684c954d5c658d7062a55a', '69564bf33aa3d7af606a1fc0cc09f3d977fd172f', '1987-03-22', 'Singapore', '0149110459', '..', '11655', '-', 'Maybank', '65456', '6446633', 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2021-03-22 06:09:05', '2021-03-22 06:10:38'),
(119, 'd884d3b3a1987061fcf4e1b80c0a9241', 'tester', 'tester@gmail.com', NULL, NULL, NULL, NULL, 'ec98ab6a20069e56b57f5de9d4dedce104a0b19c07c6f3bd6d19d2dfbe57c4b6', 'dcd28207e815318739d6e47bc8b5aa2d4df3c86d', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2022-02-08 04:09:04', '2022-02-08 04:09:04'),
(120, 'a68fbe04f13c6d9e906bcb0e422cd0a1', 'mike1', 'mike1@gmail.com', NULL, NULL, NULL, NULL, '51e352e9d2535bc7564ebb2b5399456a8b4188e3fb6cf970bddab15d3eefdf2d', 'd617fffbaf4785c6250fbc6066104e206a959f94', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '100.00', '10.00', '0.00', '0.00', '0.00', '2022-02-10 05:45:09', '2022-04-19 07:04:29'),
(121, '3208f7e0641debe426e374a74ae3a607', 'mike1.5', 'mike1.5@gmail.com', NULL, NULL, NULL, NULL, 'b7149c7bb47a6853aa81a3e44e332ccc64ded6e4fa2fbc74064b3179c12dc268', '7306c33e160d3dad63fc4042511cbc8c01906088', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '50.00', '5.00', '0.00', '0.00', '0.00', '2022-02-10 06:57:11', '2022-04-19 07:04:24'),
(122, 'e9140833d49ab185007f6b36e6c98ba7', 'mike2', 'mike2@gmail.com', NULL, NULL, NULL, NULL, 'e953bb41959dcd3325cb296c6880337c798aabd519d4b34d5b9b744056f1f420', 'b7bc3e7cca27b71288ac0df2f7f6589d0d8eaea6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2022-02-10 06:57:57', '2022-02-21 09:46:28'),
(123, 'a7e3cdba2b585f5e4fb622599bb325eb', 'mike3', 'mike3@gmail.com', NULL, NULL, NULL, NULL, 'aa3ad798febab785923afee6634ff1e57ec83f7420f3a4e856dba37a1bfda411', '5e773e9e52e4b420ef11fe41c99be1d9d7347c98', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2022-02-10 06:58:28', '2022-02-10 06:58:28'),
(124, '99a995b1f70685db86d72a4be10559e9', 'mike2.5', 'mike2.5@gmail.com', NULL, NULL, NULL, NULL, 'fca79ada778ef3094e413b03c4b55404f5219ff0e179c8b044b856dd0c0ad48f', 'abfc87e9e4f520cf6087225f3475fc2007e37476', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2022-02-10 06:59:28', '2022-02-10 06:59:28'),
(125, 'f852301f0d6f70cbbe689d298c9a0d29', 'mike3.5', 'mike3.5@gmail.com', NULL, NULL, NULL, NULL, '17dbe96988b22b296bef4a08fc40a9e78c8aa0c93057ac79034db09fc73764c2', 'c01a56e9d866ffc95f9f9dd1f0fae5aca4dae93c', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2022-02-10 06:59:47', '2022-02-10 06:59:47'),
(126, 'a59fb29f14526bccb6f723e00138b41a', 'mike4', 'mike4@gmail.com', NULL, NULL, NULL, NULL, '1c56eae8a48c4216565b3e99f4a41e196029c12e940a9713e5dd127c2b86205f', '87b9e5c4f4872438ab5c5cf47ee8a89a0a4bc273', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '2022-02-13 16:38:27', '2022-02-13 16:38:27'),
(127, 'c3263bccfb131fa6a68b8f98f7320b31', 'user two', 'user2@gmail.com', NULL, NULL, NULL, NULL, '91d7fbea92b9520a29bcf4e767a9dea24942611c19ae18a3a1d4cbd4c535e212', '81946ba5a1ddd46b45431a630c907e946ff0e301', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Member', NULL, '0', NULL, '25.00', '2.50', '0.00', '0.00', '0.00', '2022-04-13 04:58:56', '2022-04-19 07:04:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_address`
--
ALTER TABLE `billing_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bonus`
--
ALTER TABLE `bonus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bonus_sales_or_rebate`
--
ALTER TABLE `bonus_sales_or_rebate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bonus_star`
--
ALTER TABLE `bonus_star`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bonus_star_claim`
--
ALTER TABLE `bonus_star_claim`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `livestream`
--
ALTER TABLE `livestream`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ordering_list`
--
ALTER TABLE `ordering_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_list`
--
ALTER TABLE `order_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `preorder_list`
--
ALTER TABLE `preorder_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_commission`
--
ALTER TABLE `sales_commission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_commission_backup`
--
ALTER TABLE `sales_commission_backup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transfer_record`
--
ALTER TABLE `transfer_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upgrading`
--
ALTER TABLE `upgrading`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `billing_address`
--
ALTER TABLE `billing_address`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bonus`
--
ALTER TABLE `bonus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `bonus_sales_or_rebate`
--
ALTER TABLE `bonus_sales_or_rebate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `bonus_star`
--
ALTER TABLE `bonus_star`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `bonus_star_claim`
--
ALTER TABLE `bonus_star_claim`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `livestream`
--
ALTER TABLE `livestream`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ordering_list`
--
ALTER TABLE `ordering_list`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `order_list`
--
ALTER TABLE `order_list`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `preorder_list`
--
ALTER TABLE `preorder_list`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `referral_history`
--
ALTER TABLE `referral_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT for table `sales_commission`
--
ALTER TABLE `sales_commission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `sales_commission_backup`
--
ALTER TABLE `sales_commission_backup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `transfer_record`
--
ALTER TABLE `transfer_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `upgrading`
--
ALTER TABLE `upgrading`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
