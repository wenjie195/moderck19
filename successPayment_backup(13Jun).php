<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
// $_SESSION['url'] = $_SERVER['REQUEST_URI'];

$tempUid = $_SESSION['temp_uid'];
$uplineUid = $_SESSION['upline_uid'];

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Success Payment | MODERCK" />
<title>Success Payment  | MODERCK</title>
<?php include 'css.php'; ?>
</head>
<body class="body">

<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>

<div class="width100 same-padding fixed-bar">
	<?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
        <div class="table-padding width100 same-padding details-min-height padding-top2 overflow overflow-x">  
				
                <?php
                // echo $uid;
                $conn = connDB();
                $getUserRow = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
                if($getUserRow)
                {
                ?>
                
                <div class="thankyou-div text-center">
                	<img src="img/thank-you.png" class="thankyou-png">
                	<p class="thankyou-p gold-text">The payment is successfully make. Thank you! We will ship out the items to you in a few business days. Kindly check <b>order history</b> for the status.</p>
                      <a href="userPurchaseHistory.php">
                            <button class="clean yellow-btn edit-profile-width ow-margin-left0" style="margin-top:20px; width:100% !important;">
                                View Order History
                            </button>
                        </a>
				</div>

                <?php
                }
                else
                {
                ?>
                
                <!-- <div class="login-div margin-auto"> -->

                    <div class="thankyou-div text-center">
                        <img src="img/thank-you.png" class="thankyou-png">
                        <p class="thankyou-p gold-text">The payment is successfully make.<br>Please Register !</p>

                        <?php $uid ;?>
                        <?php $uplineUid ;?>

                    </div>

                    <div class="login-div margin-auto">

                        <!-- <form action="utilities/adminRegisterNewUserRenewFunction.php" method="POST"> -->
                        <form action="utilities/registerNewUserFunction.php" method="POST">

                        <div class="fake-input-div">
                            <input type="text" class="input-css clean icon-input dark-tur-text2" placeholder="Username" id="username" name="username" onChange="javascript:this.value=this.value.toLowerCase();" required>
                        </div>

                        <div class="clear"></div>

                        <div class="fake-input-div">
                            <input type="email" class="input-css clean icon-input dark-tur-text2" placeholder="Email" id="email" name="email" required>
                        </div>

                        <!-- new added input for contact -->
                        <div class="fake-input-div">
                            <input type="text" class="input-css clean icon-input dark-tur-text2" placeholder="Contact" id="contact" name="contact" required>
                        </div>

                        <div class="fake-input-div">
                            <input type="password" class="input-css clean password-input dark-tur-text2" placeholder="Password" id="password" name="password" required>
                            <img src="img/view.png" class="input-icon view-icon opacity-hover"  onclick="myFunctionA()" alt="Password" title="Password">
                        </div>

                        <div class="clear"></div>

                        <div class="fake-input-div">
                            <input type="password" class="input-css clean password-input dark-tur-text2" placeholder="Confirm Password" id="retype_password" name="retype_password" required>
                            <img src="img/view.png" class="input-icon view-icon opacity-hover"  onclick="myFunctionB()" alt="Password" title="Password">
                        </div>

                        <div class="clear"></div>

                        <!-- <div class="fake-input-div">
                            <input type="text" class="input-css clean icon-input dark-tur-text2" value="<?php //echo $userDetails[0]->getUsername();?>" readonly>
                        </div> -->

                        <input class="input-css clean icon-input dark-tur-text2" type="hidden" value="<?php echo $tempUid ;?>" id="user_uid" name="user_uid" readonly>
                        <input class="input-css clean icon-input dark-tur-text2" type="hidden" value="<?php echo $uplineUid ;?>" id="upline_uid" name="upline_uid" readonly>

                        <button class="clean white-button ow-red-bg white-text" name="submit">Register</button>

                        </form>

                    </div>

                <?php
                }
                ?>
        
                <!-- <div class="thankyou-div text-center">
                	<img src="img/thank-you.png" class="thankyou-png">
                	<p class="thankyou-p gold-text">The payment is successfully make. Thank you! We will ship out the items to you in a few business days. Kindly check <b>order history</b> for the status.</p>
                      <a href="userPurchaseHistory.php">
                            <button class="clean yellow-btn edit-profile-width ow-margin-left0" style="margin-top:20px; width:100% !important;">
                                View Order History
                            </button>
                        </a>
				</div> -->
                <div class="clear"></div>




        </div>
    </div>
</div>

<?php include 'js.php'; ?>

<script>
function myFunctionA()
{
    var x = document.getElementById("password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("retype_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>

</body>
</html>