<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Purchase Details | MODERCK" />
<title>Purchase Details | MODERCK</title>
<?php include 'css.php'; ?>
</head>
<body class="body">

<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Purchase Details</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    
        <div class="table-padding width100 same-padding details-min-height padding-top2 overflow overflow-x">

            <div class="width100 scroll-div">
                <table class="gold-table">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>DATE</th>
                            <!-- <th>REF NO</th> -->
                            <!-- <th>USERNAME</th> -->
                            <th>PRODUCT NAME</th>
                            <th>PRODUCT CODE</th>
                            <th>QUANTITY</th>
                            <th>UNIT PRICE (RM)</th>
                            <th>TOTAL AMOUNT (RM)</th>
                            <!-- <th>ACTION</th> -->
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        if(isset($_POST['order_id']))
                        {
                        $conn = connDB();
                        $orderDetails = getOrderList($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"s");
                        ?>

                            <?php
                            if($orderDetails)
                            {
                                for($cnt = 0;$cnt < count($orderDetails) ;$cnt++)
                                {
                                ?>

                                    <tr>
                                        <td><?php echo ($cnt+1)?></td>
                                        <td>
                                            <?php echo $date = date("d.m.Y",strtotime($orderDetails[$cnt]->getDateCreated()));?>
                                        </td>
                                        <td><?php echo $orderDetails[$cnt]->getProductName();?></td>
                                        <td>
                                            <?php 
                                                $productUid = $orderDetails[$cnt]->getProductUid();
                                                $conn = connDB();
                                                $productDetails = getProduct($conn," WHERE uid = ? ", array("uid") ,array($productUid),"s");
                                                echo $productDetails[0]->getCode();
                                                $conn->close();
                                            ?>
                                        </td>
                                        <td><?php echo $orderDetails[$cnt]->getQuantity();?></td>
                                        <td><?php echo $orderDetails[$cnt]->getOriginalPrice();?></td>
                                        <td><?php echo $orderDetails[$cnt]->getTotalPrice();?></td>
                                    </tr>

                                <?php
                                }
                            }
                            ?>  

                        <?php
                        }
                        ?>
                              
                    </tbody>
                </table>

                <h1 class="top-title brown-text">Delivery Infomation</h1>

                <?php
                if(isset($_POST['order_id']))
                {
                $conn = connDB();
                $orderInfo = getOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"s");
                ?>

                    <div class="dual-input">
                        <p class="top-p">Recipient Name</p>
                        <input type="text" class="line-input clean" placeholder="Recipient Name" value="<?php echo $orderInfo[0]->getName();?>" id="recipient_name" name="recipient_name" readonly>
                    </div>
                        
                    <div class="dual-input second-dual-input">  
                        <p class="top-p">Mobile Number</p>
                        <input type="text" class="line-input clean" placeholder="Mobile Number" value="<?php echo $orderInfo[0]->getContact();?>" id="mobile_no" name="mobile_no" readonly>
                    </div> 

                    <div class="clear"></div>   

                    <div class="dual-input">
                        <p class="top-p">Shipping Address</p>
                        <input type="text" class="line-input clean" value="<?php echo $orderInfo[0]->getAddressOne();?> <?php echo $orderInfo[0]->getPostcode();?> <?php echo $orderInfo[0]->getCity();?> <?php echo $orderInfo[0]->getState();?> <?php echo $orderInfo[0]->getCountry();?>" readonly>
                    </div>  

                    <div class="clear"></div>   

                    <?php 
                        $shippingStatus = $orderInfo[0]->getShippingStatus();
                        if($shippingStatus == 'Pending')
                        {
                        ?>
                        
                            <form action="utilities/adminUpdateShippingFunction.php" method="POST">

                                <div class="dual-input">
                                    <p class="top-p">Shipping Method</p>
                                    <input type="text" class="line-input clean" placeholder="Shipping Method" id="shipping_method" name="shipping_method" required>
                                </div>
                                    
                                <div class="dual-input second-dual-input">  
                                    <p class="top-p">Tracking Number</p>
                                    <input type="text" class="line-input clean" placeholder="Tracking Number" id="tracking_number" name="tracking_number" required>
                                </div>

                                <div class="clear"></div>   

                                <div class="dual-input">
                                    <p class="top-p">Shipping Date</p>
                                    <input type="date" class="line-input clean" placeholder="Shipping Date" id="shipping_date" name="shipping_date" required>
                                </div>
                                    
                                <input type="hidden" class="line-input clean" value="<?php echo $_POST['order_id'];?>" id="order_uid" name="order_uid" readonly>

                                <div class="text-center middle-div-width padding-bottom30">
                                    <button class="clean white-button ow-red-bg white-text" name="submit">Submit</button>
                                </div> 

                            </form>
                        
                        <?php
                        }
                        else
                        {
                        ?>
                        
                        <div class="dual-input">
                            <p class="top-p">Shipping Method</p>
                            <input type="text" class="line-input clean" value="<?php echo $orderInfo[0]->getShippingMethod();?>" readonly>
                        </div>
                            
                        <div class="dual-input second-dual-input">  
                            <p class="top-p">Tracking Number</p>
                            <input type="text" class="line-input clean" value="<?php echo $orderInfo[0]->getTrackingNumber();?>" readonly>
                        </div>

                        <div class="clear"></div>   

                        <div class="dual-input">
                            <p class="top-p">Shipping Date</p>
                            <input type="date" class="line-input clean" value="<?php echo $orderInfo[0]->getShippingDate();?>" readonly>
                        </div>

                        <?php
                        }
                    ?>

                <?php
                }
                ?>

            </div>

        </div>

    </div>
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>