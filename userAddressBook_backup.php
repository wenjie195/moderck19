<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Address.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$allAddress = getAddress($conn, "WHERE user_uid = ? AND status != 'Delete' ",array("user_uid"),array($uid),"s");
// $allAddress = getAddress($conn, "WHERE user_uid = ? ",array("user_uid"),array($uid),"s");

$allUser = getUser($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="My Address Book | MODERCK" />
<title>My Address Book | MODERCK</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">My Address Book</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    <div class="width100 same-padding min-height100 padding-top overflow overflow-x">
    		<div class="text-center middle-div-width">
            	<a href="userAddressBookAdd.php"><div  class="clean white-button ow-red-bg white-text smaller-button">Add</div></a> 
            </div> 
        <div class="width100 overflow-x">
            <table class="width100 gold-table ow-text-left-table">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Details</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                	<tr>
                    	<td>1</td>
                        <td>Emily Wong (Default Billing Address)<br>012 123 1234<br>9, Jalan Eco Botanic Taman Sri Tekum 11000 Klang
Selangor</td>
                        <td>                                        <form action="userAddressEdit.php" method="POST" class="left-form">
                                            <button class="clean transparent-button white-link" type="submit" name="item_uid" >
                                                <u>EDIT</u>
                                            </button>
                                        </form> 

                                        <form action="utilities/userAddressDeleteFunction.php" method="POST"  class="right-form" >
                                            <button class="clean transparent-button red-link2" type="submit" name="item_uid" >
                                                <u>DELETE</u>
                                            </button>
                                        </form> </td>
                    </tr>
                    <?php
                        if($allAddress)
                        {
                            for($cnt = 0;$cnt < count($allAddress) ;$cnt++)
                            {
                            ?>
                                <tr>
                                    <td><?php echo ($cnt+1)?></td>
                                    <td><?php echo $allAddress[$cnt]->getRecipient();?><br><?php echo $allAddress[$cnt]->getMobile();?><br>
                                        <?php echo $allAddress[$cnt]->getHouseRoad();?>&nbsp;
                                        <?php echo $allAddress[$cnt]->getPostcode();?>&nbsp;
                                        <?php echo $allAddress[$cnt]->getCity();?>&nbsp;
                                        <?php echo $allAddress[$cnt]->getState();?>&nbsp;
                                        
                                        <?php echo $allAddress[$cnt]->getCountry();?>
                                    </td>
                                    
                                    <td>
                                        <form action="userAddressEdit.php" method="POST" class="hover1">
                                            <button class="clean transparent-button" type="submit" name="item_uid" value="<?php echo $allAddress[$cnt]->getUid();?>">
                                                EDIT
                                            </button>
                                        </form> 

                                        <form action="utilities/userAddressDeleteFunction.php" method="POST" class="hover1">
                                            <button class="clean transparent-button" type="submit" name="item_uid" value="<?php echo $allAddress[$cnt]->getUid();?>">
                                                DELETE
                                            </button>
                                        </form> 
                                    </td>
                                </tr>
                            <?php
                            }
                        }
                    ?>   
                </tbody>
            </table>
        </div>


    </div>
    </div>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>