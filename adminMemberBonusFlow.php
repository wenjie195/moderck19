<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

// require_once dirname(__FILE__) . '/classes/Address.php';
require_once dirname(__FILE__) . '/classes/Bonus.php';
require_once dirname(__FILE__) . '/classes/BonusSalesOrRebate.php';
require_once dirname(__FILE__) . '/classes/BonusStar.php';
require_once dirname(__FILE__) . '/classes/SalesCommission.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $conn = connDB();
// $conn->close();

if(isset($_POST['item_uid']))
{
    $conn = connDB();

    $bonus = getBonus($conn,"WHERE receiver_uid = ? ", array("receiver_uid") ,array($_POST['item_uid']),"s");
    $bonusSalesorRebate = getBonusSalesOrRebate($conn,"WHERE receiver_uid = ? ", array("receiver_uid") ,array($_POST['item_uid']),"s");
    $bonusStar = getBonusStar($conn,"WHERE receiver_uid = ? ", array("receiver_uid") ,array($_POST['item_uid']),"s");
    $salesCommission = getSalesCommission($conn,"WHERE receiver_uid = ? ", array("receiver_uid") ,array($_POST['item_uid']),"s");
}

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="MEMBER BONUS FLOW | MODERCK" />
<title>MEMBER BONUS FLOW | MODERCK</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">BONUS FLOW</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
        <div class="width100 same-padding min-height100 padding-top overflow overflow-x">

		    <div class=" width100 scroll-div">
                <h1 class="top-title brown-text">Link / Level Bonus</h1>
                <table class="width100 gold-table">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>ORDER UID</th>
                            <th>PURCHASER</th>
                            <th>BONUS RECEIVER</th>
                            <th>AMOUNT</th>
                            <th>BONUS TYPE</th>
                            <th>DATE</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if($bonus)
                            {
                                for($cnt = 0;$cnt < count($bonus) ;$cnt++)
                                {
                                ?>
                                    <tr>
                                        <td><?php echo ($cnt+1)?></td>

                                        <td><?php echo $bonus[$cnt]->getOrderUid();?></td>
                                        <td><?php echo $bonus[$cnt]->getUsername();?></td>
                                        <td><?php echo $bonus[$cnt]->getReceiver();?></td>
                                        <td><?php echo $bonus[$cnt]->getAmount();?></td>
                                        <td><?php echo $bonus[$cnt]->getBonusType();?></td>

                                        <td>
                                            <?php echo $date = date("d.m.Y",strtotime($bonus[$cnt]->getDateCreated()));?>
                                        </td>
                                    </tr>
                                <?php
                                }
                            }
                        ?>  
                    </tbody>
                </table>
            </div>

		    <div class=" width100 scroll-div">
                <h1 class="top-title brown-text">Agent Sales Commission</h1>
                <table class="width100 gold-table">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>ORDER UID</th>
                            <th>PURCHASER</th>
                            <th>BONUS RECEIVER</th>
                            <th>AMOUNT</th>
                            <th>DATE</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if($salesCommission)
                            {
                                for($cnt = 0;$cnt < count($salesCommission) ;$cnt++)
                                {
                                ?>
                                    <tr>
                                        <td><?php echo ($cnt+1)?></td>
                                        <td><?php echo $salesCommission[$cnt]->getOrderUid();?></td>
                                        <td>
                                            <?php 
                                                $orderUid = $salesCommission[$cnt]->getOrderUid();
                                                $conn = connDB();        
                                                $ordersRow = getOrders($conn, " WHERE order_id = ? ",array("order_id"),array($orderUid),"s");
                                                $purchaserUid = $ordersRow[0]->getUid();
                                                $purchaserRows = getUser($conn, " WHERE uid = ? ",array("uid"),array($purchaserUid),"s");
                                                echo $purchaserRows[0]->getUsername();
                                            ?>
                                        </td>
                                        <td><?php echo $salesCommission[$cnt]->getUsername();?></td>
                                        <td><?php echo $salesCommission[$cnt]->getAmount();?></td>
                                        <td>
                                            <?php echo $date = date("d.m.Y",strtotime($salesCommission[$cnt]->getDateCreated()));?>
                                        </td>
                                    </tr>
                                <?php
                                }
                            }
                        ?>                                 
                    </tbody>
                </table>
            </div>

            <div class=" width100 scroll-div">
                <h1 class="top-title brown-text">First Purchase / Rebates</h1>
                <table class="width100 gold-table">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>PURCHASER</th>
                            <th>BONUS RECEIVER</th>
                            <th>AMOUNT</th>
                            <th>BONUS TYPE</th>
                            <th>DATE</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if($bonusSalesorRebate)
                            {
                                for($cnt = 0;$cnt < count($bonusSalesorRebate) ;$cnt++)
                                {
                                ?>
                                    <tr>
                                        <td><?php echo ($cnt+1)?></td>
                   
                                        <td>
                                            <?php 
                                                $orderUid = $bonusSalesorRebate[$cnt]->getOrderUid();
                                                $conn = connDB();        
                                                $ordersRow = getOrderList($conn, " WHERE order_id = ? ",array("order_id"),array($orderUid),"s");
                                                $purchaserUid = $ordersRow[0]->getUserUid();
                                                $purchaserRows = getUser($conn, " WHERE uid = ? ",array("uid"),array($purchaserUid),"s");
                                                echo $purchaserRows[0]->getUsername();
                                                // echo"  ( ";
                                                // echo $ordersRow[0]->getProductName();
                                                // echo" )  ";
                                            ?>
                                        </td>

                                        <td><?php echo $bonusSalesorRebate[$cnt]->getReceiver();?></td>
                                        <td><?php echo $bonusSalesorRebate[$cnt]->getAmount();?></td>
                                        <td><?php echo $bonusSalesorRebate[$cnt]->getBonusType();?></td>
                                        <td>
                                            <?php echo $date = date("d.m.Y",strtotime($bonusSalesorRebate[$cnt]->getDateCreated()));?>
                                        </td>
                                    </tr>
                                <?php
                                }
                            }
                        ?>                                 
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>