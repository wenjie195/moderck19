<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allLivestream = getLivestream($conn);
// $allUser = getUser($conn);
// $allUser = getUser($conn, " WHERE user_type = 1 ");
$allUser = getUser($conn, " WHERE user_type = 1 ORDER BY date_created DESC ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="MEMBER OVERVIEW | MODERCK" />
<title>MEMBER OVERVIEW | MODERCK</title>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">OVERVIEW</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    
    <div class="same-padding width100 min-sp-height overflow padding-top30 scroll-div padding-bottom30">

        <div class="search-big-div">
            <!-- <div class="fake-input-div overflow profile-h3"> -->
                <!-- <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search Username" class="clean pop-input fake-input"> -->
                <!-- <input type="text" class="input-css clean icon-input dark-tur-text2" onkeyup="myFunction()" placeholder="Search Username" id="myInput" name="myInput"> -->
            <!-- </div> -->

            <div class="dual-input">     
                <!-- <p class="top-p">Date</p>           -->
                <input type="text" class="line-input clean" onkeyup="myFunctionA()" placeholder="Search Date" id="myInputA" name="myInputA">
            </div>
            <div class="dual-input second-dual-input">  
                <!-- <p class="top-p">Referral</p>      -->
                <input type="text" class="line-input clean" onkeyup="myFunctionB()" placeholder="Search Referral" id="myInputB" name="myInputB">
            </div>  

            <div class="clear"></div>  

            <div class="dual-input">        
                <!-- <p class="top-p">Username</p>        -->
                <input type="text" class="line-input clean" onkeyup="myFunctionC()" placeholder="Search Username" id="myInputC" name="myInputC">
            </div>
            <!-- <div class="dual-input second-dual-input">  
                <p class="top-p">Account Type</p>     
                <input type="text" class="line-input clean" onkeyup="myFunctionD()" placeholder="Search Account Type" id="myInputD" name="myInputD">
            </div>  

            <div class="clear"></div>  

            <div class="dual-input">        
                <p class="top-p">Area</p>       
                <input type="text" class="line-input clean" onkeyup="myFunctionE()" placeholder="Search Area" id="myInputE" name="myInputE">
            </div> -->

            <div class="clear"></div>  

        </div>
		<div class=" width100 scroll-div">
            <table id="myTable" class="width100 gold-table">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>DATE OF REGISTRATION</th>
                        <!-- <th>PROMOTER USERNAME</th> -->
                        <th>REFERRAL</th>
                        <th>USERNAME</th>
                        <th>EMAIL</th>
                        <th>CONTACT</th>

                        <!-- <th>SALES COMMISSION</th>
                        <th>REDEMPTION POINT</th>
                        <th>BONUS FLOW</th> -->

                        <th>DATE OF LAST PURCHASE</th>
                        <!-- <th>1ST PURCHASE</th> -->
                        <th>DETAILS</th>
                        <th>RANK</th>
                        <th>ADD USER</th>
                    </tr>
                </thead>
                <!-- <tbody> -->
                <tbody id="myFilter">
               
                    <?php
                        if($allUser)
                        {
                            for($cnt = 0;$cnt < count($allUser) ;$cnt++)
                            {
                            ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td>
                                    <?php echo date("d.m.Y",strtotime($allUser[$cnt]->getDateCreated()));?>
                                </td>
                                <!-- <td> </td> -->

                                <td>
                                    <?php 
                                        $userUid = $allUser[$cnt]->getUid();
                                        $conn = connDB();

                                        $userRH = getReferralHistory($conn, " WHERE referral_id = ? ",array("referral_id"),array($userUid),"s");
                                        $referrerId = $userRH[0]->getReferrerId();

                                        $uplineDetails = getUser($conn, " WHERE uid = ? ",array("uid"),array($referrerId),"s");
                                        echo $uplineUsername = $uplineDetails[0]->getUsername();

                                        $conn->close();
                                    ?>
                                </td>

                                <td><?php echo $allUser[$cnt]->getUsername();?></td>
                                <td><?php echo $allUser[$cnt]->getEmail();?></td>
                                <td><?php echo $allUser[$cnt]->getPhoneNo();?></td>


                                <!-- <td><?php //echo $allUser[$cnt]->getSalesCommission();?></td> -->
                                <!-- <td><?php //echo $allUser[$cnt]->getRedemptionPoint();?></td> -->

                                <!-- <td>
                                    <form action="adminMemberBonusFlow.php" method="POST" class="hover1" target="blank">
                                        <button class="clean dark-tur-link view-link" type="submit" name="item_uid" value="<?php //echo $allUser[$cnt]->getUid();?>">
                                           <u>View</u>
                                        </button>
                                    </form>
                                </td> -->

                                <!-- <td>-</td> -->

                                <td>
                                    <?php
                                        $userUid = $allUser[$cnt]->getUid();
                                        $conn = connDB();
                                        $ordersDetails = getOrders($conn, " WHERE uid = ? AND status = 'APPROVED' ORDER BY date_created DESC LIMIT 1",array("uid"),array($userUid), "s");
                                        // $ordersDetails = getOrders($conn, " WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($userUid), "s");
                                        if($ordersDetails)
                                        {
                                            echo $latestOrders = date('d.m.Y',strtotime($ordersDetails[0]->getDateCreated()));
                                        }
                                        else
                                        {
                                            echo $latestOrders = "-";
                                        }
                                    ?>
                                </td>

                                <td>
                                    <form action="adminMemberProfile.php" method="POST" class="hover1">
                                        <button class="clean dark-tur-link view-link" type="submit" name="item_uid" value="<?php echo $allUser[$cnt]->getUid();?>">
                                           <u>View</u>
                                        </button>
                                    </form> 
                                </td>

                                <td>
                                    <!-- <form action="#" method="POST" class="hover1"> -->
                                    <?php 
                                        $rank = $allUser[$cnt]->getRank();
                                        if($rank == "Agent")
                                        {
                                            echo $rank;
                                        }
                                        else
                                        {
                                        ?>
                                        
                                            <!-- <form action="utilities/adminUpgradeAgentStatusFunction.php" method="POST" class="hover1">
                                                <button class="clean dark-tur-link view-link" type="submit" name="item_uid" value="<?php //echo $allUser[$cnt]->getUid();?>">
                                                <u>Upgrade</u>
                                                </button>
                                            </form>  -->

                                        <form id="action-form-<?= $cnt+1 ?>" action="utilities/adminUpgradeAgentStatusFunction.php" method="post" class="right-form">
                                            <input type="hidden" name="item_uid" value="<?php echo $allUser[$cnt]->getUid();?>">
                                            <button type="button" name="delete" onclick="upgradeRanking(<?= $cnt+1 ?>)" class="btn-danger clean dark-tur-link view-link">
                                                <u>Upgrade</u>
                                            </button>
                                        </form>

                                        <?php
                                        }
                                    ?>
                                </td>
                            
                                <td>
                                    <form action="adminAddNewUser.php" method="POST" class="hover1">
                                        <button class="clean dark-tur-link view-link" type="submit" name="user_uid" value="<?php echo $allUser[$cnt]->getUid();?>">
                                           <u>Add</u>
                                        </button>
                                    </form> 
                                </td>
</tr>
                            <?php
                            }
                        }
                    ?>  
                     
                </tbody>
            </table>
		</div>
    </div>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
function myFunctionA() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputA");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionB() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputB");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionC() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputC");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<!-- <script>
function myFunctionC() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputD");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[4];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script> -->

<!-- <script>
function myFunctionC() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputE");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[5];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script> -->

<script>
function upgradeRanking(no){
    swal({
        title: "Ranking Updating",
        text: "Are you sure you want to upgrade this user to AGENT ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
        var x = $("#action-form-"+no);
        x.find('.btn-danger').attr('type','submit');
        x.find('.btn-danger').attr('onclick','');
        x.find('.btn-danger').click();
        }
    });
}
</script>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "New User Registered !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Please Register"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>