<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Cart.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
// $orderUid = $_SESSION['order_uid'];

function addBillingAddress($conn,$uid,$orderUID,$purchaserName,$mobileNoBill,$houseRoadBill,$cityBill,$stateBill,$postcodeBill,$countryBill,$notice)
{
     if(insertDynamicData($conn,"billing_address",array("uid","order_uid","recipient","mobile","house_road","city","state","postcode","country","notice"),
          array($uid,$orderUID,$purchaserName,$mobileNoBill,$houseRoadBill,$cityBill,$stateBill,$postcodeBill,$countryBill,$notice),"ssssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");

$id = getOrders($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1 ",array("uid"),array($uid),"s");
$orderID = $id[0]->getId();
$orderUID = $id[0]->getOrderId();

$paymentMethod = 'BILLPLZ';
$paymentStatus = 'WAITING';
// $paymentStatus = 'PENDING';
// $shippingStatus = 'PENDING';

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $conn = connDB();
    
    // $orderIDpass = ($_POST['uid']);
    $orderIDpass = rewrite($_POST["uid"]);

    $name = rewrite($_POST["recipient_name"]);
    $contactNo = rewrite($_POST["mobile_no"]);
    $houseRoad = rewrite($_POST["house_road"]);
    $postcode = rewrite($_POST["postcode"]);
    $city = rewrite($_POST["city"]);
    $state = rewrite($_POST["state"]);
    $country = rewrite($_POST["country"]);
    $subtotal = ($_POST["subtotal"]);

    $purchaserName = rewrite($_POST["purchaser_name"]);
    $mobileNoBill = rewrite($_POST["mobile_no_bill"]);
    $houseRoadBill = rewrite($_POST["house_road_bill"]);
    $cityBill = rewrite($_POST["city_bill"]);
    $stateBill = rewrite($_POST["state_bill"]);
    $postcodeBill = rewrite($_POST["postcode_bill"]);
    $countryBill = rewrite($_POST["country_bill"]);
    $notice = rewrite($_POST["notice"]);

    $adjustTotal = ($subtotal * 100);

    if(isset($_POST['shippingSubmit']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
    
        //echo "save to database";
        if($name)
        {
            array_push($tableName,"name");
            array_push($tableValue,$name);
            $stringType .=  "s";
        }
        if($contactNo)
        {
            array_push($tableName,"contact");
            array_push($tableValue,$contactNo);
            $stringType .=  "s";
        }
    
        if($houseRoad)
        {
            array_push($tableName,"address_one");
            array_push($tableValue,$houseRoad);
            $stringType .=  "s";
        }
        if($postcode)
        {
            array_push($tableName,"postcode");
            array_push($tableValue,$postcode);
            $stringType .=  "s";
        }
        if($city)
        {
            array_push($tableName,"city");
            array_push($tableValue,$city);
            $stringType .=  "s";
        }
        if($state)
        {
            array_push($tableName,"state");
            array_push($tableValue,$state);
            $stringType .=  "s";
        }
        if($country)
        {
            array_push($tableName,"country");
            array_push($tableValue,$country);
            $stringType .=  "s";
        }

        if($subtotal)
        {
            array_push($tableName,"subtotal");
            array_push($tableValue,$subtotal);
            $stringType .=  "d";
        }
        if($paymentMethod)
        {
            array_push($tableName,"payment_method");
            array_push($tableValue,$paymentMethod);
            $stringType .=  "s";
        }
        if($paymentStatus)
        {
            array_push($tableName,"payment_status");
            array_push($tableValue,$paymentStatus);
            $stringType .=  "s";
        }
    
        array_push($tableValue,$orderIDpass);
        $stringType .=  "s";
        $updateOrderDetails = updateDynamicData($conn,"orders"," WHERE id = ? ",$tableName,$tableValue,$stringType);
        if($updateOrderDetails)
        {
            // echo "<script>alert('Successfully added shipping details!');</script>";

            $orderListStatus = "Sold";

            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($orderListStatus)
            {
                array_push($tableName,"status");
                array_push($tableValue,$orderListStatus);
                $stringType .=  "s";
            }    
            array_push($tableValue,$orderUID);
            $stringType .=  "s";
            $updateBonusPool = updateDynamicData($conn,"order_list"," WHERE order_id = ? ",$tableName,$tableValue,$stringType);
            if($updateBonusPool)
            {
                if(addBillingAddress($conn,$uid,$orderUID,$purchaserName,$mobileNoBill,$houseRoadBill,$cityBill,$stateBill,$postcodeBill,$countryBill,$notice))
                {}
            }
            else
            {   echo "FAIL TO CLEAR ORDER LIST";    }

        }
        else
        {
            echo "<script>alert('Fail to add shipping details!');window.location='./shoppingCartCheckout.php'</script>"; 
            // header('Location: ../shoppingCartCheckout.php');
        }
    }
    else
    {
        echo "<script>alert('ERROR 2');window.location='./shoppingCartCheckout.php'</script>"; 
        // header('Location: ../shoppingCartCheckout.php');
    }    
}
else 
{
    header('Location: ../index.php');
}

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,2);
}else
{}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Shopping Cart | MODERCK" />
<title>Shopping Cart  | MODERCK</title>
<?php include 'css.php'; ?>
</head>
<body class="body">

<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Payment Details</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
        <div class="table-padding width100 same-padding details-min-height padding-top2 overflow overflow-x">  

            <form method="post" action="billplzpost.php">
                <!-- <div class="width100 overflow">  -->
                <div class="width100 overflow-x">

                    <div class="dual-input">  
                        <p class="top-p">Name</p>
                        <input class="line-input clean" type="text" id="name" name="name" value="<?php echo $name;?>" readonly>
                    </div>

                    <div class="dual-input second-dual-input">  
                        <p class="top-p">Phone</p>
                        <input class="line-input clean" type="text" id="mobile" name="mobile" value="<?php echo $contactNo;?>" readonly> 
                    </div>

                    <div class="clear"></div>

                    <div class="dual-input">  
                        <p class="top-p">Amount</p>
                        <input class="line-input clean" type="text" value="RM <?php echo $subtotal;?>" readonly> 
                    </div>

                    <div class="clear"></div>

                    <?php  
                        // $subtotal = $orderDetails[0]->getSubtotal();
                        // $adjustTotal = ($subtotal * 100);
                    ?>

                    <!-- <input class="line-input clean" type="hidden" id="email" name="email">  -->
                    <input class="line-input clean" type="hidden" id="email" name="email" value="<?php echo $userDetails[0]->getEmail();?>"> 
                    <input class="line-input clean" type="hidden" id="amount" name="amount" value="<?php echo $adjustTotal; ?>" readonly> 
                    <!-- <input class="line-input clean" type="hidden" id="reference_1_label" name="reference_1_label" value="Order ID" readonly>  -->
                    <input class="input-name clean" type="hidden" id="reference_1_label" name="reference_1_label" value="Order ID" readonly> 
                    <input class="line-input clean" type="hidden" id="reference_1" name="reference_1" value="<?php echo $orderID; ?>" readonly>
                    <!-- <input class="line-input clean" type="hidden" id="reference_2_label" name="reference_2_label" value="ID" readonly>  -->
                    <input class="input-name clean" type="hidden" id="reference_2_label" name="reference_2_label" value="Order UID" readonly> 
                    <input class="line-input clean" type="hidden" id="reference_2" name="reference_2" value="<?php echo $orderUID; ?>" readonly> 

                    <div class="clear"></div> 

                    <!-- <div class="width100 text-center extra-spacing-up-down">
                        <button class="green-button checkout-btn clean" name="submit">Proceed To Payment</button>
                    </div>  -->

                    <div class="center-div2">
                        <button class="clean yellow-btn edit-profile-width ow-margin-left0" style="margin-top:50px;" type="submit">
                            Proceed To Payment
                        </button>
                    </div>

                </div> 
            </form>

        </div>
    </div>
</div>

<?php include 'js.php'; ?>

</body>
</html>