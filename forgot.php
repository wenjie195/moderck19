<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $referrerUID = $_SESSION['uid'];

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/" />
<link rel="canonical" href="https://agentpnchc.com/" /> -->
<meta property="og:title" content="Forgot Password | MODERCK" />
<title>Forgot Password | MODERCK</title>



<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding min-height100 padding-top login-bg padding-top-bottom">
	 <div class="overflow width100 text-center">
    	<img src="img/logo.png" class="logo" alt="Moderck" title="Moderck">
	</div>
    <h1 class="h1 red-text text-center login-h1">Forgot Password</b></h1>
    <div class="login-div margin-auto">
        <form action="utilities/forgotPasswordFunction.php" method="POST">
                <div class="fake-input-div">
                    <input type="email" class="input-css input-css2 clean dark-tur-text2" placeholder="Email" id="email" name="email">
                </div>
                <div class="clear"></div>
                <div class="clear"></div>
                <button class="clean white-button ow-red-bg white-text">
                    Submit
                </button>
            </form>
        <div class="clear"></div>
        <p class="signup-p text-center"><a href="index.php" class="dark-tur-link signup-a">Login</a></p>
    </div>
</div>







<?php include 'js.php'; ?>
</body>
</html>