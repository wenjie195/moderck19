<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Bonus.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/SalesCommission.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allBonus = getBonus($conn, " ORDER BY date_created DESC");
$allBonus = getSalesCommission($conn, " ORDER BY date_created DESC");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Payout Report (Agent Sales Commission) | MODERCK" />
<title>Payout Report (Agent Sales Commission)  | MODERCK</title>
<?php include 'css.php'; ?>
</head>
<body class="body">

<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>

<div class="width100 same-padding fixed-bar">
	<!-- <h1 class="top-title brown-text">Payout Report</h1><?php //include 'header.php'; ?> -->
    <h1 class="top-title brown-text"><a href="adminReportPayout.php" class="color-a">Link / Level</a> | Agent Sales Commission | <a href="adminReportPayoutPurReb.php" class="color-a">1st Purchase / Rebates</a> | <a href="adminReportPayoutStar.php" class="color-a">Star Agent (2%)</a></h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    
        <div class="table-padding width100 same-padding details-min-height padding-top2 overflow overflow-x">

            <div class="width100 scroll-div">
                <table class="gold-table">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>ORDER UID</th>
                            <th>PURCHASER</th>
                            <th>BONUS RECEIVER</th>
                            <th>AMOUNT</th>
                            <!-- <th>BONUS TYPE</th> -->
                            <th>DATE</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if($allBonus)
                            {
                                for($cnt = 0;$cnt < count($allBonus) ;$cnt++)
                                {
                                ?>
                                    <tr>
                                        <td><?php echo ($cnt+1)?></td>

                                        <td><?php echo $allBonus[$cnt]->getOrderUid();?></td>

                                        <td>
                                            <?php 
                                                $orderUid = $allBonus[$cnt]->getOrderUid();
                                                $conn = connDB();        
                                                $ordersRow = getOrders($conn, " WHERE order_id = ? ",array("order_id"),array($orderUid),"s");
                                                $purchaserUid = $ordersRow[0]->getUid();
                                                $purchaserRows = getUser($conn, " WHERE uid = ? ",array("uid"),array($purchaserUid),"s");
                                                echo $purchaserRows[0]->getUsername();
                                            ?>
                                        </td>

                                        <td><?php echo $allBonus[$cnt]->getUsername();?></td>
                                        <td><?php echo $allBonus[$cnt]->getAmount();?></td>
                               

                                        <td>
                                            <?php echo $date = date("d.m.Y",strtotime($allBonus[$cnt]->getDateCreated()));?>
                                        </td>
                                    </tr>
                                <?php
                                }
                            }
                        ?>                                 
                    </tbody>
                </table>
            </div>

        </div>

    </div>
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>