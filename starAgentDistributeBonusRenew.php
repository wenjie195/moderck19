<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BonusStar.php';
require_once dirname(__FILE__) . '/classes/BonusStarClaim.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';


date_default_timezone_set('Asia/Kuala_Lumpur');
$monthYear = date('m/Y', time());
$starStatus = "Pending";

function starAgent($conn,$uidNo,$starAgentUid,$starAgentName,$starStatus,$bonusName)
{
     if(insertDynamicData($conn,"bonus_star_claim",array("uid","receiver_uid","receiver","status","bonus_type"),
     array($uidNo,$starAgentUid,$starAgentName,$starStatus,$bonusName),"sssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

$conn = connDB();

$totalProductValue = getOrders($conn, "WHERE remark = ? AND payment_status = 'Approved' ",array("remark"),array($monthYear),"s");
$totalSales = 0; // initital
if($totalProductValue)
{
  for($b=0; $b <count($totalProductValue) ; $b++)
  {
    $totalSales += $totalProductValue[$b]->getSubtotal();
  }
}

echo "Total Sales This Month : ";
echo $totalSales;
echo "<br>";echo "<br>";echo "<br>";

//new star agent 2% begin

$conn = connDB();
$allBonusStar = getBonusStar($conn, " WHERE status = 'Pending' ");
// $allBonusStar = getBonusStar($conn);
if($allBonusStar)
{
  for($cnt = 0;$cnt < count($allBonusStar) ;$cnt++)
  {

    $bonusUid = $allBonusStar[$cnt]->getUid();
    $starAgentUid = $allBonusStar[$cnt]->getReceiverUid();
    // echo $starAgentUid = $allBonusStar[$cnt]->getReceiverUid();
    $SADetails = getUser($conn, "WHERE uid = ?",array("uid"),array($starAgentUid), "s");
    $starAgentName = $SADetails[0]->getUsername();
    echo " SA Pre-Qualifier Name : ";
    echo $starAgentName;
    echo " , ";
    echo " Total PV :";
    echo $starAgentPV = $allBonusStar[$cnt]->getAmount();
    // echo " , ";
    echo "<br>";


    // CLEAR BONUS STAR STATUS BEGIN
    $renewBonusStatus = "Clear";

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    //echo "save to database";
    if($renewBonusStatus)
    {
      array_push($tableName,"status");
      array_push($tableValue,$renewBonusStatus);
      $stringType .=  "s";
    }
    array_push($tableValue,$bonusUid);
    $stringType .=  "s";
    $updateBonusStatus = updateDynamicData($conn,"bonus_star"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    if($updateBonusStatus)
    {

      // look for same group member that achieve Star Agent
      // $downlineDetails = getBonusStar($conn, " WHERE receiver = ? AND amount >= 2500 ",array("receiver"),array($starAgentUid), "s");
      $downlineDetails = getBonusStar($conn, "WHERE upline_uid = ?",array("upline_uid"),array($starAgentUid), "s");
      if($downlineDetails)
      {
        if($downlineDetails)
        {   
          $totalPVAvailible = count($downlineDetails);
        }
        else
        {   
          $totalPVAvailible = 0;  
        }

        if($totalPVAvailible == '1')
        {   
          echo " Downline PV : ";
          echo $downlinePV = $downlineDetails[0]->getAmount();
          echo " , Remain PV : ";
          echo $remainPV = $starAgentPV - $downlinePV;
          echo "<br>";

          if($remainPV >= '2500')
          {
            echo "Congratulations , ";
            echo $starAgentName;
            echo " | SA Status : ";
            echo " Qualified ! ";
            echo "<br>";

            $uidNo = md5(uniqid());
            $bonusPreNameOne = "Star Agent Achievement 2% (";
            $bonusPreNameTwo = "RM".$totalSales.")";
            $bonusName = $bonusPreNameOne.$bonusPreNameTwo;
            if(starAgent($conn,$uidNo,$starAgentUid,$starAgentName,$starStatus,$bonusName))
            {
              echo "Data Store To Claim";
              echo "<br>";
            }

            echo "<br>";echo "<br>";
          }
          else
          {
            echo " Bye ! ";
            echo "<br>";echo "<br>";
          }
        }
        else
        {   
          $downlinePV = 0;
          for($cntA = 0;$cntA < count($downlineDetails) ;$cntA++)
          {
            echo " Downline PV : ";
            echo $allDownlinePV = $downlineDetails[$cntA]->getAmount();
            echo " , ";
            $downlinePV += $downlineDetails[$cntA]->getAmount();
          }
          echo "Remain PV : ";
          echo $remainPV = $starAgentPV - $downlinePV;
          echo "<br>";

          if($remainPV >= '2500')
          {
            echo "Congratulations , ";
            echo $starAgentName;
            echo " | SA Status : ";
            echo " Qualified ! ";
            echo "<br>";

            $uidNo = md5(uniqid());
            $bonusPreNameOne = "Star Agent Achievement 2% (";
            $bonusPreNameTwo = "RM".$totalSales.")";
            $bonusName = $bonusPreNameOne.$bonusPreNameTwo;
            if(starAgent($conn,$uidNo,$starAgentUid,$starAgentName,$starStatus,$bonusName))
            {
              echo "Data Store To Claim";
              echo "<br>";
            }

            echo "<br>";echo "<br>";
          }
          else
          {
            echo " Bye ! ";
            echo "<br>";echo "<br>";
          }
        }
      }
      else
      {
        echo " No Downline , ";
        echo " Remain PV : ";
        echo $starAgentPV;
        echo "<br>";

        if($starAgentPV >= '2500')
        {
          echo "Congratulations , ";
          echo $starAgentName;
          echo " | SA Status : ";
          echo " Qualified ! ";
          echo "<br>";

          $uidNo = md5(uniqid());
          $bonusPreNameOne = "Star Agent Achievement 2% (";
          $bonusPreNameTwo = "RM".$totalSales.")";
          $bonusName = $bonusPreNameOne.$bonusPreNameTwo;
          if(starAgent($conn,$uidNo,$starAgentUid,$starAgentName,$starStatus,$bonusName))
          {
            echo "Data Store To Claim";
            echo "<br>";
          }

          echo "<br>";echo "<br>";
        }
        else
        {
          echo " Bye ! ";
          echo "<br>";echo "<br>";
        }
      }

    }
    else
    {
      echo "FAIL TO CLEAR BONUS STATUS !!";
    }
    // CLEAR BONUS STAR STATUS END


    // // look for same group member that achieve Star Agent
    // // $downlineDetails = getBonusStar($conn, " WHERE receiver = ? AND amount >= 2500 ",array("receiver"),array($starAgentUid), "s");
    // $downlineDetails = getBonusStar($conn, "WHERE upline_uid = ?",array("upline_uid"),array($starAgentUid), "s");
    // if($downlineDetails)
    // {
    //   if($downlineDetails)
    //   {   
    //     $totalPVAvailible = count($downlineDetails);
    //   }
    //   else
    //   {   
    //     $totalPVAvailible = 0;  
    //   }

    //   if($totalPVAvailible == '1')
    //   {   
    //     echo " Downline PV : ";
    //     echo $downlinePV = $downlineDetails[0]->getAmount();
    //     echo " , Remain PV : ";
    //     echo $remainPV = $starAgentPV - $downlinePV;
    //     echo "<br>";

    //     if($remainPV >= '2500')
    //     {
    //       echo "Congratulations , ";
    //       echo $starAgentName;
    //       echo " | SA Status : ";
    //       echo " Qualified ! ";
    //       echo "<br>";

    //       $uidNo = md5(uniqid());
    //       $bonusPreNameOne = "Star Agent Achievement 2% (";
    //       $bonusPreNameTwo = "RM".$totalSales.")";
    //       $bonusName = $bonusPreNameOne.$bonusPreNameTwo;
    //       if(starAgent($conn,$uidNo,$starAgentUid,$starAgentName,$starStatus,$bonusName))
    //       {
    //         echo "Data Store To Claim";
    //         echo "<br>";
    //       }

    //       echo "<br>";echo "<br>";
    //     }
    //     else
    //     {
    //       echo " Bye ! ";
    //       echo "<br>";echo "<br>";
    //     }
    //   }
    //   else
    //   {   
    //     $downlinePV = 0;
    //     for($cntA = 0;$cntA < count($downlineDetails) ;$cntA++)
    //     {
    //       echo " Downline PV : ";
    //       echo $allDownlinePV = $downlineDetails[$cntA]->getAmount();
    //       echo " , ";
    //       $downlinePV += $downlineDetails[$cntA]->getAmount();
    //     }
    //     echo "Remain PV : ";
    //     echo $remainPV = $starAgentPV - $downlinePV;
    //     echo "<br>";

    //     if($remainPV >= '2500')
    //     {
    //       echo "Congratulations , ";
    //       echo $starAgentName;
    //       echo " | SA Status : ";
    //       echo " Qualified ! ";
    //       echo "<br>";

    //       $uidNo = md5(uniqid());
    //       $bonusPreNameOne = "Star Agent Achievement 2% (";
    //       $bonusPreNameTwo = "RM".$totalSales.")";
    //       $bonusName = $bonusPreNameOne.$bonusPreNameTwo;
    //       if(starAgent($conn,$uidNo,$starAgentUid,$starAgentName,$starStatus,$bonusName))
    //       {
    //         echo "Data Store To Claim";
    //         echo "<br>";
    //       }

    //       echo "<br>";echo "<br>";
    //     }
    //     else
    //     {
    //       echo " Bye ! ";
    //       echo "<br>";echo "<br>";
    //     }
    //   }
    // }
    // else
    // {
    //   echo " No Downline , ";
    //   echo " Remain PV : ";
    //   echo $starAgentPV;
    //   echo "<br>";

    //   if($starAgentPV >= '2500')
    //   {
    //     echo "Congratulations , ";
    //     echo $starAgentName;
    //     echo " | SA Status : ";
    //     echo " Qualified ! ";
    //     echo "<br>";

    //     $uidNo = md5(uniqid());
    //     $bonusPreNameOne = "Star Agent Achievement 2% (";
    //     $bonusPreNameTwo = "RM".$totalSales.")";
    //     $bonusName = $bonusPreNameOne.$bonusPreNameTwo;
    //     if(starAgent($conn,$uidNo,$starAgentUid,$starAgentName,$starStatus,$bonusName))
    //     {
    //       echo "Data Store To Claim";
    //       echo "<br>";
    //     }

    //     echo "<br>";echo "<br>";
    //   }
    //   else
    //   {
    //     echo " Bye ! ";
    //     echo "<br>";echo "<br>";
    //   }
    // }

  }
}

//new star agent 2% end
?>