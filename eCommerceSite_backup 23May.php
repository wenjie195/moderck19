<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Address.php';
require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    addToCart();
    // createOrder($conn,$uid);
    header('Location: ./shoppingCart.php');
}

// $allLivestream = getLivestream($conn);
$allProduct = getProduct($conn, " WHERE status = 'Available' ");
$addressDetails = getAddress($conn,"WHERE user_uid = ? AND default_ship = 'Yes' ", array("user_uid") ,array($uid),"s");

$products = getProduct($conn, "WHERE status = 'Available' ");

$productListHtml = "";

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,1,true);
}else{
    if(isset($_POST['product-list-quantity-input'])){
        $productListHtml = createProductList($products,1,$_POST['product-list-quantity-input']);
    }else{
        $productListHtml = createProductList($products);
    }
}

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$currentStatus = $userDetails[0]->getStatus();

// add product bonus claim checking (begin)
$pendingBonus = getOrderList($conn, " WHERE user_uid = ? AND status = 'Sold' ",array("user_uid"),array($uid),"s");

//claim bonus for product one
$pendingBonusProductOne = getOrderList($conn, " WHERE user_uid = ? AND product_uid = '35117b1cb67c78efa860c4882c2e5745' AND status = 'Sold' ",array("user_uid"),array($uid),"s");
$pendingBonusProductTwo = getOrderList($conn, " WHERE user_uid = ? AND product_uid = '547297ffdb9f49d6e5ff1db5208f2530' AND status = 'Sold' ",array("user_uid"),array($uid),"s");
$pendingBonusProductThree = getOrderList($conn, " WHERE user_uid = ? AND product_uid = '712e761968f09667885752f04d48c77f' AND status = 'Sold' ",array("user_uid"),array($uid),"s");

// add product bonus claim checking (end)

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="All Product | MODERCK" />
<title>All Product | MODERCK</title>
<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>


    <?php
    if(!$addressDetails)
    {
    ?>

        <div id="main-start" class="main-start2">
            <div class="width100 inner-bg inner-padding">
                <h1 class="top-title brown-text">
                    <a href="userAddressBook.php" class="dark-tur-link">Please Update Address Book</a>
                </h1>
            </div>
        </div>

    <?php
    }
    else
    {
    ?>

        <div class="clear"></div>

        <?php
            if($pendingBonus)
            {
                // echo "pls claim bonus!";
            ?>

                <div id="main-start" class="main-start2">
                    <div class="width100 inner-bg inner-padding">
                        <!-- <h1 class="top-title brown-text">CLICK TO CLAIM BONUS</h1> -->
                        <h1 class="top-title brown-text">1ST PURCHASE BONUS / REBATES</h1>

                        <div class="text-center width100 overflow">
                        
                            <!-- <div class="text-center middle-div-width">
                                <a href="userFirstPurchaseOrRebatesProductOne.php">
                                    <div class="clean white-button ow-red-bg white-text smaller-button">Product 1</div>
                                </a> 
                            </div>  -->

                            <?php
                            if($currentStatus == 'Inactive')
                            {
                            ?>
                            
                                <div class="text-center middle-div-width">
                                    <div class="clean white-button ow-red-bg white-text smaller-button">Pls Wait for Reactivate</div>
                                </div> 
                            
                            <?php
                            }
                            else
                            {
                            ?>
                            

                            
                            <?php
                            }
                            ?>

                            <?php
                            if($pendingBonusProductOne)
                            {
                            ?>
                            
                                <div class="text-center middle-div-width">
                                    <a href="userFirstPurchaseOrRebatesProductOne.php">
                                        <!-- <a href="userFirstPurchaseOrRebatesProductOne.php" target="_blank"> -->
                                        <div class="clean white-button ow-red-bg white-text smaller-button">CLICK TO CLAIM BONUS</div>
                                    </a> 
                                </div> 

                            <?php
                            }
                            elseif($pendingBonusProductTwo)
                            {
                            ?>
                            
                                <div class="text-center middle-div-width">
                                    <a href="userFirstPurchaseOrRebatesProductTwo.php">
                                        <div class="clean white-button ow-red-bg white-text smaller-button">CLICK TO CLAIM BONUS</div>
                                    </a> 
                                </div> 
                            
                            <?php
                            }
                            elseif($pendingBonusProductThree)
                            {
                            ?>
                            
                                <div class="text-center middle-div-width">
                                    <a href="userFirstPurchaseOrRebatesProductThree.php">
                                        <div class="clean white-button ow-red-bg white-text smaller-button">CLICK TO CLAIM BONUS</div>
                                    </a> 
                                </div> 

                            <?php
                            }
                            ?>

                        </div> 

                    </div>
                </div>

            <?php
            }
            else
            {
                // echo "proceed to order";
            ?>

                <div class="width100 same-padding fixed-bar">
                    <h1 class="top-title brown-text">All Product</h1><?php include 'header.php'; ?>
                </div>
            
                <form method="POST">
                    <div id="main-start" class="main-start2">
                        <div class="width100 inner-bg inner-padding">
                            <div class="same-padding width100 min-sp-height overflow padding-top30 scroll-div button-padding-bottom">
                                <div class="width103 product-big-div">
                                    <?php echo $productListHtml; ?> 
                                    <div class="clear"></div>
                                    <div class="stay-bottom-height"></div>
                                </div>    
                            </div>
                        </div>
                    </div>

                    <div class="width100 fixed-bottom-div">
                        <button class="clean white-button ow-red-bg white-text cart-button">Add To Cart</button>
                    </div> 
                </form>
            
            <?php
            }
        ?>

    <?php
    }
    ?>

<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Bonus Claimed !"; 
        }
        // elseif($_GET['type'] == 2)
        // {
        //     $messageType = "Password Updated !"; 
        // }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
    $(".button-minus").on("click", function(e)
    {
        e.preventDefault();
        var $this = $(this);
        var $input = $this.closest("div").find("input");
        var value = parseInt($input.val());
        if (value > 1)
        {
            value = value - 1;
        } 
        else 
        {
            value = 0;
        }
        $input.val(value);
    });

    $(".button-plus").on("click", function(e)
    {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest("div").find("input");
    var value = parseInt($input.val());
    if (value < 100)
    {
        value = value + 1;
    }
    else
    {
        value = 100;
    }
    $input.val(value);
    });
</script>
</body>
</html>