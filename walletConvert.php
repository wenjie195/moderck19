<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';


$conn = connDB();

// $allUser = getUser($conn, " WHERE user_type = '1' ");
$allUser = getUser($conn, " WHERE user_type = '1' AND status = 'Active' ");
if($allUser)
{   
  for($cnt=0; $cnt <count($allUser) ; $cnt++)
  {
     echo $allUser[$cnt]->getUsername();
     echo "    ,    ";
     $userUid = $allUser[$cnt]->getUid();
     echo "Sales Commission : ";
     echo $salesCommission = $allUser[$cnt]->getSalesCommission();
     echo ", Redemption Point : ";
     echo $redemptionPoint = $allUser[$cnt]->getRedemptionPoint();
     echo "<br>";

     $tableName = array();
     $tableValue =  array();
     $stringType =  "";
     //echo "save to database";
     if($salesCommission)
     {
          array_push($tableName,"sales_commission_month");
          array_push($tableValue,$salesCommission);
          $stringType .=  "d";
     }
     if($redemptionPoint)
     {
          array_push($tableName,"redemption_point_month");
          array_push($tableValue,$redemptionPoint);
          $stringType .=  "d";
     }

     array_push($tableValue,$userUid);
     $stringType .=  "s";
     $announcementUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
     if($announcementUpdated)
     {
          echo "Success Convert";
          // echo "<br>";
          echo "    ,    ";

          // $renewSalesCommission = "0.00";
          // $renewRedemptionPoint = "0.00";
          $renewSalesCommission = "0";
          $renewRedemptionPoint = "0";
          
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          // if($renewSalesCommission)
          if($renewSalesCommission || !$renewSalesCommission)
          {
               array_push($tableName,"sales_commission");
               array_push($tableValue,$renewSalesCommission);
               $stringType .=  "d";
          }
          // if($renewRedemptionPoint)
          if($renewRedemptionPoint || !$renewRedemptionPoint)
          {
               array_push($tableName,"redemption_point");
               array_push($tableValue,$renewRedemptionPoint);
               $stringType .=  "d";
          }
     
          array_push($tableValue,$userUid);
          $stringType .=  "s";
          $announcementUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($announcementUpdated)
          {
               echo "Clear";
               echo "<br>";
          }
          else
          {
               echo "Fail";
          } 

     }
     else
     {
          echo "Fail";
     } 

  }
}

$conn->close();
?>