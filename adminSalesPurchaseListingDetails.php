<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BillingAddress.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/classes/Bonus.php';
require_once dirname(__FILE__) . '/classes/BonusSalesOrRebate.php';
require_once dirname(__FILE__) . '/classes/SalesCommission.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Purchase Details | MODERCK" />
<title>Purchase Details | MODERCK</title>
<?php include 'css.php'; ?>
</head>
<body class="body">

<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Purchase Details</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    
        <div class="table-padding width100 same-padding details-min-height padding-top2 overflow overflow-x">

            <div class="width100 scroll-div">
                <table class="gold-table">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>DATE</th>
                            <!-- <th>REF NO</th> -->
                            <!-- <th>USERNAME</th> -->
                            <th>PRODUCT NAME</th>
                            <th>PRODUCT CODE</th>
                            <th>QUANTITY</th>
                            <th>UNIT PRICE (RM)</th>
                            <th>TOTAL AMOUNT (RM)</th>
                            <!-- <th>ACTION</th> -->
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        if(isset($_POST['order_id']))
                        {
                        $conn = connDB();
                        $orderDetails = getOrderList($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"s");
                        ?>

                            <?php
                            if($orderDetails)
                            {
                                for($cnt = 0;$cnt < count($orderDetails) ;$cnt++)
                                {
                                ?>

                                    <tr>
                                        <td><?php echo ($cnt+1)?></td>
                                        <td>
                                            <?php echo $date = date("d.m.Y",strtotime($orderDetails[$cnt]->getDateCreated()));?>
                                        </td>
                                        <td><?php echo $orderDetails[$cnt]->getProductName();?></td>
                                        <td>
                                            <?php 
                                                $productUid = $orderDetails[$cnt]->getProductUid();
                                                $conn = connDB();
                                                $productDetails = getProduct($conn," WHERE uid = ? ", array("uid") ,array($productUid),"s");
                                                echo $productDetails[0]->getCode();
                                                $conn->close();
                                            ?>
                                        </td>
                                        <td><?php echo $orderDetails[$cnt]->getQuantity();?></td>
                                        <td><?php echo $orderDetails[$cnt]->getOriginalPrice();?></td>
                                        <td><?php echo $orderDetails[$cnt]->getTotalPrice();?></td>
                                    </tr>

                                <?php
                                }
                            }
                            ?>  

                        <?php
                        }
                        ?>
                              
                    </tbody>
                </table>

                <!-- <h1 class="top-title brown-text">Delivery Infomation</h1> -->
                <h1 class="top-title brown-text">Shipping Address</h1>
                <?php
                if(isset($_POST['order_id']))
                {
                $conn = connDB();
                $orderInfo = getOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"s");
                ?>

                    <div class="dual-input">
                        <p class="top-p">Recipient Name</p>
                        <input type="text" class="line-input clean" value="<?php echo $orderInfo[0]->getName();?>" readonly>
                    </div>
                        
                    <div class="dual-input second-dual-input">  
                        <p class="top-p">Mobile Number</p>
                        <input type="text" class="line-input clean" value="<?php echo $orderInfo[0]->getContact();?>" readonly>
                    </div> 

                    <div class="clear"></div>   

                    <div class="dual-input">
                        <p class="top-p">House No & Street</p>
                        <input type="text" class="line-input clean" value="<?php echo $orderInfo[0]->getAddressOne();?>" readonly>
                    </div>  

                    <div class="dual-input second-dual-input">  
                        <p class="top-p">Postcode</p>
                        <input type="text" class="line-input clean" value="<?php echo $orderInfo[0]->getPostcode();?>" readonly>
                    </div>  

                    <div class="clear"></div>      

                    <div class="dual-input">
                        <p class="top-p">City</p>
                        <input type="text" class="line-input clean" value="<?php echo $orderInfo[0]->getCity();?>" readonly>                
                    </div>  

                    <div class="dual-input second-dual-input">  
                        <p class="top-p">State</p>
                        <input type="text" class="line-input clean" value="<?php echo $orderInfo[0]->getState();?>" readonly>                    
                    </div>  

                    <div class="clear"></div>      

                    <div class="dual-input">
                        <p class="top-p">Country</p>     
                        <input type="text" class="line-input clean" value="<?php echo $orderInfo[0]->getCountry();?>" readonly>
                    </div>    

                <?php
                }
                ?>

                <div class="clear"></div>      

                <?php
                if(isset($_POST['order_id']))
                {
                    $conn = connDB();
                    $billingAddInfo = getBillingAddress($conn,"WHERE order_uid = ? ", array("order_uid") ,array($_POST['order_id']),"s");
                    if($billingAddInfo)
                    {
                    ?>
                        <h1 class="top-title brown-text">Billing Address</h1>

                        <div class="dual-input">
                            <p class="top-p">Recipient Name</p>
                            <input type="text" class="line-input clean" value="<?php echo $billingAddInfo[0]->getRecipient();?>" readonly>
                        </div>
                            
                        <div class="dual-input second-dual-input">  
                            <p class="top-p">Mobile Number</p>
                            <input type="text" class="line-input clean" value="<?php echo $billingAddInfo[0]->getMobile();?>" readonly>
                        </div> 

                        <div class="clear"></div>   

                        <div class="dual-input">
                            <p class="top-p">House No & Street</p>
                            <input type="text" class="line-input clean" value="<?php echo $billingAddInfo[0]->getHouseRoad();?>" readonly>
                        </div>  

                        <div class="dual-input second-dual-input">  
                            <p class="top-p">Postcode</p>
                            <input type="text" class="line-input clean" value="<?php echo $billingAddInfo[0]->getPostcode();?>"readonly>
                        </div>  

                        <div class="clear"></div>      

                        <div class="dual-input">
                            <p class="top-p">City</p>
                            <input type="text" class="line-input clean" value="<?php echo $billingAddInfo[0]->getCity();?>" readonly>                
                        </div>  

                        <div class="dual-input second-dual-input">  
                            <p class="top-p">State</p>
                            <input type="text" class="line-input clean" value="<?php echo $billingAddInfo[0]->getState();?>" readonly>                    
                        </div>  

                        <div class="clear"></div>      

                        <div class="dual-input">
                            <p class="top-p">Country</p>     
                            <input type="text" class="line-input clean" value="<?php echo $billingAddInfo[0]->getCountry();?>" readonly>
                        </div>    

                        <div class="clear"></div>      

                        <div class="dual-input">
                            <p class="top-p">Notice</p>     
                            <input type="text" class="line-input clean" value="<?php echo $billingAddInfo[0]->getNotice();?>" readonly>
                        </div>  

                    <?php
                    }
                    else
                    {
                    ?>
                        <h1 class="top-title brown-text">*No Billing Address*</h1>
                    <?php
                    }
                }
                ?>

            </div>

            <div class="clear"></div>

            <h1 class="top-title brown-text">Bonus Flow</h1>

            <div class="width100 scroll-div">
                <h1 class="top-title brown-text">Link / Level Bonus</h1>
                <table class="gold-table">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>BONUS RECEIVER</th>
                            <th>AMOUNT</th>
                            <th>BONUS TYPE</th>
                            <th>DATE</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        if(isset($_POST['order_id']))
                        {
                        $conn = connDB();
                        $bonusDetails = getBonus($conn," WHERE order_uid = ? ", array("order_uid") ,array($_POST['order_id']),"s");

                            if($bonusDetails)
                            {
                                for($cnt = 0;$cnt < count($bonusDetails) ;$cnt++)
                                {
                                ?>
                                
                                    <tr>
                                        <td><?php echo ($cnt+1)?></td>
                                        <td><?php echo $bonusDetails[$cnt]->getReceiver();?></td>
                                        <td><?php echo $bonusDetails[$cnt]->getAmount();?></td>
                                        <td><?php echo $bonusDetails[$cnt]->getBonusType();?></td>
                                        <td>
                                            <?php echo $date = date("d.m.Y",strtotime($bonusDetails[$cnt]->getDateCreated()));?>
                                        </td>
                                    </tr>

                                <?php
                                }
                            }
                        }
                        ?>
                                
                    </tbody>
                </table>
            </div>

            <div class="clear"></div>

            <div class="width100 scroll-div">
                <h1 class="top-title brown-text">Agent Sales Commission</h1>
                <table class="gold-table">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>BONUS RECEIVER</th>
                            <th>AMOUNT</th>
                            <!-- <th>BONUS TYPE</th> -->
                            <th>DATE</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        if(isset($_POST['order_id']))
                        {
                        $conn = connDB();
                            // $bonusDetails = getBonus($conn," WHERE order_uid = ? ", array("order_uid") ,array($_POST['order_id']),"s");
                            $salesCommission = getSalesCommission($conn,"WHERE order_uid = ? ", array("order_uid") ,array($_POST['order_id']),"s");
                            if($salesCommission)
                            {
                                for($cnt = 0;$cnt < count($salesCommission) ;$cnt++)
                                {
                                ?>
                                
                                    <tr>
                                        <td><?php echo ($cnt+1)?></td>
                                        <td><?php echo $salesCommission[$cnt]->getUsername();?></td>
                                        <td><?php echo $salesCommission[$cnt]->getAmount();?></td>
                         
                                        <td>
                                            <?php echo $date = date("d.m.Y",strtotime($salesCommission[$cnt]->getDateCreated()));?>
                                        </td>
                                    </tr>

                                <?php
                                }
                            }
                        }
                        ?>
                                
                    </tbody>
                </table>
            </div>

            <div class="clear"></div>

            <div class="width100 scroll-div">
                <h1 class="top-title brown-text">First Purchase / Rebates</h1>
                <table class="gold-table">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>BONUS RECEIVER</th>
                            <th>AMOUNT</th>
                            <!-- <th>BONUS TYPE</th> -->
                            <th>DATE</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        if(isset($_POST['order_id']))
                        {
                        $conn = connDB();
                            // $bonusDetails = getBonus($conn," WHERE order_uid = ? ", array("order_uid") ,array($_POST['order_id']),"s");
                            // $salesCommission = getSalesCommission($conn,"WHERE order_uid = ? ", array("order_uid") ,array($_POST['order_id']),"s");
                            $bonusSalesorRebate = getBonusSalesOrRebate($conn,"WHERE order_uid = ? ", array("order_uid") ,array($_POST['order_id']),"s");
                            if($bonusSalesorRebate)
                            {
                                for($cnt = 0;$cnt < count($bonusSalesorRebate) ;$cnt++)
                                {
                                ?>
                                
                                    <tr>
                                        <td><?php echo ($cnt+1)?></td>
                                        <td><?php echo $bonusSalesorRebate[$cnt]->getReceiver();?></td>
                                        <td><?php echo $bonusSalesorRebate[$cnt]->getAmount();?></td>
                        
                                        <td>
                                            <?php echo $date = date("d.m.Y",strtotime($bonusSalesorRebate[$cnt]->getDateCreated()));?>
                                        </td>
                                    </tr>

                                <?php
                                }
                            }
                        }
                        ?>
                                
                    </tbody>
                </table>
            </div>


        </div>

    </div>
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>