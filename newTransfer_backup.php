<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$date = date('Y-m-d');
$tsValue = time();

$uid = $_SESSION['uid'];
$last5Uid = substr($uid, -5);

$conn = connDB();

$userRows = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/" />
<link rel="canonical" href="https://agentpnchc.com/" /> -->
<meta property="og:title" content="ADD NEW TRANSFER | MODERCK" />
<title>ADD NEW TRANSFER | MODERCK</title>


<?php include 'css.php'; ?>
</head>

<body class="body">

<div class="width100 same-padding  min-height100 padding-top login-bg padding-top-bottom">

	<p class="text-center"></p>
    <h1 class="h1 red-text text-center login-h1"><br><b>Add New Transfer</b></h1>

    <div class="login-div margin-auto">
        <form action="utilities/addNewTransferFunction.php" method="POST">

            <div class="fake-input-div">
                <input type="text" class="input-css clean icon-input dark-tur-text2" value="<?php echo $date;?>" placeholder="Date" id="date" name="date" readonly>
            </div>

            <div class="clear"></div>

            <div class="fake-input-div">
                <input type="text" class="input-css clean icon-input dark-tur-text2" value="<?php echo $tsValue; echo $last5Uid;?>" placeholder="Ref No." id="reference_no" name="reference_no" readonly>
            </div>

            <div class="clear"></div>
            
            <div class="fake-input-div">
                <input type="text" class="input-css clean icon-input dark-tur-text2" value="<?php echo $userDetails->getUsername();?>" placeholder="Username" id="username" name="username" readonly>
            </div>

            <div class="clear"></div>

            <div class="fake-input-div">
                <input type="text" class="input-css clean icon-input dark-tur-text2" value="<?php echo $userDetails->getFullname();?>" placeholder="Fullname" id="fullname" name="fullname" readonly>
            </div>

            <div class="clear"></div>

            <div class="fake-input-div">
                <input type="text" class="input-css clean icon-input dark-tur-text2" placeholder="Transfer To" id="transfer_to" name="transfer_to" required>
            </div>

            <div class="clear"></div>

            <div class="fake-input-div">
                <select class="input-css clean icon-input dark-tur-text2" id="transfer_type" name="transfer_type" required>
                <option>Transfer Type</option>
                <option value="MYSC">MYSC</option>
                <option value="MYRP">MYRP</option>
            </select>
            </div>

            <div class="clear"></div>

            <div class="fake-input-div">
                <input type="text" class="input-css clean icon-input dark-tur-text2" placeholder="Transfer Amount" id="amount" name="amount" required>
            </div>


            <div class="clear"></div>
            
            <button class="clean white-button ow-red-bg white-text" name="submit">  Proceed to Transfer </button>

            <div class="clear"></div>

        </form>
    </div>
</div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Please Register"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Wrong Password";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>