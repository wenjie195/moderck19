<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Address.php';
require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/PreOrderList.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$allProduct = getProduct($conn, "WHERE status = 'Available' ");

// $purchasedProduct = getPreOrderList($conn, "WHERE status = 'Available' ");
$purchasedProduct = getPreOrderList($conn,"WHERE user_uid = ? AND status = 'Available' ", array("user_uid") ,array($uid),"s");
$purchasedProductUid = $purchasedProduct[0]->getProductUid();

if($purchasedProduct)
{   
    $totalPurchasedProduct = count($purchasedProduct);
}
else
{   $totalPurchasedProduct = 0;   }

// $unPurchasedProduct = getPreOrderList($conn, "WHERE status != 'Available' ");
$unPurchasedProduct = getPreOrderList($conn,"WHERE user_uid = ? AND status != 'Available' ", array("user_uid") ,array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="All Product | MODERCK" />
<title>All Product | MODERCK</title>
<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">All Product</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    
        <div class="same-padding width100 min-sp-height overflow padding-top30 scroll-div padding-bottom30">

        <!-- <div class="width100 scroll-div">
            <table class="width100 gold-table">
                <?php
                    if($allProduct)
                    {
                        for($cnt = 0;$cnt < count($allProduct) ;$cnt++)
                        {
                        ?>
                        <tr>
                            
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $allProduct[$cnt]->getName();?></td>
                            <td><?php echo $productUid = $allProduct[$cnt]->getUid();?></td>
                            <td><?php echo $allProduct[$cnt]->getCode();?></td>
                        </tr>
                        <?php
                        }
                    }
                ?>   
            </table>
		</div> -->

        <div class="width100 scroll-div">

            <?php
                // if($purchasedProduct)
                // {   
                //     $totalPurchasedProduct = count($purchasedProduct);
                // }
                // else
                // {   $totalPurchasedProduct = 0;   }
            ?>
            <!-- <h1 class="top-title brown-text"><?php echo $totalPurchasedProduct;?></h1> -->
            <h1 class="top-title brown-text">Purchased Product</h1>
            <table class="width100 gold-table">
                <?php
                    if($purchasedProduct)
                    {
                        for($cnt = 0;$cnt < count($purchasedProduct) ;$cnt++)
                        {
                        ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $purchasedProduct[$cnt]->getProductName();?></td>
                            <td><?php echo $productUid = $purchasedProduct[$cnt]->getProductUid();?></td>

                            <td>
                                <form method="POST" action="utilities/preOrderFunction.php">
                                    <input type="number" class="line-input clean" placeholder="Amount" id="quantity" name="quantity" required>    
                                    <button class="clean transparent-button white-link" type="submit" name="item_uid" value="<?php echo $purchasedProduct[$cnt]->getProductUid();?>">
                                        <u>Add To Cart</u>
                                    </button>
                                </form> 
                            </td>

                        </tr>
                        <?php
                        }
                    }
                ?>   
            </table>
		</div>
        
        <div class="width100 scroll-div">
            <h1 class="top-title brown-text">Unpurchased Product</h1>
            <table class="width100 gold-table">
                <?php
                    if($unPurchasedProduct)
                    {
                        for($cnt = 0;$cnt < count($unPurchasedProduct) ;$cnt++)
                        {
                        ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $unPurchasedProduct[$cnt]->getProductName();?></td>
                            <td><?php echo $productUid = $unPurchasedProduct[$cnt]->getProductUid();?></td>

                            <td>
                                <form method="POST" action="utilities/preOrderFunction.php">
                                    <!-- <input type="number" class="line-input clean" placeholder="Amount" id="quantity" name="quantity" required>     -->
                                    <button class="clean transparent-button white-link" type="submit" name="item_uid" value="<?php echo $unPurchasedProduct[$cnt]->getProductUid();?>">
                                        <u>BUY NOW</u>
                                    </button>
                                </form> 
                            </td>


                        </tr>
                        <?php
                        }
                    }
                ?>   
            </table>
		</div>

		</div>

    </div>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>