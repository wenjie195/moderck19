<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

$allOrders = getOrders($conn, " WHERE uid = ? ",array("uid"),array($uid),"s");
// $allOrders = getOrders($conn, " WHERE uid = ? AND status = 'Pending' ",array("uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Purchase History | MODERCK" />
<title>Purchase History | MODERCK</title>
<?php include 'css.php'; ?>
</head>
<body class="body">

<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Purchase History</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    
        <div class="table-padding width100 same-padding details-min-height padding-top2 overflow overflow-x">

        <div class="search-big-div">
            <div class="dual-input">     
                <!-- <p class="top-p">Date</p>           -->
                <input type="text" class="line-input clean" onkeyup="myFunctionA()" placeholder="Search Date (D/M/Y)" id="myInputA" name="myInputA">
            </div>
            <div class="dual-input second-dual-input">  
                <!-- <p class="top-p">Referral</p>      -->
                <input type="text" class="line-input clean" onkeyup="myFunctionB()" placeholder="Search Ref No" id="myInputB" name="myInputB">
            </div>  

            <div class="clear"></div>  

        </div>

            <div class="width100 scroll-div">
                <table id="myTable" class="gold-table">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>DATE</th>
                            <th>REF NO</th>
                            <th>USERNAME</th>
                            <th>TOTAL AMOUNT (RM)</th>
                            <th>PAYMENT REFERENCE</th>
                            <th>PAYMENT STATUS</th>
                            <th>DELIVERY STATUS</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if($allOrders)
                            {
                                for($cnt = 0;$cnt < count($allOrders) ;$cnt++)
                                {
                                ?>
                                    <tr>
                                        <td><?php echo ($cnt+1)?></td>
                                        <td>
                                            <?php echo $date = date("d.m.Y H:i:s",strtotime($allOrders[$cnt]->getDateCreated()));?>
                                        </td>

                                        <td>
                                            INV <?php echo $date = date("Ymd",strtotime($allOrders[$cnt]->getDateCreated()));?><?php echo $allOrders[$cnt]->getId();?>
                                        </td>

                                        <td><?php echo $allOrders[$cnt]->getName();?></td>
                                        <td><?php echo $allOrders[$cnt]->getSubtotal();?></td>
                                        <td><?php echo $allOrders[$cnt]->getPaymentReference();?></td>
                                        <td><?php echo $allOrders[$cnt]->getPaymentStatus();?></td>
                                        <td><?php echo $allOrders[$cnt]->getShippingStatus();?></td>

                                        <td>
                                            <!-- <form> -->
                                            <form method="POST" action="userPurchaseDetails.php">
                                                <button class="clean dark-tur-link view-link" type="submit" name="order_id" value="<?php echo $allOrders[$cnt]->getOrderId();?>">
                                                    <u>View</u>
                                                </button>
                                            </form>
                                        </td>

                                    </tr>
                                <?php
                                }
                            }
                        ?>                                 
                    </tbody>
                </table>
            </div>

        </div>

    </div>
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>

<script>
function myFunctionA() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputA");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionB() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputB");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

</body>
</html>