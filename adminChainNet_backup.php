<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';


require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $uid = $_SESSION['uid'];
$presetUid = "70691b70ee884b1439e8a73ce33c0eef";
// echo $presetUid = "70691b70ee884b1439e8a73ce33c0eef";

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($presetUid),"s");
$userData = $userDetails[0];

$userRHDetails = getReferralHistory($conn, " WHERE referral_id =? ", array("referral_id"), array($presetUid), "s");
$userLevel = $userRHDetails[0];

// $getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);
$getWho = getWholeDownlineTree($conn, $presetUid,false);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Chain Net | MODERCK" />
<title>Chain Net | MODERCK</title>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Chain Net</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    <div class="width100 same-padding min-height100 padding-top overflow overflow-x">

        <!-- <div class="text-center middle-div-width">
          <a href="#"><div  class="clean white-button ow-red-bg white-text smaller-button">Add</div></a> 
        </div>  -->

            <?php $level = $userLevel->getCurrentLevel();?>

        <div class="width100 overflow-x">
            <table class="width100 gold-table ow-text-left-table">
                <thead>
                    <tr>
                        <!-- <th>S/N</th> -->
                        <th>LINK</th>
                        <th>REFERRAL</th>
                        <th>USERNAME</th>
                        <th>RANK</th>

                        <th>PV</th>
                        <th>GPV</th>

                        <th>STATUS</th>
                    </tr>
                </thead>
                <tbody>
                    
        <?php
        $conn = connDB();
        if($getWho)
        {
          for($cnt = 0;$cnt < count($getWho) ;$cnt++)
          {
            $totalDownline = 0;
            $downline = 0;
            $directDownline = 0;
            $downlineCurrentLvl = 0;
            $totalDirectDownline = 0;
            $downline = $getWho[$cnt]->getCurrentLevel();
            $directDownline = $downline + 1;

            $downlineUid = $getWho[$cnt]->getReferralId();
            $getWhoII = getWholeDownlineTree($conn,$downlineUid,false);
            if ($getWhoII)
            {
              for ($i=0; $i <count($getWhoII) ; $i++)
              {
                $allDownlineUid = $getWhoII[$i]->getReferralId();
                $downlineCurrentLvl = $getWhoII[$i]->getCurrentLevel();
                if ($directDownline == $downlineCurrentLvl)
                {
                  $totalDirectDownline++;
                }
              }
            }
            ?>

            <tr>
              <td>
                <?php
                  $downlineLvl = $getWho[$cnt]->getCurrentLevel();
                  echo $lvl = $downlineLvl - $level;
                ?>
              </td>

              <td>
                <?php
                  $uplineUid = $getWho[$cnt]->getReferrerId();
                  $uplineDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uplineUid), "s");
                  echo $uplineUsername = $uplineDetails[0]->getUsername();
                ?>
              </td>

              <td>
                <?php
                    $userUid = $getWho[$cnt]->getReferralId();
                    $thisUserDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($userUid), "s");
                    echo $username = $thisUserDetails[0]->getUsername();
                ?>
              </td>

              <td><?php echo $thisUserDetails[0]->getRank();?></td>

              <td></td>
              <td></td>
              <td></td>

            </tr>
          <?php
          }
          ?>
        <?php
        }
        $conn->close();
        ?>

                </tbody>
            </table>
        </div>


    </div>
    </div>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
function checkOrder(no){
    swal({
        title: "Are you sure?",
        text: "Once approve, you will not be able to recover this Order!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
        var x = $("#action-form-"+no);
        x.find('.btn-success').attr('type','submit');
        x.find('.btn-success').attr('onclick','');
        x.find('.btn-success').click();
        }
    });
}
function deleteOrder(no){
    swal({
        title: "Deleting Address",
        text: "Are you sure you want to delete the selection ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
        var x = $("#action-form-"+no);
        x.find('.btn-danger').attr('type','submit');
        x.find('.btn-danger').attr('onclick','');
        x.find('.btn-danger').click();
        }
    });
}
</script>

</body>
</html>