<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
// require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BonusSalesOrRebate.php';
require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/OrderingList.php';
require_once dirname(__FILE__) . '/classes/Product.php';
// require_once dirname(__FILE__) . '/classes/PreOrderList.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$productOneUid = '35117b1cb67c78efa860c4882c2e5745';
$productOneName = 'Verju Foryn Deer Placenta (30s)';
// $status = 'Pending';

// $productValue = '125';
// $redemptionPoint = '125';

$productDetails = getProduct($conn, " WHERE uid = ? ",array("uid"),array($productOneUid),"s");
$productValue = $productDetails[0]->getProductValue();
$redemptionPoint = $productDetails[0]->getRedemptionPoint();

function addProductOne($conn,$userUid,$productOneUid,$productOneName,$item1Amount)
{
     if(insertDynamicData($conn,"ordering_list",array("user_uid","product_uid","product_name","quantity"),
          array($userUid,$productOneUid,$productOneName,$item1Amount),"ssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

// // // first order / rebates (upline) begin 
// function firstOrderProductValueUpline($conn,$bonusUid,$receiverUid,$receiverName,$pvAmount,$pvName)
// {
//      if(insertDynamicData($conn,"bonus_sales_or_rebate",array("uid","receiver_uid","receiver","amount","bonus_type"),
//      array($bonusUid,$receiverUid,$receiverName,$pvAmount,$pvName),"sssss") === null)
//      {
//           return false;
//      }
//      else
//      {}
//      return true;
// }

// function firstOrderRedemptionPointUpline($conn,$bonusUid,$receiverUid,$receiverName,$rpAmount,$rpName)
// {
//      if(insertDynamicData($conn,"bonus_sales_or_rebate",array("uid","receiver_uid","receiver","amount","bonus_type"),
//      array($bonusUid,$receiverUid,$receiverName,$rpAmount,$rpName),"sssss") === null)
//      {
//           return false;
//      }
//      else
//      {}
//      return true;
// }
// // // first order / rebates (upline) end

// // // first order / rebates (purchaser) begin 
// function productValueRebates($conn,$bonusUid,$receiverUid,$receiverName,$pvRebateAmount,$pvRebateName)
// {
//      if(insertDynamicData($conn,"bonus_sales_or_rebate",array("uid","receiver_uid","receiver","amount","bonus_type"),
//      array($bonusUid,$receiverUid,$receiverName,$pvRebateAmount,$pvRebateName),"sssss") === null)
//      {
//           return false;
//      }
//      else
//      {}
//      return true;
// }

// function redemptionPointRebates($conn,$bonusUid,$receiverUid,$receiverName,$rpRebateAmount,$rpRebateName)
// {
//      if(insertDynamicData($conn,"bonus_sales_or_rebate",array("uid","receiver_uid","receiver","amount","bonus_type"),
//      array($bonusUid,$receiverUid,$receiverName,$rpRebateAmount,$rpRebateName),"sssss") === null)
//      {
//           return false;
//      }
//      else
//      {}
//      return true;
// }
// // // first order / rebates (purchaser) end

// // first order / rebates (upline) begin 
function firstOrderProductValueUpline($conn,$bonusUid,$receiverUid,$receiverName,$pvAmount,$pvName,$orderUid)
{
     if(insertDynamicData($conn,"bonus_sales_or_rebate",array("uid","receiver_uid","receiver","amount","bonus_type","order_uid"),
     array($bonusUid,$receiverUid,$receiverName,$pvAmount,$pvName,$orderUid),"ssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function firstOrderRedemptionPointUpline($conn,$bonusUid,$receiverUid,$receiverName,$rpAmount,$rpName,$orderUid)
{
     if(insertDynamicData($conn,"bonus_sales_or_rebate",array("uid","receiver_uid","receiver","amount","bonus_type","order_uid"),
     array($bonusUid,$receiverUid,$receiverName,$rpAmount,$rpName,$orderUid),"ssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
// // first order / rebates (upline) end

// // first order / rebates (purchaser) begin 
function productValueRebates($conn,$bonusUid,$receiverUid,$receiverName,$pvRebateAmount,$pvRebateName,$orderUid)
{
     if(insertDynamicData($conn,"bonus_sales_or_rebate",array("uid","receiver_uid","receiver","amount","bonus_type","order_uid"),
     array($bonusUid,$receiverUid,$receiverName,$pvRebateAmount,$pvRebateName,$orderUid),"ssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function redemptionPointRebates($conn,$bonusUid,$receiverUid,$receiverName,$rpRebateAmount,$rpRebateName,$orderUid)
{
     if(insertDynamicData($conn,"bonus_sales_or_rebate",array("uid","receiver_uid","receiver","amount","bonus_type","order_uid"),
     array($bonusUid,$receiverUid,$receiverName,$rpRebateAmount,$rpRebateName,$orderUid),"ssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
// // first order / rebates (purchaser) end

$allUser = getUser($conn);
if($allUser)
{
     for($cntA = 0;$cntA < count($allUser) ;$cntA++)
     {
          // echo $allUser[$cntA]->getUsername();
          echo $userUsername = $allUser[$cntA]->getUsername();
          echo "<br>";
          echo $userUid = $allUser[$cntA]->getUid();
          echo "<br>";

          $userSalesCommission = $allUser[$cntA]->getSalesCommission();
          $userRedemptionPoint = $allUser[$cntA]->getRedemptionPoint();

          $item1Amount = 0;
          // $orderListDetails = getOrderList($conn, " WHERE user_uid = ? AND product_uid = '35117b1cb67c78efa860c4882c2e5745' ",array("user_uid"),array($userUid),"s");
          // $orderListDetails = getOrderList($conn, " WHERE user_uid = ? AND product_uid = '547297ffdb9f49d6e5ff1db5208f2530' ",array("user_uid"),array($userUid),"s");
          // $orderListDetails = getOrderList($conn, " WHERE user_uid = ? AND product_uid = '712e761968f09667885752f04d48c77f' ",array("user_uid"),array($userUid),"s");

          $orderListDetails = getOrderList($conn, " WHERE user_uid = ? AND product_uid = '35117b1cb67c78efa860c4882c2e5745' AND status = 'Sold' ",array("user_uid"),array($userUid),"s");
          if($orderListDetails)
          {
               // $item1 = 0;
               for($cnt = 0;$cnt < count($orderListDetails) ;$cnt++)
               {
                    $orderUid = $orderListDetails[$cnt]->getOrderId();
                    $item1Amount += $orderListDetails[$cnt]->getQuantity();
               }
          }

          echo "Ordering : ";
          echo $item1Amount;
          echo "<br>";

          // filter out non buyer
          if($item1Amount > 0)
          {

               // $preOrderListRow = getPreOrderList($conn, " WHERE user_uid = ? AND product_uid = '35117b1cb67c78efa860c4882c2e5745' ",array("user_uid"),array($userUid),"s");
               $preOrderListRow = getOrderingList($conn, " WHERE user_uid = ? AND product_uid = '35117b1cb67c78efa860c4882c2e5745' ",array("user_uid"),array($userUid),"s");
               if($preOrderListRow)
               {
                    $previosuQuantity = $preOrderListRow[0]->getQuantity();
                    $renewQuantity = $item1Amount + $previosuQuantity;
                    // echo $renewQuantity = $item1Amount + $previosuQuantity;

                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
                    if($renewQuantity)
                    {
                         array_push($tableName,"quantity");
                         array_push($tableValue,$renewQuantity);
                         $stringType .=  "d";
                    }
                    array_push($tableValue,$userUid);
                    $stringType .=  "s";
                    // $updateLevelTwoBonus = updateDynamicData($conn,"ordering_list"," WHERE user_uid = ? ",$tableName,$tableValue,$stringType);
                    $updateLevelTwoBonus = updateDynamicData($conn,"ordering_list"," WHERE user_uid = ? AND product_uid = '35117b1cb67c78efa860c4882c2e5745' ",$tableName,$tableValue,$stringType);
                    if($updateLevelTwoBonus)
                    {
                         echo "Done";
                         $userRH = getReferralHistory($conn, " WHERE referral_id = ? ",array("referral_id"),array($userUid),"s");
                         echo "Upline Uid : ";
                         echo $directUplineUid = $userRH[0]->getReferrerId();
                         echo "<br>";  
                         $directUplineDetails = getUser($conn, " WHERE uid =? ",array("uid"),array($directUplineUid),"s");
                         $directUplineUsername = $directUplineDetails[0]->getUsername();
                         echo $directUplineSalesCommission = $directUplineDetails[0]->getSalesCommission();
                         echo "<br>";  
                         echo $directUplineRedemptionPoint = $directUplineDetails[0]->getRedemptionPoint();

                         // $productValue = '125';
                         // $redemptionPoint = '125';

                         if($item1Amount == 1)
                         {

                              echo $firstOrderPV = 0.20 * ($productValue);
                              echo $firstOrderRP = 0.02 * ($redemptionPoint);
                              echo $directUplineFirstOrderSalesCommission = $firstOrderPV + $directUplineSalesCommission;
                              echo $directUplineFirstOrderRedemptionPoint = $firstOrderRP + $directUplineRedemptionPoint;

                              $tableName = array();
                              $tableValue =  array();
                              $stringType =  "";
                              //echo "save to database";
                              if($directUplineFirstOrderSalesCommission)
                              {
                                   array_push($tableName,"sales_commission");
                                   array_push($tableValue,$directUplineFirstOrderSalesCommission);
                                   $stringType .=  "d";
                              }
                              if($directUplineFirstOrderRedemptionPoint)
                              {
                                   array_push($tableName,"redemption_point");
                                   array_push($tableValue,$directUplineFirstOrderRedemptionPoint);
                                   $stringType .=  "d";
                              }
                              array_push($tableValue,$directUplineUid);
                              $stringType .=  "s";
                              $updateUplineFirstOrderBonus = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                              if($updateUplineFirstOrderBonus)
                              {
                                   $bonusUid = md5(uniqid());
                                   $receiverUid = $directUplineUid;
                                   $receiverName = $directUplineUsername;
                                   $pvAmount = $firstOrderPV;
                                   $pvName = "20% First Purchase Commission Product A";
                                   $rpAmount = $firstOrderRP;
                                   $rpName = "2% Redemption Point Product A";

                                   if(firstOrderProductValueUpline($conn,$bonusUid,$receiverUid,$receiverName,$pvAmount,$pvName,$orderUid))
                                   {
                                        if(firstOrderRedemptionPointUpline($conn,$bonusUid,$receiverUid,$receiverName,$rpAmount,$rpName,$orderUid))
                                        {
                                             echo "DONE ALL A";
                                             echo "<br>";
                                        }
                                        else
                                        {
                                             echo "Fail Part A1";
                                        }
                                   }
                                   else
                                   {
                                        echo "Fail Part A2";
                                   }
                              }
                              else
                              {
                                   echo "fail to update upline 1st order bonus A3";
                              }
                         }
                         elseif($item1Amount > 1)
                         {
                              $remainAmount = $item1Amount - 1;

                              echo $rebatesPV = (0.20 * ($productValue))*($remainAmount);
                              echo $rebatesRP = (0.02 * ($redemptionPoint))*($remainAmount);
                              echo $purchaserRebatesSalesCommission = $rebatesPV + $userSalesCommission;
                              echo $purchaserRebatesRedemptionPoint = $rebatesRP + $userRedemptionPoint;

                              echo $firstOrderPV = 0.20 * ($productValue);
                              echo $firstOrderRP = 0.02 * ($redemptionPoint);
                              echo $directUplineFirstOrderSalesCommission = $firstOrderPV + $directUplineSalesCommission;
                              echo $directUplineFirstOrderRedemptionPoint = $firstOrderRP + $directUplineRedemptionPoint;
                              echo "<br>";

                              $tableName = array();
                              $tableValue =  array();
                              $stringType =  "";
                              //echo "save to database";
                              if($directUplineFirstOrderSalesCommission)
                              {
                                   array_push($tableName,"sales_commission");
                                   array_push($tableValue,$directUplineFirstOrderSalesCommission);
                                   $stringType .=  "d";
                              }
                              if($directUplineFirstOrderRedemptionPoint)
                              {
                                   array_push($tableName,"redemption_point");
                                   array_push($tableValue,$directUplineFirstOrderRedemptionPoint);
                                   $stringType .=  "d";
                              }
                              array_push($tableValue,$directUplineUid);
                              $stringType .=  "s";
                              $updateUplineFirstOrderBonus = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                              if($updateUplineFirstOrderBonus)
                              {
                                   $bonusUid = md5(uniqid());
                                   $receiverUid = $directUplineUid;
                                   $receiverName = $directUplineUsername;
                                   $pvAmount = $firstOrderPV;
                                   $pvName = "20% First Purchase Commission Product A";
                                   $rpAmount = $firstOrderRP;
                                   $rpName = "2% Redemption Point Product A";

                                   if(firstOrderProductValueUpline($conn,$bonusUid,$receiverUid,$receiverName,$pvAmount,$pvName,$orderUid))
                                   {
                                        if(firstOrderRedemptionPointUpline($conn,$bonusUid,$receiverUid,$receiverName,$rpAmount,$rpName,$orderUid))
                                        {
                                             echo "DONE ALL B";
                                             echo "<br>";

                                             $tableName = array();
                                             $tableValue =  array();
                                             $stringType =  "";
                                             //echo "save to database";
                                             if($purchaserRebatesSalesCommission)
                                             {
                                                  array_push($tableName,"sales_commission");
                                                  array_push($tableValue,$purchaserRebatesSalesCommission);
                                                  $stringType .=  "d";
                                             }
                                             if($purchaserRebatesRedemptionPoint)
                                             {
                                                  array_push($tableName,"redemption_point");
                                                  array_push($tableValue,$purchaserRebatesRedemptionPoint);
                                                  $stringType .=  "d";
                                             }
                                             array_push($tableValue,$userUid);
                                             $stringType .=  "s";
                                             $updateUplineFirstOrderBonus = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                             if($updateUplineFirstOrderBonus)
                                             {
                                                  $bonusUid = md5(uniqid());
                                                  $receiverUid = $userUid;
                                                  $receiverName = $userUsername;
                                                  $pvRebateAmount = $rebatesPV;
                                                  $pvRebateName = "20% Rebate Product A";
                                                  $rpRebateAmount = $rebatesRP;
                                                  $rpRebateName = "2% Redemption Point Product A";
               
                                                  if(productValueRebates($conn,$bonusUid,$receiverUid,$receiverName,$pvRebateAmount,$pvRebateName,$orderUid))
                                                  {
                                                       if(redemptionPointRebates($conn,$bonusUid,$receiverUid,$receiverName,$rpRebateAmount,$rpRebateName,$orderUid))
                                                       {
                                                            echo "DONE ALL C";
                                                            echo "<br>";
                                                       }
                                                       else
                                                       {
                                                            echo "Fail Part C1";
                                                       }
                                                  }
                                                  else
                                                  {
                                                       echo "Fail Part C2";
                                                  }
                                             }
                                             else
                                             {
                                                  echo "fail to update upline 1st order bonus C3";
                                             }
                                        }
                                        else
                                        {
                                             echo "Fail Part B1";
                                        }
                                   }
                                   else
                                   {
                                        echo "Fail Part B2";
                                   }
                              }
                              else
                              {
                                   echo "fail to update upline 1st order bonus B3, line 329";
                              }

                         }
                    }
                    else
                    {
                         echo "Fail";
                    }
               }
               else
               {
                    if(addProductOne($conn,$userUid,$productOneUid,$productOneName,$item1Amount))
                    {

                         $userRH = getReferralHistory($conn, " WHERE referral_id = ? ",array("referral_id"),array($userUid),"s");
                         echo "Upline Uid : ";
                         echo $directUplineUid = $userRH[0]->getReferrerId();
                         echo "<br>";  
                         $directUplineDetails = getUser($conn, " WHERE uid =? ",array("uid"),array($directUplineUid),"s");
                         $directUplineUsername = $directUplineDetails[0]->getUsername();
                         echo $directUplineSalesCommission = $directUplineDetails[0]->getSalesCommission();
                         echo "<br>";  
                         echo $directUplineRedemptionPoint = $directUplineDetails[0]->getRedemptionPoint();

                         // $productValue = '125';
                         // $redemptionPoint = '125';

                         if($item1Amount == 1)
                         {

                              echo $firstOrderPV = 0.20 * ($productValue);
                              echo $firstOrderRP = 0.02 * ($redemptionPoint);
                              echo $directUplineFirstOrderSalesCommission = $firstOrderPV + $directUplineSalesCommission;
                              echo $directUplineFirstOrderRedemptionPoint = $firstOrderRP + $directUplineRedemptionPoint;

                              $tableName = array();
                              $tableValue =  array();
                              $stringType =  "";
                              //echo "save to database";
                              if($directUplineFirstOrderSalesCommission)
                              {
                                   array_push($tableName,"sales_commission");
                                   array_push($tableValue,$directUplineFirstOrderSalesCommission);
                                   $stringType .=  "d";
                              }
                              if($directUplineFirstOrderRedemptionPoint)
                              {
                                   array_push($tableName,"redemption_point");
                                   array_push($tableValue,$directUplineFirstOrderRedemptionPoint);
                                   $stringType .=  "d";
                              }
                              array_push($tableValue,$directUplineUid);
                              $stringType .=  "s";
                              $updateUplineFirstOrderBonus = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                              if($updateUplineFirstOrderBonus)
                              {
                                   $bonusUid = md5(uniqid());
                                   $receiverUid = $directUplineUid;
                                   $receiverName = $directUplineUsername;
                                   $pvAmount = $firstOrderPV;
                                   $pvName = "20% First Purchase Commission Product A";
                                   $rpAmount = $firstOrderRP;
                                   $rpName = "2% Redemption Point Product A";

                                   if(firstOrderProductValueUpline($conn,$bonusUid,$receiverUid,$receiverName,$pvAmount,$pvName,$orderUid))
                                   {
                                        if(firstOrderRedemptionPointUpline($conn,$bonusUid,$receiverUid,$receiverName,$rpAmount,$rpName,$orderUid))
                                        {
                                             echo "DONE ALL A";
                                             echo "<br>";
                                        }
                                        else
                                        {
                                             echo "Fail Part A1";
                                        }
                                   }
                                   else
                                   {
                                        echo "Fail Part A2";
                                   }
                              }
                              else
                              {
                                   echo "fail to update upline 1st order bonus A3";
                              }
                         }
                         elseif($item1Amount > 1)
                         {
                              $remainAmount = $item1Amount - 1;

                              echo $rebatesPV = (0.20 * ($productValue))*($remainAmount);
                              echo $rebatesRP = (0.02 * ($redemptionPoint))*($remainAmount);
                              echo $purchaserRebatesSalesCommission = $rebatesPV + $userSalesCommission;
                              echo $purchaserRebatesRedemptionPoint = $rebatesRP + $userRedemptionPoint;

                              echo $firstOrderPV = 0.20 * ($productValue);
                              echo $firstOrderRP = 0.02 * ($redemptionPoint);
                              echo $directUplineFirstOrderSalesCommission = $firstOrderPV + $directUplineSalesCommission;
                              echo $directUplineFirstOrderRedemptionPoint = $firstOrderRP + $directUplineRedemptionPoint;
                              echo "<br>";

                              $tableName = array();
                              $tableValue =  array();
                              $stringType =  "";
                              //echo "save to database";
                              if($directUplineFirstOrderSalesCommission)
                              {
                                   array_push($tableName,"sales_commission");
                                   array_push($tableValue,$directUplineFirstOrderSalesCommission);
                                   $stringType .=  "d";
                              }
                              if($directUplineFirstOrderRedemptionPoint)
                              {
                                   array_push($tableName,"redemption_point");
                                   array_push($tableValue,$directUplineFirstOrderRedemptionPoint);
                                   $stringType .=  "d";
                              }
                              array_push($tableValue,$directUplineUid);
                              $stringType .=  "s";
                              $updateUplineFirstOrderBonus = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                              if($updateUplineFirstOrderBonus)
                              {
                                   $bonusUid = md5(uniqid());
                                   $receiverUid = $directUplineUid;
                                   $receiverName = $directUplineUsername;
                                   $pvAmount = $firstOrderPV;
                                   $pvName = "20% First Purchase Commission Product A";
                                   $rpAmount = $firstOrderRP;
                                   $rpName = "2% Redemption Point Product A";

                                   if(firstOrderProductValueUpline($conn,$bonusUid,$receiverUid,$receiverName,$pvAmount,$pvName,$orderUid))
                                   {
                                        if(firstOrderRedemptionPointUpline($conn,$bonusUid,$receiverUid,$receiverName,$rpAmount,$rpName,$orderUid))
                                        {
                                             echo "DONE ALL B";
                                             echo "<br>";

                                             $tableName = array();
                                             $tableValue =  array();
                                             $stringType =  "";
                                             //echo "save to database";
                                             if($purchaserRebatesSalesCommission)
                                             {
                                                  array_push($tableName,"sales_commission");
                                                  array_push($tableValue,$purchaserRebatesSalesCommission);
                                                  $stringType .=  "d";
                                             }
                                             if($purchaserRebatesRedemptionPoint)
                                             {
                                                  array_push($tableName,"redemption_point");
                                                  array_push($tableValue,$purchaserRebatesRedemptionPoint);
                                                  $stringType .=  "d";
                                             }
                                             array_push($tableValue,$userUid);
                                             $stringType .=  "s";
                                             $updateUplineFirstOrderBonus = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                             if($updateUplineFirstOrderBonus)
                                             {
                                                  $bonusUid = md5(uniqid());
                                                  $receiverUid = $userUid;
                                                  $receiverName = $userUsername;
                                                  $pvRebateAmount = $rebatesPV;
                                                  $pvRebateName = "20% Rebate Product A";
                                                  $rpRebateAmount = $rebatesRP;
                                                  $rpRebateName = "2% Redemption Point Product A";
               
                                                  if(productValueRebates($conn,$bonusUid,$receiverUid,$receiverName,$pvRebateAmount,$pvRebateName,$orderUid))
                                                  {
                                                       if(redemptionPointRebates($conn,$bonusUid,$receiverUid,$receiverName,$rpRebateAmount,$rpRebateName,$orderUid))
                                                       {
                                                            echo "DONE ALL C";
                                                            echo "<br>";
                                                       }
                                                       else
                                                       {
                                                            echo "Fail Part C1";
                                                       }
                                                  }
                                                  else
                                                  {
                                                       echo "Fail Part C2";
                                                  }
                                             }
                                             else
                                             {
                                                  echo "fail to update upline 1st order bonus C3";
                                             }
                                        }
                                        else
                                        {
                                             echo "Fail Part B1";
                                        }
                                   }
                                   else
                                   {
                                        echo "Fail Part B2";
                                   }
                              }
                              else
                              {
                                   echo "fail to update upline 1st order bonus B3, line 329";
                              }

                         }

                    }
                    else
                    {
                         echo "fail to add product";
                    }
               }
          }
          echo "<br>";
     }
}

$orderingList = getOrderList($conn, " WHERE product_uid = '35117b1cb67c78efa860c4882c2e5745' AND status = 'Sold' ");
if($orderingList)
{
     // $item1 = 0;
     for($cnt = 0;$cnt < count($orderingList) ;$cnt++)
     {
          $orderId = $orderingList[$cnt]->getOrderId();

          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          
          $renewStatus = "Claim";
          
          if($renewStatus)
          {
               array_push($tableName,"status");
               array_push($tableValue,$renewStatus);
               $stringType .=  "s";
          }
          array_push($tableValue,$orderId);
          $stringType .=  "s";
          $updateOrderIdStatus = updateDynamicData($conn,"order_list"," WHERE order_id = ? ",$tableName,$tableValue,$stringType);
          if($updateOrderIdStatus)
          {
               echo "update order status to claim";
               echo "<br>";
          }
          else
          {
               echo "fail to update order status to claim";
               echo "<br>";
          }

     }
}
?>