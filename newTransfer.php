<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$date = date('Y-m-d');
$tsValue = time();

$uid = $_SESSION['uid'];
$last5Uid = substr($uid, -5);

$conn = connDB();

$userRows = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/" />
<link rel="canonical" href="https://agentpnchc.com/" /> -->
<meta property="og:title" content="ADD NEW TRANSFER | MODERCK" />
<title>ADD NEW TRANSFER | MODERCK</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">New Transfer</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    <div class="width100 same-padding min-height100 padding-top overflow overflow-x">

        <form action="utilities/addNewTransferFunction.php" method="POST">

            	<div class="dual-input">
        			<p class="top-p">Date</p>
                <input type="text" class="line-input clean" value="<?php echo $date;?>" placeholder="Date" id="date" name="date" readonly>
            </div>

                <div class="dual-input second-dual-input">  
                    <p class="top-p">Reference No</p>
                <input type="text" class="line-input clean" value="<?php echo $tsValue; echo $last5Uid;?>" placeholder="Ref No." id="reference_no" name="reference_no" readonly>
            </div>

            <div class="clear"></div>
            
            	<div class="dual-input">
        			<p class="top-p">Username</p>
                <input type="text" class="line-input clean" value="<?php echo $userDetails->getUsername();?>" placeholder="Username" id="username" name="username" readonly>
            </div>

                <div class="dual-input second-dual-input">  
                    <p class="top-p">Full Name</p>
                <input type="text" class="line-input clean" value="<?php echo $userDetails->getFullname();?>" placeholder="Full Name" id="fullname" name="fullname" readonly>
            </div>

            <div class="clear"></div>

            	<div class="dual-input">
        			<p class="top-p">Transfer To</p>
                <input type="text" class="line-input clean" placeholder="Transfer To" id="transfer_to" name="transfer_to" required>
            </div>

                <div class="dual-input second-dual-input">  
                    <p class="top-p">Transfer Type</p>
                <select class="line-input clean" id="transfer_type" name="transfer_type" required>

                <option value="MYSC">MYSC</option>
                <option value="MYRP">MYRP</option>
            </select>
            </div>

            <div class="clear"></div>
            	<div class="dual-input">
        			<p class="top-p">Transfer Amount</p>
                <input type="text" class="line-input clean" placeholder="Transfer Amount" id="amount" name="amount" required>
            </div>


            <div class="clear"></div>
                 <div class="text-center middle-div-width">
            		<button class="clean white-button ow-red-bg white-text"  name="submit">  Proceed to Transfer </button>
                </div>              
            

            <div class="clear"></div>

        </form>

        </div>
    </div>
</div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Please Register"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Wrong Password";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>