<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
// require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/InactiveList.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function addInactive($conn,$uid,$userUid,$username,$todayDate,$inactiveStatus)
{
     if(insertDynamicData($conn,"inactive_list",array("uid","user_uid","username","start_date","status"),
          array($uid,$userUid,$username,$todayDate,$inactiveStatus),"sssss") === null)
     {
          echo "GG !!";
     }
     else{    }
     return true;
}

$conn = connDB();

date_default_timezone_set('Asia/Kuala_Lumpur');

echo "Current Date : ";
// echo $todayDate = "2024-01-10";
echo $todayDate = date('Y-m-d');
echo "<br>";
echo "<br>";

// date_default_timezone_set('Asia/Kuala_Lumpur');
// $dateSQL = date('Y-m-d', time());
// $dateDisplay = date('d/m/Y', time());
// $twoWeeks = date('Y-m-d',strtotime('-14 days ',strtotime($dateSQL)));
// echo $oneMonth = date('Y-m-d',strtotime('+36 month ',strtotime($todayDate)));


// $allUsers = getUser($conn);
// $allUsers = getUser($conn," WHERE user_type = 1 ");
$allUsers = getUser($conn," WHERE status = 'Active' AND user_type = 1 ");

if($allUsers) 
{
    for($cnt = 0;$cnt < count($allUsers) ;$cnt++)
    {

        // $ordersDetails = getOrders($conn, " WHERE uid = ? ORDER BY date_created DESC ",array("uid"),array($allUsers[$cnt]->getUid()), "s");
        $ordersDetails = getOrders($conn, "WHERE uid = ? AND payment_status = 'APPROVED' ORDER BY date_created DESC LIMIT 1",array("uid"),array($allUsers[$cnt]->getUid()), "s");
        if($ordersDetails) 
        {
            echo "<br>";
            echo "list of member with orders";
            echo "<br>";

            for($cntA = 0;$cntA < count($ordersDetails) ;$cntA++)
            {
                echo $username = $allUsers[$cnt]->getUsername();
                echo "<br>";
                $userUid = $allUsers[$cnt]->getUid();
                // echo "<br>";
                $currentSalesCommission = $allUsers[$cnt]->getSalesCommission();
                $currentRedemptionPoint = $allUsers[$cnt]->getRedemptionPoint();
                $salesCommissionMonthly = $allUsers[$cnt]->getSalesCommissionMonth();
                $redemptionPointMonthly = $allUsers[$cnt]->getRedemptionPointMonth();

                echo $orderYear = date('Y-m-d',strtotime($orderDateApproved = $ordersDetails[$cntA]->getDateCreated()));
                echo "<br>";  
                echo "Pls Make Order This Date : ";  
                // echo $maintenancePeriod = date('Y-m-d',strtotime('+36 month ',strtotime($orderYear)));
                echo $maintenancePeriod = date('Y-m-d',strtotime('+1096 days ',strtotime($orderYear)));
                echo "<br>";
                // echo "<br>";

                if($todayDate > $maintenancePeriod) 
                {
                    echo "expired, transfer to inactive";
                    echo "<br>";
                    // echo "<br>";

                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";

                    $renewSalesCommissionMonthly = $currentSalesCommission + $salesCommissionMonthly;
                    $renewRedemptionPointMonthly = $currentRedemptionPoint + $redemptionPointMonthly;

                    $emptySalesCommission = "0";
                    $emptyRedemptionPoint = "0";

                    $status = "Inactive";
                
                    if($status)
                    {
                        array_push($tableName,"status");
                        array_push($tableValue,$status);
                        $stringType .=  "s";
                    }      
                    
                    if($renewSalesCommissionMonthly)
                    {
                        array_push($tableName,"sales_commission_month");
                        array_push($tableValue,$renewSalesCommissionMonthly);
                        $stringType .=  "d";
                    }     
                    if($renewRedemptionPointMonthly)
                    {
                        array_push($tableName,"redemption_point_month");
                        array_push($tableValue,$renewRedemptionPointMonthly);
                        $stringType .=  "d";
                    }    
                    if($emptySalesCommission || !$emptySalesCommission)
                    {
                        array_push($tableName,"sales_commission");
                        array_push($tableValue,$emptySalesCommission);
                        $stringType .=  "d";
                    }
                    if($emptyRedemptionPoint || !$emptyRedemptionPoint)
                    {
                        array_push($tableName,"redemption_point");
                        array_push($tableValue,$emptyRedemptionPoint);
                        $stringType .=  "d";
                    }

                    array_push($tableValue,$userUid);
                    $stringType .=  "s";
                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($passwordUpdated)
                    {
                        echo "Inactive Status Updated !!";
                        echo "<br>";
                        // echo "<br>";

                        $uid = md5(uniqid());
                        $inactiveStatus = "Running";
                        if(addInactive($conn,$uid,$userUid,$username,$todayDate,$inactiveStatus))
                        {  
                            echo "details added to DB !";
                            echo "<br>";
                            echo "<br>";
                        }
                        else
                        {
                            echo "ERROR";
                        } 

                    }
                    else
                    {
                        echo "ERROR 1 : FAIL To Update Inactive Status!!";
                        echo "<br>";
                        echo "<br>";
                    }

                }
                else
                {
                    echo "active";
                    echo "<br>";
                    echo "<br>";
                }

            }

            // echo $userUsername = $allUsers[$cnt]->getUsername();
            // echo "<br>";
            echo "<br>";
        }
        else
        {

            echo "list of member with no order";
            echo "<br>";

            echo $username = $allUsers[$cnt]->getUsername();
            echo "<br>";

            $userUid = $allUsers[$cnt]->getUid();

            $currentSalesCommission = $allUsers[$cnt]->getSalesCommission();
            $currentRedemptionPoint = $allUsers[$cnt]->getRedemptionPoint();
            $salesCommissionMonthly = $allUsers[$cnt]->getSalesCommissionMonth();
            $redemptionPointMonthly = $allUsers[$cnt]->getRedemptionPointMonth();
            
            echo $registerDate = $allUsers[$cnt]->getDateCreated();
            echo "<br>";
            echo "Active Until This Date : ";  
            // echo $activePeriod = date('Y-m-d',strtotime('+36 month ',strtotime($registerDate)));
            echo $activePeriod = date('Y-m-d',strtotime('+1096 days ',strtotime($registerDate)));
            echo "<br>";
            // echo "<br>";

            if($todayDate > $activePeriod) 
            {
                echo "expired, transfer to inactive";
                echo "<br>";
                // echo "<br>";

                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";

                $renewSalesCommissionMonthly = $currentSalesCommission + $salesCommissionMonthly;
                $renewRedemptionPointMonthly = $currentRedemptionPoint + $redemptionPointMonthly;

                $emptySalesCommission = "0";
                $emptyRedemptionPoint = "0";

                $status = "Inactive";
            
                if($status)
                {
                    array_push($tableName,"status");
                    array_push($tableValue,$status);
                    $stringType .=  "s";
                }                       

                if($renewSalesCommissionMonthly)
                {
                    array_push($tableName,"sales_commission_month");
                    array_push($tableValue,$renewSalesCommissionMonthly);
                    $stringType .=  "d";
                }     
                if($renewRedemptionPointMonthly)
                {
                    array_push($tableName,"redemption_point_month");
                    array_push($tableValue,$renewRedemptionPointMonthly);
                    $stringType .=  "d";
                }    
                if($emptySalesCommission || !$emptySalesCommission)
                {
                    array_push($tableName,"sales_commission");
                    array_push($tableValue,$emptySalesCommission);
                    $stringType .=  "d";
                }
                if($emptyRedemptionPoint || !$emptyRedemptionPoint)
                {
                    array_push($tableName,"redemption_point");
                    array_push($tableValue,$emptyRedemptionPoint);
                    $stringType .=  "d";
                }

                array_push($tableValue,$userUid);
                $stringType .=  "s";
                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($passwordUpdated)
                {
                    echo "Inactive Status !!";
                    echo "<br>";
                    // echo "<br>";

                    $uid = md5(uniqid());
                    $inactiveStatus = "Running";
                    if(addInactive($conn,$uid,$userUid,$username,$todayDate,$inactiveStatus))
                    {  
                        echo "details added to DB !";
                        echo "<br>";
                        echo "<br>";
                    }
                    else
                    {
                        echo "ERROR";
                    } 

                }
                else
                {
                    echo "ERROR 2 : FAIL To Update Inactive Status!!";
                    echo "<br>";
                    echo "<br>";
                }

            }
            else
            {
                echo "active";
                echo "<br>";
                echo "<br>";
            }

        }
    }
}
?>