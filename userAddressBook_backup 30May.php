<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Address.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

// $allAddress = getAddress($conn, "WHERE user_uid = ? ",array("user_uid"),array($uid),"s");
// $allAddress = getAddress($conn, "WHERE user_uid = ? AND status != 'Delete' ",array("user_uid"),array($uid),"s");
// $allAddress = getAddress($conn, "WHERE user_uid = ? AND default_ship != 'Delete' ",array("user_uid"),array($uid),"s");
// $allAddress = getAddress($conn, "WHERE user_uid = ? AND status = 'Available' ",array("user_uid"),array($uid),"s");

// $defaultAddress = getAddress($conn, "WHERE user_uid = ? AND status = 'Available' AND default_ship = 'Yes' ",array("user_uid"),array($uid),"s");
// $allAddress = getAddress($conn, "WHERE user_uid = ? AND status = 'Available' AND default_ship = 'No' ",array("user_uid"),array($uid),"s");

$defaultBillAddress = getAddress($conn, "WHERE user_uid = ? AND status = 'Available' AND default_bill = 'Yes' ",array("user_uid"),array($uid),"s");
$defaultShipAddress = getAddress($conn, "WHERE user_uid = ? AND status = 'Available' AND default_ship = 'Yes' ",array("user_uid"),array($uid),"s");
$allAddress = getAddress($conn, "WHERE user_uid = ? AND status = 'Available' AND default_bill = 'No' AND default_ship = 'No' ",array("user_uid"),array($uid),"s");

// $allUser = getUser($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="My Address Book | MODERCK" />
<title>My Address Book | MODERCK</title>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">My Address Book</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    <div class="width100 same-padding min-height100 padding-top overflow overflow-x">
    		<div class="text-center middle-div-width">
            	<a href="userAddressBookAdd.php"><div  class="clean white-button ow-red-bg white-text smaller-button">Add</div></a> 
            </div> 
        <div class="width100 overflow-x">
            <table class="width100 gold-table ow-text-left-table">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Details</th>
                        <th>ACTION</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                        if($defaultBillAddress)
                        {
                            for($cnt = 0;$cnt < count($defaultBillAddress) ;$cnt++)
                            {
                            ?>
                                <tr>
                                    <td></td>
                                    <td>
                                        <?php echo $defaultBillAddress[$cnt]->getRecipient();?>
                                        <?php 
                                            $statusBill = $defaultBillAddress[$cnt]->getDefaultBill();
                                            if($statusBill == 'Yes')
                                            {
                                                echo "(Default Billing Address)";
                                            }
                                        ?>

                                        <br><?php echo $defaultBillAddress[$cnt]->getMobile();?><br>
                                        <?php echo $defaultBillAddress[$cnt]->getHouseRoad();?>,&nbsp;
                                        <?php echo $defaultBillAddress[$cnt]->getPostcode();?>,&nbsp;
                                        <?php echo $defaultBillAddress[$cnt]->getCity();?>,&nbsp;
                                        <?php echo $defaultBillAddress[$cnt]->getState();?>,&nbsp;
                                        <?php echo $defaultBillAddress[$cnt]->getCountry();?>
                                    </td>
                                    
                                    <td>
                                        <form action="userAddressEdit.php" method="POST" class="left-form">
                                            <button class="clean transparent-button white-link" type="submit" name="item_uid" value="<?php echo $defaultBillAddress[$cnt]->getUid();?>">
                                                <u>Edit</u>
                                            </button>
                                        </form> 

                                        <form id="action-form-<?= $cnt+1 ?>" action="utilities/userAddressDeleteFunction.php" method="post" class="right-form">
                                            <input type="hidden" name="item_uid" value="<?php echo $defaultBillAddress[$cnt]->getUid();?>">
                                            <button type="button" name="delete" onclick="deleteOrder(<?= $cnt+1 ?>)" class="btn-danger clean transparent-button red-link2">
                                                <u>Delete</u>
                                            </button>
                                        </form> 
                                    </td>
                                </tr>
                            <?php
                            }
                        }
                    ?>   
                </tbody>

                <tbody>
                    <?php
                        if($defaultShipAddress)
                        {
                            for($cnt = 0;$cnt < count($defaultShipAddress) ;$cnt++)
                            {
                            ?>
                                <tr>
                                    <td></td>
                                    <td>
                                        <?php echo $defaultShipAddress[$cnt]->getRecipient();?>
                                        <?php 
                                            $statusBill = $defaultShipAddress[$cnt]->getDefaultShip();
                                            if($statusBill == 'Yes')
                                            {
                                                echo "(Default Shipping Address)";
                                            }
                                        ?>

                                        <br><?php echo $defaultShipAddress[$cnt]->getMobile();?><br>
                                        <?php echo $defaultShipAddress[$cnt]->getHouseRoad();?>,&nbsp;
                                        <?php echo $defaultShipAddress[$cnt]->getPostcode();?>,&nbsp;
                                        <?php echo $defaultShipAddress[$cnt]->getCity();?>,&nbsp;
                                        <?php echo $defaultShipAddress[$cnt]->getState();?>,&nbsp;
                                        <?php echo $defaultShipAddress[$cnt]->getCountry();?>
                                    </td>
                                    
                                    <td>
                                        <form action="userAddressEdit.php" method="POST" class="left-form">
                                            <button class="clean transparent-button white-link" type="submit" name="item_uid" value="<?php echo $defaultShipAddress[$cnt]->getUid();?>">
                                                <u>Edit</u>
                                            </button>
                                        </form> 

                                        <form id="action-form-<?= $cnt+1 ?>" action="utilities/userAddressDeleteFunction.php" method="post" class="right-form">
                                            <input type="hidden" name="item_uid" value="<?php echo $defaultShipAddress[$cnt]->getUid();?>">
                                            <button type="button" name="delete" onclick="deleteOrder(<?= $cnt+1 ?>)" class="btn-danger clean transparent-button red-link2">
                                                <u>Delete</u>
                                            </button>
                                        </form> 
                                    </td>
                                </tr>
                            <?php
                            }
                        }
                    ?>   
                </tbody>

                <tbody>
                    <?php
                        if($allAddress)
                        {
                            for($cnt = 0;$cnt < count($allAddress) ;$cnt++)
                            {
                            ?>
                                <tr>
                                    <td><?php echo ($cnt+1)?></td>

                                    <td>
                                        <?php echo $allAddress[$cnt]->getRecipient();?>
                                        
                                        <?php 
                                            $statusShip = $allAddress[$cnt]->getDefaultShip();
                                            if($statusShip == 'Yes')
                                            {
                                                echo "(Default Shipping Address)";
                                            }
                                        ?>

                                        <?php 
                                            $statusBill = $allAddress[$cnt]->getDefaultBill();
                                            if($statusBill == 'Yes')
                                            {
                                                echo "(Default Billing Address)";
                                            }
                                        ?>

                                        <br><?php echo $allAddress[$cnt]->getMobile();?><br>
                                    
                                        <?php echo $allAddress[$cnt]->getHouseRoad();?>,&nbsp;
                                        <?php echo $allAddress[$cnt]->getPostcode();?>,&nbsp;
                                        <?php echo $allAddress[$cnt]->getCity();?>,&nbsp;
                                        <?php echo $allAddress[$cnt]->getState();?>,&nbsp;
                                        <?php echo $allAddress[$cnt]->getCountry();?>
                                    </td>
                                    
                                    <td>
                                        <form action="userAddressEdit.php" method="POST" class="left-form">
                                            <button class="clean transparent-button white-link" type="submit" name="item_uid" value="<?php echo $allAddress[$cnt]->getUid();?>">
                                                <u>Edit</u>
                                            </button>
                                        </form> 

                                        <!-- <form action="utilities/userAddressDeleteFunction.php" method="POST" class="right-form" >
                                            <button class="clean transparent-button red-link2" type="submit" name="item_uid" value="<?php //echo $allAddress[$cnt]->getUid();?>">
                                                DELETE
                                            </button>
                                        </form>  -->

                                        <form id="action-form-<?= $cnt+1 ?>" action="utilities/userAddressDeleteFunction.php" method="post" class="right-form">
                                            <input type="hidden" name="item_uid" value="<?php echo $allAddress[$cnt]->getUid();?>">
                                            <button type="button" name="delete" onclick="deleteOrder(<?= $cnt+1 ?>)" class="btn-danger clean transparent-button red-link2">
                                                <u>Delete</u>
                                            </button>
                                        </form> 
                                    </td>
                                </tr>
                            <?php
                            }
                        }
                    ?>   
                </tbody>
            </table>
        </div>


    </div>
    </div>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
// function checkOrder(no){
//     swal({
//         title: "Are you sure?",
//         text: "Once approve, you will not be able to recover this Order!",
//         icon: "warning",
//         buttons: true,
//         dangerMode: true,
//         })
//         .then((willDelete) => {
//         if (willDelete) {
//         var x = $("#action-form-"+no);
//         x.find('.btn-success').attr('type','submit');
//         x.find('.btn-success').attr('onclick','');
//         x.find('.btn-success').click();
//         }
//     });
// }
function deleteOrder(no){
    swal({
        title: "Deleting Address",
        text: "Are you sure you want to delete the selection ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
        var x = $("#action-form-"+no);
        x.find('.btn-danger').attr('type','submit');
        x.find('.btn-danger').attr('onclick','');
        x.find('.btn-danger').click();
        }
    });
}
</script>

</body>
</html>