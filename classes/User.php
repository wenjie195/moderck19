<?php
class User {
    /* Member variables */
    var $id,$uid,$username,$email,$fullname,$firstname,$lastname,$icno,$password,$salt,$birthDate,$country,$phoneNo,$address,$zipcode,$state,
        $bankName,$bankAccNumber,$bankAccName,$loginType,$userType,$rank,$achieveRank,$firstOrder,$orderStatus,$salesCommission,$redemptionPoint,$salesCommissionMonth,$redemptionPointMonth,$wallet,
        $status,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $uid
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * @param mixed $fullname
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getIcno()
    {
        return $this->icno;
    }

    /**
     * @param mixed $icno
     */
    public function setIcno($icno)
    {
        $this->icno = $icno;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param mixed $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return mixed
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param mixed $birthDate
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getPhoneNo()
    {
        return $this->phoneNo;
    }

    /**
     * @param mixed $phoneNo
     */
    public function setPhoneNo($phoneNo)
    {
        $this->phoneNo = $phoneNo;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param mixed $zipcode
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param mixed $bankName
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }
    
    /**
     * @return mixed
     */
    public function getBankAccNumber()
    {
        return $this->bankAccNumber;
    }

    /**
     * @param mixed $bankAccNumber
     */
    public function setBankAccNumber($bankAccNumber)
    {
        $this->bankAccNumber = $bankAccNumber;
    }

    /**
     * @return mixed
     */
    public function getBankAccName()
    {
        return $this->bankAccName;
    }

    /**
     * @param mixed $bankAccName
     */
    public function setBankAccName($bankAccName)
    {
        $this->bankAccName = $bankAccName;
    }

    /**
     * @return mixed
     */
    public function getLoginType()
    {
        return $this->loginType;
    }

    /**
     * @param mixed $loginType
     */
    public function setLoginType($loginType)
    {
        $this->loginType = $loginType;
    }

    /**
     * @return mixed
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * @param mixed $userType
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;
    }

    /**
     * @return mixed
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * @param mixed $rank
     */
    public function setRank($rank)
    {
        $this->rank = $rank;
    }

    /**
     * @return mixed
     */
    public function getAchieveRank()
    {
        return $this->achieveRank;
    }

    /**
     * @param mixed $achieveRank
     */
    public function setAchieveRank($achieveRank)
    {
        $this->achieveRank = $achieveRank;
    }

    /**
     * @return mixed
     */
    public function getFirstOrder()
    {
        return $this->firstOrder;
    }

    /**
     * @param mixed $firstOrder
     */
    public function setFirstOrder($firstOrder)
    {
        $this->firstOrder = $firstOrder;
    }

    /**
     * @return mixed
     */
    public function getOrderStatus()
    {
        return $this->orderStatus;
    }

    /**
     * @param mixed $orderStatus
     */
    public function setOrderStatus($orderStatus)
    {
        $this->orderStatus = $orderStatus;
    }

    /**
     * @return mixed
     */
    public function getSalesCommission()
    {
        return $this->salesCommission;
    }

    /**
     * @param mixed $salesCommission
     */
    public function setSalesCommission($salesCommission)
    {
        $this->salesCommission = $salesCommission;
    }

    /**
     * @return mixed
     */
    public function getRedemptionPoint()
    {
        return $this->redemptionPoint;
    }

    /**
     * @param mixed $redemptionPoint
     */
    public function setRedemptionPoint($redemptionPoint)
    {
        $this->redemptionPoint = $redemptionPoint;
    }

    /**
     * @return mixed
     */
    public function getSalesCommissionMonth()
    {
        return $this->salesCommissionMonth;
    }

    /**
     * @param mixed $salesCommissionMonth
     */
    public function setSalesCommissionMonth($salesCommissionMonth)
    {
        $this->salesCommissionMonth = $salesCommissionMonth;
    }

    /**
     * @return mixed
     */
    public function getRedemptionPointMonth()
    {
        return $this->redemptionPointMonth;
    }

    /**
     * @param mixed $redemptionPointMonth
     */
    public function setRedemptionPointMonth($redemptionPointMonth)
    {
        $this->redemptionPointMonth = $redemptionPointMonth;
    }

    /**
     * @return mixed
     */
    public function getWallet()
    {
        return $this->wallet;
    }

    /**
     * @param mixed $wallet
     */
    public function setWallet($wallet)
    {
        $this->wallet = $wallet;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getUser($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","username","email","fullname","firstname","lastname","icno","password","salt","birth_date","country","phone_no",
                            "address","zipcode","state","bank_name","bank_acc_number","bank_acc_name","login_type","user_type","rank","achieve_rank",
                            "first_order","order_status","sales_commission","redemption_point","sales_commission_month","redemption_point_month","wallet","status","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"user");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$username,$email,$fullname,$firstname,$lastname,$icno,$password,$salt,$birthDate,$country,$phoneNo,$address,$zipcode,$state,
                            $bankName,$bankAccNumber,$bankAccName,$loginType,$userType,$rank,$achieveRank,$firstOrder,$orderStatus,$salesCommission,$redemptionPoint,
                            $salesCommissionMonth,$redemptionPointMonth,$wallet,$status,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $user = new User;
            $user->setId($id);
            $user->setUid($uid);
            $user->setUsername($username);
            $user->setEmail($email);
            $user->setFullname($fullname);
            $user->setFirstname($firstname);
            $user->setLastname($lastname);
            $user->setIcno($icno);
            $user->setPassword($password);
            $user->setSalt($salt);
            $user->setBirthDate($birthDate);
            $user->setCountry($country);
            $user->setPhoneNo($phoneNo);
            $user->setAddress($address);
            $user->setZipcode($zipcode);
            $user->setState($state);
            $user->setBankName($bankName);
            $user->setBankAccNumber($bankAccNumber);
            $user->setBankAccName($bankAccName);
            $user->setLoginType($loginType);
            $user->setUserType($userType);
            $user->setRank($rank);
            $user->setAchieveRank($achieveRank);
            $user->setFirstOrder($firstOrder);
            $user->setOrderStatus($orderStatus);
            $user->setSalesCommission($salesCommission);
            $user->setRedemptionPoint($redemptionPoint);

            $user->setSalesCommissionMonth($salesCommissionMonth);
            $user->setRedemptionPointMonth($redemptionPointMonth);

            $user->setWallet($wallet);
            $user->setStatus($status);
            $user->setDateCreated($dateCreated);
            $user->setDateUpdated($dateUpdated);

            array_push($resultRows,$user);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
