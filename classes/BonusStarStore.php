<?php
class BonusStarStore {
    /* Member variables */
    var $id,$orderUid,$uid,$username,$receiverUid,$receiver,$uplineUid,$userType,$amount,$personalPV,$groupPV,$monthYear,$bonusType,$status,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getOrderUid()
    {
        return $this->orderUid;
    }

    /**
     * @param mixed $orderUid
     */
    public function setOrderUid($orderUid)
    {
        $this->orderUid = $orderUid;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getReceiverUid()
    {
        return $this->receiverUid;
    }

    /**
     * @param mixed $receiverUid
     */
    public function setReceiverUid($receiverUid)
    {
        $this->receiverUid = $receiverUid;
    }

    /**
     * @return mixed
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * @param mixed $receiver
     */
    public function setReceiver($receiver)
    {
        $this->receiver = $receiver;
    }

    /**
     * @return mixed
     */
    public function getUplineUid()
    {
        return $this->uplineUid;
    }

    /**
     * @param mixed $uplineUid
     */
    public function setUplineUid($uplineUid)
    {
        $this->uplineUid = $uplineUid;
    }


    /**
     * @return mixed
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * @param mixed $userType
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;
    }
    
    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }


    /**
     * @return mixed
     */
    public function getPersonaPV()
    {
        return $this->personalPV;
    }

    /**
     * @param mixed $personalPV
     */
    public function setPersonaPV($personalPV)
    {
        $this->personalPV = $personalPV;
    }

    /**
     * @return mixed
     */
    public function getGroupPV()
    {
        return $this->groupPV;
    }

    /**
     * @param mixed $groupPV
     */
    public function setGroupPV($groupPV)
    {
        $this->groupPV = $groupPV;
    }

    /**
     * @return mixed
     */
    public function getMonthYear()
    {
        return $this->monthYear;
    }

    /**
     * @param mixed $monthYear
     */
    public function setMonthYear($monthYear)
    {
        $this->monthYear = $monthYear;
    }


    /**
     * @return mixed
     */
    public function getBonusType()
    {
        return $this->bonusType;
    }

    /**
     * @param mixed $bonusType
     */
    public function setBonusType($bonusType)
    {
        $this->bonusType = $bonusType;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }
        /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }
}

function getBonusStarStore($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","order_uid","uid","username","receiver_uid","receiver","upline_uid","user_type","personal_pv","group_pv","month_year","amount","bonus_type","status",
                            "date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"bonus_star_store");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$orderUid,$uid,$username,$receiverUid,$receiver,$uplineUid,$userType,$amount,$personalPV,$groupPV,$monthYear,$bonusType,$status,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new BonusStarStore();
            $class->setId($id);
            $class->setOrderUid($orderUid);
            $class->setUid($uid);
            $class->setUsername($username);
            $class->setReceiverUid($receiverUid);
            $class->setReceiver($receiver);
            $class->setUplineUid($uplineUid);
            $class->setUserType($userType);
            $class->setAmount($amount);

            $class->setPersonaPV($personalPV);
            $class->setGroupPV($groupPV);
            $class->setMonthYear($monthYear);

            $class->setBonusType($bonusType);
            $class->setStatus($status);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }

}
