<?php
class Level {
    /* Member variables */
    var $id,$level,$salesCommission,$redemptionPoint,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }            

    /**
     * @return mixed
     */
    public function getSalesCommission()
    {
        return $this->salesCommission;
    }

    /**
     * @param mixed $salesCommission
     */
    public function setSalesCommission($salesCommission)
    {
        $this->salesCommission = $salesCommission;
    }       

    /**
     * @return mixed
     */
    public function getRedemptionPoint()
    {
        return $this->redemptionPoint;
    }

    /**
     * @param mixed $redemptionPoint
     */
    public function setRedemptionPoint($redemptionPoint)
    {
        $this->redemptionPoint = $redemptionPoint;
    }       

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getLevel($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","level","sales_commission","redemption_point","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"states");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */

        $stmt->bind_result($id,$level,$salesCommission,$redemptionPoint,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Level;
            $class->setId($id);
            $class->setLevel($level);
            $class->setSalesCommission($salesCommission);
            $class->setRedemptionPoint($redemptionPoint);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
