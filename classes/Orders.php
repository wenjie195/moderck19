<?php
class Orders {
    /* Member variables */
    var $id,$orderId,$uid,$name,$contact,$email,$addressOne,$addressTwo,$addressThree,$city,$postcode,$state,$country,$subtotal,$total,$productValue,$redemptionPoint,
        $bonusStatus,$payment_method,$payment_amount,$payment_reference,$payment_status,$shipping_status,$shipping_method,$shipping_date,$tracking_number,$remark,$receipt,
        $dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param mixed $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param mixed $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $id
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getAddressOne()
    {
        return $this->addressOne;
    }

    /**
     * @param mixed $addressOne
     */
    public function setAddressOne($addressOne)
    {
        $this->addressOne = $addressOne;
    }

    /**
     * @return mixed
     */
    public function getAddressTwo()
    {
        return $this->addressTwo;
    }

    /**
     * @param mixed $addressTwo
     */
    public function setAddressTwo($addressTwo)
    {
        $this->addressTwo = $addressTwo;
    }

    /**
     * @return mixed
     */
    public function getAddressThree()
    {
        return $this->addressThree;
    }

    /**
     * @param mixed $addressThree
     */
    public function setAddressThree($addressThree)
    {
        $this->addressThree = $addressThree;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param mixed $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * @param mixed $subtotal
     */
    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return mixed
     */
    public function getProductValue()
    {
        return $this->productValue;
    }

    /**
     * @param mixed $productValue
     */
    public function setProductValue($productValue)
    {
        $this->productValue = $productValue;
    }

    /**
     * @return mixed
     */
    public function getRedemptionPoint()
    {
        return $this->redemptionPoint;
    }

    /**
     * @param mixed $redemptionPoint
     */
    public function setRedemptionPoint($redemptionPoint)
    {
        $this->redemptionPoint = $redemptionPoint;
    }

    /**
     * @return mixed
     */
    public function getBonusStatus()
    {
        return $this->bonusStatus;
    }

    /**
     * @param mixed $bonusStatus
     */
    public function setBonusStatus($bonusStatus)
    {
        $this->bonusStatus = $bonusStatus;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethod()
    {
        return $this->payment_method;
    }

    /**
     * @param mixed $payment_method
     */
    public function setPaymentMethod($payment_method)
    {
        $this->payment_method = $payment_method;
    }

    /**
     * @return mixed
     */
    public function getPaymentAmount()
    {
        return $this->payment_amount;
    }

    /**
     * @param mixed $payment_amount
     */
    public function setPaymentAmount($payment_amount)
    {
        $this->payment_amount = $payment_amount;
    }

    /**
     * @return mixed
     */
    public function getPaymentReference()
    {
        return $this->payment_reference;
    }

    /**
     * @param mixed $payment_reference
     */
    public function setPaymentReference($payment_reference)
    {
        $this->payment_reference = $payment_reference;
    }

    /**
     * @return mixed
     */
    public function getPaymentStatus()
    {
        return $this->payment_status;
    }

    /**
     * @param mixed $payment_status
     */
    public function setPaymentStatus($payment_status)
    {
        $this->payment_status = $payment_status;
    }

    /**
     * @return mixed
     */
    public function getShippingStatus()
    {
        return $this->shipping_status;
    }

    /**
     * @param mixed $shipping_status
     */
    public function setShippingStatus($shipping_status)
    {
        $this->shipping_status = $shipping_status;
    }

    /**
     * @return mixed
     */
    public function getShippingMethod()
    {
        return $this->shipping_method;
    }

    /**
     * @param mixed $shipping_method
     */
    public function setShippingMethod($shipping_method)
    {
        $this->shipping_method = $shipping_method;
    }

    /**
     * @return mixed
     */
    public function getShippingDate()
    {
        return $this->shipping_date;
    }

    /**
     * @param mixed $shipping_date

     */
    public function setShippingDate($shipping_date)
    {
        $this->shipping_date = $shipping_date;
    }

    /**
     * @return mixed
     */
    public function getTrackingNumber()
    {
        return $this->tracking_number;
    }

    /**
     * @param mixed $tracking_number
     */
    public function setTrackingNumber($tracking_number)
    {
        $this->tracking_number = $tracking_number;
    }

    /**
     * @return mixed
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * @param mixed $remark
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;
    }

    /**
     * @return mixed
     */
    public function getReceipt()
    {
        return $this->receipt;
    }

    /**
     * @param mixed $receipt
     */
    public function setReceipt($receipt)
    {
        $this->receipt = $receipt;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getOrders($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","order_id","uid","name","contact","email","address_one","address_two","address_three","city","postcode","state","country","subtotal","total",
                            "product_value","redemption_point","bonus_status","payment_method","payment_amount","payment_reference","payment_status","shipping_status",
                            "shipping_method","shipping_date","tracking_number","remark","receipt","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"orders");
    if($whereClause){
        $sql .= $whereClause;
    }

    // echo $whereClause;

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$orderId,$uid,$name,$contact,$email,$addressOne,$addressTwo,$addressThree,$city,$postcode,$state,$country,$subtotal,$total,
                            $productValue,$redemptionPoint,$bonusStatus,$payment_method,$payment_amount,$payment_reference,$payment_status,
                            $shipping_status,$shipping_method,$shipping_date,$tracking_number,$remark,$receipt,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Orders();
            $class->setId($id);
            $class->setOrderId($orderId);
            $class->setUid($uid);
            $class->setName($name);
            $class->setContact($contact);
            $class->setEmail($email);
            $class->setAddressOne($addressOne);
            $class->setAddressTwo($addressTwo);
            $class->setAddressThree($addressThree);
            $class->setCity($city);
            $class->setPostcode($postcode);
            $class->setState($state);
            $class->setCountry($country);
            $class->setSubtotal($subtotal);
            $class->setTotal($total);

            $class->setProductValue($productValue);
            $class->setRedemptionPoint($redemptionPoint);
            $class->setBonusStatus($bonusStatus);

            $class->setPaymentMethod($payment_method);
            $class->setPaymentAmount($payment_amount);
            $class->setPaymentReference($payment_reference);
            $class->setPaymentStatus($payment_status);
            $class->setShippingStatus($shipping_status);
            $class->setShippingMethod($shipping_method);
            $class->setShippingDate($shipping_date);
            $class->setTrackingNumber($tracking_number);
            $class->setRemark($remark);
            $class->setReceipt($receipt);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
