<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Address.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Edit Address | MODERCK" />
<title>Edit Address | MODERCK</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Edit Address Book</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">

    <div class="width100 inner-bg inner-padding">
        <div class="width100 same-padding normal-min-height padding-top overflow">
                
        <?php
        if(isset($_POST['item_uid']))
        {
        $conn = connDB();
        $itemDetails = getAddress($conn,"WHERE uid = ? ", array("uid") ,array($_POST['item_uid']),"s");
        ?>

            <form action="utilities/userAddressEditFunction.php" method="POST">
            	<div class="dual-input">
        			<p class="top-p">Recipient Name</p>
                <input type="text" class="line-input clean" placeholder="Recipient Name" value="<?php echo $itemDetails[0]->getRecipient();?>" id="recipient_name" name="recipient_name" required>

				</div>
                 
                <div class="dual-input second-dual-input">  
                    <p class="top-p">Mobile Number</p>
                <input type="text" class="line-input clean" placeholder="Mobile Number" value="<?php echo $itemDetails[0]->getMobile();?>" id="mobile_no" name="mobile_no" required>
				</div> 
                <div class="clear"></div>   
            	<div class="dual-input">
        			<p class="top-p">House No & Street</p>
                <input type="text" class="line-input clean" placeholder="House / Road No" value="<?php echo $itemDetails[0]->getHouseRoad();?>" id="address" name="address" required>
				</div>  
                <div class="dual-input second-dual-input">  
                <p class="top-p">Postcode</p>
				<input type="text" class="line-input clean" placeholder="Postcode" id="postcode" value="<?php echo $itemDetails[0]->getPostcode();?>" name="postcode" required>
				</div>  
                <div class="clear"></div>      
            	<div class="dual-input">
                    <p class="top-p">City</p>
                <input type="text" class="line-input clean" placeholder="City" value="<?php echo $itemDetails[0]->getCity();?>" id="city" name="city" required>
				</div>  
                <div class="dual-input second-dual-input">  
        			<p class="top-p">State</p>
                <input type="text" class="line-input clean" placeholder="State" value="<?php echo $itemDetails[0]->getState();?>" id="state" name="state" required>
				</div>  
                <div class="clear"></div>      
            	<div class="dual-input">
        			<p class="top-p">Country</p>     

                <input type="text" class="line-input clean"  placeholder="Country" value="<?php echo $itemDetails[0]->getCountry();?>" id="country" name="country" required>

                </div>    
                <div class="dual-input second-dual-input">  
        			<p class="top-p">Set As Default Shipping Address?</p>
                    <!-- <select  class="line-input clean" id="default_ship" name="default_ship" > -->
                    <select  class="line-input clean" id="default_ship" name="default_ship" required>
                        <!-- <option value='Yes'>Yes</option>
                        <option value='No'>No</option> -->

                        <?php
                            $ship = $itemDetails[0]->getDefaultShip();
                            if($ship == 'Yes')
                            {
                            ?>
                                <option selected value="<?php echo $ship ?>"><?php echo $ship;?></option>
                                <option value="No">No</option>
                            <?php
                            }
                            elseif($ship == 'No')
                            {
                            ?>
                                <option selected value="<?php echo $ship ?>"><?php echo $ship;?></option>
                                <option value="Yes">Yes</option>
                            <?php
                            }
                            else
                            {
                            ?>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            <?php
                            }
                        ?>
                    </select>                  
				</div>    
                <div class="clear"></div>      
                <div class="dual-input ">  
        			<p class="top-p">Set As Default Billing Address?</p>
                    <!-- <select  class="line-input clean" id="default_bill" name="default_bill"> -->
                    <select  class="line-input clean" id="default_bill" name="default_bill" required>
                        <!-- <option value='Yes'>Yes</option>
                        <option value='No'>No</option> -->

                        <?php
                            $bill = $itemDetails[0]->getDefaultBill();
                            if($bill == 'Yes')
                            {
                            ?>
                                <option selected value="<?php echo $bill ?>"><?php echo $bill;?></option>
                                <option value="No">No</option>
                            <?php
                            }
                            elseif($bill == 'No')
                            {
                            ?>
                                <option selected value="<?php echo $bill ?>"><?php echo $bill;?></option>
                                <option value="Yes">Yes</option>
                            <?php
                            }
                            else
                            {
                            ?>
                                <option>    </option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            <?php
                            }
                        ?>
                    </select>                    
				</div>  
                <div class="clear"></div>     

                <input type="hidden" class="rec-input clean" value="<?php echo $itemDetails[0]->getUid();?>" id="item_uid" name="item_uid" readonly>
                <input type="hidden" class="rec-input clean" value="<?php echo $itemDetails[0]->getUserUid();?>" id="user_uid" name="user_uid" readonly>

                <div class="text-center middle-div-width">
                <button class="clean yellow-btn edit-profile-width" name="submit">Save</button>        
            	</div>
            </form>    

        <?php
        }
        ?>

</div>
    </div>

    <div class="clear"></div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>