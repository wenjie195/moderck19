<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
// require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Shipping.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $uid = $_SESSION['uid'];
// $uid = $_SESSION['temp_uid'];
$orderUid = $_SESSION['order_uid'];

$uid = $_SESSION['temp_uid'];
$uplineUid = $_SESSION['upline_uid'];


$conn = connDB();

// $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

// $orderDetails = getOrders($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1 ",array("uid"),array($uid),"s");
// $orderDetails = getOrders($conn," WHERE order_id = ?  ",array("order_id"),array($orderUid),"s");
$orderDetails = getOrders($conn," WHERE order_id = ? ORDER BY date_created DESC LIMIT 1 ",array("order_id"),array($orderUid),"s");
$orderID = $orderDetails[0]->getId();
$orderUID = $orderDetails[0]->getOrderId();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Shopping Cart | MODERCK" />
<title>Shopping Cart  | MODERCK</title>
<?php include 'css.php'; ?>
</head>
<body class="body">

<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Payment Details</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
        <div class="table-padding width100 same-padding details-min-height padding-top2 overflow overflow-x">  

            <h1 class="top-title brown-text"><?php echo $uid ;?></h1>
            <h1 class="top-title brown-text"><?php echo $uplineUid ;?></h1>

            <form method="post" action="billplzpost.php">
                <!-- <div class="width100 overflow">  -->
                <div class="width100 overflow-x">

                    <div class="dual-input">  
                        <p class="top-p">Name</p>
                        <input class="line-input clean" type="text" id="name" name="name" value="<?php echo $orderDetails[0]->getName();?>" readonly>
                    </div>

                    <div class="dual-input second-dual-input">  
                        <p class="top-p">Phone</p>
                        <input class="line-input clean" type="text" id="mobile" name="mobile" value="<?php echo $orderDetails[0]->getContact();?>" readonly> 
                    </div>

                    <div class="clear"></div>

                    <div class="dual-input">  
                        <p class="top-p">Amount</p>
                        <input class="line-input clean" type="text" value="RM <?php echo $orderDetails[0]->getSubtotal();?>" readonly> 
                    </div>

                    <div class="clear"></div>

                    <?php  
                        $subtotal = $orderDetails[0]->getSubtotal();
                        $adjustTotal = ($subtotal * 100);
                    ?>

                    <input class="line-input clean" type="hidden" id="email" name="email"> 
                    <!-- <input class="line-input clean" type="hidden" id="email" name="email" value="<?php //echo $userDetails[0]->getEmail();?>">  -->
                    <input class="line-input clean" type="hidden" id="amount" name="amount" value="<?php echo $adjustTotal; ?>" readonly> 
                    <!-- <input class="line-input clean" type="hidden" id="reference_1_label" name="reference_1_label" value="Order ID" readonly>  -->
                    <input class="input-name clean" type="hidden" id="reference_1_label" name="reference_1_label" value="Order ID" readonly> 
                    <input class="line-input clean" type="hidden" id="reference_1" name="reference_1" value="<?php echo $orderID; ?>" readonly>
                    <!-- <input class="line-input clean" type="hidden" id="reference_2_label" name="reference_2_label" value="ID" readonly>  -->
                    <input class="input-name clean" type="hidden" id="reference_2_label" name="reference_2_label" value="Order UID" readonly> 
                    <input class="line-input clean" type="hidden" id="reference_2" name="reference_2" value="<?php echo $orderUID; ?>" readonly> 

                    <div class="clear"></div> 

                    <!-- <div class="width100 text-center extra-spacing-up-down">
                        <button class="green-button checkout-btn clean" name="submit">Proceed To Payment</button>
                    </div>  -->

                    <div class="center-div2">
                        <button class="clean yellow-btn edit-profile-width ow-margin-left0" style="margin-top:50px;" type="submit">
                            Proceed To Payment
                        </button>
                    </div>

                </div> 
            </form>

        </div>
    </div>
</div>

<?php include 'js.php'; ?>

</body>
</html>