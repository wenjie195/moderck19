<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/DeliverRecord.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/User.php';
// require_once dirname(__FILE__) . '/classes/Payment.php';
// require_once dirname(__FILE__) . '/classes/Product.php';
// require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();

$allOrders = getOrders($conn, "WHERE payment_status = 'APPROVED' ");
$totalSales = 0; // initital
if($allOrders)
{
    for ($b=0; $b <count($allOrders) ; $b++)
    {
        $totalSales += $allOrders[$b]->getSubtotal();
    }
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/adminDashboard.php" />
<link rel="canonical" href="https://agentpnchc.com/adminDashboard.php" /> -->
<meta property="og:title" content="Admin Dashboard | MODERCK" />
<title>Admin Dashboard | MODERCK</title>

<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Dashboard</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">

    <div class="width100 inner-bg inner-padding">
    <!-- <div class="clear"></div> -->

        <div class="same-padding width100 min-sp-height overflow padding-top30">
            <div class="first-flex">
                <!-- <a href="profile.php"> -->
                <a href="#.php"  class="ow-flex-three2">
                    <div class="three-div-inner">
                        
                           
                            <p class="three-div-p gold-text">MYSC<br>1000.00</p>
                       
                    </div>
                </a>
                <a href="#.php"  class="ow-flex-three2">
                    <div class="three-div-inner">
                       
                            
                            <p class="three-div-p gold-text">MYRP<br>2000.00</p>
                        
                    </div>
                </a>
                <a href="#.php"  class="ow-flex-three2">
                    <div class="three-div-inner">
                       
                           
                            <p class="three-div-p gold-text">KPTS<br>12</p>
                       
                    </div>
                </a>
                
                <a href="#.php"  class="ow-flex-three2">
                    <div class="three-div-inner">
                        
                            
                            <p class="three-div-p gold-text">Total Sales (MYR)<br><?php echo $totalSales;?></p>
                        
                    </div>
                </a>
                <a href="#.php"  class="ow-flex-three2">
                    <div class="three-div-inner">
                        
                            
                            <p class="three-div-p gold-text">MG Transfer</p>
                        
                    </div>
                </a>
                <a href="#.php"  class="ow-flex-three2">
                    <div class="three-div-inner">
                        
                           
                            <p class="three-div-p gold-text">KRYP Transfer</p>
                        
                    </div>
                </a>                      
            </div>
        </div>

        <h1 class="top-title brown-text padding-top30">Generate Bonus (1st Purchase / Rebates)</h1>
        <div class="text-center width100 overflow">

            <!-- <div class="text-center middle-div-width">
                <a href="adminAllOrdersRenewProOne.php" target="_blank">
                    <div class="clean white-button ow-red-bg white-text smaller-button">Product 1</div>
                </a> 
            </div> 

            <div class="clear"></div>

            <div class="text-center middle-div-width">
                <a href="adminAllOrdersRenewProTwo.php" target="_blank">
                    <div class="clean white-button ow-red-bg white-text smaller-button">Product 2</div>
                </a> 
            </div> 

            <div class="clear"></div>

            <div class="text-center middle-div-width">
                <a href="adminAllOrdersRenewProThree.php" target="_blank">
                    <div class="clean white-button ow-red-bg white-text smaller-button">Product 3</div>
                </a> 
            </div>  -->

            <!-- <div class="text-center middle-div-width">
                <a href="adminAllOrdersRenewProOneRenew.php" target="_blank">
                    <div class="clean white-button ow-red-bg white-text smaller-button">Product 1</div>
                </a> 
            </div>  -->

            <div class="text-center middle-div-width">
                <a href="adminFirstPurchaseOrRebatesProductOne.php" target="_blank">
                    <div class="clean white-button ow-red-bg white-text smaller-button">Product 1</div>
                </a> 
            </div> 

            <div class="clear"></div>

            <div class="text-center middle-div-width">
                <!-- <a href="adminAllOrdersRenewProTwoRenew.php" target="_blank"> -->
                <a href="adminFirstPurchaseOrRebatesProductTwo.php" target="_blank">
                    <div class="clean white-button ow-red-bg white-text smaller-button">Product 2</div>
                </a> 
            </div> 

            <div class="clear"></div>

            <div class="text-center middle-div-width">
                <!-- <a href="adminAllOrdersRenewProThreeRenew.php" target="_blank"> -->
                <a href="adminFirstPurchaseOrRebatesProductThree.php" target="_blank">
                    <div class="clean white-button ow-red-bg white-text smaller-button">Product 3</div>
                </a> 
            </div> 

            <div class="text-center middle-div-width">
                <a href="starAgent.php" target="_blank">
                    <div class="clean white-button ow-red-bg white-text smaller-button">Find Star Agent (250k PV)</div>
                </a> 
            </div> 

            <!-- <div class="text-center middle-div-width">
                <a href="starAgentDistributeBonus.php" target="_blank">
                    <div class="clean white-button ow-red-bg white-text smaller-button">Distribute Star Agent Bonus</div>
                </a> 
            </div>  -->

            <div class="text-center middle-div-width">
                <a href="starAgentGetQualifier.php" target="_blank">
                    <div class="clean white-button ow-red-bg white-text smaller-button">Get Qualifier</div>
                </a> 
            </div> 

            <div class="text-center middle-div-width">
                <a href="starAgentShareBonus.php" target="_blank">
                    <div class="clean white-button ow-red-bg white-text smaller-button">Distribute Star Agent Bonus</div>
                </a> 
            </div> 

            <div class="clear"></div>

        </div>

    </div>

    <div class="clear"></div>

</div>

<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>