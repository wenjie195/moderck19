<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/InactiveList.php';
// require_once dirname(__FILE__) . '/../classes/Upgrading.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

date_default_timezone_set('Asia/Kuala_Lumpur');
echo "Current Date : ";
// echo $todayDate = "2024-01-10";
echo $todayDate = date('Y-m-d');

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $userUid = rewrite($_POST['user_uid']);

     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");  

     $salesCommission = $userDetails[0]->getSalesCommission();
     $redemptionPoint = $userDetails[0]->getRedemptionPoint();
     $emptySalesCommission = "0";
     $emptyRedemptionPoint = "0";
     $status = "Active";
     
     $inactiveDetails = getInactiveList($conn," WHERE user_uid = ? AND status = 'Running' ORDER BY date_created DESC LIMIT 1 ",array("user_uid"),array($userUid),"s");   
     $inactiveListUid = $inactiveDetails[0]->getUid();
     $renewStatus = "Clear";

     if($userDetails)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";

          // if($salesCommission)
          if($emptySalesCommission || !$emptySalesCommission)
          {
               array_push($tableName,"sales_commission");
               array_push($tableValue,$emptySalesCommission);
               $stringType .=  "d";
          }
          // if($redemptionPoint)
          if($emptyRedemptionPoint || !$emptyRedemptionPoint)
          {
               array_push($tableName,"redemption_point");
               array_push($tableValue,$emptyRedemptionPoint);
               $stringType .=  "d";
          }
          if($status)
          {
               array_push($tableName,"status");
               array_push($tableValue,$status);
               $stringType .=  "s";
          }    

          array_push($tableValue,$userUid);
          $stringType .=  "s";
          $userTableUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($userTableUpdated)
          {
               // echo "User Table Updated !";
               if($inactiveDetails)
               {   
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
          
                    // if($emptySalesCommission || !$emptySalesCommission)
                    if($salesCommission)
                    {
                         array_push($tableName,"sales_commission");
                         array_push($tableValue,$salesCommission);
                         $stringType .=  "d";
                    }
                    // if($emptyRedemptionPoint || !$emptyRedemptionPoint)
                    if($redemptionPoint)
                    {
                         array_push($tableName,"redemption_point");
                         array_push($tableValue,$redemptionPoint);
                         $stringType .=  "d";
                    }
                    if($renewStatus)
                    {
                         array_push($tableName,"status");
                         array_push($tableValue,$renewStatus);
                         $stringType .=  "s";
                    }    
                    if($todayDate)
                    {
                         array_push($tableName,"end_date");
                         array_push($tableValue,$todayDate);
                         $stringType .=  "s";
                    }  
          
                    array_push($tableValue,$inactiveListUid);
                    $stringType .=  "s";
                    $userTableUpdated = updateDynamicData($conn,"inactive_list"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($userTableUpdated)
                    {
                         // echo "inactive List Table Updated !";
                         $_SESSION['messageType'] = 1;
                         header('Location: ../adminSalesPurchaseListing.php?type=1');
                    }
                    else
                    {
                         // echo "Fail";
                         $_SESSION['messageType'] = 1;
                         header('Location: ../adminSalesPurchaseListing.php?type=2');
                    }
               }
               else
               {
                    // echo "Error";
                    $_SESSION['messageType'] = 1;
                    header('Location: ../adminSalesPurchaseListing.php?type=3');
               }
          }
          else
          {
               // echo "Fail";
               $_SESSION['messageType'] = 1;
               header('Location: ../adminSalesPurchaseListing.php?type=4');
          }
     }
     else
     {
          // echo "Error";
          $_SESSION['messageType'] = 1;
          header('Location: ../adminSalesPurchaseListing.php?type=5');
     }

}
else
{
     header('Location: ../index.php');
}
?>