<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$email = $_POST['email'];
$userDetails = getUser($conn, "WHERE email = ? ",array("email"),array($email), "s");
if(empty($userDetails))
{
  $alert = "Available Email.";
  $alertBin = 0;
}
else
{
  $alert = "You have entered an existing email in our current membership records.";
  $alertBin = 1;
}

$result[] = array("result" => $alert, "alertBin" => $alertBin);
echo json_encode($result);
?>