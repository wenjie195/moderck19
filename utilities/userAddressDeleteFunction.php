<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Address.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $itemUid = rewrite($_POST['item_uid']);

     // $userUid = $_SESSION['uid'];

     $status = "Delete";

     $addressDetails = getAddress($conn," WHERE uid = ? ",array("uid"),array($itemUid),"s");   

     if($addressDetails)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($status)
          {
               array_push($tableName,"status");
               array_push($tableValue,$status);
               $stringType .=  "s";
          }
          array_push($tableValue,$itemUid);
          $stringType .=  "s";
          $announcementUpdated = updateDynamicData($conn,"address"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($announcementUpdated)
          {
               // echo "Success";
               header('Location: ../userAddressBook.php');
          }
          else
          {
               echo "Fail";
          }
     }
     else
     {
          echo "Error";
     }

}
else 
{
     header('Location: ../index.php');
}
?>