<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

// require_once dirname(__FILE__) . '/../classes/Bonus.php';
require_once dirname(__FILE__) . '/../classes/BonusSalesOrRebate.php';
// require_once dirname(__FILE__) . '/../classes/BonusStar.php';
// require_once dirname(__FILE__) . '/../classes/Level.php';
require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/OrderList.php';
// require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
// require_once dirname(__FILE__) . '/../classes/SalesCommissionBackup.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     echo "Order UID : ";  
     echo $orderUid = rewrite($_POST["order_uid"]);
     echo "<br>";  
     echo "<br>";

     // $orderListDetails = getOrderList($conn, " WHERE order_id =? ",array("order_id"),array($orderUid),"s");
     // if($orderListDetails)
     // {
     //      $totalProductValue = 0;
     //      $totalRedemptionPoint = 0;
     //      for($cnt = 0;$cnt < count($orderListDetails) ;$cnt++)
     //      {
     //           echo $orderListDetails[$cnt]->getProductName();
     //           echo " , Quantity : ";
     //           echo $orderListDetails[$cnt]->getQuantity();
     //           echo " , PV :";
     //           echo $orderListDetails[$cnt]->getProductValue();
     //           echo " , RP : ";
     //           echo $orderListDetails[$cnt]->getRedemptionPoint();
     //           echo "<br>";

     //           $totalProductValue += $orderListDetails[$cnt]->getProductValue();
     //           $totalRedemptionPoint += $orderListDetails[$cnt]->getRedemptionPoint();
     //      }

     //      // $totalAmount = 0; // initital
     //      // for ($cnt=0; $cnt <count($orderListDetails) ; $cnt++)
     //      // {
     //      //     $totalAmount += $orderListDetails[$b]->getProductValue();
     //      // }
     // }

     // echo "Total Product Value :";
     // echo $totalProductValue;
     // echo "<br>";
     // echo "Total Redemption Point :";
     // echo $totalRedemptionPoint;
     // echo "<br>";
     // echo "<br>";

     // $purchaserOrderDetails = getOrders($conn, " WHERE order_id = '$orderUid' ");
     $purchaserOrderDetails = getOrders($conn, " WHERE order_id =? ",array("order_id"),array($orderUid),"s");
     echo $purchaserUid = $purchaserOrderDetails[0]->getUid();
     echo " , ";
     $purchaserDetails = getUser($conn, " WHERE uid = ? ",array("uid"),array($purchaserUid),"s");
     echo $purchaserUsername = $purchaserDetails[0]->getUsername();
     echo "<br>";  
     echo "PV : ";
     echo $purchaserSalesCommission = $purchaserDetails[0]->getSalesCommission();
     echo " , RP : ";
     echo $purchaserRedemptionPoint = $purchaserDetails[0]->getRedemptionPoint();
     echo "<br>";
     // echo "<br>";

     // $rebateBonusDetails = getBonusSalesOrRebate($conn, " WHERE order_uid = ? AND receiver_uid = ? ",array("order_uid","receiver_uid"),array($orderUid,$purchaserUid),"ss");
     // if($rebateBonusDetails)
     // {
     //      for($cnt = 0;$cnt < count($rebateBonusDetails) ;$cnt++)
     //      {
     //           echo $rebateBonusDetails[$cnt]->getId();
     //           echo " , ";
     //           echo $rebateBonusDetails[$cnt]->getReceiver();
     //           echo " , ";
     //           echo $rebateBonusDetails[$cnt]->getAmount();
     //           echo "<br>";
     //      }
     // }
     // else
     // {
     //      echo "no rebates";
     // }

     echo "Rebate For Purchaser";
     echo "<br>";
     echo "Rebates";
     echo "<br>";
     $rebateBonusDetails = getBonusSalesOrRebate($conn, " WHERE order_uid = ? AND receiver_uid = ? AND bonus_type = '20% Rebate' ",array("order_uid","receiver_uid"),array($orderUid,$purchaserUid),"ss");
     if($rebateBonusDetails)
     {
          $totalRebateBonus = 0;
          for($cnt = 0;$cnt < count($rebateBonusDetails) ;$cnt++)
          {
               echo $rebateBonusDetails[$cnt]->getId();
               echo " , ";
               echo $rebateBonusDetails[$cnt]->getReceiver();
               echo " , ";
               echo $rebateBonusDetails[$cnt]->getAmount();
               $totalRebateBonus += $rebateBonusDetails[$cnt]->getAmount();
               echo "<br>";
          }
     }
     else
     {
          echo "no rebates";
     }
     echo "Total Rebates :";
     echo $totalRebateBonus;
     echo "<br>";

     echo "Redemption Point";
     echo "<br>";
     $rebateBonusRPDetails = getBonusSalesOrRebate($conn, " WHERE order_uid = ? AND receiver_uid = ? AND bonus_type = '2% Redemption Point' ",array("order_uid","receiver_uid"),array($orderUid,$purchaserUid),"ss");
     if($rebateBonusDetails)
     {
          $totalRedemptionPoint = 0;
          for($cnt = 0;$cnt < count($rebateBonusRPDetails) ;$cnt++)
          {
               echo $rebateBonusRPDetails[$cnt]->getId();
               echo " , ";
               echo $rebateBonusRPDetails[$cnt]->getReceiver();
               echo " , ";
               echo $rebateBonusRPDetails[$cnt]->getAmount();
               $totalRedemptionPoint += $rebateBonusRPDetails[$cnt]->getAmount();
               echo "<br>";
          }
     }
     else
     {
          echo "no rebates";
     }
     echo "Total Redemption Point :";
     echo $totalRedemptionPoint;

     echo "<br>";
     echo "<br>";

     // $bonusSalesAndRebateDetails = getBonusSalesOrRebate($conn, " WHERE order_uid =? ",array("order_uid"),array($orderUid),"s");
     // if($bonusSalesAndRebateDetails)
     // {
     //      for($cnt = 0;$cnt < count($bonusSalesAndRebateDetails) ;$cnt++)
     //      {
     //           echo $bonusSalesAndRebateDetails[$cnt]->getId();
     //           echo " , ";
     //           echo $bonusSalesAndRebateDetails[$cnt]->getReceiver();
     //           echo " , ";
     //           echo $bonusSalesAndRebateDetails[$cnt]->getAmount();
     //           echo "<br>";
     //      }
     // }

     // Referral History
     $userRH = getReferralHistory($conn, " WHERE referral_id = ? ",array("referral_id"),array($purchaserUid),"s");
     //Direct Upline Details
     echo $directUplineUid = $userRH[0]->getReferrerId();
     echo " , ";
     $uplineDetails = getUser($conn, " WHERE uid = ? ",array("uid"),array($directUplineUid),"s");
     echo $directUplineUsername = $uplineDetails[0]->getUsername();
     echo "<br>";  
     echo "PV : ";
     echo $uplineSalesCommission = $uplineDetails[0]->getSalesCommission();
     echo " , RP : ";
     echo $uplineRedemptionPoint = $uplineDetails[0]->getRedemptionPoint();
     echo "<br>";  

     // $salesBonusDetails = getBonusSalesOrRebate($conn, " WHERE order_uid = ? AND receiver_uid = ? ",array("order_uid","receiver_uid"),array($orderUid,$directUplineUid),"ss");
     // if($salesBonusDetails)
     // {
     //      for($cnt = 0;$cnt < count($salesBonusDetails) ;$cnt++)
     //      {
     //           echo $salesBonusDetails[$cnt]->getId();
     //           echo " , ";
     //           echo $salesBonusDetails[$cnt]->getReceiver();
     //           echo " , ";
     //           echo $salesBonusDetails[$cnt]->getAmount();
     //           echo "<br>";
     //      }
     // }
     // else
     // {
     //      echo "no bonus";
     // }

     echo "Commission For Upline";
     echo "<br>";
     echo "First Purchase Commission";
     echo "<br>";
     $salesBonusCommissionDetails = getBonusSalesOrRebate($conn, " WHERE order_uid = ? AND receiver_uid = ? AND bonus_type = '20% First Purchase Commission' ",array("order_uid","receiver_uid"),array($orderUid,$directUplineUid),"ss");
     if($salesBonusCommissionDetails)
     {
          $totalCommission= 0;
          for($cnt = 0;$cnt < count($salesBonusCommissionDetails) ;$cnt++)
          {
               echo $salesBonusCommissionDetails[$cnt]->getId();
               echo " , ";
               echo $salesBonusCommissionDetails[$cnt]->getReceiver();
               echo " , ";
               echo $salesBonusCommissionDetails[$cnt]->getAmount();
               $totalCommission += $salesBonusCommissionDetails[$cnt]->getAmount();
               echo "<br>";
          }
     }
     else
     {
          echo "no bonus";
     }
     echo "Total Commission :";
     echo $totalCommission;

     echo "<br>";
     echo "Redemption Point";
     echo "<br>";
     $salesBonusRPDetails = getBonusSalesOrRebate($conn, " WHERE order_uid = ? AND receiver_uid = ? AND bonus_type = '2% Redemption Point' ",array("order_uid","receiver_uid"),array($orderUid,$directUplineUid),"ss");
     if($salesBonusRPDetails)
     {
          $totalRedemptionPointUpline = 0;
          for($cnt = 0;$cnt < count($salesBonusRPDetails) ;$cnt++)
          {
               echo $salesBonusRPDetails[$cnt]->getId();
               echo " , ";
               echo $salesBonusRPDetails[$cnt]->getReceiver();
               echo " , ";
               echo $salesBonusRPDetails[$cnt]->getAmount();
               $totalRedemptionPointUpline += $salesBonusRPDetails[$cnt]->getAmount();
               echo "<br>";
          }
     }
     else
     {
          echo "no bonus";
     }
     echo "Total Redemption Point :";
     echo $totalRedemptionPointUpline;

     echo "<br>";
     echo "<br>";
     echo "<br>";

     echo "After Deduction";
     echo "<br>";
     echo "Purchaser";
     echo "<br>";
     echo "Sales Commission : ";
     echo $renewSalesCommissionPurchaser = $purchaserSalesCommission -$totalRebateBonus;
     echo " , Redemption Point : ";
     echo $renewRedemptionPointPurchaser = $purchaserRedemptionPoint -$totalRedemptionPoint;
     echo "<br>";
     echo "Upline";
     echo "<br>";
     echo "Sales Commission : ";
     echo $renewSalesCommissionUpline = $uplineSalesCommission -$totalCommission;
     echo " , Redemption Point : ";
     echo $renewRedemptionPointUpline = $uplineRedemptionPoint -$totalRedemptionPointUpline;

     // $orderListDetails = getOrderList($conn, " WHERE order_id =? ",array("order_id"),array($orderUid),"s");
     // if($orderListDetails)
     // {
     //      $tableName = array();
     //      $tableValue =  array();
     //      $stringType =  "";
     //      //echo "save to database";
     //      if($username)
     //      {
     //           array_push($tableName,"username");
     //           array_push($tableValue,$username);
     //           $stringType .=  "s";
     //      }
     //      array_push($tableValue,$itemUid);
     //      $stringType .=  "s";
     //      $announcementUpdated = updateDynamicData($conn,"livestream"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
     //      if($announcementUpdated)
     //      {
     //           echo "Success";
     //           // header('Location: ../adminLivestreamAll.php');
     //      }
     //      else
     //      {
     //           echo "Fail";
     //      }
     // }

     echo "<br>";
     echo "<br>";

     $orderListDetails = getOrderList($conn, " WHERE order_id =? ",array("order_id"),array($orderUid),"s");
     if($orderListDetails)
     {
          $updateStatus = "Deducted";

          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($updateStatus)
          {
               array_push($tableName,"status");
               array_push($tableValue,$updateStatus);
               $stringType .=  "s";
          }
          array_push($tableValue,$orderUid);
          $stringType .=  "s";
          $updateOrderList = updateDynamicData($conn,"order_list"," WHERE order_id = ? ",$tableName,$tableValue,$stringType);
          if($updateOrderList)
          {
               echo "Success 1";
               // header('Location: ../adminLivestreamAll.php');

               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";

               // if($renewSalesCommissionUpline)
               if(!$renewSalesCommissionUpline || $renewSalesCommissionUpline)
               {
                    array_push($tableName,"sales_commission");
                    array_push($tableValue,$renewSalesCommissionUpline);
                    $stringType .=  "s";
               }
               // if($renewRedemptionPointUpline)
               if(!$renewRedemptionPointUpline || $renewRedemptionPointUpline)
               {
                    array_push($tableName,"redemption_point");
                    array_push($tableValue,$renewRedemptionPointUpline);
                    $stringType .=  "s";
               }
               array_push($tableValue,$directUplineUid);
               $stringType .=  "s";
               $updateOrderList = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               if($updateOrderList)
               {
                    echo "Success 2";

                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
                    
                    if(!$renewSalesCommissionPurchaser || $renewSalesCommissionPurchaser)
                    {
                         array_push($tableName,"sales_commission");
                         array_push($tableValue,$renewSalesCommissionPurchaser);
                         $stringType .=  "s";
                    }
                    if(!$renewRedemptionPointPurchaser || $renewRedemptionPointPurchaser)
                    {
                         array_push($tableName,"redemption_point");
                         array_push($tableValue,$renewRedemptionPointPurchaser);
                         $stringType .=  "s";
                    }
                    array_push($tableValue,$purchaserUid);
                    $stringType .=  "s";
                    $updateOrderList = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($updateOrderList)
                    {
                         echo "Success 3";
                         // header('Location: ../adminSalesPurchaseListing.php');

                         $updateOrderStatus = "Rejected";

                         $tableName = array();
                         $tableValue =  array();
                         $stringType =  "";
                         //echo "save to database";
                         if($updateOrderStatus)
                         {
                              array_push($tableName,"payment_status");
                              array_push($tableValue,$updateOrderStatus);
                              $stringType .=  "s";
                         }
                         array_push($tableValue,$orderUid);
                         $stringType .=  "s";
                         $updatePurchaseStatus = updateDynamicData($conn,"orders"," WHERE order_id = ? ",$tableName,$tableValue,$stringType);
                         if($updatePurchaseStatus)
                         {
                              // echo "Success 4";
                              header('Location: ../adminSalesPurchaseListing.php');
                         }
                         else
                         {
                              echo "Fail to update order status";
                         }
                    }
                    else
                    {
                         echo "Fail";
                    }

               }
               else
               {
                    echo "Fail";
               }

          }
          else
          {
               echo "Fail";
          }
     }
}
else 
{
    header('Location: ../index.php');
}
?>