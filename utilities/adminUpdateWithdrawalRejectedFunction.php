<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Withdrawal.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $withdrawalUid = rewrite($_POST["item_uid"]);

     $reason = rewrite($_POST["reject_season"]);
     $status = "REJECTED";

     // //   FOR DEBUGGING
     // echo "<br>";
     // // echo $approvedStatus."<br>";
     // // echo $rejectedStatus."<br>";

     $itemDetails = getWithdrawal($conn," WHERE withdrawal_uid = ? ",array("withdrawal_uid"),array($withdrawalUid),"s");   

     $withdrawalAmount =  $itemDetails[0]->getAmount();
     $userUid =  $itemDetails[0]->getUid();

     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");   
     $currentCommission =  $userDetails[0]->getSalesCommission();
     $refundAmount = $currentCommission + $withdrawalAmount;

     if($itemDetails)
     {  
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($reason)
          {
               array_push($tableName,"reason");
               array_push($tableValue,$reason);
               $stringType .=  "s";
          }
          if($status)
          {
               array_push($tableName,"status");
               array_push($tableValue,$status);
               $stringType .=  "s";
          }

          array_push($tableValue,$withdrawalUid);
          $stringType .=  "s";
          $itemUpdated = updateDynamicData($conn,"withdrawal"," WHERE withdrawal_uid = ? ",$tableName,$tableValue,$stringType);
          if($itemUpdated)
          {
               // echo "Success !";
               // header('Location: ../adminWithdrawalCompleted.php');

               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
               if($refundAmount)
               {
                    // array_push($tableName,"sales_commission");
                    array_push($tableName,"sales_commission_month");
                    array_push($tableValue,$refundAmount);
                    $stringType .=  "d";
               }
               array_push($tableValue,$userUid);
               $stringType .=  "s";
               $updateUser = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               if($updateUser)
               {
                    // echo "Success !";
                    header('Location: ../adminWithdrawalCompleted.php');
               }
               else
               {
                    echo "Fail 1 !";
               }

          }
          else
          {
               echo "Fail 2 !";
          }
     }
     else
     {    echo "ERROR !"; }
}
else
{
     header('Location: ../index.php');
}
?>