<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$username = $_POST['username'];
$userDetails = getUser($conn, "WHERE username = ? ",array("username"),array($username), "s");
if(empty($userDetails))
{
  $alert = "Available Username.";
  $alertBin = 0;
}
else
{
  $alert = "You have entered an existing username in our current membership records.";
  $alertBin = 1;
}

$result[] = array("result" => $alert, "alertBin" => $alertBin);
echo json_encode($result);
?>