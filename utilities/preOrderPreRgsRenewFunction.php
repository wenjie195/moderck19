<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/PreOrderList.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Product.php';

// require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $userUid = $_SESSION['uid'];

function addPreOrder($conn,$userUid,$itemUid,$productName,$productPrice,$quantity,$totalPrice,$status,$mainProductUid,$totalProductValue,$totalRedemptionPoint)
{
     if(insertDynamicData($conn,"preorder_list",array("user_uid","product_uid","product_name","original_price","quantity","totalPrice","status","main_product_uid","product_value","redemption_point"),
          array($userUid,$itemUid,$productName,$productPrice,$quantity,$totalPrice,$status,$mainProductUid,$totalProductValue,$totalRedemptionPoint),"ssssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $uid = md5(uniqid());

     echo $_SESSION['upline_uid'] = rewrite($_POST['upline_uid']);
     echo "<br>";
     $_SESSION['temp_uid'] = rewrite($_POST['temp_uid']);
     echo $userUid = $_SESSION['temp_uid'];
     echo "<br>";

     // $_SESSION['uid'] = rewrite($_POST['temp_uid']);
     // $userUid = $_SESSION['uid'];

     $itemUid = rewrite($_POST['item_uid']);
     $quantity = rewrite($_POST['quantity']);

     $productDetails = getProduct($conn,"WHERE uid = ? ", array("uid") ,array($itemUid),"s");
     $productName = $productDetails[0]->getName();
     $productPrice = $productDetails[0]->getPrice();

     $productValue = $productDetails[0]->getProductValue();
     $redemptionPoint = $productDetails[0]->getRedemptionPoint();
     $totalProductValue = $productValue * $quantity;
     $totalRedemptionPoint = $redemptionPoint * $quantity;

     $mainProductUid = rewrite($_POST['item_uid']);
     // $mainProductUid = " ";

     $totalPrice = $productPrice * $quantity;
     $status = 'Pending';

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $name."<br>";

     // if(addPreOrder($conn,$userUid,$itemUid,$productName,$productPrice,$quantity,$totalPrice,$status,$mainProductUid,$totalProductValue,$totalRedemptionPoint))
     // {
     //      // echo "product added to cart";
     //      // header('Location: ../shoppingCart.php');
     //      // header('Location: ../shoppingCartPreRgs.php');

     //      // echo "<script> window.history.go(-1);</script>";

     //      // header('Location: ../eCommerceSitePreRgsWithUid.php');

     //      echo "Upline Uid : ";
     //      echo $_SESSION['upline_uid'];
     //      echo "<br>";
     //      echo "Temp Uid : ";
     //      echo $_SESSION['temp_uid'];
     //      header('Location: ../eCommerceSitePreRgsContinueWithUid.php');

     // }
     // else
     // {
     //      // echo "fail to add product into cart";
     //      echo "<script> window.history.go(-1);</script>";
     // }

     // if($quantity < 1)
     // {
     //      // header('Location: ../eCommerceSite.php');
     //      echo "<script> window.history.go(-1);</script>";
     // }
     // else
     // {
     //      if(addPreOrder($conn,$userUid,$itemUid,$productName,$productPrice,$quantity,$totalPrice,$status,$mainProductUid,$totalProductValue,$totalRedemptionPoint))
     //      {
     //           echo "Upline Uid : ";
     //           echo $_SESSION['upline_uid'];
     //           echo "<br>";
     //           echo "Temp Uid : ";
     //           echo $_SESSION['temp_uid'];
     //           header('Location: ../eCommerceSitePreRgsContinueWithUid.php');
     //      }
     //      else
     //      {
     //           echo "<script> window.history.go(-1);</script>";
     //      }
     // }

     // $itemPendingOrder = getPreOrderList($conn, "WHERE user_uid = ? AND product_uid = ? AND status = 'Pending' ",array("user_uid, product_uid"),array($userUid,$itemUid),"ss");
     // if($itemPendingOrder)
     // {
     //      // echo "Yes";
     //      $previousQuantity = $itemPendingOrder[0]->getQuantity();
     //      $renewQuantity = $quantity + $previousQuantity;
     //      $renewPrice = ($renewQuantity * $productPrice);

     //      $productValue = $productDetails[0]->getProductValue();
     //      $redemptionPoint = $productDetails[0]->getRedemptionPoint();
     //      $renewTotalProductValue = $productValue * $renewQuantity;
     //      $renewTotalRedemptionPoint = $redemptionPoint * $renewQuantity;

     // }
     // else
     // {
     //      echo "No";
     // }

     if($quantity < 1)
     {
          // header('Location: ../eCommerceSite.php');
          echo "<script> window.history.go(-1);</script>";
     }
     else
     {
          $itemPendingOrder = getPreOrderList($conn, "WHERE user_uid = ? AND product_uid = ? AND status = 'Pending' ",array("user_uid, product_uid"),array($userUid,$itemUid),"ss");
          if($itemPendingOrder)
          {
               // echo "Yes";
               $previousOrderId = $itemPendingOrder[0]->getId();

               $previousQuantity = $itemPendingOrder[0]->getQuantity();
               $renewQuantity = $quantity + $previousQuantity;
               $renewPrice = ($renewQuantity * $productPrice);
     
               $productValue = $productDetails[0]->getProductValue();
               $redemptionPoint = $productDetails[0]->getRedemptionPoint();
               $renewTotalProductValue = $productValue * $renewQuantity;
               $renewTotalRedemptionPoint = $redemptionPoint * $renewQuantity;

               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
               if($renewQuantity)
               {
                   array_push($tableName,"quantity");
                   array_push($tableValue,$renewQuantity);
                   $stringType .=  "s";
               }
               if($renewPrice)
               {
                   array_push($tableName,"totalPrice");
                   array_push($tableValue,$renewPrice);
                   $stringType .=  "s";
               }
               if($renewTotalProductValue)
               {
                   array_push($tableName,"product_value");
                   array_push($tableValue,$renewTotalProductValue);
                   $stringType .=  "s";
               }
               if($renewTotalRedemptionPoint)
               {
                   array_push($tableName,"redemption_point");
                   array_push($tableValue,$renewTotalRedemptionPoint);
                   $stringType .=  "s";
               }       
               array_push($tableValue,$previousOrderId);
               $stringType .=  "s";
               $passwordUpdated = updateDynamicData($conn,"preorder_list"," WHERE id = ? ",$tableName,$tableValue,$stringType);
               if($passwordUpdated)
               {
                    echo "Upline Uid : ";
                    echo $_SESSION['upline_uid'];
                    echo "<br>";
                    echo "Temp Uid : ";
                    echo $_SESSION['temp_uid'];
                    header('Location: ../eCommerceSitePreRgsContinueWithUid.php');

                    // echo "yes";
               }
               else
               {
                   echo "FAIL";
               }
     
          }
          else
          {
               if(addPreOrder($conn,$userUid,$itemUid,$productName,$productPrice,$quantity,$totalPrice,$status,$mainProductUid,$totalProductValue,$totalRedemptionPoint))
               {
                    echo "Upline Uid : ";
                    echo $_SESSION['upline_uid'];
                    echo "<br>";
                    echo "Temp Uid : ";
                    echo $_SESSION['temp_uid'];
                    header('Location: ../eCommerceSitePreRgsContinueWithUid.php');
               }
               else
               {
                    echo "<script> window.history.go(-1);</script>";
               }
          }
     }

     // if($quantity < 1)
     // {
     //      header('Location: ../eCommerceSite.php');
     // }
     // else
     // {
     //      if(addPreOrder($conn,$userUid,$itemUid,$productName,$productPrice,$quantity,$totalPrice,$status,$mainProductUid,$totalProductValue,$totalRedemptionPoint))
     //      {
     //           // echo "product added to cart";
     //           header('Location: ../shoppingCart.php');
     //      }
     //      else
     //      {
     //           echo "fail to add product into cart";
     //      }
     // }
}
else 
{
     header('Location: ../index.php');
}
?>