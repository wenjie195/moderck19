<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Livestream.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $itemUid = rewrite($_POST['item_uid']);

     $status = "Delete";

     $liveStreamDetails = getLivestream($conn," WHERE uid = ? ",array("uid"),array($itemUid),"s");   

     if($liveStreamDetails)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($status)
          {
               array_push($tableName,"status");
               array_push($tableValue,$status);
               $stringType .=  "s";
          }

          array_push($tableValue,$itemUid);
          $stringType .=  "s";
          $announcementUpdated = updateDynamicData($conn,"livestream"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($announcementUpdated)
          {
               // echo "Success";
               header('Location: ../adminLivestreamAll.php');
          }
          else
          {
               echo "Fail";
          }
     }
     else
     {
          echo "Error";
     }

}
else
{
     header('Location: ../index.php');
}
?>