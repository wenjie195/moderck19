<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

// require_once dirname(__FILE__) . '/classes/Address.php';
require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/PreOrderList.php';
// require_once dirname(__FILE__) . '/classes/States.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $uid = $_SESSION['uid'];
$uid = $_SESSION['temp_uid'];
$uplineUid = $_SESSION['upline_uid'];
$orderUid = $_SESSION['order_uid'];

$conn = connDB();

// $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

// $addressDetails = getAddress($conn," WHERE user_uid = ? AND default_ship = 'Yes' ", array("user_uid") ,array($uid),"s");
// $userAddressRow = getAddress($conn," WHERE uid = ? ", array("uid") ,array($uid),"s");
// $addressDetails = $userAddressRow[0];

// $products = getPreOrderList($conn, "WHERE status = 'Pending' ");
// $products = getOrderList($conn, "WHERE user_uid = ? AND status = 'Pending' ",array("user_uid"),array($uid),"s");
$products = getOrderList($conn, "WHERE user_uid = ? AND order_id = ? ",array("user_uid","order_id"),array($uid,$orderUid),"ss");

// $states = getStates($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Checkout | MODERCK" />
<title>Checkout | MODERCK</title>
<?php include 'css.php'; ?>
</head>
<body class="body">

<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Checkout</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">

    <?php $uid ;?>
    <?php $uplineUid ;?>

        <div class="table-padding width100 same-padding details-min-height padding-top2 overflow overflow-x">
            <div class="width100 overflow-x">

                <table class="width100 tur-table">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Product Name</th>
                            <th>Unit Price (RM)</th>
                            <th>Quantity</th>
                            <th>Subtotal (RM)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if($products)
                            {
                                for($cnt = 0;$cnt < count($products) ;$cnt++)
                                {
                                ?>
                                    <tr>
                                        <td><?php echo ($cnt+1)?></td>
                                        <td><?php echo $products[$cnt]->getProductName();?></td>
                                        <td><?php echo $products[$cnt]->getOriginalPrice();?></td>
                                        <td><?php echo $products[$cnt]->getQuantity();?></td>
                                        <td><?php echo $products[$cnt]->getTotalPrice();?></td>
                                    </tr>
                                <?php
                                }
                            }
                        ?>                                 
                    </tbody>
                </table>

                <div class="clear"></div>   

                <h1 class="top-title brown-text">Shipping Information</h1>

                <div class="clear"></div>   

                <!-- <form method="POST" action="utilities/createOrderFunction.php"> -->
                <form method="POST" action="utilities/createOrderPreRgsFunction.php">
                    <div class="dual-input">
                        <p class="top-p">Recipient Name</p>
                        <input type="text" class="line-input clean" placeholder="Recipient Name" id="recipient_name" name="recipient_name" required>
                    </div>
                        
                    <div class="dual-input second-dual-input">  
                        <p class="top-p">Mobile Number</p>
                        <input type="text" class="line-input clean" placeholder="Mobile Number" id="mobile_no" name="mobile_no" required>
                    </div> 

                    <div class="clear"></div>   

                    <div class="dual-input">
                        <p class="top-p">House No & Street</p>
                        <input type="text" class="line-input clean" placeholder="House/Road No" id="house_road" name="house_road" required>
                    </div>  

                    <div class="dual-input second-dual-input">  
                        <p class="top-p">Postcode</p>
                        <input type="text" class="line-input clean" placeholder="Postcode" id="postcode" name="postcode" required>
                    </div>  

                    <div class="clear"></div>      

                    <div class="dual-input">
                        <p class="top-p">City</p>
                        <input type="text" class="line-input clean" placeholder="City" id="city" name="city" required>                
                    </div>  

                    <div class="dual-input second-dual-input">  
                        <p class="top-p">State</p>
                        <input type="text" class="line-input clean" placeholder="State" id="state" name="state" required>                    
                    </div>  

                    <div class="clear"></div>      

                    <div class="dual-input">
                        <p class="top-p">Country</p>     
                        <input type="text" class="line-input clean" placeholder="Country" id="country" name="country" required>
                    </div>    

                    <div class="clear"></div>      

                    <h1 class="top-title brown-text">Billing Information</h1>

                    <div class="dual-input">
                        <p class="top-p">Purchaser Name</p>
                        <input type="text" class="line-input clean" placeholder="Purchaser Name" id="purchaser_name" name="purchaser_name" required>
                    </div>
                        
                    <div class="dual-input second-dual-input">  
                        <p class="top-p">Mobile Number</p>
                        <input type="text" class="line-input clean" placeholder="Mobile Number" id="mobile_no_bill" name="mobile_no_bill" required>
                    </div> 

                    <div class="clear"></div>   

                    <div class="dual-input">
                        <p class="top-p">House / Road No</p>
                        <input type="text" class="line-input clean" placeholder="House/Road No" id="house_road_bill" name="house_road_bill" required>
                    </div>  

                    <div class="dual-input second-dual-input">  
                        <p class="top-p">Postcode</p>
                        <input type="text" class="line-input clean" placeholder="Postcode" id="postcode_bill" name="postcode_bill" required>                    
                    </div>  

                    <div class="clear"></div>      

                    <div class="dual-input">
                        <p class="top-p">City</p>
                        <input type="text" class="line-input clean" placeholder="City" id="city_bill" name="city_bill" required>
                    </div>  

                    <div class="dual-input second-dual-input">  
                        <p class="top-p">State</p>
                        <input type="text" class="line-input clean" placeholder="State" id="state_bill" name="state_bill" required>                
                    </div>  

                    <div class="clear"></div>      

                    <div class="dual-input">
                        <p class="top-p">Country</p>     
                        <input type="text" class="line-input clean" placeholder="Country" id="country_bill" name="country_bill" required>
                    </div>    

                    <div class="clear"></div>   

                    <div class="dual-input">
                        <p class="top-p">Notice</p>     
                        <input type="text" class="line-input clean" placeholder="Notice" id="notice" name="notice">
                    </div>  

                    <div class="clear"></div>    

                    <?php
                    if($products)
                    {
                    $totalOrderAmount = 0;
                    for ($cnt=0; $cnt <count($products) ; $cnt++)
                    {
                        $totalOrderAmount += $products[$cnt]->getTotalPrice();
                        // echo "<br>";
                        // echo '123';
                    }
                    }
                    else
                    {
                        $totalOrderAmount = 0 ;
                    }
                    ?>

                    <!-- <input type="text" id="order_uid" name="order_uid" value="<?php //echo $orderUid ?>" readonly>
                    <input type="text" id="subtotal" name="subtotal" value="<?php //echo $totalOrderAmount;?>" readonly>  -->

                    <input type="hidden" id="order_uid" name="order_uid" value="<?php echo $orderUid ?>" readonly>
                    <input type="hidden" id="subtotal" name="subtotal" value="<?php echo $totalOrderAmount;?>" readonly> 


                    <div class="center-div2">
                        <button class="clean yellow-btn edit-profile-width ow-margin-left0" style="margin-top:50px;" type="submit" name="user_uid" value="<?php echo $products[0]->getUserUid();?>">
                            Continue
                        </button>
                    </div>
                    
                </form>

            </div>
        </div>

        <?php
        if($products)
        {
        $totalOrderAmount = 0;
        for ($cnt=0; $cnt <count($products) ; $cnt++)
        {
            $totalOrderAmount += $products[$cnt]->getTotalPrice();
            // echo "<br>";
            // echo '123';
        }
        }
        else
        {
            $totalOrderAmount = 0 ;
        }
        ?>
    
    </div>
</div>

<?php include 'js.php'; ?>

</body>
</html>