<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

// require_once dirname(__FILE__) . '/classes/Address.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();
    echo $referrerUidLink = rewrite($_POST["upline_uid"]);
    echo $tempUid = rewrite($_POST["temp_uid"]);

    $allProduct = getProduct($conn, " WHERE status = 'Available' ");

    $conn->close();
}

// $allProduct = getProduct($conn, " WHERE status = 'Available' ");

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="All Product | MODERCK" />
<title>All Product | MODERCK</title>
<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">All Product</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    
    <div class="same-padding width100 min-sp-height overflow padding-top30 scroll-div padding-bottom30">

        <div class="width100 scroll-div">

        <h1 class="top-title brown-text"><?php $tempUid ;?></h1>
        <h1 class="top-title brown-text"><?php $referrerUidLink ;?></h1>

            <table class="width100 gold-table">
                <thead>
                    <tr>
                        <th>PRODUCT NAME</th>
                        <th>SELLING PRICE (RM)</th>
                        <th>PV</th>
                        <!-- <th>STATUS</th> -->
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($allProduct)
                        {
                            for($cnt = 0;$cnt < count($allProduct) ;$cnt++)
                            {
                            ?>
                            <tr>
                                <td><?php echo $allProduct[$cnt]->getName();?></td>
                                <td><?php echo $allProduct[$cnt]->getPrice();?></td>
                                <td><?php echo $allProduct[$cnt]->getProductValue();?></td>
                                <!-- <td><?php //echo $allProduct[$cnt]->getStatus();?></td> -->


                                <td>
                                    <!-- <form method="POST" action="utilities/preOrderFunction.php"> -->
                                    <!-- <form method="POST" action="utilities/preOrderPreRgsFunction.php"> -->
                                    <form method="POST" action="utilities/preOrderPreRgsRenewFunction.php">

                                    <input type="hidden" class="line-input clean" value="<?php echo $referrerUidLink ;?>" id="upline_uid" name="upline_uid" readonly>  
                                    <input type="hidden" class="line-input clean" value="<?php echo $tempUid ;?>" id="temp_uid" name="temp_uid" readonly>  

                                        <!-- <input type="number" class="line-input clean" placeholder="Amount" id="quantity" name="quantity" required>     -->
                                        <!-- <input type="hidden" class="line-input clean" value="1" id="quantity" name="quantity" readonly>    
                                        <button class="clean transparent-button white-link" type="submit" name="item_uid" value="<?php echo $allProduct[$cnt]->getUid();?>">
                                            <u>BUY NOW</u>
                                        </button> -->

                                        <input type="number" class="line-input clean" placeholder="Amount" id="quantity" name="quantity" required>    
                                        <button class="clean transparent-button white-link" type="submit" name="item_uid" value="<?php echo $allProduct[$cnt]->getUid();?>">
                                            <u>Add To Cart</u>
                                        </button>

                                    </form> 
                                </td>


                                
                            </tr>
                            <?php
                            }
                        }
                    ?>   
                </tbody>
            </table>
		</div>
    </div>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>