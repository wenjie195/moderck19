<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Address.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

// $allAddress = getAddress($conn, "WHERE user_uid = ? AND status != 'Delete' ",array("user_uid"),array($uid),"s");
$allAddress = getAddress($conn, "WHERE user_uid = ? AND status = 'Available' ",array("user_uid"),array($uid),"s");

$allUser = getUser($conn);

$timestamp = time();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Add Address | MODERCK" />
<title>Add Address | MODERCK</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Add Address</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    <div class="width100 same-padding min-height100 padding-top overflow overflow-x">
    	
        <div class="width100 overflow-x">

           
			<div class="clear"></div>
            <form action="utilities/userAddressAddFunction.php" method="POST">
            	<div class="dual-input">
        			<p class="top-p">Recipient Name</p>
                    <input type="text" class="line-input clean" placeholder="Recipient Name" id="recipient_name" name="recipient_name" required>
                    <!-- <input type="text" class="line-input clean" value="<?php //echo $userData->getUsername();?>" id="recipient_name" name="recipient_name" required> -->
				</div>
                 
                <div class="dual-input second-dual-input">  
                    <p class="top-p">Mobile Number</p>
                    <input type="text" class="line-input clean" placeholder="Mobile Number" id="mobile_no" name="mobile_no" required>
                    <!-- <input type="text" class="line-input clean" value="012<?php //echo $timestamp;?>" id="mobile_no" name="mobile_no" required> -->
				</div> 

                <div class="clear"></div>   

            	<div class="dual-input">
        			<p class="top-p">House No & Street</p>
                    <input type="text" class="line-input clean" placeholder="House/Road No" id="house_road" name="house_road" required>
                    <!-- <input type="text" class="line-input clean" value="<?php //echo $timestamp;?> , House Road" id="house_road" name="house_road" required> -->
				</div>  

                <div class="dual-input second-dual-input">  
                    <p class="top-p">Postcode</p>
                    <input type="text" class="line-input clean" placeholder="Postcode" id="postcode" name="postcode" required>
                    <!-- <input type="text" class="line-input clean" value="<?php //echo $timestamp;?>" id="postcode" name="postcode" required> -->
				</div>  

                <div class="clear"></div>      

            	<div class="dual-input">
                    <p class="top-p">City</p>
                    <input type="text" class="line-input clean" placeholder="City" id="city" name="city" required>                
                    <!-- <input type="text" class="line-input clean" value="<?php //echo $timestamp;?> , City" id="city" name="city" required>      -->
				</div>  

                <div class="dual-input second-dual-input">  
        			<p class="top-p">State</p>
                    <input type="text" class="line-input clean" placeholder="State" id="state" name="state" required>                    
                    <!-- <input type="text" class="line-input clean" value="PG / KDH / KL" id="state" name="state" required>        -->
				</div>  

                <div class="clear"></div>      

            	<div class="dual-input">
        			<p class="top-p">Country</p>     
                    <!-- <input type="text" class="line-input clean" placeholder="Country" id="country" name="country" required> -->
                    <input type="text" class="line-input clean" value="Malaysia" id="country" name="country" required>
                </div>    

                <div class="dual-input second-dual-input">  
        			<p class="top-p">Set As Default Shipping Address?</p>
                	<!-- <select class="line-input clean"> -->
                    <select  class="line-input clean" id="default_ship" name="default_ship" required>
                        
                        <option value='Yes'>Yes</option>
                        <option value='No'>No</option>
                    </select>                    
				</div>  

                <div class="clear"></div>      

                <div class="dual-input ">  
        			<p class="top-p">Set As Default Billing Address?</p>  
                    <select  class="line-input clean" id="default_bill" name="default_bill" required>
                        <option value='Yes'>Yes</option>
                        <option value='No'>No</option>
                    </select>              
				</div>  

                <div class="clear"></div>      
                                      
                <div class="text-center middle-div-width">
            		<button class="clean white-button ow-red-bg white-text" name="submit">Save</button> 
                </div>         
            </form>    

        </div>

    </div>
    </div>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>