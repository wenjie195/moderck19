<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Add Product | MODERCK" />
<title>Add Product | MODERCK</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>
<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Add Product</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">

    <div class="width100 inner-bg inner-padding">
        <div class="width100 same-padding normal-min-height padding-top overflow">
                
            <form action="utilities/adminProductAddFunction.php" method="POST">
                <div class="dual-input">
                    <p class="top-p">Product Name</p>
                    <input type="text" class="line-input clean" placeholder="Product Name" id="product_name" name="product_name" required>
                </div>

                <div class="dual-input second-dual-input"> 
                    <p class="top-p">Product Code</p>
                    <input type="text" class="line-input clean" placeholder="Product Code" id="product_code" name="product_code" required>
                </div>

                <div class="clear"></div>     

                <div class="dual-input">
                    <p class="top-p">Selling Price (RM)</p>
                    <input type="text" class="line-input clean" placeholder="Selling Price (RM)" id="selling_price" name="selling_price" required>
                </div>

                <div class="dual-input second-dual-input"> 
                    <p class="top-p">Product Value (PV)</p>
                    <input type="text" class="line-input clean" placeholder="Product Value (PV)" id="product_value" name="product_value" required>
                </div>

                <div class="clear"></div>     

                <div class="dual-input">
                    <p class="top-p">Redemption Point (RP)</p>
                    <input type="text" class="line-input clean" placeholder="Redemption Point (RP)" id="redemption_point" name="redemption_point" required>
                </div>

                <div class="clear"></div>  

                <div class="dual-input">
                    <p class="top-p">Description</p>
                    <textarea type="text" class="line-input clean" placeholder="Description" id="description" name="description"></textarea>
                </div>

                <div class="clear"></div>  

                <div class="text-center middle-div-width">
                <button class="clean yellow-btn edit-profile-width" name="submit">Submit</button>
                </div>          
            </form>    

        </div>
    </div>

<div class="clear"></div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>