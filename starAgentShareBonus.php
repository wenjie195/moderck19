<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BonusStarClaim.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';


date_default_timezone_set('Asia/Kuala_Lumpur');
$monthYear = date('m/Y', time());

$starStatus = "Claim";

$conn = connDB();

// $totalProductValue = getOrders($conn, "WHERE remark = ? AND status = 'Pending' ",array("remark"),array($monthYear),"s");
$totalProductValue = getOrders($conn, "WHERE remark = ? AND payment_status = 'Approved' ",array("remark"),array($monthYear),"s");
$totalSales = 0; // initital
if($totalProductValue)
{
  for($b=0; $b <count($totalProductValue) ; $b++)
  {
    $totalSales += $totalProductValue[$b]->getSubtotal();
    // echo "<br>";
    // echo "<br>";
  }
}

echo "Total Sales This Month : ";
echo $totalSales;
echo "<br>";

$qualifier = getBonusStarClaim($conn, "WHERE status  = 'Pending' ");
if($qualifier)
{   
  echo "Total Qualifier : ";
  echo $totalQualifier = count($qualifier);
  echo "<br>";

  for($cnt=0; $cnt <count($qualifier) ; $cnt++)
  {
    echo $receiverUid = $qualifier[$cnt]->getReceiverUid();
    echo " , ";
    echo $qualifier[$cnt]->getReceiver();
    echo "<br>";

    // $qualifier = getUser($conn, "WHERE status  = 'Pending' ");
    $userRows = getUser($conn, "WHERE uid = ? ",array("uid"),array($receiverUid),"s");
    $currentSalesComs = $userRows[0]->getSalesCommission();
    // $username = $userRows[0]->getUsername();
  }

}
else
{
  echo "Total Qualifier : ";
  echo $totalQualifier = 0;
  echo "<br>";
}

echo "Amount Sharing : ";
// $amountSharing = $totalSales / $totalQualifier;
$amountSharing =(($totalSales * 0.02) / $totalQualifier);
echo $amountSharingFormat = number_format($amountSharing,2);
echo "<br>";



$renewBonusStatus = "Claim";
$allQualifier = getBonusStarClaim($conn, "WHERE status  = 'Pending' ");
// $allQualifier = getBonusStar($conn, "WHERE status  = 'Pending' AND user_type = '1' ");
if($allQualifier)
{   
  for($cntA=0; $cntA <count($allQualifier) ; $cntA++)
  {
    echo $bonusId = $allQualifier[$cntA]->getId();
    echo "<br>";
    echo $receiverUid = $allQualifier[$cntA]->getReceiverUid();
    echo "<br>";

    // $qualifier = getUser($conn, "WHERE status  = 'Pending' ");
    $userRows = getUser($conn, "WHERE uid = ? ",array("uid"),array($receiverUid),"s");
    echo $currentSalesComs = $userRows[0]->getSalesCommission();
    echo "<br>";
    $username = $userRows[0]->getUsername();
    echo "Renew Commission : ";
    echo $renewSalesComs = $currentSalesComs + $amountSharing;
    echo "<br>";

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    //echo "save to database";
    if($renewSalesComs)
    {
      array_push($tableName,"sales_commission");
      array_push($tableValue,$renewSalesComs);
      $stringType .=  "d";
    }
    array_push($tableValue,$receiverUid);
    $stringType .=  "s";
    $updateUserSalesComs = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    if($updateUserSalesComs)
    {
      echo "Credit To User !";
      echo "<br>";

      $tableName = array();
      $tableValue =  array();
      $stringType =  "";
      //echo "save to database";
      if($renewBonusStatus)
      {
        array_push($tableName,"status");
        array_push($tableValue,$renewBonusStatus);
        $stringType .=  "s";
      }
      if($amountSharing)
      {
        array_push($tableName,"amount");
        array_push($tableValue,$amountSharing);
        $stringType .=  "d";
      }
      array_push($tableValue,$bonusId);
      $stringType .=  "s";
      $updateUserSalesComs = updateDynamicData($conn,"bonus_star_claim"," WHERE id = ? ",$tableName,$tableValue,$stringType);
      if($updateUserSalesComs)
      {
        echo "Update Bonus Star Status To Claim !";
        echo "<br>";echo "<br>";
      }
      else
      {
        echo "Fail To Update Bonus Star Claim Status !";
      }

    }
    else
    {
      echo "Fail A";
    }   
  }
}
else
{
  echo "ERROR all qualifier !";
}
$conn->close();
?>