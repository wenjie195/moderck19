<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Cart.php';
// require_once dirname(__FILE__) . '/classes/PreOrderList.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

// if($_SERVER['REQUEST_METHOD'] == 'POST'){
//     addToCart();
//     createOrder($conn,$uid);
//     header('Location: ./shoppingCartCheckout.php');
// }

// $products = getPreOrderList($conn, "WHERE user_uid = ? AND status = 'Pending' ",array("user_uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://agentpnchc.com/editProfile.php" />
<link rel="canonical" href="https://agentpnchc.com/editProfile.php" /> -->
<meta property="og:title" content="Shopping Cart | MODERCK" />
<title>Shopping Cart  | MODERCK</title>
<?php include 'css.php'; ?>
</head>
<body class="body">

<div class="background-container">
   <img src="img/flower-top.png" class="flower-img1">
   <img src="img/flower-bottom.png" class="flower-img2">
    <div class="stars"></div>
    <div class="twinkling"></div> 
</div>

<div class="width100 same-padding fixed-bar">
	<h1 class="top-title brown-text">Shopping Cart</h1><?php include 'header.php'; ?>
</div>

<div id="main-start">
	<div class="width100 inner-bg inner-padding">
    
        <div class="table-padding width100 same-padding details-min-height padding-top2 overflow overflow-x">

            <div class="width100 overflow-x text-center">
            		<img src="img/empty.png" class="empty">
					<h1 class="text-center empty-h1 gold-text">Your cart is empty now.</h1>
                <?php
                    $conn = connDB();
                    if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'])
                    {
                        unset($_SESSION['shoppingCart']);
                    ?>
                       
                    <?php
                    }
                    else
                    {   }
                    $conn->close();
                ?>

            </div>

        <div class="width100 overflow text-center margin-bottom-5px">
            <a href="eCommerceSite.php" ><div class="clean white-button ow-red-bg white-text margin-auto" style="width:200px !important;">Go Shopping</div></a>
        </div>

        </div>

    </div>
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>